<?php
/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

class PBXManager_Detail_View extends Vtiger_Detail_View{
    
    /**
     * Overrided to disable Ajax Edit option in Detail View of
     * PBXManager Record
     */
    function isAjaxEnabled($recordModel) {
		return false;
	}
 
    /*
     * Overided to convert totalduration to minutes
     */
    function preProcess(Vtiger_Request $request, $display=true) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();
		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();
        
       // To show recording link only if callstatus is 'completed' 
        if($recordModel->get('callstatus') != 'completed') { 
            $recordModel->set('recordingurl', ''); 
        }
        return parent::preProcess($request, true);
	}
	
	 /* ------ START ------------ */
   
	
	function process(Vtiger_Request $request) {
		
		$currentUserModel = Users_Record_Model::getCurrentUserModel();

		parent::process($request);
		
		$this->getAllConferenceCalls($request);
	}
	
	function getAllConferenceCalls(Vtiger_Request $request){
		
		$adb = PearDatabase::getInstance();
		
		$recordId = $request->get('record');
		
		$moduleName = $request->getModule();
		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		
		$recordModel = $this->record->getRecord();
        
		if( $recordModel->isConference() ){
			
			$callsid = $recordModel->get('sourceuuid');
			
			$conferences = array();
			
			$conferenceResult = $adb->pquery("SELECT * FROM vtiger_conference_calls 
	    		INNER JOIN vtiger_conferences on vtiger_conferences.id = vtiger_conference_calls.conference_id 
	    		WHERE BINARY call_sid = ? 
	    		ORDER BY vtiger_conference_calls.id ASC", array($callsid));
			
			$conferenceRow = $adb->num_rows($conferenceResult);
			
			if( $conferenceRow ){
				
				$conferenceId = $adb->query_result($conferenceResult, 0, 'conference_id');
			
				$conferences['name'] = $adb->query_result($conferenceResult, 0, 'name');
				$conferences['calls'] = array();
				
				$relatedCallsResult = $adb->pquery("SELECT * FROM vtiger_conference_calls 
	    		INNER JOIN vtiger_conferences on vtiger_conferences.id = vtiger_conference_calls.conference_id 
	    		WHERE vtiger_conferences.id = ? 
	    		ORDER BY vtiger_conference_calls.id ASC", array($conferenceId));
			
				$totalCalls = $adb->num_rows($relatedCallsResult);
				
				if( $totalCalls ){
					
					
					
					
					require_once ('modules/Twilio/twilio-php/Services/Twilio.php');
	
					$api_version = '2010-04-01';
					
					$serverModel = PBXManager_Server_Model::getInstance();
						
					$accountSid = $serverModel->get ( "webappurl" );	
					$token = $serverModel->get ( "outboundcontext" );
					
					$http = new Services_Twilio_TinyHttp(
						    'https://api.twilio.com',
						    array('curlopts' => array(
						        CURLOPT_SSL_VERIFYPEER => false,
						    ))
					);
					
					$client = new Services_Twilio($accountSid, $token, $api_version, $http);
	
					
					
					
	
					for($i=0; $i<$totalCalls; $i++){
						
						$part_callsid = $adb->query_result($relatedCallsResult, $i, 'call_sid');
						
						$callDetails = $client->account->calls->get($part_callsid);
						
						$participant = array(
							'callsid' 			=> $part_callsid,
							'conference_id' 	=> $adb->query_result($relatedCallsResult, $i, 'conference_id'),
							'from' 				=> $callDetails->from,
							'to' 				=> $callDetails->to,
						);
						
						$is_initiator = $adb->query_result($relatedCallsResult, $i, 'is_initiator');
						$is_listener = $adb->query_result($relatedCallsResult, $i, 'is_listener');
						
						if( $is_initiator ){
	
							$participant['type'] = 'Initiator';
						
						} else {
							
							if( $is_listener ){
	
								$participant['type'] = 'Listener';
							
							} else {
								
								$participant['type'] = 'Participant';
							
							}
						} 
					
						$conferences['calls'][] = $participant;
						
					} //end foreach totalCalls
					
					
					
				}
			}
			
			$viewer = $this->getViewer($request);
			
			/*echo "<pre>";
			print_r($conferences['calls']);
			echo "</pre>";*/
			
			$viewer->assign('RELATED_CALLS' , $conferences);
			$viewer->assign('MODULE', $moduleName);
			$viewer->assign('RECORD', $recordModel);
	
			echo $viewer->view('ConferenceCallsDetailBlock.tpl', $moduleName, 'true');
		
		}		
	}
	
	/* ------ END ------------ */
   
	
}
