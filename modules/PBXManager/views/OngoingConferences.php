<?php

class PBXManager_OngoingConferences_View extends Vtiger_Index_View {
	
	function __construct() {
		parent::__construct();
	}
	
	function process(Vtiger_Request $request) {
		
		$mode = $request->getMode();
		
		if(!empty($mode)) {
			echo $this->invokeExposedMethod($mode, $request);
			return;
		}

		$moduleName = $request->getModule();
		
		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		
		require_once('modules/Twilio/twilio-php/Services/Twilio.php'); // Loads the library
 
		$api_version = '2010-04-01';
		
		$serverModel = PBXManager_Server_Model::getInstance();
	
		$accountSid = $serverModel->get ( "webappurl" );	
		$authToken = $serverModel->get ( "outboundcontext" );	
		$appSid = $serverModel->get("vtigersecretkey");
			
		$http = new Services_Twilio_TinyHttp(
			    'https://api.twilio.com',
			    array('curlopts' => array(
			        CURLOPT_SSL_VERIFYPEER => false,
			    ))
		);
		
		// Instantiate a new Twilio Rest Client
		
		$client = new Services_Twilio($accountSid, $authToken, $api_version, $http);
	 
		$conferences = array();
		
		// Loop over the list of conferences 

		
		
		
		
		
		/*
		foreach ( 
			$client->account->conferences->getIterator(
			0, 
			20, 
			array(
				"Status" => "in-progress",
				"DateCreated>" => date('Y-m-d')
			)
			) 
			as $conference
		) {*/

		$allConf = $client->account->conferences->getIterator(
			0, 20, array( "Status" => "in-progress", "DateCreated>" => date('Y-m-d') )
		); 
			
		foreach ( $allConf as $conference ) {
			
			if( count($conferences) == 5 ){
				break;
			}
			
			$participants = array();
			
			foreach ( $conference->participants as $participant ) {

				$call = $client->account->calls->get($participant->call_sid);
				
				$call_direction = strtolower(trim($call->direction));	    	
		    	$call_direction = preg_replace('/\-dial/','',$call_direction);
		    	$call_direction = preg_replace('/\-api/','',$call_direction);		    	
		    	
		    	if( $call_direction == 'inbound' ){
		    		$display_name = $call->from;
		    	} else {
		    		$display_name = $call->to;
		    	}
	    	
		    	//$display_name = preg_replace('/client:/','',$display_name);
		    	
		    	$display_name = $this->getParticipantName($display_name);
		    	
				$participants[] = array(
					/*'call_sid'					=>	$participant->call_sid,
					'uri' 						=>	$participant->uri,
					'start_conference_on_enter'	=>	$participant->start_conference_on_enter,
					'date_created'				=>	$participant->date_created,
					'muted'						=>	$participant->muted,
					'end_conference_on_exit'	=>	$participant->end_conference_on_exit,*/
					'display_name' 				=>	$display_name
				);
			}

    		$conferences[] = array(
				'sid' 			=>	$conference->sid,
				'friendly_name' =>	$conference->friendly_name,
				'status' 		=>  $conference->status,
    			'date_created'	=>	$conference->date_created,
    			'participants'	=>	$participants,
			);    		
		}
		
	
    		
		$viewer = $this->getViewer($request);
		$viewer->assign('CONFERENCES', $conferences);
		
		$viewer->assign('CURRENT_USER_MODEL', $currentUserModel);
		
		echo $viewer->view('ConferenceList.tpl',$moduleName,true);
	}
	
	public function getParticipantName($val = ''){
		
		global $adb, $current_user;

		$result = $val;
		
		if( strpos(strtolower($val),'client') !== FALSE){		/*client:admin*/
			
			$parts = explode(':', $val);
			
	    	if( isset($parts[1]) ){
    			$userName = $parts[1];	
    			$userResult = $adb->pquery("SELECT CONCAT(first_name,' ', last_name) as name, phone_crm_extension  
    			 FROM vtiger_users WHERE user_name = ?", array( $userName ));
				if($adb->num_rows($userResult)){
					$result = $adb->query_result($userResult, 0, 'name');
					$fromNo = $adb->query_result($userResult, 0, 'phone_crm_extension');
				}
	    	}
	    	
	    	$result = '<span style="font-weight: 800;">' . $result . "</span>";
		    	
		} else {		/*any number*/
				
				
			$fromNo = preg_replace ( '/\D+/', '', $val);

			$lookUpNumber = substr($fromNo, -10);
				

			if( $lookUpNumber != '' ){
				$lookUpQuery = "SELECT * FROM vtiger_crm_phonenumbers inner join 
					vtiger_crmentity on vtiger_crmentity.crmid = vtiger_crm_phonenumbers.crmid  
					WHERE deleted = 0 and 
					(phone = '" . $lookUpNumber . "' OR phone like '%" . $lookUpNumber . "') AND 
					(phone != '' AND phone is not null) 
					";
					
				$lookUpResult = $adb->pquery($lookUpQuery, array());
					
				$numRows = $adb->num_rows($lookUpResult);
				
				$crmid = '';
				
				if($numRows){
					if($numRows == 1){	
						$crmid = $adb->query_result($lookUpResult, 0, 'crmid');					
					} else {
						for($i=0; $i<$numRows; $i++){
							$crmid = $adb->query_result($lookUpResult, $i, 'crmid');
							if(getSalesEntityType($crmid) == 'Contacts'){
								break;
							}
						}							
					}
				}
				
				if($crmid != ''){
						$fromName = Vtiger_Functions::getCRMRecordLabel($crmid);
						$result = '<a target="_blank" href="index.php?module='.getSalesEntityType($crmid). 
						'&view=Detail&record=' . $crmid . '">' . $fromName . '</a>';				
				}
			}
						
		}
		return $result;	
	}
	
	function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);
		$moduleName = $request->getModule();

		$jsFileNames = array(
			'~layouts/vlayout/modules/Vtiger/resources/Conferences.js'
		);

		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}
	
}