<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
include_once 'include/Webservices/Create.php';
include_once 'include/utils/utils.php';

class PBXManager_IncomingCallPoll_Action extends Vtiger_Action_Controller{
    
    function __construct() {
            $this->exposeMethod('searchIncomingCalls');
            $this->exposeMethod('createRecord');
            $this->exposeMethod('getCallStatus');
            $this->exposeMethod('checkModuleViewPermission');
            $this->exposeMethod('checkPermissionForPolling');
            $this->exposeMethod('getToken');
            $this->exposeMethod('userUpdateOnline');
   	}
    
    public function process(Vtiger_Request $request) {
		$mode = $request->getMode();
		if(!empty($mode) && $this->isMethodExposed($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}
    
    public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		$userPrivilegesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$permission = $userPrivilegesModel->hasModulePermission($moduleModel->getId());

		if(!$permission) {
			throw new AppException('LBL_PERMISSION_DENIED');
		}
	}
    
    public function checkModuleViewPermission(Vtiger_Request $request){
        $response = new Vtiger_Response();
        $modules = array('Contacts','Leads');
        $view = $request->get('view');
        Users_Privileges_Model::getCurrentUserPrivilegesModel();
        foreach($modules as $module){
            if(Users_Privileges_Model::isPermitted($module, $view)){
                $result['modules'][$module] = true;
            }else{
                $result['modules'][$module] = false;
            }
        }
        $response->setResult($result);
        $response->emit();
    }
    
    public function searchIncomingCalls(Vtiger_Request $request){
        $recordModel = PBXManager_Record_Model::getCleanInstance();
        $response = new Vtiger_Response();
        $user = Users_Record_Model::getCurrentUserModel();
        
        $recordModels = $recordModel->searchIncomingCall();
        // To check whether user have permission on caller record
        if($recordModels){
            foreach ($recordModels as $recordModel){
                // To check whether the user has permission to see contact name in popup
                $recordModel->set('callername', null);

                $callerid = $recordModel->get('customer');
                if($callerid){
                    $moduleName = $recordModel->get('customertype');
                    if(!Users_Privileges_Model::isPermitted($moduleName, 'DetailView', $callerid)){
                        $name = $recordModel->get('customernumber').vtranslate('LBL_HIDDEN','PBXManager');
                        $recordModel->set('callername',$name);
                    }else{
                        $entityNames = getEntityName($moduleName, array($callerid));
                        $callerName = $entityNames[$callerid];
                        $recordModel->set('callername',$callerName);
                    }
                }
                // End
                $direction = $recordModel->get('direction');
                if($direction == 'inbound'){
                    $userid = $recordModel->get('user');
                    if($userid){
                        $entityNames = getEntityName('Users', array($userid));
                        $userName = $entityNames[$userid];
                        $recordModel->set('answeredby',$userName);
                    }
                }
                $recordModel->set('current_user_id',$user->id);
                $calls[] = $recordModel->getData();
            }
        }
        $response->setResult($calls);
        $response->emit();
    }
    
   public function createRecord(Vtiger_Request $request){
            $user = Users_Record_Model::getCurrentUserModel();
            $moduleName = $request->get('modulename');
            $name = explode("@",$request->get('email'));
            $element['lastname'] = $name[0];
            $element['email'] = $request->get('email');
            $element['phone'] = $request->get('number');
            $element['assigned_user_id'] = vtws_getWebserviceEntityId('Users', $user->id);
            
            $moduleInstance = Vtiger_Module_Model::getInstance($moduleName);
            $mandatoryFieldModels = $moduleInstance->getMandatoryFieldModels();
            foreach($mandatoryFieldModels as $mandatoryField){
                $fieldName = $mandatoryField->get('name');
                $fieldType = $mandatoryField->getFieldDataType();
                $defaultValue = decode_html($mandatoryField->get('defaultvalue'));
                if(!empty($element[$fieldName])){
                    continue;
                }else{
                    $fieldValue = $defaultValue;
                    if(empty($fieldValue)) {
                        $fieldValue = Vtiger_Util_Helper::getDefaultMandatoryValue($fieldType);
                    }
                    $element[$fieldName] = $fieldValue ;
                }
            }
            
            $entity = vtws_create($moduleName, $element, $user);
            $this->updateCustomerInPhoneCalls($entity, $request);
            $response = new Vtiger_Response();
            $response->setResult(true);
            $response->emit();
    }
    
    public function updateCustomerInPhoneCalls($customer, $request){
        $id = vtws_getIdComponents($customer['id']);
        $sourceuuid = $request->get('callid');
        $module = $request->get('modulename');
        $recordModel = PBXManager_Record_Model::getInstanceBySourceUUID($sourceuuid);
        $recordModel->updateCallDetails(array('customer'=>$id[1], 'customertype'=>$module));
    }
    
    public function getCallStatus($request){
        $phonecallsid = $request->get('callid');
        $recordModel = PBXManager_Record_Model::getInstanceById($phonecallsid);
        $response = new Vtiger_Response();
        $response->setResult($recordModel->get('callstatus'));
        $response->emit();
    }
    
    function checkPermissionForPolling(Vtiger_Request $request) {
        Users_Privileges_Model::getCurrentUserPrivilegesModel();
        $callPermission = Users_Privileges_Model::isPermitted('PBXManager', 'ReceiveIncomingCalls');
        
        $serverModel = PBXManager_Server_Model::getInstance();
        $gateway = $serverModel->get("gateway");

        $user = Users_Record_Model::getCurrentUserModel();
        $userNumber = $user->phone_crm_extension;
        
        $result = false;
        if($callPermission && $userNumber && $gateway ){
            $result = true;
        }
        
        $response = new Vtiger_Response();
        $response->setResult($result);
        $response->emit();
    }
    
    function getToken(Vtiger_Request $request){
    	require_once 'modules/Twilio/Services/Twilio/Capability.php';
    	$user = Users_Record_Model::getCurrentUserModel();
	$serverModel = PBXManager_Server_Model::getInstance ();
	$gateway = $serverModel->get ( "gateway" );
	$accountSid = $serverModel->get ( "webappurl" ); // Twilio_Config::get('sid',false); // Your Account SID from www.twilio.com/user/account
	$authToken = $serverModel->get ( "outboundcontext" ); // Twilio_Config::get('token',false); // Your Auth Token from www.twilio.com/user/account
	$appSid = $serverModel->get("vtigersecretkey");  // Twilio_Config::get('vtigersecretkey',false);
	
	$capability = new Services_Twilio_Capability($accountSid, $authToken);
	$capability->allowClientOutgoing($appSid);
	$capability->allowClientIncoming($user->user_name);
	$token = $capability->generateToken();
	$user = Users_Record_Model::getCurrentUserModel();
	$response = new Vtiger_Response();
	$response->setResult($token);
	$response->emit();
    }
    
    function userUpdateOnline(Vtiger_Request $request) {
    	global $adb;
    	$table_name = "twilio_client_user";
    	$user = Users_Record_Model::getCurrentUserModel();
    	$date_var = date("Y-m-d H:i:s");
    	$currentTime = $adb->formatDate($date_var, true);

    	$sqlQuery = $adb->pquery("SELECT userid from twilio_client_user WHERE userid = ?", array($user->id));
    	if (isset($sqlQuery)) {
    		$rowCount = $adb->num_rows($sqlQuery);
    		if ($rowCount > 0) {
    			$sql1 = "update $table_name set login_time =? where " . "userid" . "=?";
    			$params = array($currentTime, $this->id);
    			$adb->pquery($sql1, array($currentTime, $user->id), true, "Error marking record deleted: ");
    		} else {
	    		$sql1 = "insert into $table_name (userid,username,login_time) values(?,?,?)";
	    		$params = array($user->id,$user->user_name,$currentTime);
	    		$adb->pquery($sql1, $params);
    		}
    	}
    }

}

?>