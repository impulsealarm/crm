<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
class Settings_RelatedListviewEdits_Settings_View extends Settings_Vtiger_Index_View {

    function __construct() {
        parent::__construct();
     
    }


    public function process(Vtiger_Request $request) {
        $current_user = Users_Record_Model::getCurrentUserModel();
        $moduleName = $request->getModule();
        $qualifiedModuleName = $request->getModule(false);
        $viewer = $this->getViewer($request);

        $settingModel = new Settings_RelatedListviewEdits_Settings_Model();
        $list_modules = $settingModel->getEntityModules();
        $current_module_name = $request->get('current_module');
        if(!$current_module_name){
            $current_module_name = $list_modules[0]['name'];
        }
        $tmp = $settingModel->getEntityModules(array($current_module_name));
        $current_module = $tmp[0];

        $moduleHandler = vtws_getModuleHandlerFromName($current_module['name'], $current_user);
        $moduleMeta = $moduleHandler->getMeta();
        $moduleFields = $moduleMeta->getModuleFields();
        $current_module_fields = array();
        foreach($moduleFields as $fieldModel){
            $selected = 0;
            if(in_array($fieldModel->getFieldName(), $current_module['fields'])){
                $selected = 1;
            }
            $current_module_fields[] = array('fieldname'=>$fieldModel->getFieldName(), 'fieldlabel'=>$fieldModel->getFieldLabelKey(), 'selected'=>$selected);
        }
        $current_module['fields'] = $current_module_fields;

        $viewer->assign('PORTAL_URL', vglobal('PORTAL_URL'));
        $viewer->assign('QUALIFIED_MODULE', $qualifiedModuleName);
        $viewer->assign('LIST_MODULES', $list_modules);
        $viewer->assign('CURRENT_MODULE', $current_module);

        $viewer->view('Settings.tpl', $qualifiedModuleName);
    }

    /**
     * Function to get the list of Script models to be included
     * @param Vtiger_Request $request
     * @return <Array> - List of Vtiger_JsScript_Model instances
     */
    function getHeaderScripts(Vtiger_Request $request) {
        $headerScriptInstances = parent::getHeaderScripts($request);
        $moduleName = $request->getModule();

        $jsFileNames = array(
            "modules.Settings.$moduleName.resources.RelatedListviewEdits"
        );

        $jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
        $headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
        return $headerScriptInstances;
    }
}