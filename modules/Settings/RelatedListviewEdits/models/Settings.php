<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
class Settings_RelatedListviewEdits_Settings_Model extends Vtiger_Base_Model {
    var $user;
    var $db;

    function __construct() {
        global $current_user;

        $this->user = $current_user;
        $this->db = PearDatabase::getInstance();
    }

    function getEntityModules($modulenames = array()){
        $where_extra = " ";
        if(!empty($modulenames)){
            $where_extra = " AND vtiger_tab.name IN('".implode("','", $modulenames)."') ";
        }
        $result = $this->db->pquery('SELECT *
                                    FROM vtiger_tab
                                    LEFT JOIN vte_rle_module_settings ON vte_rle_module_settings.modulename = vtiger_tab.name
                                    WHERE vtiger_tab.isentitytype = 1 AND vtiger_tab.presence = 0
                                        AND vtiger_tab.parent IS NOT NULL
	                                    AND vtiger_tab.parent != ""
                                        '.$where_extra.'
                                    ORDER BY vtiger_tab.name', array());
        $arr = array();

        if($this->db->num_rows($result)){
            while($row = $this->db->fetchByAssoc($result)){
                $row['fields'] = json_decode(html_entity_decode($row['fields']));
                if(empty($row['fields'])){
                    $row['fields'] = array();
                }
                $row['tablabel'] = vtranslate($row['name'], $row['name']);
                $arr[] = $row;
            }
        }

        return $arr;
    }

    function getModuleFields($moduleName){
        $moduleHandler = vtws_getModuleHandlerFromName($moduleName, $this->user);
        $moduleMeta = $moduleHandler->getMeta();
        return $moduleMeta->getModuleFields();
    }

    function saveSetting($request){
        $moduleName = $request->get('current_module');

        $module_data = $this->getSettingByModuleName($moduleName);
        if(!empty($module_data)){
            $this->updateSetting($request);
        }else{
            $this->addSetting($request);
        }

        return true;
    }

    function getSettingByModuleName($moduleName){
        $result = $this->db->pquery('SELECT * FROM vte_rle_module_settings
                                    WHERE vte_rle_module_settings.modulename LIKE ?
                                    LIMIT 0, 1',
            array($moduleName));
        $module_data = array();
        if($this->db->num_rows($result) > 0){
            $module_data['fields'] = json_decode($this->db->query_result($result, 0, 'fields'));
        }

        return $module_data;
    }

    function updateSetting($request){
        $moduleName = $request->get('current_module');
        $active = $request->get('module_active', 0);
        $active_type = $request->get('module_active_type', 'Both');
        $allow_edit_all_fields = $request->get('allow_edit_all_fields', 0);
        $module_fields = $request->get('module_fields', array());

        $this->db->pquery('UPDATE vte_rle_module_settings SET `active` = ?, `active_type` = ?, `fields` = ?, `allow_edit_all_fields` = ? WHERE `modulename` LIKE ?',
            array($active, $active_type, json_encode($module_fields), $allow_edit_all_fields, $moduleName));

        return true;
    }

    function addSetting($request){
        $moduleName = $request->get('current_module');
        $active = $request->get('module_active', 0);
        $active_type = $request->get('module_active_type', 'Both');
        $allow_edit_all_fields = $request->get('allow_edit_all_fields', 0);
        $module_fields = $request->get('module_fields', array());
        $this->db->pquery('INSERT INTO vte_rle_module_settings(`modulename`, `active`, `active_type`, `fields`, `allow_edit_all_fields`) VALUES(?, ?, ?, ?)',
            array($moduleName, $active, $active_type, json_encode($module_fields), $allow_edit_all_fields));

        return true;
    }
}
