<?php

class Settings_Vtiger_TrelloBoards_View extends Settings_Vtiger_Index_View {
    
    public function process(Vtiger_Request $request) {
        $model = Settings_Vtiger_TrelloBoards_Model::getInstance();
        
        $user_trello_board_maaping = $model->get("user_trello_board");
        
        $viewer = $this->getViewer($request);
        $qualifiedName = $request->getModule(false);
        
        $trelloApiModel = Leads_TrelloApi_Model::getInstance();
        $trelloBoards = $trelloApiModel->getAllTrelloBoards();

        if(!empty($user_trello_board_maaping))
        	$viewer->assign('USER_TRELLO_MAPPING',$user_trello_board_maaping);
        	
        $viewer->assign('MODEL',$model);
        $viewer->assign("CURRENT_USER_MODEL", Users_Record_Model::getCurrentUserModel());
        $viewer->assign("TRELLO_BOARDS", $trelloBoards);
        $viewer->view('UsersTrelloBoards.tpl',$qualifiedName);
    }
	
	function getPageTitle(Vtiger_Request $request) {
		$qualifiedModuleName = $request->getModule(false);
		return vtranslate('Trello Boards',$qualifiedModuleName);
	}	
			
	/**
	 * Function to get the list of Script models to be included
	 * @param Vtiger_Request $request
	 * @return <Array> - List of Vtiger_JsScript_Model instances
	 */
	function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);
		$moduleName = $request->getModule();

		$jsFileNames = array(
			"modules.Settings.$moduleName.resources.TrelloBoards"
		);

		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}
}
    