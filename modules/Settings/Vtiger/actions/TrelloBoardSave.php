<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Settings_Vtiger_TrelloBoardSave_Action extends Settings_Vtiger_Basic_Action {
    
    public function process(Vtiger_Request $request) {
    	
    	$user_trello_board = $request->get('trello_mapping');
    	
    	if(count($user_trello_board) > 1)
    		unset($user_trello_board['0']);
    		
        $model = Settings_Vtiger_TrelloBoards_Model::getInstance();
        $model->set("user_trello_board", $user_trello_board);
        $model->save();
        
        $response = new Vtiger_Response();
        $response->setResult(array("success" => true));
		$response->emit();
    }
    
    public function validateRequest(Vtiger_Request $request) { 
        $request->validateWriteAccess(); 
    } 
}