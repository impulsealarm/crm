<?php
class Settings_Vtiger_VtigerAffiliateURLs_Action extends Settings_Vtiger_Basic_Action {
    
    public function process(Vtiger_Request $request) {
    	
    	$mode = $request->get("mode");
    	
    	if($mode == "saveAffiliateURLs"){
    		$this->saveAffiliateURLs($request);
    	}
    }
    
    public function saveAffiliateURLs(Vtiger_Request $request){
    	
    	$result = array();
    	
    	$affiliate_links = $request->get("affiliate_links");
    	
    	if(!empty($affiliate_links)){
    		$model = Settings_Vtiger_VtigerAffiliate_Model::getInstance();
    		
	        $model->set("affiliate_links", $affiliate_links);
	        $model->save();
	        
	        $result['success'] = true;
	        $result['message'] = "Save Links Successfully.";
	    } else {
	    	$result['success'] = false;
    		$result['message'] = "Please add value to the Link.";
    	}
    	
        $response = new Vtiger_Response();
        $response->setResult($result);
		$response->emit();
    }
}