<?php
class Settings_Vtiger_VtigerAffiliate_Model extends Vtiger_Base_Model{
    
    const tableName = 'vtiger_affiliate_urls';
    
    public function save() {
        
    	$adb = PearDatabase::getInstance();
    	
    	$affiliate_links = $this->get("affiliate_links");
    	
    	$affiliate_result = $adb->pquery('SELECT * FROM '.self::tableName,array());
        
    	$existing_urls = array();
    	
    	if($adb->num_rows($affiliate_result)){
        	$adb->pquery("TRUNCATE TABLE ".self::tableName,array());	
        }
        
    	if(!empty($affiliate_links)){
    		
    		foreach($affiliate_links as $affiliate_data){
    			
    			if(!empty($affiliate_data)){
    				
    				$name = $affiliate_data['name'];
    				
    				$affiliate_link = $affiliate_data['url'];
    				
    				if($affiliate_link != '' && $name != ''){
	    				$params = array($adb->getUniqueID(self::tableName), $name, $affiliate_link);
	        			$adb->pquery("insert ".self::tableName." (id, affiliate_url_name, affiliate_url) VALUES(?,?,?)", $params);
    				}
    			}
    		}
    	}
    }

    public static function getInstance() {
        $db = PearDatabase::getInstance();
        $query = 'SELECT * FROM '.self::tableName.' order by id ASC';
        $result = $db->pquery($query,array());
        $instance = new self();
        $affiliate_urls = array();
        if($db->num_rows($result) > 0) {
        	while($row = $db->fetchByAssoc($result)){
        		$affiliate_urls[] = array("name" => $row['affiliate_url_name'], "url" => $row['affiliate_url']);
        	}
        }
        $instance->set("affiliate_links", $affiliate_urls);
        return $instance;
    }
}