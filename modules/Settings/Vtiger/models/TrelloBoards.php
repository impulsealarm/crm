<?php
class Settings_Vtiger_TrelloBoards_Model extends Vtiger_Base_Model{
    
    const tableName = 'vtiger_users_trelloboardids';
    
    public function save() {
        
    	$db = PearDatabase::getInstance();
        
        $user_trello_board_mapping = $this->get("user_trello_board");
        
        $existing_user_board_mapping = array();
        
        $check_result = $db->pquery("select * from ".self::tableName,array());
        
        if($db->num_rows($check_result)){
        	
        	while($row = $db->fetchByAssoc($check_result)){
        		
        		$existing_user_board_mapping[$row['user_id']] = $row['trello_board_id'];
        	}
        }
        
        if(!empty($user_trello_board_mapping)){
        	
        	foreach($user_trello_board_mapping as $user_trello_board){
        		
        		$user_id = $user_trello_board['user'];
        		
        		$board_id = $user_trello_board['board'];
        		
        		$result = $db->pquery("select * from ".self::tableName." where user_id = ?",array($user_id));

        		if($db->num_rows($result)){
        			
        			$db->pquery("update ".self::tableName." set trello_board_id = ? where user_id = ?",array($board_id, $user_id));
        			
        			unset($existing_user_board_mapping[$user_id]);
        		} else {
        			
        			$query = 'INSERT INTO '.self::tableName.' (id,user_id,trello_board_id) VALUES(?,?,?)';
        		    
        			$params = array($db->getUniqueID(self::tableName), $user_id, $board_id);
        			
        			$result = $db->pquery($query, $params);
            	}
        	}
        	
        	if(!empty($existing_user_board_mapping)){
        		
        		foreach($existing_user_board_mapping as $user_id => $board_id){
        			$db->pquery("delete from ".self::tableName." where user_id = ?",array($user_id));
        		}
        	}
        }
    }
    
    public static function getInstance() {
        $db = PearDatabase::getInstance();
        $query = 'SELECT * FROM '.self::tableName.' order by id desc';
        $result = $db->pquery($query,array());
        $instance = new self();
        $user_trello_board = array();
        if($db->num_rows($result) > 0) {
        	while($row = $db->fetchByAssoc($result)){
        		$user_trello_board[] = array("user" => $row['user_id'], "board" => $row['trello_board_id']);
        	}
        }
        $instance->set("user_trello_board", $user_trello_board);
        return $instance;
    }
}