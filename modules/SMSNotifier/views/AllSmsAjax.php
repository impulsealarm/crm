<?php

class SMSNotifier_AllSmsAjax_View extends SMSNotifier_AllSms_View {

	function __construct() {
		parent::__construct();
		$this->exposeMethod('showAllRecentContactedRecords');
	}

	function preProcess(Vtiger_Request $request) {
		return true;
	}

	function postProcess(Vtiger_Request $request) {
		return true;
	}

	function process(Vtiger_Request $request) {
		$mode = $request->get('mode');
		if(!empty($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}

	/*
	 * Function to show the recently contacted records
	 */
	function showAllRecentContactedRecords(Vtiger_Request $request) {
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		//$recentRecords = $moduleModel->getRecentRecords();

		$recentRecords = $this->getRecentContactedRecords();
		
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('RECORDS', $recentRecords);

		echo $viewer->view('RecentContactedRecordList.tpl', $moduleName, true);
	}

	
	function getRecentContactedRecords(){
		
		global $current_user, $adb;
		
		$moduleName = 'SMSNotifier';
		
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);		
		
		$current_user = Users_Record_Model::getCurrentUserModel();
		
		$phone_crm_extension = trim($current_user->phone_crm_extension);
		$phone_crm_extension = preg_replace('/[^0-9]/','', $phone_crm_extension);
		
		$result = array();
		
		if( $phone_crm_extension  != '' && $phone_crm_extension != null ){
			
			$query = "SELECT 
			DISTINCT(vtiger_crmentityrel.relcrmid) as parent_id, 
			MAX(vtiger_crmentityrel.crmid) as smsid 			 			
			
			FROM `vtiger_crmentityrel` 
			
			INNER JOIN vtiger_smsnotifier_status on vtiger_smsnotifier_status.smsnotifierid = vtiger_crmentityrel.crmid 
			
			AND 
			( 
				vtiger_smsnotifier_status.tonumber like '%{$phone_crm_extension}' 
				OR 
				vtiger_smsnotifier_status.fromnumber like '%{$phone_crm_extension}' 
			) 
			
			WHERE vtiger_crmentityrel.module = 'SMSNotifier' 
			GROUP BY vtiger_crmentityrel.relcrmid  
			ORDER BY smsid DESC 			
			";
			
			$queryResult = $adb->pquery($query, array());		
			
			$numRows = $adb->num_rows($queryResult);
			
			if( $numRows ){			
				for( $i=0; $i<($numRows); $i++ ){
					$parent_crmId = $adb->query_result($queryResult, $i, 'parent_id');
					if(isRecordExists($parent_crmId)){
						$result[] = Vtiger_Record_Model::getInstanceById($parent_crmId, getSalesEntityType($parent_crmId));
					}
				}
			}
		}
		
		return $result;
		
		
	}
	
	function getRecordsListFromRequest(Vtiger_Request $request) {
		$cvId = $request->get('cvid');
		$selectedIds = $request->get('selected_ids');
		$excludedIds = $request->get('excluded_ids');

		if(!empty($selectedIds) && $selectedIds != 'all') {
			if(!empty($selectedIds) && count($selectedIds) > 0) {
				return $selectedIds;
			}
		}

		$customViewModel = CustomView_Record_Model::getInstanceById($cvId);
		if($customViewModel) {
            $searchKey = $request->get('search_key');
            $searchValue = $request->get('search_value');
            $operator = $request->get('operator');
            if(!empty($operator)) {
                $customViewModel->set('operator', $operator);
                $customViewModel->set('search_key', $searchKey);
                $customViewModel->set('search_value', $searchValue);
            }
			return $customViewModel->getRecordIds($excludedIds);
		}
	}
}