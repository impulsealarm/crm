<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class SMSNotifier_MassActionAjax_View extends Vtiger_MassActionAjax_View {
	function __construct() {
		parent::__construct();
		$this->exposeMethod('showReplySMSForm');
	}

	function process(Vtiger_Request $request) {
		$mode = $request->get('mode');
		if(!empty($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}

	
		/**
	 * Function shows form that will lets you reply to incoming SMS
	 * @param Vtiger_Request $request
	 */
	function showReplySMSForm(Vtiger_Request $request) {

		$sourceModule = $request->getModule();
		$moduleName = 'SMSNotifier';
		
		//pass parent sms ids as selectedIds
		
		$selectedIds = $this->getRecordsListFromRequest($request);
		$excludedIds = $request->get('excluded_ids');
		$cvId = $request->get('viewname');

		$user = Users_Record_Model::getCurrentUserModel();
        $moduleModel = Vtiger_Module_Model::getInstance($sourceModule);
        
        $viewer = $this->getViewer($request);
		
        //always one selectedid as Respond Button will appear only in SMS Detail View
        
        $adb = PearDatabase::getInstance();
        
		if(count($selectedIds) == 1){
			
			$smsnotiferID = $selectedIds[0];
			$selectedRecordModel = Vtiger_Record_Model::getInstanceById($smsnotiferID, $sourceModule);
			
			//to allow reply only for single incoming sms
			
			if( $selectedRecordModel->get('direction') != 'incoming' ){
				$selectedIds = array();
			}
			
			$viewer->assign('SINGLE_RECORD', $selectedRecordModel);
		
		} else if(count($selectedIds) > 1){
			
			foreach($selectedIds as $k => $smsnotiferID){
				
				$selectedRecordModel = Vtiger_Record_Model::getInstanceById($smsnotiferID, $sourceModule);
			
				//to allow reply only for incoming sms				
				if( $selectedRecordModel->get('direction') != 'incoming' ){
					unset($selectedIds[$k]);
				}
			}
		}
		
		$viewer->assign('VIEWNAME', $cvId);
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('SOURCE_MODULE', $sourceModule);
		$viewer->assign('SELECTED_IDS', $selectedIds);
		$viewer->assign('EXCLUDED_IDS', $excludedIds);
		$viewer->assign('USER_MODEL', $user);
		
		echo $viewer->view('ReplySMSForm.tpl', $moduleName, true);
	}
	
	
	/**
	 * Function returns the record Ids selected in the current filter
	 * @param Vtiger_Request $request
	 * @return integer
	 */
	function getRecordsListFromRequest(Vtiger_Request $request, $module = false) {
		$cvId = $request->get('viewname');
		$selectedIds = $request->get('selected_ids');
		$excludedIds = $request->get('excluded_ids');
        if(empty($module)) {
            $module = $request->getModule();
        }
		if(!empty($selectedIds) && $selectedIds != 'all') {
			if(!empty($selectedIds) && count($selectedIds) > 0) {
				return $selectedIds;
			}
		}
		
		$sourceRecord = $request->get('sourceRecord');
		$sourceModule = $request->get('sourceModule');
		if ($sourceRecord && $sourceModule) {
			$sourceRecordModel = Vtiger_Record_Model::getInstanceById($sourceRecord, $sourceModule);
			return $sourceRecordModel->getSelectedIdsList($module, $excludedIds);
		}

		$customViewModel = CustomView_Record_Model::getInstanceById($cvId);
		if($customViewModel) {
			$searchKey = $request->get('search_key');
			$searchValue = $request->get('search_value');
			$operator = $request->get('operator');
			if(!empty($operator)) {
				$customViewModel->set('operator', $operator);
				$customViewModel->set('search_key', $searchKey);
				$customViewModel->set('search_value', $searchValue);
			}
            $customViewModel->set('search_params', $request->get('search_params'));
			return $customViewModel->getRecordIds($excludedIds,$module);
		}
	}

	
}