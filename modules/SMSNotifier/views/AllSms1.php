<?php

class SMSNotifier_AllSms_View extends Vtiger_Index_View {
	
	function __construct() {
		parent::__construct();
		//$this->exposeMethod('');
	}

	public function preProcess (Vtiger_Request $request, $display=true) {
		
		parent::preProcess($request, false);

		$viewer = $this->getViewer($request);

		$moduleName = $request->getModule();
		
		if(!empty($moduleName)) {
			
			$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
			$currentUser = Users_Record_Model::getCurrentUserModel();
			$userPrivilegesModel = Users_Privileges_Model::getInstanceById($currentUser->getId());
			$permission = $userPrivilegesModel->hasModulePermission($moduleModel->getId());
			$viewer->assign('MODULE', $moduleName);

			if(!$permission) {
				$viewer->assign('MESSAGE', 'LBL_PERMISSION_DENIED');
				$viewer->view('OperationNotPermitted.tpl', $moduleName);
				exit;
			}

			$linkParams = array('MODULE'=>$moduleName, 'ACTION'=>$request->get('view'));
			$linkModels = $moduleModel->getSideBarLinks($linkParams);
			
			$linkModels = array();
			
			$quickWidgets = array(
				array(
					'linktype' => 'SIDEBARWIDGET',
					'linklabel' => 'Filter Results',
					'linkurl' => 'module=SMSNotifier&view=AllSmsAjax&mode=showAllRecentContactedRecords',
					'linkicon' => ''
				),
			);
			foreach($quickWidgets as $quickWidget) {
				$linkModels['SIDEBARWIDGET'][] = Vtiger_Link_Model::getInstanceFromValues($quickWidget);
			}
		
			
			$viewer->assign('QUICK_LINKS', $linkModels);
		}
		
		$viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$viewer->assign('CURRENT_VIEW', $request->get('view'));
		if($display) {
			$this->preProcessDisplay($request);
		}
	}

	protected function preProcessTplName(Vtiger_Request $request) {
		return 'AllSmsPreProcess.tpl';
	}
	
	function process(Vtiger_Request $request) {
		
		$mode = $request->get('mode');
		if(!empty($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
		
		global $current_user, $adb;
		
		$moduleName = $request->getModule();
		
		$current_user = Users_Record_Model::getCurrentUserModel();
		
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);		
		
		
		$pageNumber = $request->get('pageNumber');
		
		if(empty ($pageNumber)){
			$pageNumber = $request->get('page');
		}
		
		if(empty ($pageNumber)){
			$pageNumber = '1';			
		}
		
		$request_parent_crmid = $request->get('parent_crmid');
		
		$boxesPerPage = 4;
		
		$pagingModel = new Vtiger_Paging_Model();
		$pagingModel->set('page', $pageNumber);
		$pagingModel->set('limit', $boxesPerPage);
		
		$startIndex = $pagingModel->getStartIndex();
		$pageLimit = $pagingModel->getPageLimit();
		
		$phone_crm_extension = trim($current_user->phone_crm_extension);
		$phone_crm_extension = preg_replace('/[^0-9]/','', $phone_crm_extension);
		
		$currentPageRows = 0;
		$totalRows = 0;
		
		$currentPage_Entries = array();
		
		if( $phone_crm_extension  != '' && $phone_crm_extension != null ){
			
			$totalRows = $this->getTotalRowsCount();
			
			//get top 9 recent contacted records 
			
			$smsBoxesQuery = "SELECT 
			DISTINCT(vtiger_crmentityrel.relcrmid) as parent_id, 
			MAX(vtiger_crmentityrel.crmid) as smsid
			
			FROM `vtiger_crmentityrel` 
			
			INNER JOIN vtiger_smsnotifier_status on vtiger_smsnotifier_status.smsnotifierid = vtiger_crmentityrel.crmid 
			
			AND 
			( 
				vtiger_smsnotifier_status.tonumber like '%{$phone_crm_extension}' 
				OR 
				vtiger_smsnotifier_status.fromnumber like '%{$phone_crm_extension}' 
			) 
			
			WHERE vtiger_crmentityrel.module = 'SMSNotifier' ";
			
			if( $request_parent_crmid != '' ){
				$smsBoxesQuery .= " AND vtiger_crmentityrel.relcrmid = '" . $request_parent_crmid . "' "; 
			}
			
			$smsBoxesQuery .= "
			GROUP BY vtiger_crmentityrel.relcrmid  
			ORDER BY smsid DESC 			
			";
			
			$orig_smsBoxesQuery = $smsBoxesQuery;
			
			//fetch 1 more than page limit
			
			$smsBoxesQuery .= " LIMIT $startIndex,".($pageLimit+1);
		 	
			$smsBoxesResult = $adb->pquery($smsBoxesQuery, array());		
			
			$currentPageRows = $adb->num_rows($smsBoxesResult);
			
			if( $currentPageRows ){
			
				for( $i=0; $i<($currentPageRows); $i++ ){
					
					$parent_crmId = $adb->query_result($smsBoxesResult, $i, 'parent_id');
					
					$lastsmsId = $adb->query_result($smsBoxesResult, $i, 'smsid');
					
					$filteredPhone = '';
					$origPhone = '';
					
					$phoneResults = $adb->pquery("SELECT tonumber, fromnumber   
					FROM vtiger_smsnotifier_status  
					WHERE smsnotifierid = ? ", array($lastsmsId));
					
					if($adb->num_rows($phoneResults)){
						
						$toPhone = $adb->query_result($phoneResults, 0, 'tonumber');
						$fromPhone = $adb->query_result($phoneResults, 0, 'fromnumber');
						
						if( strpos($toPhone, $phone_crm_extension) === false ){
							$origPhone = $toPhone;
						} else {
							$origPhone = $fromPhone;
						}
						$filteredPhone = preg_replace('/[^0-9]/','', trim($origPhone));
					}
	
					$currentPage_Entries[] = array(
						'parent_crmid' => $parent_crmId,
						'phone' => $filteredPhone,
						'origphone' => $origPhone,
					);
					
				}
				
			}
						
			
		}	//	End CRM Phone Extension

	
		$pagingModel->calculatePageRange($currentPage_Entries);

		if($currentPageRows > $pageLimit){
			array_pop($currentPage_Entries);
			$pagingModel->set('nextPageExists', true);
		}else{
			$pagingModel->set('nextPageExists', false);
		}
		
		
		$viewer = $this->getViewer ($request);
				
		$viewer->assign('MODULE',$moduleName);		
		$viewer->assign('MODULE_MODEL', $moduleModel);
		$viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel()); 
		
		$viewer->assign('PAGING_MODEL', $pagingModel);
		$viewer->assign('PAGE_NUMBER',$pageNumber);
		
		
		$viewer->assign('PARENT_RECORDS', $currentPage_Entries);
        $viewer->assign('CURRENT_ENTRIES_COUNT', count($currentPage_Entries));
        
		//calculate total pages
		
		$pageLimit = $pagingModel->getPageLimit();

		$pageCount = ceil((int) $totalRows / (int) $pageLimit);
		if($pageCount == 0){
			$pageCount = 1;
		}
		
		$viewer->assign('PAGE_COUNT', $pageCount);
		$viewer->assign('LISTVIEW_COUNT', $totalRows);

		
		echo $viewer->view('AllSMSes.tpl',$moduleName,true);		
	}
	
	
	public function getTotalRowsCount(){
		
		global $current_user, $adb;
		
		$current_user = Users_Record_Model::getCurrentUserModel();
		
		$phone_crm_extension = trim($current_user->phone_crm_extension);
		$phone_crm_extension = preg_replace('/[^0-9]/','', $phone_crm_extension);
		
		$totalRows = 0;
		
		if( $phone_crm_extension  != '' && $phone_crm_extension != null ){
			
			//get top 9 recent contacted records 
			
			$boxesQuery = "SELECT 
			DISTINCT(vtiger_crmentityrel.relcrmid) as parent_id, 
			MAX(vtiger_crmentityrel.crmid) as smsid
			
			FROM `vtiger_crmentityrel` 
			
			INNER JOIN vtiger_smsnotifier_status on vtiger_smsnotifier_status.smsnotifierid = vtiger_crmentityrel.crmid 
			
			AND 
			( 
				vtiger_smsnotifier_status.tonumber like '%{$phone_crm_extension}' 
				OR 
				vtiger_smsnotifier_status.fromnumber like '%{$phone_crm_extension}' 
			) 
			
			WHERE vtiger_crmentityrel.module = 'SMSNotifier' 
			GROUP BY vtiger_crmentityrel.relcrmid  
			ORDER BY smsid DESC 			
			";
			
			$result = $adb->pquery($boxesQuery, array());		
			$totalRows = $adb->num_rows($result);
		}
		
		return $totalRows;
	}
	
	
	function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);
		$moduleName = $request->getModule();

		$jsFileNames = array(
			"modules.$moduleName.resources.AllSms",
		);

		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}
	
	
	function getHeaderCss(Vtiger_Request $request) {
		
		$headerCssInstances = parent::getHeaderCss($request);
		
		$smsCssFileName = 'allsms.css';
		
		$selectedThemePath = Vtiger_Theme::getThemePath();
		
		$filePath =  $selectedThemePath . '/' . $smsCssFileName;		
		$completeFilePath = Vtiger_Loader::resolveNameToPath('~'.$filePath);
		
		$selectedThemeCssPath = '';
		
		if(file_exists($completeFilePath)){

			$selectedThemeCssPath = $filePath;
		
		} else {
			
			$fallbackPath = Vtiger_Theme::getBaseThemePath() . '/' . $smsCssFileName ;
			$completeFallBackPath = Vtiger_Loader::resolveNameToPath('~'.$fallbackPath);
		
			if(file_exists($completeFallBackPath)){
				$selectedThemeCssPath = $fallbackPath;
			}
		}
		
		
		$isLessType = (strpos($selectedThemeCssPath, ".less") !== false)? true:false;
		
		$cssScriptModel = new Vtiger_CssScript_Model();
		$headerCssInstances[] = $cssScriptModel->set('href', $selectedThemeCssPath)
									->set('rel',
											$isLessType?
											Vtiger_CssScript_Model::LESS_REL :
											Vtiger_CssScript_Model::DEFAULT_REL);

		return $headerCssInstances;
	}
	
}
