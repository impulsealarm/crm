<?php

class SMSNotifier_Detail_View extends Vtiger_Detail_View{
	
	/**
	 * Function returns Related Module details
	 * @param Vtiger_Request $request
	 */
	function showModuleDetailView(Vtiger_Request $request) {
		echo parent::showModuleDetailView($request);
		$this->showRelatedToBlocks($request);
	}
	
	function showDetailViewByMode(Vtiger_Request $request) {
		return $this->showModuleDetailView($request);
	}

	function showModuleBasicView($request) {
		return $this->showModuleDetailView($request);
	}
	
	function showRelatedToBlocks($request){
		$record = $request->get('record');
		$moduleName = $request->getModule();

		$recordModel = Vtiger_Record_Model::getInstanceById($record);
		$relatedmodules = $recordModel->getRelatedModule();
		
		if(!empty($relatedmodules)) {
			
			$viewer = $this->getViewer($request);
			$viewer->assign('RELATED_MODULES', $relatedmodules);
			$viewer->assign('RECORD', $recordModel);
			$viewer->assign('MODULE_NAME',$moduleName);
	
			$viewer->view('RelatedModuleBlocksDetail.tpl', $moduleName);
		}
		
	}
}