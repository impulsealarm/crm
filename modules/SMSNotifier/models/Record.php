<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
vimport('~~/modules/SMSNotifier/SMSNotifier.php');

class SMSNotifier_Record_Model extends Vtiger_Record_Model {

    const moduletableName = 'vtiger_smsnotifier';
    const detailtableName = 'vtiger_smsnotifier_status';
    const entitytableName = 'vtiger_crmentity';
    
    static function getCleanInstance(){
        return new self;
    }
    
    /**
     * Function to get sms details(polling)
     * return <array> sms
     */
    public function searchIncomingSMS($smownerid = false){
    	
        $db = PearDatabase::getInstance();
        $query = "SELECT * FROM " . self::moduletableName . " as smsnotify   
        INNER JOIN " . self::entitytableName . " as entity ON entity.crmid = smsnotify.smsnotifierid   
        INNER JOIN " . self::detailtableName . " as statuss ON statuss.smsnotifierid = smsnotify.smsnotifierid   
        WHERE entity.deleted = '0' AND
        smsnotify.notified = '0' AND 
        smsnotify.direction= 'incoming' ";
        
        if($smownerid && $smownerid != '' ){
        	$query .= " AND entity.smownerid = '" . $smownerid . "'";
        }
        
        $result = $db->pquery($query,array());
        
        $recordModels = array();
        $rowCount =  $db->num_rows($result);
        for($i=0; $i<$rowCount; $i++) {
            $rowData = $db->query_result_rowdata($result, $i);
            
            $record = new self();
            $record->setData($rowData);
            $recordModels[] = $record;            
        }    
        return $recordModels;
    }
    
    
	public static function SendSMS($message, $toNumbers, $currentUserId, $recordIds, $moduleName, $sendsmsfrom_userid='' ) {
		SMSNotifier::sendsms($message, $toNumbers, $currentUserId, $recordIds, $moduleName, $sendsmsfrom_userid);
	}

	public function checkStatus() {
		$statusDetails = SMSNotifier::getSMSStatusInfo($this->get('id'));

		$statusColor = $this->getColorForStatus($statusDetails[0]['status']);

		$this->setData($statusDetails[0]);
		
		/* -------------------- */
		
		$this->set('statuscolor', $statusColor);
		
		/* -------------------- */
		

		return $this;
	}

	public function getCheckStatusUrl() {
		return "index.php?module=".$this->getModuleName()."&view=CheckStatus&record=".$this->getId();
	}

	public function getColorForStatus($smsStatus) {

		if ($smsStatus == 'Processing') {
			$statusColor = '#FFFCDF';
		} elseif ($smsStatus == 'Dispatched') {
			$statusColor = '#E8FFCF';
		} elseif ($smsStatus == 'Failed') {
			$statusColor = '#FFE2AF';
		} 
		/* ------------------- */
		elseif($smsStatus == 'queued') {
			$statusColor = '#FFFCDF';
		} elseif($smsStatus == 'sent') {
			$statusColor = '#E8FFCF';
		} elseif($smsStatus == 'delivered') {
			$statusColor = '#219C0B';
		} elseif($smsStatus == 'received') {
			$statusColor = '#219C0B';
		} 
		/* ----------------------  */
		
		else {
			$statusColor = '#FFFFFF';
		}
	
		return $statusColor;
	}


	/**
	 * Function Get the Related Module Name List of SMS Notifier
	 * @return the array of the status,to_number,from_number,Module,EntityName
	 */
	public function getRelatedModule(){
		
		global $adb;

		$block_details = array();
		
		$result = $adb->pquery("select relcrmid,relmodule from vtiger_crmentityrel where crmid = ? ",array($this->getId()));
		
		if($adb->num_rows($result)){
			
			$smsnotfier_status_details = $this->getSMSNotifierStatusDetails();
			
			$block_details['from_number'] = $smsnotfier_status_details['fromnumber'];
			
			$block_details['to_number'] = $smsnotfier_status_details['tonumber'];
			
			$block_details['status'] = $smsnotfier_status_details['status'];
			
			
			for($i=0; $i<$adb->num_rows($result);$i++){
				
				$relcrmid = $adb->query_result($result,$i,"relcrmid");
				
				$related_module = $adb->query_result($result,$i,"relmodule");
				
				$EntityName = getEntityName($related_module,array($relcrmid));
				
				$block_details['related_to'][$related_module][] = '<a href="index.php?module='.$related_module.'&amp;view=Detail&amp;record='.$relcrmid.'">'.$EntityName[$relcrmid].'</a>';
			}
		}
		
		return $block_details;
	}
	
	public function getSMSNotifierStatusDetails(){
		
		global $adb;
		
		$sms_details = array();
		
		$result = $adb->pquery("select * from vtiger_smsnotifier_status where smsnotifierid = ?",array($this->getId()));
		
		if($adb->num_rows($result)){
			$sms_details['tonumber'] = 	$adb->query_result($result,0,"tonumber");
			$sms_details['status'] = $adb->query_result($result,0,"status");
			$sms_details['statusmessage'] = $adb->query_result($result,0,"statusmessage");
			$sms_details['fromnumber'] = $adb->query_result($result,0,"fromnumber");
		}
			
		return $sms_details;
	}
	/*-----------------END----------------------------------------*/





}