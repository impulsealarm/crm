<?php

class SMSNotifier_AllSms_Model extends Vtiger_Base_Model {

	public static function getSMS_ofRecord($recordid = '', $filteredPhone = ''){
		
		$smses = array();
		
		if($recordid != ''){
			
			global $adb;
			
			$current_user = Users_Record_Model::getCurrentUserModel();
			
			$smsQuery = " SELECT 
			vtiger_smsnotifier.*, vtiger_smsnotifier_status.* 
			
			FROM vtiger_smsnotifier 
			
			INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_smsnotifier.smsnotifierid 
			AND vtiger_crmentity.deleted = '0' 
			AND vtiger_crmentity.smownerid = '{$current_user->id}' 
			
			INNER JOIN vtiger_smsnotifier_status on vtiger_crmentity.crmid = vtiger_smsnotifier_status.smsnotifierid  
			AND 
			(
				vtiger_smsnotifier_status.tonumber like '%{$filteredPhone}' OR 
				vtiger_smsnotifier_status.fromnumber like '%{$filteredPhone}' 
			)	
			
			INNER JOIN vtiger_crmentityrel on vtiger_crmentityrel.crmid = vtiger_smsnotifier.smsnotifierid  
			AND vtiger_crmentityrel.module = 'SMSNotifier' 
			AND vtiger_crmentityrel.relcrmid = '{$recordid}' ";

			//$smsQuery .= " WHERE vtiger_crmentity.createdtime >= ( CURRENT_DATE - INTERVAL 2 DAY ) ";
			
			$smsQuery .= " 
			ORDER BY vtiger_smsnotifier.smsnotifierid DESC ";
			
			//$smsResult = $adb->pquery($smsQuery." LIMIT 0,5 ", array());
			
			$totalRows = $adb->num_rows($smsResult);
			
			if( $totalRows ){
				
				for( $i=0; $i<$totalRows; $i++ ){
					
					$rowData = $adb->query_result_rowdata($smsResult, $i);
			            
		            $record = new SMSNotifier_Record_Model();
		            $record->setData($rowData);
		            $smses[] = $record;
				}
			}			
		}
		return $smses;
	}
	
	
	public static function getTop5sms($recordid = '', $phoneNo = ''){
		
		$smses = array();
		
		if($phoneNo != ''){
			
			global $adb;
			
			$current_user = Users_Record_Model::getCurrentUserModel();
			
			$phone_crm_extension = trim($current_user->phone_crm_extension);
			$phone_crm_extension = preg_replace('/[^0-9]/','', $phone_crm_extension);
		
			$filteredPhone = preg_replace('/[^0-9]/','', $phoneNo);
			
			if( $filteredPhone != '' && $phone_crm_extension != '' ){
				$smsQuery = " SELECT 
				vtiger_smsnotifier.*, vtiger_smsnotifier_status.* 
				
				FROM vtiger_smsnotifier 
				
				INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_smsnotifier.smsnotifierid 
				AND vtiger_crmentity.deleted = '0'
				
				INNER JOIN vtiger_smsnotifier_status on vtiger_crmentity.crmid = vtiger_smsnotifier_status.smsnotifierid  
				AND 
				( 
					( 
					vtiger_smsnotifier_status.tonumber like '%{$filteredPhone}' 
					AND 
					vtiger_smsnotifier_status.fromnumber like '%{$phone_crm_extension}' 
					) 
	  				OR 
	  				( 
	  				vtiger_smsnotifier_status.fromnumber like '%{$filteredPhone}' 
	  				AND 
	  				vtiger_smsnotifier_status.tonumber like '%{$phone_crm_extension}' 
	  				)
	  			)	
				
				ORDER BY vtiger_smsnotifier.smsnotifierid DESC ";
				
				$smsResult = $adb->pquery($smsQuery." LIMIT 0,5 ", array());
				
				$totalRows = $adb->num_rows($smsResult);
				
				if( $totalRows ){
					
					for( $i=0; $i<$totalRows; $i++ ){
						
						$rowData = $adb->query_result_rowdata($smsResult, $i);
				            
			            $record = new SMSNotifier_Record_Model();
			            $record->setData($rowData);
			            $smses[] = $record;
					}
					
					$smses = array_reverse($smses);
				}	
			}		
		}
		return $smses;
	}
	
	public static function getSmallSmsHistory($recordid = '', $phoneNo = ''){
		
		$smses = array();
		
		if($phoneNo != ''){
			
			global $adb;
			
			$current_user = Users_Record_Model::getCurrentUserModel();
			
			$phone_crm_extension = trim($current_user->phone_crm_extension);
			$phone_crm_extension = preg_replace('/[^0-9]/','', $phone_crm_extension);
		
			$filteredPhone = preg_replace('/[^0-9]/','', $phoneNo);
			
			if( $filteredPhone != '' && $phone_crm_extension != '' ){
				$smsQuery = " SELECT 
				vtiger_smsnotifier.*, vtiger_smsnotifier_status.* 
				
				FROM vtiger_smsnotifier 
				
				INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_smsnotifier.smsnotifierid 
				AND vtiger_crmentity.deleted = '0'
				
				INNER JOIN vtiger_smsnotifier_status on vtiger_crmentity.crmid = vtiger_smsnotifier_status.smsnotifierid  
				AND 
				( 
					( 
					vtiger_smsnotifier_status.tonumber like '%{$filteredPhone}' 
					AND 
					vtiger_smsnotifier_status.fromnumber like '%{$phone_crm_extension}' 
					) 
	  				OR 
	  				( 
	  				vtiger_smsnotifier_status.fromnumber like '%{$filteredPhone}' 
	  				AND 
	  				vtiger_smsnotifier_status.tonumber like '%{$phone_crm_extension}' 
	  				)
	  			)	";
				
	  			$smsQuery .= " WHERE vtiger_crmentity.createdtime >= ( CURRENT_DATE - INTERVAL 2 DAY ) ";
			
				$smsQuery .= " ORDER BY vtiger_smsnotifier.smsnotifierid DESC ";
				
				//$smsQuery .= " LIMIT 0,5 ";
				
				$smsResult = $adb->pquery($smsQuery, array());
				
				$totalRows = $adb->num_rows($smsResult);
				
				if( $totalRows ){
					
					for( $i=0; $i<$totalRows; $i++ ){
						
						$rowData = $adb->query_result_rowdata($smsResult, $i);
				            
			            $record = new SMSNotifier_Record_Model();
			            $record->setData($rowData);
			            $smses[] = $record;
					}
					
					$smses = array_reverse($smses);
				}	
			}		
		}
		return $smses;
	}
	
	public function hasMoreSMS($smsid){
		
	}
}