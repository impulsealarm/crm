<?php

vimport('~~/modules/SMSNotifier/SMSNotifier.php');

class SMSNotifier_ReplySMS_Action extends Vtiger_Mass_Action {

	function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		if(!$currentUserPriviligesModel->hasModuleActionPermission($moduleModel->getId(), 'Save')) {
			throw new AppException(vtranslate($moduleName).' '.vtranslate('LBL_NOT_ACCESSIBLE'));
		}
	}

	/**
	 * Function that saves SMS records
	 * @param Vtiger_Request $request
	 */
	public function process(Vtiger_Request $request) {
		
		$moduleName = $request->getModule();

		$message = $request->get('message');
		
		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$adb = PearDatabase::getInstance();
		
		$smsIds = $this->getRecordsListFromRequest($request);
				
		$replyToNumbers = array();
		$recordIds = array();
		
		$replied = false;
		
		//parse each parent smsIds and reply to each
		
		foreach($smsIds as $recordId) {
			
			if( getSalesEntityType($recordId) != 'SMSNotifier' ){
				continue;
			}
			
			$numberResult = $adb->pquery("SELECT * from vtiger_smsnotifier_status where smsnotifierid = ?",array($recordId));
			
			if($adb->num_rows($numberResult)){

				$sendto = $adb->query_result($numberResult, 0, 'fromnumber');	// the number from which sms came
				
				//if($sendto == '' || strpos($sendto,'+') === false || strpos($sendto,'+') != 0 ){

				if($sendto == '' || strlen($sendto) < 10){
					continue;				
				
				} else {
					
					//$replyToNumbers = $sendto;
					
					$linkToIds = false;
					
					$linkToResult = $adb->pquery("
					SELECT CONVERT(GROUP_CONCAT( vtiger_crmentityrel.relcrmid ) USING utf8) as relids 
					FROM vtiger_crmentityrel where crmid = ? and module = ?",array($recordId, 'SMSNotifier'));
				
					if($adb->num_rows($linkToResult)){
						$relatedIds = $adb->query_result($linkToResult, 0, 'relids');
						$linkToIds = ($relatedIds != '') ? explode(',',$relatedIds) : false;
					}

					if(strlen($sendto) > 10){
						$sendto = '+' . preg_replace ( '/\D+/', '', $sendto);
					}
					
					if($linkToIds !== false){
						$replyToNumbers[$linkToIds[0]] = $sendto;
					}
				
					
					SMSNotifier::sendreply($message, $replyToNumbers, $currentUserModel->getId(), $linkToIds, false, $recordId);
			
				}
			}
		}

		$response = new Vtiger_Response();
        
		if(!empty($replyToNumbers)) {
			$response->setResult(true);
		} else {
			$response->setResult(false);
		}
		return $response;
	}
}
