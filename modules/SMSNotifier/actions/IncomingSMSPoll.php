<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
include_once 'include/Webservices/Create.php';
include_once 'include/utils/utils.php';

class SMSNotifier_IncomingSMSPoll_Action extends Vtiger_Action_Controller{
    
    function __construct() {
            $this->exposeMethod('searchIncomingSMS');
            //$this->exposeMethod('createRecord');
            //$this->exposeMethod('getCallStatus');
            $this->exposeMethod('checkModuleViewPermission');
            $this->exposeMethod('checkPermissionForPolling');
            //$this->exposeMethod('getToken');
            //$this->exposeMethod('userUpdateOnline');
            
            $this->exposeMethod('updateIncomingSMS');
   	}
    
    public function process(Vtiger_Request $request) {
		$mode = $request->getMode();
		if(!empty($mode) && $this->isMethodExposed($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}
    
    public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		$userPrivilegesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$permission = $userPrivilegesModel->hasModulePermission($moduleModel->getId());

		if(!$permission) {
			throw new AppException('LBL_PERMISSION_DENIED');
		}
	}
    
    public function checkModuleViewPermission(Vtiger_Request $request){
        $response = new Vtiger_Response();
        $modules = array('Contacts','Leads');
        $view = $request->get('view');
        Users_Privileges_Model::getCurrentUserPrivilegesModel();
        foreach($modules as $module){
            if(Users_Privileges_Model::isPermitted($module, $view)){
                $result['modules'][$module] = true;
            }else{
                $result['modules'][$module] = false;
            }
        }
        $response->setResult($result);
        $response->emit();
    }
    
    public function searchIncomingSMS(Vtiger_Request $request){
        
        $response = new Vtiger_Response();
        $user = Users_Record_Model::getCurrentUserModel();
        
        $recordModel = SMSNotifier_Record_Model::getCleanInstance();
        $recordModels = $recordModel->searchIncomingSMS($user->id);
     
        $smses = array();
        
        $adb = PearDatabase::getInstance();
        
        // To check whether user have permission on caller record
        if($recordModels){
            foreach ($recordModels as $recordModel){
            	
            	$smsnotifierid = $recordModel->get('smsnotifierid');
            	
            	$sms_fromNumber = $recordModel->get('fromnumber');
            	$recordModel->set('fromNumber', $sms_fromNumber);
            	
            
            	// Show From Name in Notification Popup.
            	
            	$fromName = "";
            	
            	$fromResult = $adb->pquery("select * from vtiger_crmentityrel where crmid = ?",array($smsnotifierid));
            	
            	if(!$adb->num_rows($fromResult)){
            		
            		//$fromName = "N/A (" . $sms_fromNumber . ")";
			
            		//continue;
            		
            	} else {
            		$crmid = $adb->query_result($fromResult, 0, 'relcrmid');
            		$fromNumberResult = $adb->pquery("select phone from vtiger_crm_phonenumbers where crmid = ?",array($crmid));
            		$fromNo = $adb->query_result($fromNumberResult,0,'phone');

            		$recordModel->set('fromNumber',$fromNo);
            		
            		/*$relatedmodule = $adb->query_result($fromResult, 0, 'relmodule');
            		$entityName = getEntityName($relatedmodule,array($crmid));
            		$fromName = '<a href="index.php?module='.$relatedmodule.'&view=Detail&record='.$crmid.'">'.$entityName[$crmid].' ('.$fromNo.') </a>';
            		*/
            	}
            	
            	//$recordModel->set('fromName',$fromName);
            	
            	//------------END-----------------------
            		
                $recordModel->set('current_user_id',$user->id);
                $smses[] = $recordModel->getData();
            }
        }
        $response->setResult($smses);
        $response->emit();
    }
    
    function checkPermissionForPolling(Vtiger_Request $request) {
        Users_Privileges_Model::getCurrentUserPrivilegesModel();
        $callPermission = Users_Privileges_Model::isPermitted('SMSNotifier', 'DetailView');
        
        $db = PearDatabase::getInstance();
        $queryResult = $db->pquery("SELECT * from vtiger_smsnotifier_servers where isactive = '1'",array());
        if($db->num_rows($queryResult)){
        	$providertype = $db->query_result($queryResult, 0, 'providertype');
        }
        

        $user = Users_Record_Model::getCurrentUserModel();
        $userNumber = $user->phone_crm_extension;
        
        $result = false;
        if($callPermission && $userNumber && $providertype ){
            $result = true;
        }
        
        $response = new Vtiger_Response();
        $response->setResult($result);
        $response->emit();
    }
    
    function updateIncomingSMS(Vtiger_Request $request){
    	
    	$user = Users_Record_Model::getCurrentUserModel();
		$db = PearDatabase::getInstance();

		$record = $request->get('record');
		
		$response = new Vtiger_Response();
        
		if( $request->get('notified') && $record != '' ){
			$db->pquery( "UPDATE vtiger_smsnotifier 
			INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_smsnotifier.smsnotifierid 
			SET notified = '1' WHERE smownerid = ? and smsnotifierid = ?", array( $user->id, $record ) );
			
			$response->setResult(true);
		} else {
			$response->setResult(false);
		}
		
        
        $response->emit();
    }


}

?>
