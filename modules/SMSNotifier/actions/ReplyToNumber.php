<?php

vimport('~~/modules/SMSNotifier/SMSNotifier.php');

class SMSNotifier_ReplyToNumber_Action extends Vtiger_Mass_Action {

	function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		if(!$currentUserPriviligesModel->hasModuleActionPermission($moduleModel->getId(), 'Save')) {
			throw new AppException(vtranslate($moduleName).' '.vtranslate('LBL_NOT_ACCESSIBLE'));
		}
	}

	/**
	 * Function that saves SMS records
	 * @param Vtiger_Request $request
	 */
	public function process(Vtiger_Request $request) {
		
		$moduleName = $request->getModule();

		$message = $request->get('message');
		
		$sendToNumber = $request->get('sendto');
		
		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$adb = PearDatabase::getInstance();
		
		//$parentcrmIds = $this->getRecordsListFromRequest($request);

		$parentcrmId = $request->get('parent_crmid');
		
		$replyToNumbers = array();
		$recordIds = array();
		
		$sendto = $sendToNumber;
				
		if( $sendto != '' && strlen($sendto) >= 10 && $parentcrmId != '' ){
			
			if(strlen($sendto) > 10){
				$sendto = '+' . preg_replace ( '/\D+/', '', $sendto);
			}
			
			$linkToIds = false; 
			if($parentcrmId){
				$replyToNumbers[$parentcrmId] = $sendto;
				$linkToIds = array($parentcrmId);
			}
						
			
			SMSNotifier::sendsms($message, $replyToNumbers, $currentUserModel->getId(), $linkToIds, false, '');
			
		}

		$response = new Vtiger_Response();
        
		if(!empty($replyToNumbers)) {
			$response->setResult(true);
		} else {
			$response->setResult(false);
		}
		return $response;
	}
}