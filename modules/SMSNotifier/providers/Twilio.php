<?php

class SMSNotifier_Twilio_Provider implements SMSNotifier_ISMSProvider_Model {

	private $userName;
	private $password;
	private $parameters = array();

	const SERVICE_URI = 'http://api.twilio.com';
	private static $REQUIRED_PARAMETERS = array();//array('api_id', 'from', 'mo');

	/**
	 * Function to get provider name
	 * @return <String> provider name
	 */
	public function getName() {
		return 'Twilio';
	}

	/**
	 * Function to get required parameters other than (userName, password)
	 * @return <array> required parameters list
	 */
	public function getRequiredParams() {
		return self::$REQUIRED_PARAMETERS;
	}

	/**
	 * Function to get service URL to use for a given type
	 * @param <String> $type like SEND, PING, QUERY
	 */
	public function getServiceURL($type = false) {
		if($type) {
			switch(strtoupper($type)) {
				case self::SERVICE_AUTH:	return self::SERVICE_URI . '/http/auth';
				case self::SERVICE_SEND:	return self::SERVICE_URI . '/http/sendmsg';
				case self::SERVICE_QUERY:	return self::SERVICE_URI . '/http/querymsg';
			}
		}
		return false;
	}

	/**
	 * Function to set authentication parameters
	 * @param <String> $userName
	 * @param <String> $password
	 */
	public function setAuthParameters($userName, $password) {
		$this->userName = $userName;
		$this->password = $password;
	}

	/**
	 * Function to set non-auth parameter.
	 * @param <String> $key
	 * @param <String> $value
	 */
	public function setParameter($key, $value) {
		$this->parameters[$key] = $value;
	}

	/**
	 * Function to get parameter value
	 * @param <String> $key
	 * @param <String> $defaultValue
	 * @return <String> value/$default value
	 */
	public function getParameter($key, $defaultValue = false) {
		if(isset($this->parameters[$key])) {
			return $this->parameters[$key];
		}
		return $defaultValue;
	}

	/**
	 * Function to prepare parameters
	 * @return <Array> parameters
	 */
	protected function prepareParameters() {
		$params = array('accountsid' => $this->userName, 'authtoken' => $this->password);
		foreach (self::$REQUIRED_PARAMETERS as $key) {
			$params[$key] = $this->getParameter($key);
		}
		return $params;
	}

	/**
	 * Function to handle SMS Send operation
	 * @param <String> $message
	 * @param <Mixed> $toNumbers One or Array of numbers
	 */
	public function send($message, $toNumbers, $sendsmsfrom_userid = "") {
		
		$from_no = "";
		
		if(!is_array($toNumbers)) {
			$toNumbers = array($toNumbers);
		}

		
		$params = $this->prepareParameters();
		$params['text'] = $message;
		//$params['to'] = implode(',', $toNumbers);
		
		$params['to'] = $toNumbers;
		

		if( $sendsmsfrom_userid != '' ){
			$user = Vtiger_Record_Model::getInstanceById( $sendsmsfrom_userid, "Users" );
			$from_no = $user->get('phone_crm_extension');
		}
		
		if( $from_no == "" ){
			$user = Users_Record_Model::getCurrentUserModel();
			$from_no = $user->phone_crm_extension;
		}
		
		$from_no = preg_replace('/[^0-9]/','',$from_no);
		
		if(strlen($from_no) > 10){
			$from_no = '+' . $from_no;
		} else {
			$from_no = '+1' . $from_no;
		}

		$params['from'] = $from_no;
		
		
		// Loads PHP helper library 
		
		ini_set ( "include_path", "../../" );
		require "modules/Twilio/twilio-php/Services/Twilio.php"; 


		// Your Account Sid and Auth Token from twilio.com/user/account

		$accountSid = $params['accountsid'];	//'AC3a9ccfb3c6f0f0b6aae6b389b847d399';	//live cred
		$authToken = $params['authtoken'];		//'7d8f1ec51d2430a4b99dd9f62eb654c7';

		$http = new Services_Twilio_TinyHttp(
		    'https://api.twilio.com',
		    array('curlopts' => array(
		        CURLOPT_SSL_VERIFYPEER => false,
		        //CURLOPT_SSL_VERIFYHOST => 2,
		    ))
		);
		
		$client = new Services_Twilio($accountSid, $authToken, "2010-04-01", $http);
		
		$results = array();
		
		//$toNumbers = explode(',',$params['to']);
		
		
		foreach($toNumbers as $parentId => $send_to){
			
			if($send_to == '') continue;
			
			$sendTo = preg_replace('/[^0-9]/','',$send_to);
			
			if(strlen($sendTo) > 10){
				$sendTo = '+' . $sendTo;
			} else {
				$sendTo = '+1' . $sendTo;
				//$sendTo = '+91' . $sendTo;
			}
			
			$messageToSend = $params['text'];
			
			if($parentId > 0 && getSalesEntityType($parentId) != ''){
				$messageToSend = getMergedDescription($messageToSend, $parentId, getSalesEntityType($parentId));
			}
			
			$result = array( 'error' => false, 'statusmessage' => '' );
			
			try{
			
				
				$sms = $client->account->messages->create(array(
				  "From" => $params['from'],
				  "To" => $sendTo,
				  "Body" => $messageToSend,
				  "StatusCallback" => "http://crm.impulsealarm.com/modules/Twilio/sms_status.php"
				));
				
				//$result["SmsSid"] = $sms->sid;
				$result["id"] = $sms->sid;
				$result["status"] = $sms->status;
				$result['to']  = $sms->to;
				$result['from'] = $sms->from;
			
			
				
			}catch(Exception $e){				
				$result["error"] = true;
				$result["statusmessage"] = $e->getMessage();
				$result['to']  = $sendTo;
				$result['from'] = $params['from'];
			}
			$result['provider'] = 'twilio';
			$result['direction'] = 'outgoing';
			
			$results[] = $result;
		}

		return $results;		
	}

	/**
	 * Function to get query for status using messgae id
	 * @param <Number> $messageId
	 */
	public function query($messageId) {
		$params = $this->prepareParameters();
		$params['apimsgid'] = $messageId;

		$result = array( 'error' => false, 'needlookup' => 1, 'statusmessage' => '' );
		
		return $result;
		
		$serviceURL = $this->getServiceURL(self::SERVICE_QUERY);
		$httpClient = new Vtiger_Net_Client($serviceURL);
		$response = $httpClient->doPost($params);
		$response = trim($response);

		
		if(preg_match("/ERR: (.*)/", $response, $matches)) {
			$result['error'] = true;
			$result['needlookup'] = 0;
			$result['statusmessage'] = $matches[0];
		} else if(preg_match("/ID: ([^ ]+) Status: ([^ ]+)/", $response, $matches)) {
			$result['id'] = trim($matches[1]);
			$status = trim($matches[2]);

			// Capture the status code as message by default.
			$result['statusmessage'] = "CODE: $status";

			if($status == '002' || $status == '008' || $status == '011' ) {
				$result['status'] = self::MSG_STATUS_PROCESSING;
			} else if($status == '003' || $status == '004') {
				$result['status'] = self::MSG_STATUS_DISPATCHED;
				$result['needlookup'] = 0;
			} else {
				$statusMessage = '';
				switch($status) {
					case '001': $statusMessage = 'Message unknown';					break;
					case '005': $statusMessage = 'Error with message';				break;
					case '006': $statusMessage = 'User cancelled message delivery';	break;
					case '007': $statusMessage = 'Error delivering message';		break;
					case '009': $statusMessage = 'Routing error';					break;
					case '010': $statusMessage = 'Message expired';					break;
					case '012': $statusMessage = 'Out of credit';					break;
				}
				if(!empty($statusMessage)) {
					$result['error'] = true;
					$result['needlookup'] = 0;
					$result['statusmessage'] = $statusMessage;
				}
			}
		}
		return $result;
	}
}
?>