<?php
class MandrillUtil {
	public $attachment = array ();
	function AddEmbeddedImage($path, $cid, $name = "", $encoding = "base64", $type = "application/octet-stream") {
		if (! @is_file ( $path )) {
			$this->SetError ( $this->Lang ( "file_access" ) . $path );
			return false;
		}
		
		$filename = basename ( $path );
		if ($name == "")
			$name = $filename;
		$this->attachment ['content'] = base64_encode ( $path );
		$this->attachment ['name'] = $filename;
		$this->attachment ['type'] = $type;
		return $this->attachment;
	}
}