<?php

class Vtiger_DetailPopup_View extends Vtiger_Footer_View {

	function __construct() {
		parent::__construct();	
	}

	public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();

		if (!Users_Privileges_Model::isPermitted($moduleName, 'DetailView')) {
			throw new AppException('LBL_PERMISSION_DENIED');
		}
	}

    function preProcess(Vtiger_Request $request, $display=true) {
        return parent::preProcess($request,$display);
        //return '';
    }
    
	function isAjaxEnabled($recordModel) {
		return false;
		return $recordModel->isEditable();
	}

	public function process(Vtiger_Request $request) {
        
        $viewer = $this->getViewer($request);
		
		$mode = $request->getMode();
		if(!empty($mode)) {
			echo $this->invokeExposedMethod($mode, $request);
			return;
		}
		
		
		
		$recordId = $request->get('record');
		$moduleName = $request->getModule();
		
		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		
		$recordModel = $this->record->getRecord();
		
		$viewer->assign('RECORD', $recordModel);
		
		
		$detailViewLinkParams = array('MODULE'=>$moduleName,'RECORD'=>$recordId);
		$detailViewLinks = $this->record->getDetailViewLinks($detailViewLinkParams);

		$viewer->assign('DETAILVIEW_LINKS', $detailViewLinks);
		
		$viewer->assign('RELATED_ACTIVITIES', '');
		
		
		$viewer->assign('MODULE_SUMMARY', $this->showModuleSummaryView($request));
		


		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		
		$viewer->assign('MODULE_MODEL', $this->record->getModule());
		
		$viewer->assign('IS_EDITABLE', $this->record->getRecord()->isEditable($moduleName));
		$viewer->assign('IS_DELETABLE', $this->record->getRecord()->isDeletable($moduleName));


		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$viewer->assign('DEFAULT_RECORD_VIEW', $currentUserModel->get('default_record_view'));

		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
		
		$viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
		
		$viewer->assign('MODULE_NAME', $moduleName);

		$recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_DETAIL);
		$structuredValues = $recordStrucure->getStructure();

        $moduleModel = $recordModel->getModule();

		$viewer->assign('RECORD_STRUCTURE', $structuredValues);
        $viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
        
		
		echo $viewer->view('DetailPopup.tpl', $moduleName, true);
        
	}

	function postProcess(Vtiger_Request $request) {
		return parent::postProcess($request);
	}
	
	function showModuleSummaryView($request) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();

		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();
		$recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_SUMMARY);

        $moduleModel = $recordModel->getModule();
		$viewer = $this->getViewer($request);
		$viewer->assign('RECORD', $recordModel);
        $viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());

		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
		$viewer->assign('SUMMARY_RECORD_STRUCTURE', $recordStrucure->getStructure());
		$viewer->assign('RELATED_ACTIVITIES', '');

		return $viewer->view('DetailPopupFields.tpl', $moduleName, true);
	}
	
	
	function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);
		$moduleName = $request->getModule();

		$jsFileNames = array(
			'libraries.bootstrap.js.eternicode-bootstrap-datepicker.js.bootstrap-datepicker',
			'~libraries/bootstrap/js/eternicode-bootstrap-datepicker/js/locales/bootstrap-datepicker.'.Vtiger_Language_Handler::getShortLanguageName().'.js',
			'~libraries/jquery/timepicker/jquery.timepicker.min.js',
            'modules.Vtiger.resources.Header',
			
            //'modules.Vtiger.resources.Edit',
			//"modules.$moduleName.resources.Edit",
		
			'modules.Vtiger.resources.Vtiger',
			"modules.$moduleName.resources.$moduleName",
		
			'modules.Vtiger.resources.Popup',
			"modules.$moduleName.resources.Popup",
			
			//'modules.Vtiger.resources.Detail',
			//"modules.$moduleName.resources.Detail",
			
			
			'modules.Vtiger.resources.BaseList',
			"modules.$moduleName.resources.BaseList",
			'libraries.jquery.jquery_windowmsg',
			'modules.Vtiger.resources.validator.BaseValidator',
			'modules.Vtiger.resources.validator.FieldValidator',
			"modules.$moduleName.resources.validator.FieldValidator",
		
			"libraries.jquery.ckeditor.ckeditor",
			"libraries.jquery.ckeditor.adapters.jquery",
			"modules.Vtiger.resources.CkEditor",
			"modules.Emails.resources.MassEdit",
			
			"modules.Vtiger.resources.DetailPopup",
			
		
		);

		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}

}
