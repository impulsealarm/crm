<?php

class Vtiger_GetNotifications_Action extends Vtiger_Action_Controller{


	function __construct() {
		parent::__construct();
	}
	
    function checkPermission(Vtiger_Request $request) {
		return true;
	}
	
	function validateRequest(Vtiger_Request $request) {
		return true;
	}
	
    public function process(Vtiger_Request $request){
        
    	global $adb;
    	
        $response = new Vtiger_Response();
        
        $user = Users_Record_Model::getCurrentUserModel();
        
        /*$notificationsResult = $adb->pquery("SELECT vtiger_notifications.* FROM vtiger_notifications 
        INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_notifications.parentid 
        WHERE vtiger_notifications.userid = ? AND vtiger_crmentity.deleted = '0' 
        ORDER BY vtiger_notifications.createdtime DESC 
        LIMIT 0,15", array($user->id));*/
        
        $notificationsResult = $adb->pquery("SELECT vtiger_notifications.* FROM vtiger_notifications 
        WHERE vtiger_notifications.userid = ? 
        ORDER BY vtiger_notifications.createdtime DESC 
        LIMIT 0,15", array($user->id));

        $totalNotifications = $adb->num_rows($notificationsResult);
                
        $notifications = array();
	        
        $newCount = 0;
        
        if($totalNotifications){        	
        	
        	for($i=0; $i<$totalNotifications; $i++){
        		
        		$rowData = $adb->query_result_rowdata($notificationsResult, $i);
        	
        		if( $request->get('setSeen') == 'true' ){
	            	
        			$adb->pquery("update vtiger_notifications set seen = '1' where id = ?", array($rowData['id']));
	            	
	            	if( $rowData['parentmodule'] == 'SMSNotifier' ){
	            		$adb->pquery( "UPDATE vtiger_smsnotifier 
	            		INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_smsnotifier.smsnotifierid 
	            		SET notified = '1' WHERE smownerid = ? and smsnotifierid = ?", array( $user->id, $rowData['parentid'] ) );			
	            	}
	            }   
	            
	            if($rowData['seen'] != '1'){
	            	$newCount++;
	            }
	            
	            $createdTime = date("Y-m-d H:i:s",strtotime($rowData['createdtime']));
	            
	            $date_ob1 = date_create($createdTime);
				$date_ob2 = date_create(date('Y-m-d H:i:s'), timezone_open('UTC'));				
				$dateDiff = date_diff($date_ob1, $date_ob2);
				
				$addedon = 'At ' . date("Y-m-d H:i A",strtotime($createdTime));
				
				if($dateDiff->d > 0){
					$addedon = $dateDiff->d . ' day(s) ago at ' . date("H:i A",strtotime($createdTime));					
				} else {
					if($dateDiff->h > 0){
						$addedon = $dateDiff->h . ' hour(s) ago';
					} else {
						if($dateDiff->i > 0){
							$addedon = $dateDiff->i . ' minute(s) ago';
						} else {
							if($dateDiff->s > 0){
								$addedon = $dateDiff->i . ' seconds(s) ago';
							} else {
								$addedon = 'Just Now';
							}
						}
					}
				}
				
				$rowData['addedon'] = $addedon;
				
        		foreach($rowData as $key => $val){
        			$rowData[$key] = decode_html($val);
        		}
        		
        		$notifications[] = $rowData;   	            
        	}
        }
        
        $result = array( 'notifications' => $notifications, 'count' => $newCount );

        $response->setResult($result);
        $response->emit();
    }
    
    
}

?>
