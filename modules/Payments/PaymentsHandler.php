<?php
class PaymentsHandler extends VTEventHandler {
	    
	function handleEvent($eventName, $entityData) {

		$moduleName = $entityData->getModuleName();

        if ($moduleName != 'Payments') {
            return;
        }

        global $current_user;

        if ($eventName == 'vtiger.entity.aftersave') {
            
            $id = $entityData->getId();
            
            $realtor = $entityData->get("realtor_id");
            
            $contactid = $entityData->get("contactid");
            
			global $adb;
			
			$realtor_result = $adb->pquery("select * from vtiger_realtor where realtorid = ?", array($realtor));
			
			if($adb->num_rows($realtor_result)){
				
				$affiliate_id =  $adb->query_result($realtor_result, 0, "affiliate_id");
				
				$firstname = $adb->query_result($realtor_result, 0, "firstname");
				
				$lastname = $adb->query_result($realtor_result, 0, "lastname");
				
				$email = $adb->query_result($realtor_result, 0, "primary_email");
				
			}
			
			$paymentsAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
            
            if(!$affiliate_id){
            	
				$affiliate_info = array("email" => $email, "firstname" => $firstname, "lastname" => $lastname);
		        
				$result = $paymentsAffiliateModel->CreateAffiliateUsingApi($affiliate_info);
		
		        if(!empty($result) && isset($result['affiliate_id']) && $result['affiliate_id'] != ''){
		        	
		        	$affiliate_id = $result['affiliate_id'];
		        	
		        	$status = $result['info']['status'];
		        	
		        	if($status == 'active') 
		        		$status = "activate";
		        	if($status == 'inactive')
		        		$status = "deactivate";
					
					$realtor_obj->column_fields['affiliate_status'] = $status;
					$realtor_obj->column_fields['affiliate_id'] = $result['affiliate_id'];
					$realtor_obj->mode = "edit";
					$realtor_obj->save("Realtor");
				}
            }
            
            $contactRecordModel = Vtiger_Record_Model::getInstanceById($contactid, "Contacts");
			$contactRecordModel->set('id', $contactid);
			$contactRecordModel->set('mode', "edit");
			$referral_id = $contactRecordModel->get("referral_id");
	  
			// Create Referral for Contact if not set.
			if($affiliate_id && !$referral_id){
            	
            	$referral_data = array();

            	$reference = "";
		
				$reference = $contactRecordModel->get("firstname");
				
				$con_email = $contactRecordModel->get("email");
				
				if(!$reference)
					$reference = $contactRecordModel->get("lastname");
				else
					$reference .= " ".$contactRecordModel->get("lastname");	
					
				if($reference && $con_email != '')
					$reference .= " (" . $contactRecordModel->get("email") . ")";
					
				$referral_data['description'] = $contactRecordModel->get('description');
				
				$referral_data['reference'] = $reference;
				
				$referral_data['context'] = $contactRecordModel->get('contact_no');
				
            	$response = $paymentsAffiliateModel->CreateAffiliateReferralUsingAPI($affiliate_id, $referral_data);
            	
            	if(
					!empty($response) && isset($response['status']) && $response['status'] == 'ok' && 
					isset($response['referral_id']) && $response['referral_id'] != ''
				){
					
            		$referral_id = $response['referral_id'];
					
            		$referral_info = array("action" => "accept");
					
            		$ref_response = $paymentsAffiliateModel->ChangeReferralStatusUsingApi($referral_id, $referral_info);
            	
            		$status = "Pending";
            		
            		if(isset($ref_response['status']) && $ref_response['status'] == 'ok'){
					
						if(isset($ref_response['update_referral']) && isset($ref_response['referral_status']) && $ref_response['referral_status'] != '')
							$status = $ref_response['referral_status'];
						else
							$status = $ref_response['referral_status'];
							
					} else if(isset($ref_response['status']) && $ref_response['status'] == 'error'){
						$status = $ref_response['error'];
					}
					
        			$contactRecordModel->set('referral_id', $referral_id);

        			$contactRecordModel->set('referral_status', $status);

        			$contactRecordModel->save();
        		}
            }
        }
	}
}

/// End Here ..... /



function Payments_changeReferralStatusToUnpaid($entityData){
	
	$adb = PearDatabase::getInstance();
	
	$moduleName = $entityData->getModuleName();
	
	$wsId = $entityData->getId();
	
	$parts = explode('x', $wsId);
	
	$entityId = $parts[1];

	$contactid = $entityData->get("contactid");

	if($contactid){

		$contactid = explode('x', $contactid);
	
		$contactid = $contactid['1'];
		
		$contactRecordModel = Vtiger_Record_Model::getInstanceById($contactid, "Contacts");
		
		$referral_id = $contactRecordModel->get("referral_id");
	
		$con_ref_status = $contactRecordModel->get("referral_status");
		
		if($referral_id){
			
			$paymentsAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
            
			$update_contact = true;
			
			$response = array();
			
			$referral_info = array("action" => "mark_as_unpaid");//accept
				
			$response = $paymentsAffiliateModel->ChangeReferralStatusUsingApi($referral_id, $referral_info);
		
			if(!empty($response)){
				
				$status = "";
				
				if(isset($response['status']) && $response['status'] == 'ok'){
					
					if(isset($response['update_referral']) && $response['update_referral'] != '' && isset($response['status']))
						$status = $response['referral_status'];
					else if($response['referral_status'] == $con_ref_status)
						$update_contact = false;
					else
						$status = $response['referral_status'];
						
				} else if(isset($response['status']) && $response['status'] == 'error'){
					$status = $response['error'];
					$update_contact = true;
				}
				
				if($update_contact){

					$contactRecordModel->set('id', $contactid);
		
            		$contactRecordModel->set('mode', "edit");
			
					$contactRecordModel->set('referral_id', $referral_id);
        			$contactRecordModel->set('referral_status', $status);

        			$contactRecordModel->save();
        		}
			}
		}
	}
}