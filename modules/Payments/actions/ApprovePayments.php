<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Payments_ApprovePayments_Action extends Vtiger_Action_Controller{
    
	function __construct(){
		parent::__construct();
		$this->exposeMethod("checkStripeAccount");
		$this->exposeMethod("checkAffiliateAndReferral");
	}
	
    function checkPermission(Vtiger_Request $request) {
		return true;
    }
	
    public function process(Vtiger_Request $request) {
    	global $adb;
		
    	$mode = $request->get("mode");
    	
    	if($mode == 'checkStripeAccount'){
    		$this->invokeExposedMethod($mode, $request);
    		return;
    	} else if($mode == 'checkAffiliateAndReferral'){
    		$this->invokeExposedMethod($mode, $request);
    		return;
    	}
    	
    	$sms_code = $request->get('sms_code');
		
		$check_sms_code = false;
		
    	if($sms_code){
			
			$actual_sms_code = $_SESSION['sms_code'];
		
			if($sms_code === $actual_sms_code)
				$check_sms_code = true;
			else
				$check_sms_code = false;
		} else {
			$check_sms_code = true;
		}
		
		if($check_sms_code) {
		
			$_SESSION['sms_code_time'] = date("y-m-d H:i:s");
						
			$record = $request->get('record');
		
			if($record){
			
				$payment_res = $adb->pquery("select vtiger_payments.* from vtiger_payments inner join 
				vtiger_crmentity on vtiger_crmentity.crmid = vtiger_payments.paymentsid where 
				vtiger_payments.paymentsid = ? and vtiger_crmentity.deleted = 0",array($record));
				
				if($adb->num_rows($payment_res)){
					
					$payment_amount = $adb->query_result($payment_res,0,'amount');
					
					$managed_accountId = $this->getRealtorAccount($adb->query_result($payment_res,0,'realtor_id'));
				}
				
				$response = madeStripePayment($managed_accountId, $payment_amount);
				
				if(is_array($response) && isset($response['message'])){

					$result = array('success' => false ,"message" => $response['message']);
				} else {
					
					if($response == 'paid'){
						
						$payment_obj = CRMEntity::getInstance("Payments");

						$payment_obj->id = $record;
						
						$payment_obj->retrieve_entity_info($record, "Payments");
						
						$payment_obj->column_fields['payment_status'] = "Processed";
						$payment_obj->column_fields['payment_date'] = date("Y-m-d");
				
						$payment_obj->mode = "edit";
						
						$payment_obj->save("Payments");
					
						$recordModel = Vtiger_Record_Model::getInstanceById($record, "Payments");
						
						$contactid = $recordModel->get("contactid");
		
						if($contactid){
							
							$contact_referral_info = $recordModel->getContactReferralInfo($contactid);
							
							if(!empty($contact_referral_info)) {
								$recordModel->updateContactReferralStatus($contactid, $contact_referral_info);
							}
						}
							
						$result = array('success' => true, "message" => "Paid");
					}else {
						$result = array('success' => false, "message" => "Something Went Wrong.");
					}
				}
			}
		} else {
			$result = array('success' => false,"message" => "Invalid SMSCode");
		} 
		
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
    }
    
    function checkStripeAccount(Vtiger_Request $request){
    	
    	global $adb;
	
    	$result = array();
    	
    	$result['success'] = false;
    	
    	$realtor_id = $request->get("realtor");
    	
    	$managed_accountId = $this->getRealtorAccount($realtor_id);
		
		if(!$managed_accountId){
			$result['success'] = false;
			$result['message'] = "No Stripe Account Exists for this Realtor";
		} else {
			$result['success'] = true;
		}
		
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
    }
    
    function checkAffiliateAndReferral(Vtiger_Request $request){
    	
    	global $adb;

    	$result = array();
    	
    	$affiliate_id = $referral_id = "";
    	
    	$realtor = $request->get("realtor");
    	
    	$contact = $request->get("contact");
    	
    	$realtor_result = $adb->pquery("select affiliate_id from vtiger_realtor where realtorid = ?", array($realtor));
    	
    	if($adb->num_rows($realtor_result)){
    		$affiliate_id = $adb->query_result($realtor_result,"0","affiliate_id");
    	} else {
    		$result['success'] = false;
    		$result['message'] = "Realtor Doesnot Exists.";
    	}
    	
    	$referral_result = $adb->pquery("select referral_id from vtiger_contactdetails where contactid = ?", array($contact));
    	
    	if($adb->num_rows($referral_result)){
    		$referral_id = $adb->query_result($referral_result,"0","referral_id");
    	} else {
    		$result['success'] = false;
    		$result['message'] = "Contact Doesnot Exists.";
    	}
    	
    	if($referral_id){
    		
    		if($affiliate_id){
    			
    			$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
            
    			$response= $manageAffiliateModel->checkAffiliateReferral($affiliate_id, $referral_id);
    			
    			if(!empty($response)){
    				
    				if(isset($response['status']) && $response['status'] == 'ok'){
    					
    					if($response['is_affiliate_referral']){
    						$result['success'] = true;
    					} else {
    						$result['success'] = false;
    						$result['message'] = "This Contact Doesnot belong to Realtor.";
    					}
    					
    				} else if(isset($response['status']) && $response['status'] == 'error'){
						$result['success'] = false;
    					$result['message'] = $response['error'];
    				} 
    				
    			}
    			
    		} else {
    			$result['success'] = false;
    			$result['message'] = "This Contact Doesnot belong to Realtor.";
    		}
    	} else {
    		$result['success'] = true;
    	}
    	
    	$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}

	function getRealtorAccount($realtor_id){
		
		global $adb;
		
		$managed_accountId = "";
		
		$realtor_res = $adb->pquery("select vtiger_realtor.stripe_account_id from vtiger_realtor 
		inner join vtiger_crmentity on vtiger_crmentity.crmid = vtiger_realtor.realtorid
		where vtiger_realtor.realtorid = ? and vtiger_crmentity.deleted = 0",array($realtor_id));
		
		if($adb->num_rows($realtor_res)){
			$managed_accountId = $adb->query_result($realtor_res,0,'stripe_account_id');
		}
		
		return $managed_accountId;
	}
}

use Stripe\Stripe;
use Stripe\Transfer;

require('include/Stripe/init.php');

function madeStripePayment($stripe_accountId, $payment_amount){
	
	global $Stripe_secretKey;

	if($stripe_accountId == '') return array("success" => false, "message" => "Invalid Stripe Account.");
	
	Stripe::setApiKey($Stripe_secretKey);

	try{
		
		$transfer = Transfer::create(array(
				"amount" => intval($payment_amount * 100),
				"currency" => "usd",
				"destination" => $stripe_accountId,
				"description" => "Transfer for managed account"
		));
		
	} catch(\Stripe\Error\Card $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	} catch(\Stripe\Error\RateLimit $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\InvalidRequest $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\Authentication $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\ApiConnection $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\Base $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (Exception $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}
	
	if(!empty($transfer))
		$statusInfo = $transfer->getLastResponse()->json;
	
	if($statusInfo['status'])
		$status = $statusInfo['status'];
	
	return $status;

}

?>