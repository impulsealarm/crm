<?php
class Payments_StripeActionAjax_Action extends Vtiger_Action_Controller{

	function checkPermission(Vtiger_Request $request){
		return true;
	}
	function process(Vtiger_Request $request){
		
		$selectedOption = $request->get("save_record");
		
		$realtor = $request->get("realtor");
		
		if($selectedOption == "yes" && $realtor > 0){

			$realtor_obj = CRMEntity::getInstance("Realtor");
			
			$realtor_obj->id = $realtor;
			
			$realtor_obj->retrieve_entity_info($realtor_obj->id, "Realtor");
			
			$realtor_obj->column_fields['cf_923'] = 1;
			
			$realtor_obj->mode = "edit";
			
			$realtor_obj->save("Realtor");
			
			$result = array("success" => true);
		} else {
			$result = array("success" => false);
		}
		
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}
}