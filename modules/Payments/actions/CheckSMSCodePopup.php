<?php
/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

require_once('modules/Twilio/twilio-php/Services/Twilio.php'); 

class Payments_CheckSMSCodePopup_Action extends Vtiger_Action_Controller {

	function checkPermission(Vtiger_Request $request) {
		return true;
	}

	public function process(Vtiger_Request $request) {

		$adb = PearDatabase::getInstance();
		
		$result = array();
		
		$show_popup = false;
		
		if(isset($_SESSION['sms_code_time'])){
			
			if($_SESSION['sms_code_time'] != ''){
			 	
				$dateOne = new DateTime($_SESSION['sms_code_time']);
				
				$dateTwo = new DateTime();
				
				$interval = $dateOne->diff($dateTwo);

				$hours = $interval->h;
				
				$min = $interval->i;
				
				if($hours >= 1 && $min > 0){
					$show_popup = true;
					$_SESSION['sms_code_time'] = "";
				}
			} else {
				$show_popup = true;
			}
		} else {
			
			$_SESSION['sms_code_time'] = "";
			
			$show_popup = true;
		}
		
		if($show_popup){
			
			$cred_res = $adb->pquery("select * from vtiger_smsnotifier_servers Limit 1",array());
				
			if($adb->num_rows($cred_res) > 0){
				
				$sms_code = $this->GetRandomCode();
				
				$accountSid = $adb->query_result($cred_res,0,'username');
				
				$authToken = $adb->query_result($cred_res,0,'password');
				
				$message = "Code for Transaction Approval:" . $sms_code;
				
				//Test Credx
				$fromNo = "+19093253120";
			
				$toNo = "+19257830592";
						
				try {
					$http = new Services_Twilio_TinyHttp(
						'https://api.twilio.com',
						array('curlopts' => array(CURLOPT_SSL_VERIFYPEER => false))
					);
						
					$client = new Services_Twilio($accountSid, $authToken, "2010-04-01", $http);
						
					$sms = $client->account->messages->create(array(
					  "From" => $fromNo,
					  "To" => $toNo,
					  "Body" => $message,
					));
						
					if($sms->sid){
						$_SESSION['sms_code'] = $sms_code;
						$result = array('success'=>true, "show_sms_popup" => true,"sms_code" => $sms_code);
					}
					
				} catch(Exception $e) {			
					$result = array('success' => false,"message" => $e->getMessage());
				}
			} else {
				$result = array('success'=>false,"message" => "require_sms_server_setting");
			}
		} else {
			$result = array('success'=>true,"show_sms_popup" => false);
		}
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}
	
	function GetRandomCode(){
		
		$random_code = ""; $length = 6;
		
		for ($i = 0; $i<$length; $i++) {
    		$random_code .= mt_rand(0,9);
		}
		
		return $random_code;
	}
}
