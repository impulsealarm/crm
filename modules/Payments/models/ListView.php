<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Payments_ListView_Model extends Vtiger_ListView_Model {

	public function getListViewMassActions($linkParams) {
		
		$massActionLinks = parent::getListViewMassActions($linkParams);
			
		$massActionLink = array(
			'linktype' => 'LISTVIEWMASSACTION',
			'linklabel' => 'Approve Payments',
			'linkurl' => 'javascript:Payments_List_Js.triggerApprovePayments("index.php?module='.$this->getModule()->getName().'&view=MassActionAjax&mode=MassApprovePayments","Payments");',
			'linkicon' => ''
		);
		
		$massActionLinks['LISTVIEWMASSACTION'][] = Vtiger_Link_Model::getInstanceFromValues($massActionLink);
		
		return $massActionLinks;
	}
}