<?php 

class Payments_Record_Model extends Vtiger_Record_Model {

	public function getContactReferralInfo($contactid){
		
		global $adb;
		
		$contact_ref_info = array();
		
		$result = $adb->pquery("select referral_id, referral_status from vtiger_contactdetails where contactid = ?", array($contactid));
		
		if($adb->num_rows($result)){
			$contact_ref_info['referral_id'] = $adb->query_result($result,"0","referral_id");
			$contact_ref_info['referral_status'] = $adb->query_result($result,"0","referral_status");
		}
		
		return $contact_ref_info;
	}
	
	public function updateContactReferralStatus($contactid, $referral_info){
		
		global $adb;
		
		$update_contact = true;
		
		$referral_id = $referral_info['referral_id'];
		
		$con_ref_status = $referral_info['referral_status'];
		
		if($referral_id != '' && $referral_id > 0){
									
			$data = array("action" => "mark_as_paid");
								
			$paymentsAffiliateModel = Payments_ManageAffiliate_Model::getInstance();

			$referral_status = $paymentsAffiliateModel->ChangeReferralStatusUsingApi($referral_id, $data);
		
			if(!empty($referral_status)){
				
				$status = "";
				
				if(isset($referral_status['status']) && $referral_status['status'] == 'ok'){
					if(isset($referral_status['update_referral']) && $referral_status['update_referral'] != '' && isset($referral_status['status']))
						$status = $referral_status['referral_status'];
					else if($referral_status['referral_status'] == $con_ref_status){
						$update_contact = false;
					}else
						$status = "already paid status";
						
				}else if(isset($referral_status['status']) && $referral_status['status'] == 'error'){
					$status = $referral_status['error'];
					$update_contact = true;
				}
				
				if($update_contact){

					$contact_obj = CRMEntity::getInstance("Contacts");

					$contact_obj->id = $contactid;
					
					$contact_obj->retrieve_entity_info($contactid, "Contacts");
					
					$contact_obj->column_fields['referral_status'] = $status;
			
					$contact_obj->mode = "edit";
					
					$contact_obj->save("Contacts");
				}
			}
		}
	}
}

