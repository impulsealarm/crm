<?php

class Payments_ManageAffiliate_Model extends Vtiger_Base_Model{
	
	public static function getInstance() {
		
		$db = PearDatabase::getInstance();
		$currentUser = vglobal('current_user');

		$modelClassName = Vtiger_Loader::getComponentClassName('Model', 'ManageAffiliate', "Payments");
		
		$instance = new $modelClassName();
		
		return $instance;
	}
	
	function GetAuthCookie(){
		
		$wordpress_admin_username = "developer";
		
		$wordpress_admin_password = "Pass4SQL!Manish@Goyal";
		
		$url = "https://impulsealarm.com/api/auth/generate_auth_cookie/?username=".$wordpress_admin_username."&password=".$wordpress_admin_password;
	
		$ch = curl_init();
			
		curl_setopt($ch, CURLOPT_URL, $url);
			
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
		curl_setopt($ch, CURLOPT_HTTPGET, true);
			
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
		$response = curl_exec($ch);
			
		curl_close($ch);
			
		$response = json_decode($response,true);
		
		return $response;
	}
	
	/** 
	 * 1. To Authenticate User
	 * @params username, password and seconds
	 * username and password fields are mendatory.
	 * Optional 'seconds'. If provided, generated cookie will be valid for that many seconds, otherwise default is for 14 days.
	 * return response with cookie.
	 * 2. Use that cookie To create user and Affiliate
	 * 
	 * return array of the affilate with id and info.
	 * */
	
	function CreateAffiliateUsingApi($data){
		
		if($data['email'] == '')
			return array("error" => "Email Cannot be Empty.");
			
		$response = $this->GetAuthCookie();
		
		if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
		} else {
			return $response;
		}
		
		if($cookie != '') {
			
			//$url = "https://impulsealarm.com/api/Auth/create_affiliate/";
			
			$url = "https://impulsealarm.com/api/Auth/create_affiliate/";
			
			$url .= "?cookie=$cookie";
		
			$ch = curl_init();
				
			curl_setopt($ch, CURLOPT_URL, $url);
				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				
			curl_setopt($ch, CURLOPT_POST, true);
        
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
				
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
         
			$response = curl_exec($ch);
				
			curl_close($ch);

			$response = json_decode($response, true);
        
			return $response;
		}
	}
		
	function ChangeAffiliateStatusUsingApi($affiliate_id, $status){

		$response = $this->GetAuthCookie();
		
		if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
		} else {
			return $response;
		}
		
		if($cookie != '') {
		
			$url = "https://impulsealarm.com/api/Auth/ChangeAffiliateStatus/";
			
			$url .= "?cookie=$cookie&affiliate_id=$affiliate_id&status=$status";
		
			$ch = curl_init();
				
			curl_setopt($ch, CURLOPT_URL, $url);
				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				
			curl_setopt($ch, CURLOPT_HTTPGET, true);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
				
			$response = curl_exec($ch);
				
			curl_close($ch);
			
			return json_decode($response, true);
		}
		
		return false;
	}
	
	
	/**
	 * Create referral for the given affiliate
	 * @param int $affiliate_id
	 * @param array $referral_info
	 * @return int $referral_id if successfully create referral otherwise false
	 */
	function CreateAffiliateReferralUsingAPI($affiliate_id,$referral_info){
		
		$response = $this->getAuthCookie();
		
		if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
		} else {
			return $response;
		}
		
		if($cookie != '') {
			
			$url = "https://impulsealarm.com/api/Auth/create_referral/";
		
			$url .= "?cookie=$cookie&affiliate_id=$affiliate_id";
		
			$ch = curl_init();
				
			curl_setopt($ch, CURLOPT_URL, $url);
				
			curl_setopt($ch, CURLOPT_POST, true);
	        
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $referral_info);
	          
			$response = curl_exec($ch);
				
			curl_close($ch);
				
			$response = json_decode($response, true);
		
			return $response;
		}
		return false;
	}
	
	public function ChangeReferralStatusUsingApi($referral_id, $referral_info){
		
		$response = $this->GetAuthCookie();
		
		if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
		} else {
			return $response;
		}
		
		if($cookie != '') {
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
			$url = "https://impulsealarm.com/api/Auth/ChangeReferralBasicAction/";
		
			$url .= "?cookie=$cookie&referral_id=$referral_id";
		
			$ch = curl_init();
				
			curl_setopt($ch, CURLOPT_URL, $url);
				
			curl_setopt($ch, CURLOPT_POST, true);
	        
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $referral_info);
	          
			$response = curl_exec($ch);
				
			curl_close($ch);
				
			return json_decode($response, true);
		}
		return false;
	}
	
	function checkAffiliateReferral($affiliate_id, $referral_id){
		
		$response = $this->GetAuthCookie();
		
		if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
		} else {
			return $response;
		}
		
		if($cookie != '') {
			
			$data = array("affiliate_id" => $affiliate_id, "referral_id" => $referral_id);

			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
			$url = "https://impulsealarm.com/api/Auth/CheckAffiliateReferral/";
		
			$url .= "?cookie=$cookie";
				
			$ch = curl_init();
				
			curl_setopt($ch, CURLOPT_URL, $url);
				
			curl_setopt($ch, CURLOPT_POST, true);
	        
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	          
			$response = curl_exec($ch);
				
			curl_close($ch);

			return json_decode($response, true);
		}
	}
	
	
	public function CreateGoogleShortenLink($username){
		
		$apikey = "AIzaSyDGyhkXpgeiGs5-6YttXqcgyrEChn1J5h8";

		$urls = array(
			"https://impulsealarm.com/n5v59anolqv/", 
			"https://impulsealarm.com/g3cwifp/", 
			"https://impulsealarm.com/y9kjyycipzw/"
		);
		
		$short_links = array();
		
		foreach($urls as $url){
			
			$url .= "ref/".$username;
			
			$data_string = '{ "longUrl": "'.$url.'" }';
			
			//$short_url_response = $this->createShortenerLink($data_string);
			
			$short_url_response = $this->createShortenerLink($url);
						
			$short_links[] = $short_url_response['data']['url'];
		}
		
		return $short_links;
	}
	
	public function createShortenerLink($url){

		$url = "https://api-ssl.bitly.com/v3/shorten?access_token=152018dda9d93eaf27c31eca8329311093df660e&longUrl=$url";
		
		$ch = curl_init();
	
		curl_setopt($ch, CURLOPT_URL, $url);
				
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$response = curl_exec($ch);
	
		return json_decode($response, true);
		
	}
	
	
	function updateReferralUsingAPI($referral_id, $referral_data){
		
		$response = $this->GetAuthCookie();
		
		if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
		} else {
			return $response;
		}
		
		if($cookie != '') {

			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
			$url = "https://impulsealarm.com/api/Auth/update_referral/";
		
			$url .= "?cookie=$cookie&referral_id=$referral_id";
			
			$ch = curl_init();
				
			curl_setopt($ch, CURLOPT_URL, $url);
				
			curl_setopt($ch, CURLOPT_POST, true);
	        
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $referral_data);
	          
			$response = curl_exec($ch);
				
			curl_close($ch);

			return json_decode($response, true);
		}
	}
	
	function SaveAffiliateOtherFieldsUsingAPI($affiliate_id, $affiliate_data){
	
		$response = $this->GetAuthCookie();
		
		if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
			
			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
		} else {
			return $response;
		}
		
		if($cookie != '') {

			$cookie = $response['cookie'];
			
			$cookie_name = $response['cookie_name'];
			
			$user_details = $response['user'];
			
			$url = "https://impulsealarm.com/api/Auth/SaveAffiliateOtherFields/";
		
			$url .= "?cookie=$cookie&affiliate_id=$affiliate_id";
			
			$ch = curl_init();
				
			curl_setopt($ch, CURLOPT_URL, $url);
				
			curl_setopt($ch, CURLOPT_POST, true);
	        
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $affiliate_data);
	          
			$response = curl_exec($ch);
				
			curl_close($ch);

			return json_decode($response, true);
		}
	}
	
	
	
	function RegenerateAffiliateReferralLinks($record, $affiliate_id, $username){
		
		$affiliate_referral_urls = $this->SaveRealtorReferralURLs($record, $username);
	
		if(!empty($affiliate_referral_urls)){
		
			$response = $this->GetAuthCookie();
		
			if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
				$cookie = $response['cookie'];
			} else {
				return $response;
			}
			
			if($cookie != '') {
				
				$affiliate_info = array("affiliate_id" => $affiliate_id, "affiliate_urls" => $affiliate_referral_urls);
	
				$field_string = $this->build_query($affiliate_info);
				
		
				$url = "https://impulsealarm.com/api/Auth/saveAffiliateReferralURLs/";
			
				$url .= "?cookie=$cookie";
				
				$ch = curl_init();
					
				curl_setopt($ch, CURLOPT_URL, $url);
				
				curl_setopt($ch, CURLOPT_POST, true);
		        
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					
				curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
				
				curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
		          
				$response = curl_exec($ch);
					
				curl_close($ch);
	
				return json_decode($response, true);
			}
		} else {
			return json_decode(array("status" =>"error", "error" => "Please save atleast one link in Vtiger Affiliate URLs Settings."));
		}
	}
	
	function SaveRealtorReferralURLs($record, $username){
		
		$adb = PearDatabase::getInstance();
		
		$vtiger_affiliate_model = Settings_Vtiger_VtigerAffiliate_Model::getInstance();
		
		$affiliate_links = $vtiger_affiliate_model->get("affiliate_links");
		
		$referral_urls = array();
			
		if(!empty($affiliate_links)){
			
			$adb->pquery("delete from vtiger_realtor_affiliate_urls where realtor_id = ?",array($record));
			
			foreach($affiliate_links as $affiliate_data){
					
				$affiliate_link = $affiliate_data['url'];
				
				$affiliate_link_name = $affiliate_data['name'];
				
				$affiliate_link .= "ref/".$username;
			
				$short_url_response = $this->createShortenerLink($affiliate_link);
					
				$referral_url = $short_url_response['data']['url'];
				
				if($referral_url){
					
					$referral_urls[$affiliate_link_name] = $referral_url;
					
					//$adb->pquery("insert into vtiger_realtor_affiliate_urls(realtor_id, referral_url) values (?,?)",array($record, $referral_url));
					
					$adb->pquery("insert into vtiger_realtor_affiliate_urls(realtor_id,referral_url_name, referral_url) values (?,?,?)",
					array($record, $affiliate_link_name, $referral_url));
				
				}
			}
		}
		return $referral_urls;
	}
	
	function build_query( $data ) {
		return $this->_http_build_query( $data, null, '&', '', false );
	}
	
	function _http_build_query( $data, $prefix = null, $sep = null, $key = '', $urlencode = true ) {
		$ret = array();
	
		foreach ( (array) $data as $k => $v ) {
			if ( $urlencode)
				$k = urlencode($k);
			if ( is_int($k) && $prefix != null )
				$k = $prefix.$k;
			if ( !empty($key) )
				$k = $key . '%5B' . $k . '%5D';
			if ( $v === null )
				continue;
			elseif ( $v === false )
				$v = '0';
	
			if ( is_array($v) || is_object($v) )
				array_push($ret,$this->_http_build_query($v, '', $sep, $k, $urlencode));
			elseif ( $urlencode )
				array_push($ret, $k.'='.urlencode($v));
			else
				array_push($ret, $k.'='.$v);
		}
	
		if ( null === $sep )
			$sep = ini_get('arg_separator.output');
	
		return implode($sep, $ret);
	}
}