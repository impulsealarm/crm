<?php

class Payments_ShowStripeAlertWindow_View extends Vtiger_Index_View{
		
	function checkPermission(Vtiger_Request $request) {
		return true;
	}
	
	function process(Vtiger_Request $request){
		
		$viewer = $this->getViewer($request);
		
		$realtor = $request->get("realtor");
		
		$module = $request->get('module');
		
		$viewer->assign('MODULE', $module);
		$viewer->assign("REALTOR", $realtor);
		
		$viewer->view('ShowStripeModelWindow.tpl', $module);
	}
}