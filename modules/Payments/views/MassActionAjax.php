<?php

require_once('modules/Twilio/twilio-php/Services/Twilio.php'); 

class Payments_MassActionAjax_View extends Vtiger_MassActionAjax_View {
	
	function __construct() {
		parent::__construct();
		$this->exposeMethod("validatePaymentApproveSMSCode");
		$this->exposeMethod("showSMSCodePopup");
		$this->exposeMethod("MassApprovePayments");
	}

	function process(Vtiger_Request $request) {
		$mode = $request->get('mode');
		if(!empty($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}
	
	function showSMSCodePopup(Vtiger_Request $request){
		
		$viewer = $this->getViewer($request);
		
		$module = $request->get('module');
		
		$view = $request->get('view');
		
		$mode = "validatePaymentApproveSMSCode";
			
		if(!$view)
			$view = "MassActionAjax";
	
		$cvId = $request->get('viewname');
		
		$selectedIds = $request->get('selected_ids');
		
		$excludedIds = $request->get('excluded_ids');
    
		$viewer->assign('MODULE', $module);
		$viewer->assign('MODE', $mode);
		$viewer->assign('VIEW', $view);
		
		$viewer->assign('SELECTED_IDS', $selectedIds);
		$viewer->assign('EXCLUDED_IDS', $excludedIds);
		$viewer->assign('VIEWNAME', $cvId);
		
		$viewer->assign('MASS_APPROVED_PAYMENTS', true);
			
		echo $viewer->view('ShowSMSCodePopup.tpl', $module, true);
	}
	
	function validatePaymentApproveSMSCode(Vtiger_Request $request){
		
		$result = array();
		
		$sms_code = $request->get("sms_code");
		
		if($sms_code){
			
			$actual_sms_code = $_SESSION['sms_code'];
			
			if($sms_code === $actual_sms_code){
				$_SESSION['sms_code_time'] = date("y-m-d H:i:s");
				$result = array("success" => true);
			}else{
				$result = array('success' => false,"message" => "Invalid SMSCode");
			}
		} else {
			$result = array('success' => false,"message" => "Invalid SMSCode");
		}
		
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
    }
	
	
	function MassApprovePayments(Vtiger_Request $request){
		
		global $adb;
		
		$result = array();

		$recordIds = $this->getRecordsListFromRequest($request);
			
		if(!empty($recordIds)){
			
			foreach($recordIds as $recordId) {
				
				$recordModel = Vtiger_Record_Model::getInstanceById($recordId);
				
				$payment_status = $recordModel->get("payment_status");
				
				if($payment_status == 'Processed') continue;
		
				$payment_res = $adb->pquery("select vtiger_payments.* from vtiger_payments inner join 
				vtiger_crmentity on vtiger_crmentity.crmid = vtiger_payments.paymentsid where 
				vtiger_payments.paymentsid = ? and vtiger_crmentity.deleted = 0",array($recordId));
				
				if($adb->num_rows($payment_res)){
					
					$payment_amount = $adb->query_result($payment_res,0,'amount');
					
					$managed_accountId = $this->getRealtorAccount($adb->query_result($payment_res,0,'realtor_id'));
				}
				
				if(!$managed_accountId) continue;
				
				$response = madeStripePayment($managed_accountId, $payment_amount);
				
				if(is_array($response) && isset($response['message'])){
					
					$result['result'][$recordId] = $response['message'];
				
				} else {
						
					if($response == 'paid'){

						$payment_obj = CRMEntity::getInstance("Payments");

						$payment_obj->id = $recordId;
						
						$payment_obj->retrieve_entity_info($recordId, "Payments");
						
						$payment_obj->column_fields['payment_status'] = "Processed";
						$payment_obj->column_fields['payment_date'] = date("Y-m-d");
				
						$payment_obj->mode = "edit";
						
						$payment_obj->save("Payments");
						
						$recordModel = Vtiger_Record_Model::getInstanceById($recordId, "Payments");
						
						$contactid = $recordModel->get("contactid");
		
						if($contactid){
							
							$contact_referral_info = $recordModel->getContactReferralInfo($contactid);
							
							if(!empty($contact_referral_info)) {
								$recordModel->updateContactReferralStatus($contactid, $contact_referral_info);
							}
						}
					}
				}
			}
			$result['success'] = true;
			$result['message'] = "Paid";
		}
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
    }
    
	function getRealtorAccount($realtor_id){
		
		global $adb;
		
		$realtor_res = $adb->pquery("select * from vtiger_realtor inner join vtiger_crmentity on crmid = realtorid
		where realtorid = ? and deleted = 0",array($realtor_id));
		
		if($adb->num_rows($realtor_res)){
			$managed_accountId = $adb->query_result($realtor_res,0,'stripe_account_id');
		}
		
		return $managed_accountId;
	}
    
}
	
use Stripe\Stripe;
use Stripe\Transfer;

require('include/Stripe/init.php');

function madeStripePayment($stripe_accountId, $payment_amount){
	
	global $Stripe_secretKey;

	Stripe::setApiKey($Stripe_secretKey);
	
	try{
			
		$transfer = Transfer::create(array(
				"amount" => intval($payment_amount * 100),
				"currency" => "usd",
				"destination" => $stripe_accountId,
				"description" => "Transfer for managed account"
		));
		
	} catch(\Stripe\Error\Card $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	} catch(\Stripe\Error\RateLimit $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\InvalidRequest $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\Authentication $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\ApiConnection $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (\Stripe\Error\Base $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}catch (Exception $e) {
		$body = $e->getJsonBody();
		$err  = $body['error'];
		$status = $err;
	}
	
	if(!empty($transfer))
		$statusInfo = $transfer->getLastResponse()->json;
	
	if($statusInfo['status'])
		$status = $statusInfo['status'];
	
	return $status;
}
