<?php

class PostCards_Record_Model extends Vtiger_Record_Model {
	
	/**
	 * Function to get the id of the record
	 * @return <Number> - Record Id
	 */
	public function getId() {
		return $this->get('postcardsid');
	}
	
	/**
	 * Function to set the id of the record
	 * @param <type> $value - id value
	 * @return <Object> - current instance
	 */
	public function setId($value) {
		return $this->set('postcardsid',$value);
	}
	
	/**
	 * Function to delete the email template
	 * @param type $recordIds
	 */
	public function delete(){
		$this->getModule()->deleteRecord($this);
	}
	
	/**
	 * Function to delete all the email templates
	 * @param type $recordIds
	 */
	public function deleteAllRecords(){
		$this->getModule()->deleteAllRecords();
	}
	
	/**
	 * Function to get template fields
	 * To get the fields from module, which has the email field
	 * @return <arrray> template fields
	 */
	public function getPostCardsAddressFields($record = ""){
		
		global $adb;
		
		$params = $address_lists = array();
		
		$cls_address_fields = array("closings_tks_street", "closings_tks_city", "closings_tks_state", "closings_tks_zip", "closings_tks_county");
		
		if($record){
			
			$sql = "select * from vtiger_closings where closingsid = ?";
			
			$params = array_push($params,$record);
		} else {
			$sql = "select vtiger_closings.* from vtiger_closings 
			inner join vtiger_crmentity on vtiger_crmentity.crmid = vtiger_closings.closingsid
			where vtiger_crmentity.deleted = 0";
		}
		
		$closings_result = $adb->pquery($sql, array($params));
		
		if($adb->num_rows($closings_result)){
			
			while($row = $adb->fetchByAssoc($closings_result)){			
				
				$id = $row["closingsid"];
				
				$option = array();
				
				foreach($row as $req_field => $field_value){
					
					if(!$field_value) continue;
					
					if(in_array($req_field, $cls_address_fields)){
						
						$option[$req_field] = $field_value;
					}
				}
				if(!empty($option))
					$address_lists[$id] = $this->ConvertIntoString($option);
			}
			if($record)
				return $address_lists[$record];
			else
				return $address_lists;
		}
	}
	
	function ConvertIntoString($address){
		
		$string = "";

		$counter = 1;
		
		foreach($address as $field_name => $field_value){
			
			if($string == "")
				$string .= $field_value;
			else
				$string .= " ".$field_value;
				
			if($counter > 0  && ($counter%2) == '1'){
				$string = trim($string);
				$string .= ",";
			}
			
			$counter++;
		}
		
		if($string){
			$string = rtrim($string,",");
		}
	
		return $string;
	}
	
	
	/**
	 * Function to get the Email Template Record
	 * @param type $record
	 * @return <EmailTemplate_Record_Model>
	 */
	
	public function getTemplateData($record){
		return $this->getModule()->getTemplateData($record);
	}
	
	/**
	 * Function to get the Detail View url for the record
	 * @return <String> - Record Detail View Url
	 */
	public function getDetailViewUrl() {
		$module = $this->getModule();
		return 'index.php?module='.$this->getModuleName().'&view='.$module->getDetailViewName().'&record='.$this->getId();
	}
	
	/**
	 * Function to get the instance of Custom View module, given custom view id
	 * @param <Integer> $cvId
	 * @return CustomView_Record_Model instance, if exists. Null otherwise
	 */
	public static function getInstanceById($recordId, $module=null) {
		$db = PearDatabase::getInstance();
		$sql = 'SELECT * FROM vtiger_postcards WHERE postcardsid = ?';
		$params = array($recordId);
		$result = $db->pquery($sql, $params);
		if($db->num_rows($result) > 0) {
			$row = $db->query_result_rowdata($result, 0);
			$recordModel = new self();
			return $recordModel->setData($row)->setModule('PostCards');
		}
		return null;
	}
	
}
