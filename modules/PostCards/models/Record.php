<?php

class PostCards_Record_Model extends Vtiger_Record_Model {
	
	/**
	 * Function to get the id of the record
	 * @return <Number> - Record Id
	 */
	public function getId() {
		return $this->get('postcardsid');
	}
	
	/**
	 * Function to set the id of the record
	 * @param <type> $value - id value
	 * @return <Object> - current instance
	 */
	public function setId($value) {
		return $this->set('postcardsid',$value);
	}
	
	/**
	 * Function to delete the email template
	 * @param type $recordIds
	 */
	public function delete(){
		$this->getModule()->deleteRecord($this);
	}
	
	/**
	 * Function to delete all the email templates
	 * @param type $recordIds
	 */
	public function deleteAllRecords(){
		$this->getModule()->deleteAllRecords();
	}
	
	/**
	 * Function to get the Detail View url for the record
	 * @return <String> - Record Detail View Url
	 */
	public function getDetailViewUrl() {
		$module = $this->getModule();
		return 'index.php?module='.$this->getModuleName().'&view='.$module->getDetailViewName().'&record='.$this->getId();
	}
	
	/**
	 * Function to get the instance of Custom View module, given custom view id
	 * @param <Integer> $cvId
	 * @return CustomView_Record_Model instance, if exists. Null otherwise
	 */
	public static function getInstanceById($recordId, $module=null) {
		$db = PearDatabase::getInstance();
		$sql = 'SELECT * FROM vtiger_postcards WHERE postcardsid = ?';
		$params = array($recordId);
		$result = $db->pquery($sql, $params);
		if($db->num_rows($result) > 0) {
			$row = $db->query_result_rowdata($result, 0);
			$recordModel = new self();
			return $recordModel->setData($row)->setModule('PostCards');
		}
		return null;
	}
	
	public function getPostCardsImage($fieldname){
		
		$record = $this->getId();
		
		$imageDetails = array();
			
		$imagePath = $this->get($fieldname);
		
		$imageName = explode("_",$imagePath);
		
		$imageName = $imageName[count($imageName)-1];
		
		if($imageName){
			$imageDetails = array(
				'path' => $imagePath,
				'name' => $imageName
			);
		}
		return $imageDetails;
	}
	
	public function getPostCardsFields(){
		return $this->getModule()->getAllModulePostCardsFields();
	}
	
	public function getAllPostCards(){
		
		global $adb;

		$postcards = array();
		
		$result = $adb->pquery("select * from vtiger_postcards where deleted = 0",array());
		
		if($adb->num_rows($result)){
			while($row = $adb->fetchByAssoc($result)){
				$postcards[$row['postcardsid']] = PostCards_Record_Model::getInstanceById($row['postcardsid']);
			}		
		}
		return $postcards;
	}
	
	public function getAddressFields(){
		
		return array("post_card_company" => "COMPANY", "post_card_address_line_1" => "ADDRESS LINE 1", 
		"post_card_address_line_2" => "ADDRESS LINE 2", "post_card_city" => "CITY", "post_card_state" => "STATE", 
		"post_card_zip" => "ZIP/POSTAL CODE", "post_card_country" => "COUNTRY");
	}
}
