<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class PostCards_Save_Action extends Vtiger_Save_Action {
	
	public function checkPermission(Vtiger_Request $request) {
		return true;
	}
	public function process(Vtiger_Request $request) {
		
		global $upload_badext;
		
		$adb = PearDataBase::getInstance();
		
		$moduleName = $request->getModule();
		
		$record = $request->get('record');
		
		$recordModel = new PostCards_Record_Model();
		
		$recordModel->setModule($moduleName);
		
		if(!$record){
			$record = $adb->getUniqueID("vtiger_postcards");
			$recordModel->set("mode", "create");
		}else{
			$recordModel = PostCards_Record_Model::getInstanceById($record);
			$recordModel->set("mode", "edit");
		}
		
		if(!empty($record)) {
			$recordModel->setId($record);
		}
		
		$old_front_image = $recordModel->get("front");
		
		$front_image_details = $_FILES['front'];
		
		if($front_image_details['name'] != '' && $front_image_details['size'] > 0){
		
			$save = true;
			
			$file_name = $front_image_details['name'];
		
			$binFile = sanitizeUploadFileName($file_name, $upload_badext);
			
			$filename = ltrim(basename(" " . $binFile));
			
			$filetype = $front_image_details['type'];
			
			$filesize = $front_image_details['size'];
			
			$filetmp_name = $front_image_details['tmp_name'];
	
			$upload_file_path = decideFilePath();
			
			$upload_file_path .=  $record . "_front_" . $binFile;
	
			if($old_front_image != '' && $upload_file_path == $old_front_image)
				$save = false;
				
			if($save)
				$upload_status = move_uploaded_file($filetmp_name, $upload_file_path);
			
			if($upload_status)
				$recordModel->set("front", $upload_file_path);		
		}

		if($old_front_image && $save)
			$this->deleteImage($old_front_image);
			
		$back_option = $request->get("choose_back");
		
		$old_back_image = $recordModel->get("back");
		
		if($back_option == 'file'){
			
			$back_file_details = $_FILES['back_file'];
			
			if($back_file_details['name'] != '' && $back_file_details['size'] > 0){

				$save_back = true;
				
				$file_name = $back_file_details['name'];
		
				$binFile = sanitizeUploadFileName($file_name, $upload_badext);
				
				$filename = ltrim(basename(" " . $binFile));
				
				$filetype = $back_file_details['type'];
				
				$filesize = $back_file_details['size'];
				
				$filetmp_name = $back_file_details['tmp_name'];
		
				$upload_file_path = decideFilePath();
				
				$upload_file_path .=  $record . "_back_" . $binFile;
		
				if($old_back_image != '' && $upload_file_path == $old_back_image)
					$save_back = false;
					
				if($save_back)
					$upload_status = move_uploaded_file($filetmp_name, $upload_file_path);
				
				if($upload_status)
					$recordModel->set("back", $upload_file_path);		
			}
		}else{
			$recordModel->set('back', $request->get('back'));
		}
		
		if($old_back_image && $save_back)
			$this->deleteImage($old_back_image);
		
		
		$recordModel->set("name", $request->get("name"));
		$recordModel->set("back_option", $back_option);
		$recordModel->set('description', $request->get('description'));
		$recordModel->set('size', $request->get('size'));
		
		$recordModel->save();
		
		$loadUrl = $recordModel->getDetailViewUrl();
		
		header("Location: $loadUrl");
	}  

	public function deleteImage($file_path){
		unlink($file_path);
	}
}