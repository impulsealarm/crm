<?php

Class PostCards_Edit_View extends Vtiger_Edit_View {
	
	public function checkPermission(Vtiger_Request $request) {
		return true;
	}

	public function process(Vtiger_Request $request) {
		
		$viewer = $this->getViewer ($request);
		
		$moduleName = $request->getModule();
		
		$record = $request->get('record');
		
		if(!empty($record)) {
				
			$recordModel = PostCards_Record_Model::getInstanceById($record);
			
			$viewer->assign('RECORD_ID', $record);
            $viewer->assign('MODE', 'edit');
            
            $postCardsImages['front'] = $recordModel->getPostCardsImage("front");
		
			$back_option = $recordModel->get("back_option");
			
			if($back_option == 'file'){
				$postCardsImages['back'] = $recordModel->getPostCardsImage("back");
			} else {
				$back = $recordModel->get("back");
				$viewer->assign('BACK', $back);
			}	
		
            $viewer->assign('BACK_OPTION', $back_option);
           	$viewer->assign("IMAGE_DETAILS", $postCardsImages);
		} else {
			$recordModel = new PostCards_Record_Model();
          	$viewer->assign('MODE', '');
			$recordModel->set('description','');
			$recordModel->set('front','');
			$recordModel->set('back','');
			$recordModel->set('back_option','');
		}
		
		$recordModel->setModule('PostCards');
        
		if(!$this->record){
            $this->record = $recordModel;
        }
				
		$allFiledsOptions = $this->record->getPostCardsFields();
		
		$viewer->assign('RECORD', $this->record);
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$viewer->assign('ALL_FIELDS', $allFiledsOptions);
		$viewer->view('EditView.tpl', $moduleName);
	}
	
}
