<?php

function Leads_rejectReferral($entityData){
	
	$adb = PearDatabase::getInstance();
	
	$moduleName = $entityData->getModuleName();
	
	$wsId = $entityData->getId();
	
	$parts = explode('x', $wsId);
	
	$entityId = $parts[1];
	
	$referral_id = $entityData->get("referral_id");
	
	$lead_status = $entityData->get("leadstatus");
	
	if($referral_id != '' && $lead_status == 'Lost Lead'){
		
		$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
            
		$referral_info = array("action" => "reject");
		
		$manageAffiliateModel->ChangeReferralStatusUsingApi($referral_id, $referral_info);
	}
}

?>