<?php
function CreateTrelloCard($entityData){
	
	
	$adb = PearDatabase::getInstance();
	
	$moduleName = $entityData->getModuleName();
	
	$wsId = $entityData->getId();
	
	$parts = explode('x', $wsId);
	
	$entityId = $parts[1];
	
	$assigned_userid = $entityData->get("assigned_user_id");
	
	$assigned_userid = explode("x", $assigned_userid);
	
	$assigned_userid = $assigned_userid['1'];
	
	$trelloAPI = Leads_TrelloApi_Model::getInstance();
	
	$board_id = $trelloAPI->getTrelloBoardId($assigned_userid);
	
	if($board_id){
		
		$boardLists = $trelloAPI->getBoardLists($board_id);
		
		if(!empty($boardLists)){
			
			$list_id = array_search("new", $boardLists);
			
			if($list_id){
				
				$userModel = Users_Record_Model::getInstanceById($assigned_userid,"Users");
				
				$salesRepEmail = $userModel->get("email1");
				
				$card_info = array();
				
				$card_name = $description = $realtor_name = "";
				
				$card_name = $entityData->get("firstname");
				
				if($card_name)
					$card_name .= " " . $entityData->get("lastname");
				else
					$card_name = $entityData->get("lastname");
					
				$lead_address = $entityData->get("lane"). " " . $entityData->get("city") . " " . 
					$entityData->get("state"). " ". $entityData->get("code");
					
				$realtorid = $entityData->get("realtorid");
				
				$realtorid = explode("x", $realtorid);
				
				$realtorid = $realtorid['1'];


				if($realtorid > 0){
					$realtorModel = Vtiger_Record_Model::getInstanceById($realtorid, "Realtor");

					$realtor_name = $realtorModel->get("firstname");
					
					if($realtor_name)
						$realtor_name .= " ". $realtorModel->get("lastname");
					else
						$realtor_name = $realtorModel->get("lastname");
						
					$primary_phone = $realtorModel->get("primary_phone");
					$primary_email = $realtorModel->get("primary_email");
					
				}
				
				$description .= "Sales Rep Information:";
				$description .= "\n";
				$description .= "---";
				$description .= "\n";
				$description .= "**Sales Rep:** $salesRepEmail "; 
				$description .= "\n";
				$description .= "**Feeder:**";   
				$description .= "\n";
				$description .= "Realtor Information:";
				$description .= "\n";
				$description .= "---";
				$description .= "\n";
				$description .= "**Name:** ".$realtor_name;  
				$description .= "\n";
				$description .= "**Phone:** ".$primary_phone; 
				$description .= "\n";
				$description .= "**Email:** ".$primary_email;  
				$description .= "\n";
				$description .= "Lead Information:";
				$description .= "\n";
				$description .= "---";
				$description .= "\n";
				$description .= "**Name:** $card_name"; 
				$description .= "\n";
				$description .= "**Phone:** ".$entityData->get('phone');   
				$description .= "\n";
				$description .= "**Email:** ".$entityData->get('email');  
				$description .= "\n";
				$description .= "**Callback:**  at";   //02/01/2016
				$description .= "\n";
				$description .= "Address Information:";
				$description .= "\n";
				$description .= "---";
				$description .= "\n";
				$description .= trim($lead_address);  
				$description .= "\n";
				$description .= "Notes:";
				$description .= "\n";
				$description .= "---";
				$description .= "\n";
				$description .= "**Created:** ";  //02/01/2016
				$description .= "\n";
				$description .= "**Realtor Gift Notes:** "; //Realtor wants to keep $500  
				$description .= "\n";
				$description .= "**Lead Notes:** ";	//No number was provided realtor mentions client responds better to email.

				$card_info['name'] = trim($card_name);
				
				$card_info['description'] = $description;
				
				$response = $trelloAPI->createTrelloCard($card_info,$list_id);
			}
		}
	}
}
