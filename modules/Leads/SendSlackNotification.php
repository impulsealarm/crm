<?php
function SendSlackNotification($entityData){
	
	
	$adb = PearDatabase::getInstance();
	
	$moduleName = $entityData->getModuleName();
	
	$wsId = $entityData->getId();
	
	$parts = explode('x', $wsId);
	
	$entityId = $parts[1];
	
	$message = '';
	
	$realtor_id = $entityData->get("realtorid");
	
	$realtor_id = explode("x", $realtor_id);
	
	$realtor_id = $realtor_id['1'];
	
	$realtor_result = $adb->pquery("select * from vtiger_realtor where realtorid = ?", array($realtor_id));
	
	if($adb->num_rows($realtor_result)){
		
		$message .= $adb->query_result($realtor_result, 0, 'firstname') . ' ' . $adb->query_result($realtor_result, 0, 'lastname');
		
		$message .= " has referred " .  $entityData->get("firstname") . ' ' . $entityData->get("lastname") . ' for '; 

	} else {
		$message .= " New Lead " .  $entityData->get("firstname") . ' ' . $entityData->get("lastname") . ' added for '; 
	}

	$assigned_userid = $entityData->get("assigned_user_id");
	
	$assigned_userid = explode("x", $assigned_userid);
	
	$assigned_userid = $assigned_userid['1'];
	
	$userModel = Users_Record_Model::getInstanceById($assigned_userid,"Users");
				
	$message .= $userModel->get("first_name") . ' ' . $userModel->get("last_name") . '.';
	
	$token = "xoxp-8998175120-8998299591-12708012006-112e0d39a8";

	$url = "https://slack.com/api/chat.postMessage";
	
	global $current_user;
	
	$post_params = array(
		"token" 	=> $token,
		"channel" 	=> "#sales",//C0L63T5DW
		"text"		=> $message,
		"as_user"	=> false,
		"username"  => 'Bridge'
	);
	
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_URL, $url);
	
	curl_setopt($ch, CURLOPT_POST, true);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
	  
	$response = curl_exec($ch);
		
	curl_close($ch);
	
}