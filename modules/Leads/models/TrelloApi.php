<?php 
class Leads_TrelloApi_Model extends Vtiger_Base_Model{
	
	public static function getInstance() {
		
		$db = PearDatabase::getInstance();
		
		$currentUser = vglobal('current_user');

		$modelClassName = Vtiger_Loader::getComponentClassName('Model', 'TrelloApi', "Leads");
		
		$instance = new $modelClassName();
		
		return $instance;
	}
	
	/**
	 * get the trello_board_id
	 * @param $user_id
	 * @return int trello_board_id
	 */
	public function getTrelloBoardId($user_id){
		
		$adb = PearDatabase::getInstance();
		
		$result = $adb->pquery('select trello_board_id from vtiger_users_trelloboardids where user_id = ?',array($user_id));

		if($adb->num_rows($result)){
			return $adb->query_result($result,"0","trello_board_id");
		}
		
		return false;
	}
	
	/**
	 * function will return all the board information
	 * Board Possible values
	 * all, closed, members, open, organization, pinned, public, starred, unpinned
	 * members will return all the private boards belongs to my board
	 * return @array of boards detail 
	 */ 
	
	public function getAllTrelloBoards(){

		$boards_array = array();
		
		$token = "6ad3866da07b7a3e5ad59b0bbc100c687503b5c8a03708b451beb39c58185515";
		
		$key = "1775d565c3498e2a609a2e768a678237";
		
		$url = "https://api.trello.com/1/members/me?boards=members&board_fields=name&token=$token&key=$key";

		$board_response = $this->CallRequest($url);
	
		$boards = $board_response['boards'];
		
		if(!empty($boards)){
			
			foreach($boards as $board){
				$boards_array[$board['id']] = $board['name'];
			}
		}
		return $boards_array;
	}
	
	/**
	 * lists possible values are all, closed, none, open
	 * deafult none
	 * @param int $board_id
	 * @return array of board list
	 */
	public function getBoardLists($board_id){
		
		$token = "6ad3866da07b7a3e5ad59b0bbc100c687503b5c8a03708b451beb39c58185515";
		
		$key = "1775d565c3498e2a609a2e768a678237";
		
		$url = "https://api.trello.com/1/boards/$board_id?lists=all&list_fields=name&fields=name,desc&key=$key&token=$token";
		
		$list_array = array();
		
		$board_lists = $this->CallRequest($url);
	
		if(!empty($board_lists) && isset($board_lists['lists']) && !empty($board_lists['lists'])){
			
			$lists = $board_lists['lists'];
			
			foreach($lists as $list){
				
				$listname = $list['name'];
				
				$list_array[$list['id']] = strtolower($listname);	
			}
		}
		return $list_array;
	}
	
	public function createTrelloCard($card_details, $list_id){
		
		$url = "https://api.trello.com/1/cards";
			
		$post_params = array(
			"key"	=> "1775d565c3498e2a609a2e768a678237",
			"token" => "6ad3866da07b7a3e5ad59b0bbc100c687503b5c8a03708b451beb39c58185515",
			"name" 	=> $card_details['name'],
			"desc"	=> $card_details['description'],
			"idList"=> $list_id
		);
		
		$response = $this->CallRequest($url, $post_params);
		
		return $response;
	}
	
	public function CallRequest($url, $post_params = array()){
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		
		if(!empty($post_params))
			curl_setopt($ch, CURLOPT_POST, true);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	
		if(!empty($post_params))
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
		  
		$response = curl_exec($ch);
		
		curl_close($ch);
		
		return json_decode($response,true);
	}
}
	