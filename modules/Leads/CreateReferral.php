<?php

function Leads_createAffiliateReferral($entityData){
	
	$adb = PearDatabase::getInstance();
	
	$moduleName = $entityData->getModuleName();
	
	$wsId = $entityData->getId();
	
	$parts = explode('x', $wsId);
	
	$entityId = $parts[1];

	$email = $entityData->get('email');
	
	$realtorid = $entityData->get("realtorid");
	
	if($realtorid){
		
		$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
        
		$lead_obj = CRMEntity::getInstance("Leads");

		$lead_obj->id = $entityId;
		
		$lead_obj->retrieve_entity_info($entityId, "Leads");
		
		$realtorid = explode("x", $realtorid);
			
		$realtorid = $realtorid['1'];
			
		$affiliate_id = getReatorAffiliate($realtorid);
						
		$refferal = $lead_obj->column_fields["referral_id"];
		
		$referral_data = array();
		
		if($affiliate_id && !$refferal){
			
			$referral_data['description'] = $entityData->get('description');
			
			$referral_data['reference'] = $entityData->get("email");
			
			$referral_data['context'] = $entityData->get('lead_no');
			
			$response = $manageAffiliateModel->CreateAffiliateReferralUsingAPI($affiliate_id, $referral_data);
            	
			if(!empty($response) && isset($response['status']) && $response['status'] == 'ok' && isset($response['referral_id']) && $response['referral_id'] != ''){
					
            	$referral_id = $response['referral_id'];

				$lead_obj->column_fields['referral_id'] = $referral_id;
		
				$lead_obj->mode = "edit";
				
				$lead_obj->save("Leads");
				
				updateReferralOtherFields($entityId);
			}
		} else if($affiliate_id > 0 && $refferal > 0){
			
			//Allow to Change the Context And Description
			updateReferralOtherFields($entityId, true);
		}
	}
}

/**
 * function Firstly check for realtor Affiliated. if yes then update affiliate info in wordpress 
 * otherwise create affiliate for the realtor and update affiliate info in wordpress
 * @param Int $realtorid
 * @return Int affiliate_id
 */
function getReatorAffiliate($realtorid){
	
	$realtor_obj = CRMEntity::getInstance("Realtor");

	$realtor_obj->id = $realtorid;
	
	$realtor_obj->retrieve_entity_info($realtorid, "Realtor");
	
	$affiliate_id = $realtor_obj->column_fields["affiliate_id"];

	$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
        
	if(!$affiliate_id){
	
		$email = $realtor_obj->column_fields["primary_email"];

	    $firstname = $realtor_obj->column_fields["firstname"];
	
        $lastname = $realtor_obj->column_fields["lastname"];
        
        $affiliate_info = array("email" => $email, "firstname" => $firstname, "lastname" => $lastname);
        
        $result = $manageAffiliateModel->CreateAffiliateUsingApi($affiliate_info);

        if(!empty($result) && isset($result['affiliate_id']) && $result['affiliate_id'] != ''){
        
			$shorten_links = $result['bonus_urls'];
			
			if(!empty($shorten_links)){
				
				foreach($shorten_links as $index => $short_link){
					
					$realtor_obj->column_fields[$index] = $short_link;
				}
			}
			
        	$affiliate_id = $result['affiliate_id'];
        	
        	$status = $result['info']['status'];
        	
        	if($status == 'active') 
        		$status = "activate";
        	if($status == 'inactive')
        		$status = "deactivate";
			
			$realtor_obj->column_fields['affiliate_status'] = $status;
			$realtor_obj->column_fields['affiliate_id'] = $result['affiliate_id'];
			$realtor_obj->mode = "edit";
			$realtor_obj->save("Realtor");
		}
    }
	
	return $affiliate_id;
}


function updateReferralOtherFields($lead_id, $change_basic_referral_fields = false){

	$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
        
	$lead_obj = CRMEntity::getInstance("Leads");

	$lead_obj->id = $lead_id;
	
	$lead_obj->retrieve_entity_info($lead_id, "Leads");
	
	$package = $referral_name = "";
	
	$referral_data = array();
	
	$refferal = $lead_obj->column_fields['referral_id'];
	 
	$referral_name = $lead_obj->column_fields['firstname'];
	
	if($referral_name)
		$referral_name .= " ".$lead_obj->column_fields['lastname'];
	else
		$referral_name = $lead_obj->column_fields['lastname'];
	
	$package = $lead_obj->column_fields['source_url'];
	
	if(strpos($package, "y9kjyycipzw") !== false)
		$package = "$500 Bonus";
	else if(strpos($package, "g3cwifp") !== false)
		$package = "$250 Bonus";
	else if(strpos($package, "n5v59anolqv") !== false)
		$package = "Gifted Bonus";
	
		
	//Set Amount 0
	$referral_data['set_zero_amount'] = true;
	
	$referral_data['lead_status'] = $lead_obj->column_fields['leadstatus'];
	
	$referral_data['call_dispo'] = $lead_obj->column_fields['cf_785'];

	$referral_data['referral_name'] = $referral_name;
	
	$referral_data['referral_phone'] = $lead_obj->column_fields['phone'];
	
	$referral_data['referral_email'] = $lead_obj->column_fields['email'];
	
	$referral_data['referral_city'] = $lead_obj->column_fields['city'];
	
	$referral_data['referral_package'] = $package;
	
	if($change_basic_referral_fields) {
		
		if($lead_obj->column_fields['description'])
			$referral_data['description'] = $lead_obj->column_fields['description'];
		
		$referral_data['context'] = $lead_obj->column_fields['lead_no'];
	}
	
	$response = $manageAffiliateModel->updateReferralUsingAPI($refferal, $referral_data);
}