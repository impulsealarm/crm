<?php

class Twilio_MassActionAjax_View extends Vtiger_MassActionAjax_View {
	
	function __construct() {
		parent::__construct();
		$this->exposeMethod('showTwilioCallingForm');
	}

	function process(Vtiger_Request $request) {
		$mode = $request->get('mode');
		if(!empty($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}

		
	/**
	 * Function shows form that will lets you send SMS
	 * @param Vtiger_Request $request
	 */
	function showTwilioCallingForm(Vtiger_Request $request) {
		
		$user = Users_Record_Model::getCurrentUserModel();
		
		$sourceModule = $request->get('sourceModule');
		$moduleName = $request->getModule();
		
		$serverModel = PBXManager_Server_Model::getInstance ();
		
		$appSid = $serverModel->get("vtigersecretkey");

		if( isset($_SESSION[$user->user_name.'token']) ) {
			$token = $_SESSION[$user->user_name.'token'];
		}
		
		if ((!$appSid) || (!$token))
			throw new Exception ( "Twilio configuration not set" );
		
		
		$viewer = $this->getViewer($request);
		
		$viewer->assign("CALLERID", $user->phone_crm_extension);
		
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('SOURCE_MODULE', $sourceModule);
		$viewer->assign('SOURCE_RECORD', $request->get('sourceRecord'));
		$viewer->assign('USER_MODEL', $user);
		$viewer->assign('TWILIO_TOKEN',$token);
		$viewer->assign('TONUMBER',$request->get('number'));
		
		echo $viewer->view('TwilioOutgoing.tpl', $moduleName, true);
	}
}