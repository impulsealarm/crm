<?php
class Twilio_GetToken_Action extends Vtiger_Action_Controller {

    function checkPermission(Vtiger_Request $request) {
    	return;
    }

    public function process(Vtiger_Request $request) {

    	$user = Users_Record_Model::getCurrentUserModel();
		
    	$token = '';
    	
    	if( 
			$request->get('mode') == 'generate_token' || 
			!isset($_SESSION[$user->user_name.'token']) 
		){

			if( $user->phone_crm_extension != '' && $user->phone_crm_extension != null ){

				require_once 'modules/Twilio/Services/Twilio/Capability.php';
			
				$serverModel = PBXManager_Server_Model::getInstance();
				
				$gateway = $serverModel->get ( "gateway" );
			
				$accountSid = $serverModel->get ( "webappurl" );
			
				$authToken = $serverModel->get ( "outboundcontext" );
			
				$appSid = $serverModel->get("vtigersecretkey");
			
				$capability = new Services_Twilio_Capability($accountSid, $authToken);
				
				$capability->allowClientOutgoing($appSid);
				
				$capability->allowClientIncoming($user->user_name);
				
				$token = $capability->generateToken();
			
			    $_SESSION[$user->user_name.'token'] = $token;
			    
			    /* TO DO: make use of twilio-php
			     * 
			     * require_once ('modules/Twilio/twilio-php/Services/Twilio/Capability.php');
				
		$appSid = $serverModel->get("vtigersecretkey");
		$capability = new Services_Twilio_Capability($accountSid, $token);
		$capability->allowClientOutgoing($appSid);
					
		
		// Instantiate a new Twilio Rest Client
		
		$client = new Services_Twilio($accountSid, $token, $api_version, $http);
	 
			     */
				
			} else {
				$_SESSION[$user->user_name.'token'] = '';
			}

			
		} else {

			$token = $_SESSION[$user->user_name.'token'];
		
		}
		
		$response = new Vtiger_Response();
		
		$response->setResult($token);
		
		return $response;        
    }
}
?>