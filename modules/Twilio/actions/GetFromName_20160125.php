<?php

class Twilio_GetFromName_Action extends Vtiger_Action_Controller {
	
	function checkPermission(Vtiger_Request $request) {
    	return;
    }

	function process(Vtiger_Request $request){
		
		global $adb;
		
		$adb = PearDatabase::getInstance();
		
		$from_number = $request->get('from_number');
		$fromNumber = preg_replace ( '/\D+/', '', trim($from_number));
	
		$fromName = "N/A";
		
		$return = array();
				
		$result = $adb->pquery("select crmid from vtiger_crm_phonenumbers 
		where phone = ? or phone like ? ", array($fromNumber, "%$fromNumber"));
			
		if($adb->num_rows($result) > 0){
		
			$crmid = $adb->query_result($result,0,"crmid");
				
			$modulename = getSalesEntityType($crmid);
			
			if ($crmid != '' && $modulename != '') {
				$fromName = getEntityName($modulename, array($crmid));
				$fromName = '<a href="index.php?module='.$modulename.'&view=Detail&record='.$crmid.'">'.$fromName[$crmid].' ('.$from_number.') </a>';
            }
		} else {
			$fromName .= " (".$from_number.") ";
		}
		
		$return['from_name'] = $fromName;
			
		$response = new Vtiger_Response();
		
		$response->setResult($return);
		
		$response->emit();
		
	}
}