<?php

/*
$fh = fopen("call.txt", "a");
fwrite($fh, "\nIncoming Call file \n");
fwrite($fh, json_encode($_REQUEST));
fwrite($fh, "\n\n");
fclose($fh);
*/

ini_set ( "include_path", "../../" );

require_once ('includes/main/WebUI.php');

if( !empty($_REQUEST)  && isset($_REQUEST['DialCallStatus']) && $_REQUEST['DialCallStatus'] == 'no-answer' ){
  
    global $adb;
    $adb = PearDatabase::getInstance();


   /* --- START: Notification Changes 10-2015 ---- */
		
	if( isset($_REQUEST['From']) && $_REQUEST['From'] != '' && isset($_REQUEST['sendto']) ){
		
		$toNumber = $_REQUEST['sendto'];
    	$toNumber = preg_replace ( '/\D+/', '', $toNumber);
	
    	$callToResult = $adb->pquery( "SELECT id FROM vtiger_users 
    	WHERE phone_crm_extension LIKE '%". trim($toNumber) . "' OR phone_crm_extension LIKE '%".trim($toNumber)."'", array() );

   		if( $adb->num_rows($callToResult) ) {   			
   			$callToUserId = $adb->query_result($callToResult, 0, 'id');
   		}
   
		if( isset($callToUserId) && $callToUserId != '' ){
			
			$callMissedTime = new DateTime("".date('Y-m-d H:i:s'));
	 		$callMissedTime->setTimeZone(new DateTimeZone('UTC'));
	 		$callMissedTime = $callMissedTime->format('Y-m-d H:i:s');
		
			$callFrom = $_REQUEST['From'];
		
			$fromNo = '';
			$fromName = '';
		
		
			if( strpos(strtolower($callFrom),'client') !== FALSE){		/*client:admin*/
			
				$fromName = $callFrom;	
				$parts = explode(':',$callFrom);
				
		    	if( isset($parts[1]) ){
	    			$userName = $parts[1];	
	    			$userResult = $adb->pquery("SELECT CONCAT(first_name,' ', last_name) as name, phone_crm_extension  
	    			 FROM vtiger_users WHERE user_name = ?", array( $userName ));
					if($adb->num_rows($userResult)){
						$fromName = 'Client: '.$adb->query_result($userResult, 0, 'name');
						$fromNo = $adb->query_result($userResult, 0, 'phone_crm_extension');
					}
		    	}
		    	
			} else {		/*any number*/
				
				$fromNo = preg_replace ( '/\D+/', '', $callFrom);
				$lookUpNumber = substr($fromNo, -10);
				
				$lookUpQuery = "SELECT * FROM vtiger_crm_phonenumbers inner join 
				vtiger_crmentity on vtiger_crmentity.crmid = vtiger_crm_phonenumbers.crmid  
				WHERE deleted = 0 and (phone = '" . $lookUpNumber . "' OR phone like '%" . $lookUpNumber . "')";
				
				$lookUpResult = $adb->pquery($lookUpQuery, array());
				
				$numRows = $adb->num_rows($lookUpResult);
				$crmid = '';
				
				if($numRows){

					if($numRows == 1){		

						$crmid = $adb->query_result($lookUpResult, 0, 'crmid');					
					
					} else {
						
						/*
						for($i=0; $i<$numRows; $i++){
							$crmid = $adb->query_result($lookUpResult, $i, 'crmid');
							if(getSalesEntityType($crmid) == 'Contacts'){
								break;
							}
						}*/
						
						$contactid = '';
						$realtorid = '';
						$crm1id = '';
						
						for( $i=0; $i<$numRows; $i++ ){
							
							if( $adb->query_result($lookUpResult, $i, "crmid") == '' ) continue;
							
							$crmModuleName = getSalesEntityType( $adb->query_result($lookUpResult, $i, "crmid") );
							
							if( $crmModuleName == 'Realtor' ){
								$realtorid = $adb->query_result($lookUpResult, $i, "crmid");
							} else if( $crmModuleName == 'Contacts' ){
								$contactid = $adb->query_result($lookUpResult, $i, "crmid");
							} else {
								$crm1id = $adb->query_result($lookUpResult, $i, "crmid");
							}
						}
				
						if( $contactid != '' ){
							$crmid = $contactid;
						} else {
							if( $realtorid != '' ){
								$crmid = $realtorid;
							} else {
								$crmid = $crm1id;
							}
						}
													
					}
				}
	
				if($crmid != ''){
					$fromName = Vtiger_Functions::getCRMRecordLabel($crmid);
					$fromName = '<a href="index.php?module='.getSalesEntityType($crmid). 
					'&view=Detail&record=' . $crmid . '">' . $fromName . '</a>';				
				}
			}
		
			$notificationContent = "Missed a Call From: ";
		
			if($fromName != ''){
				$notificationContent .= $fromName;
			}
			
			if($fromNo != ''){
				$fromNo = preg_replace ( '/\D+/', '', $fromNo);			
				$notificationContent .= ' ' . '<a onClick="Vtiger_Twilio_Js.registerTwilioCall(' . "'{$fromNo}',''" . ')">'.$fromNo.'</a>';			
			}
	          
			$adb->pquery("INSERT INTO vtiger_notifications(createdtime, parentid, parentmodule, linkurl, content, userid) 
			VALUES(?,?,?,?,?,?)", array($callMissedTime, '', 'PBXManager', '', $notificationContent, $callToUserId));
			
		}
	
	}

        
	/* --- END: Notification Changes 10-2015 ---- */


    
    $number = $_REQUEST['sendto'];

    $phoneNumber = preg_replace ( '/\D+/', '', trim($number));
	
    $sqlQuery = $adb->pquery("select email1,user_name from vtiger_users where 
    phone_crm_extension like '%". trim($phoneNumber) . "' or phone_crm_extension like '%".trim($number)."'", array() );

   $response = '';

   if( isset($sqlQuery) && $adb->num_rows($sqlQuery) ) {

      $email1 = $adb->query_result( $sqlQuery, 0, 'email1' );
      $userEmail = urldecode( $email1 );
	
      header ("Content-type: text/xml");
   
      $response .= '<Response>';
      $response .= '<Say>Hello. Person is not available at this time. Please leave a voicemail after the beep, and remember to speak clearly.</Say>';
      $response .= '<Record transcribe="true" transcribeCallback="transcribed.php?email=' . $userEmail . '" action="goodbye.php" maxLength="30" />';
      $response .= '</Response>';
   
  } else {
      
      header ("Content-type: text/xml");
   
      $response .= '<Response>';
      $response .= '<Say voice="woman">Person is not available at this time.</Say>';
      $response .= '</Response>';
   }

   echo $response;

}else if( !empty($_REQUEST)  && isset($_REQUEST['DialCallStatus']) && $_REQUEST['DialCallStatus'] == 'busy' ){

      header ("Content-type: text/xml");
      $response .= '<Response>';
      $response .= '<Say voice="woman">Agent is Busy Please try again later.</Say>';
      $response .= '</Response>';
      echo $response;

}else if( !empty($_REQUEST)  && isset($_REQUEST['DialCallStatus']) && $_REQUEST['DialCallStatus'] == 'completed' ){

      /*if( isset($_REQUEST['RecordingUrl']) && $_REQUEST['RecordingUrl'] != '' ){

             global $adb;
             $adb = PearDatabase::getInstance();
   
             $recordUrl = $_REQUEST['RecordingUrl'];
             //$recordUrl .= '.mp3?Download=true';
 
             $adb->pquery("insert into vtiger_pbxmanager_recordings set sourceuuid = ?, recordingurl = ?, recording_sid = ?", 
             array( $_REQUEST['DialCallSid'], $recordUrl, $_REQUEST['RecordingSid']) );
            
      }*/

      header ("Content-type: text/xml");
      $response .= '<Response>';
      $response .= '<Say voice="woman">Thank You for calling.</Say>';
      $response .= '</Response>';
      echo $response;
}
?>
