<?php
ini_set ( "include_path", "../../" );

require_once ('includes/main/WebUI.php');

global $adb;

global $current_user;

/* --- START : Listen To Conference Silently --- */

if( 
	isset($_REQUEST['listen_conference']) && 
	$_REQUEST['listen_conference'] == 'true' 
){
	
	$conf_name = '';
	
	if( isset($_REQUEST['conf_name']) && $_REQUEST['conf_name'] != '' ){
		$conf_name = $_REQUEST['conf_name'];
	}
	
	if( $conf_name != '' ){
	
		/* -- START : save listener callsid in conferences_call table ---- */
		
		$conference_uniqueid = '';
	
		$conferenceResult = $adb->pquery("select * from vtiger_conferences where name = ?",array($conf_name));
	
		if($adb->num_rows($conferenceResult)){
			
			$conference_uniqueid = $adb->query_result($conferenceResult, 0, 'id');
		
		} else {
			
			$adb->pquery("insert into vtiger_conferences set name = ?",array($conf_name));
			
			$conferenceResult = $adb->pquery("select * from vtiger_conferences where name = ?",array($conf_name));
			
			if($adb->num_rows($conferenceResult)){
				$conference_uniqueid = $adb->query_result($conferenceResult, 0, 'id');
			}
			
		}
		
		if( isset($_REQUEST['CallSid']) && $_REQUEST['CallSid'] != '' ){
			$listener_callSid = $_REQUEST['CallSid'];			
			$adb->pquery("insert into vtiger_conference_calls set conference_id = ?, call_sid = ?, is_listener = '1'", 
	        array($conference_uniqueid, $listener_callSid));	
		}
		
		
		
		/* -- END : save listener callsid in conferences_call table ---- */
		
		
		$response = '<?xml version="1.0" encoding="UTF-8"?>';
		$response .= '<Response>
						<Dial>
							<Conference muted="true" beep="false">'.$conf_name.'</Conference>
						</Dial>
					 </Response>';
	
	} else { 
	
		$response = '<?xml version="1.0" encoding="UTF-8"?>';
		$response .= '<Response>
							<Say>This Call no longer exists. Thank You</Say>
					</Response>';
	
	}
	
	header('Content-type: text/xml');
	
	echo $response;
	
	exit;
}
/* --- END : Listen To Conference Silently --- */

/* -- START : create conference if user is in training mode and add other numbers --- */

if( isset($_REQUEST['training_mode']) && $_REQUEST['training_mode'] == '1' ){

	require_once ('modules/Twilio/twilio-php/Services/Twilio.php');

	$api_version = '2010-04-01';
	
	$serverModel = PBXManager_Server_Model::getInstance();
		
	$accountSid = $serverModel->get ( "webappurl" );	
	$token = $serverModel->get ( "outboundcontext" );
	
	$http = new Services_Twilio_TinyHttp(
		    'https://api.twilio.com',
		    array('curlopts' => array(
		        CURLOPT_SSL_VERIFYPEER => false,
		    ))
	);
	
	$client = new Services_Twilio($accountSid, $token, $api_version, $http);
	
	$cname = date('YmdHis');
	if( isset($_REQUEST['cname']) && $_REQUEST['cname'] != '' ){
		$cname = $_REQUEST['cname'];
	}
	
	$response = '<?xml version="1.0" encoding="UTF-8"?>';
	$response .= '<Response><Dial><Conference record="record-from-start" endConferenceOnExit="true" beep="false">'.$cname.'</Conference></Dial></Response>';
	header('Content-type: text/xml');
	echo $response;
	
	/* --------  START : create conference entry in vtiger_conferences  ---------- */
	
	$conference_uniqueid = '';
	$conferenceResult = $adb->pquery("select * from vtiger_conferences where name = ?",array($cname));
	if($adb->num_rows($conferenceResult)){
		$conference_uniqueid = $adb->query_result($conferenceResult, 0, 'id');
	} else {
		$adb->pquery("insert into vtiger_conferences set name = ?",array($cname));
		$conferenceResult = $adb->pquery("select * from vtiger_conferences where name = ?",array($cname));
		if($adb->num_rows($conferenceResult)){
			$conference_uniqueid = $adb->query_result($conferenceResult, 0, 'id');
		}
	}

	if( isset($_REQUEST['CallSid']) && $_REQUEST['CallSid'] != '' ){
		$initiater_callSid = $_REQUEST['CallSid'];
		$adb->pquery("insert into vtiger_conference_calls set conference_id = ?, call_sid = ?, is_initiator = '1'", 
	        array($conference_uniqueid, $initiater_callSid));	
	}
		
	
	if ( $_REQUEST['PhoneNumber'] != "" ) {	
		
		//add outgoing to current client
		
		require_once ('modules/Twilio/twilio-php/Services/Twilio/Capability.php');
				
		$appSid = $serverModel->get("vtigersecretkey");
		
		$capability = new Services_Twilio_Capability($accountSid, $token);
		
		$capability->allowClientOutgoing($appSid);
		
		$client = new Services_Twilio($accountSid, $token, $api_version, $http);
		
		if($_REQUEST['CallerID']){
			$callerId = "+".$_REQUEST['CallerID'];
		} else {
			$callerId = "+16193199330";
		}
	
		$from = $callerId;
		
		$number = preg_replace ( '/\D+/', '', trim($_REQUEST['PhoneNumber']));
	
		if( strlen($number) == 10 ){
			$number = "1" . $number;
		}
		
		$number = '+' . $number;
		
		$participants = array($number);
		
		$conference_url = 'http://crm.impulsealarm.com/modules/Twilio/conference_twiml.php?cname='.$cname;
		
		// Go through the participants array and call each person.
		
		foreach ( $participants as $participant ) {
			
			$vars = array(
				'From' => $from,
				'To' => $participant,
				'Url' => $conference_url);
			
			$call = $client->account->calls->create(
	            $from, 			// From a valid Twilio number
	            $participant, 	// Call this number
	            $conference_url	// Read TwiML at this URL
	        );
	      
			/* -- START : save callsid in conferences_call table ---- */
			
	        $participant_callSid = $call->sid;
			
	        $adb->pquery("insert into vtiger_conference_calls set conference_id = ?, call_sid = ?", 
	        array($conference_uniqueid, $participant_callSid));	
		}
	}
	exit;
}
/* -- END : create conference if user is in training mode and add other numbers --- */

// Only Outgoing Handling of Call
if ($_REQUEST['PhoneNumber'] != "") {
	
	global $adb;
	
	$adb = PearDatabase::getInstance ();
	
	// Remove all the Special Characters from Phone No
	
	$phoneNumber = preg_replace ( '/\D+/', '', trim($_REQUEST ['PhoneNumber']));
	
	$result = $adb->pquery ( 'select user_name from vtiger_users where phone_crm_extension like ? or 
		phone_crm_extension like ?', array ("%" . trim ( $phoneNumber ),"%" . trim ( $_REQUEST ['PhoneNumber'] ) 
	));
	
	// if Number Found	
	
	if ($adb->num_rows ( $result ) > 0) {
		if($_REQUEST['CallStatus'] == 'ringing'){
			$number = $adb->query_result ( $result, 0, 'user_name' );
			
			$incoming_to_number = ( isset($_REQUEST['To']) && $_REQUEST['To'] != '' ) ? $_REQUEST['To'] : $_REQUEST['PhoneNumber'];

			header ( "Content-type: text/xml; charset=utf-8" );			
			$response = '<?xml version="1.0" encoding="utf-8"?>';
			
			$response .= '<Response><Dial record="true" action="incomingcall.php?">';	
			$response .= "<Client>" . $number . "</Client>";
			$response .= '</Dial></Response>';
			echo $response;
        }
	
	} else {
		
		if($_REQUEST['CallerID']){
			$callerId = "+".$_REQUEST['CallerID'];
		} else {
			$callerId = "+19252022088";
		}

		$number = $_REQUEST ['PhoneNumber'];
		
        if(strlen($number) == 10){
			$number = "1" . $number;
		}

		echo getXmlNumberResponse($number, $callerId);
		
		exit;
	}
	
} else if($_REQUEST ['To'] != ""  && $_REQUEST['CallStatus'] == 'ringing'){

	global $adb;
	
	$adb = PearDatabase::getInstance ();
	
	$phoneNumber = preg_replace ( '/\D+/', '', trim($_REQUEST ['To']));
	
	$result = $adb->pquery ( 'select user_name from vtiger_users where phone_crm_extension like ? or 
		phone_crm_extension like ?', array ("%" . trim ( $phoneNumber ), "%" . trim ( $_REQUEST ['To'] ) 
	));
	
	// if Number Found
	
	if ($adb->num_rows ( $result ) > 0) {
		
		$number = $adb->query_result ( $result, 0, 'user_name' );
                
		header ( "Content-type: text/xml; charset=utf-8" );
		$response = '<?xml version="1.0" encoding="utf-8"?>';
		$response .= '<Response><Dial record="true" timeout="30" action="incomingcall.php?sendto=' . $phoneNumber . '"  method="GET">';
		$response .= "<Client >" . $number . "</Client>";
		$response .= '</Dial></Response>';
		echo $response;
		
	}

}

exit;

function getXmlNumberResponse($number, $callerId) {
	header ( "Content-type: text/xml; charset=utf-8" );
	$response = '<?xml version="1.0" encoding="utf-8"?>';
	
        //$response .= '<Response><Dial callerId="' . $callerId . '">';
        $response .= '<Response><Dial callerId="' . $callerId . '" record="true" action="incomingcall.php?">';
	
        $response .= "<Number>" . $number . "</Number>";
	$response .= '</Dial> </Response>';
	return $response;
}

?>