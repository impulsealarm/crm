var Vtiger_Twilio_Js = {

	conn : false,		//Current Connection 
	
	c_sid : false,	//Current CallSid of Ongoing Call

	incoming_c_sid : false,	//CallSid of incoming Call
	
	twilio_token : false,
	
	twilioContainer : false,
	
	callerId : false,
	
	// array of connections in form : [CallSid] => Conenction Object
	
	incoming_connections : [],	
	
	/**
	 * To get current Outgoing Call Popup Container 
	 */
	getContainer : function(){
		
		if(this.twilioContainer){
			return this.twilioContainer;
		}
		
		var container = $('div[id="twilioCallContainer"]');
		
		if(container.length){
			this.twilioContainer = container;
		}
		
		return this.twilioContainer;		
	
	},
	
	/**
	 *  Main Function to be called on Page Load
	 */
	
	registerEvents : function(){
		var thisInstance = this;		
		thisInstance.setTokenFromPhpSession();    
	},
	
	/**
     * Function to set Twilio Token From Session variable
     */
	
	setTokenFromPhpSession : function(){
		
		var thisInstance = this;
		
		var url = 'index.php?module=Twilio&action=GetToken';
		
        AppConnector.request(url).then(function(data){
            
        	if(data.result) {
            	
            	thisInstance.twilio_token = data.result;
            	
            	if( thisInstance.twilio_token != '' ){
            		
	            	Twilio.Device.setup(thisInstance.twilio_token, { debug: true, closeProtection : true });
	            	thisInstance.setUpDevice();
            	}
             }
        });		
	},
	
	/**
	 * Function to generate new Twilio token if previous token expires
	 */
	
	generateNewToken: function(){
		
		var thisInstance = this;
		
		var url = 'index.php?module=Twilio&action=GetToken&mode=generate_token';
		
		AppConnector.request(url).then(function(data){
			
            if(data.result) {
            	
            	thisInstance.twilio_token = data.result;
            	
            	if( thisInstance.twilio_token != '' ){
            		Twilio.Device.setup(thisInstance.twilio_token, { debug: true, closeProtection : true });
            		thisInstance.setUpDevice();
            	}
             }
            
        });
	
	},
	
	/**
	 * Function To Setup and Make Current Device Ready 
	 * For various Twilio Actions
	 */
	
	setUpDevice : function(){
		
		var thisInstance = this;
		
		Twilio.Device.ready(function (device) {
			console.log('Device Ready: start call');		
			$('#log').text('Device Ready: Start Call');			
		});

	 	Twilio.Device.error(function (error) {	
	 		
	 		console.log("Error: " + error.code + " - " + error.message);
	 		
	 		$("#log").text("Error: " + error.message);
	 		
	 		if(error.code == '31205' || error.code == '31204'){	
	 			$('#log').text('Getting new Token');
	 			thisInstance.generateNewToken();
	 		}
	 		
	 	});
	 
	 	Twilio.Device.connect(function (conn) {
	 		
	 		console.log("Call Connected");
	 		
	 		$("#log").text("Call Connected");
	 		thisInstance.c_sid = conn.parameters.CallSid;	 		
	    });
	  
	 	Twilio.Device.disconnect(function (conn) {
	    	
	 		console.log("Call Ended");	    	
	    
	 		$("#log").text("Call Ended");
	    	
	 	});
	 
	 	Twilio.Device.offline(function (device) {
	 		
	 		//ToDo: in case token expired call Device.setup() with new valid token
	 		console.log('Device is Offline');
		
	 		$('#log').text('Device is Offline');
	 		
	 	});
	 	
	 	Twilio.Device.presence(function(presenceEvent) {
	 		 console.log("Presence Event: " + presenceEvent.from + " " + presenceEvent.available);
	 	});
	 	
	 	Twilio.Device.incoming(function (conn) {	    
	 		
	 		console.log("Incoming Call From " + conn.parameters.From);
	 		
			thisInstance.incoming_c_sid = conn.parameters.CallSid;	 		
	 		
	 		thisInstance.incoming_connections[conn.parameters.CallSid] = conn;
	 		
	 		var calling_from = conn.parameters.From;
    		
	 		//thisInstance.showIncomingCallPopup(calling_from, conn.parameters.CallSid);
	 		
	 		
	 		/* Show From name In notification popup*/
	 		
	 		//if(calling_from.length > 10)
	 			calling_from = calling_from.replace("+","");
	 		
			var url = 'index.php?module=Twilio&action=GetFromName&from_number='+calling_from;
	        
			AppConnector.request(url).then(function(data){
				if(data.success && data.result) {
					thisInstance.showIncomingCallPopup(data.result.from_name, conn.parameters.CallSid);
			 	}
			});		
	 	});
	},
	
	/**
	 * Function to Show Incoming Call Notification 
	 * along with several buttons to handle Call
	 */
	
	showIncomingCallPopup : function(from_number, callsid) {
		
		var thisInstance = this;
		
		var params = {
            title: app.vtranslate('JS_PBX_INCOMING_CALL'),
            text: '<div class="row-fluid pbxcall" id="' + callsid + '" style="color:black">' + 
            		'<span class="span12" >' + 
            			'<input type="hidden" id="notifyid" value="' + callsid + '" /> ' +
            			app.vtranslate('JS_PBX_CALL_FROM') + ' : ' + from_number + 
            			'<br><br>' +
        				'<button class="btn btn-success" id="accept_' + callsid + '" data-callsid="' + callsid + '" >Accept</button>' +
        				'&nbsp;&nbsp;&nbsp;' + 
        				'<button class="btn btn-success" style="background-color:#D30D27;" id="reject_' + callsid + '" ' + 
        				'data-callsid="' + callsid + '" >Reject</button>' +
        				'&nbsp;&nbsp;' +
        				'<button class="btn btn-success hide" id="hangup_' + callsid + '" data-callsid="' + callsid + '" >HangUp</button>' +
        			'</span>' +
        		'</div>',
        		
            width: '30%',
            min_height: '75px',
            addclass:'vtCall',
            icon: 'vtCall-icon',
            hide:false,
            closer:true,
            type:'info',
        };
        
        var notify = Vtiger_Helper_Js.showPnotify(params);

		jQuery('#reject_' + callsid).bind('click', function(e) {
			var element = jQuery(e.currentTarget);
			var connec = thisInstance.incoming_connections[element.data('callsid')];
			
			connec.reject();
			connec.disconnect();
			
			notify.remove();
		});
		
		jQuery('#accept_' + callsid).bind('click', function(e) {
			var element = jQuery(e.currentTarget);
			var connec = thisInstance.incoming_connections[element.data('callsid')];
			
			connec.accept();
			
			var conn_status = connec.status();

		});
		
		jQuery('#hangup_' + callsid).bind('click', function(e) {
			var element = jQuery(e.currentTarget);
			var connec = thisInstance.incoming_connections[element.data('callsid')];
			
			console.log(connec.status());
			connec.disconnect();
			notify.remove();
		});
		
		jQuery('#hold_' + callsid).bind('click', function(e) {
			var element = jQuery(e.currentTarget);
			var connec = thisInstance.incoming_connections[element.data('callsid')];			
			connec.reject();
			alert("Call on Hold");
		});
		
		jQuery('#unhold_' + callsid).bind('click', function(e) {
			var element = jQuery(e.currentTarget);
			var connec = thisInstance.incoming_connections[element.data('callsid')];			
			connec.reject();
			alert("Call UnHold");
		});
		
		jQuery('#transfer_' + callsid).bind('click', function(e) {
			var element = jQuery(e.currentTarget);
			var connec = thisInstance.incoming_connections[element.data('callsid')];			
			connec.reject();
			alert("Call Transfer");
		});		
	},
	
	call : function(){
    	
		var thisInstance = this;
		
    	var container = thisInstance.getContainer();
    	
    	thisInstance.callerId = container.find("#caller_id").val();
    	
    	if( thisInstance.callerId == '' || thisInstance.callerId == false || thisInstance.twilio_token == '' ){ 
    	
    		thisInstance.showEmptyCallerIdPopup();
    	
    	} else {
	    	// get the phone number to connect the call to 
	    	
	    	params = {
	    			"PhoneNumber"    : container.find("#dial_phone_number").val(), 
	    			"CallerID"       : container.find("#caller_id").val(),
	    			"training_mode" : container.find("#training_mode").val(),
	    	};
	    	
	    	//console.log(params);
	    	
	    	var connection = Twilio.Device.connect(params);
	    	
	    	thisInstance.conn = connection;
    	}
    	
    },
    
    showEmptyCallerIdPopup : function(){
    	var params = {
                title: 'Phone CRM Extension',
                text: 'Invalid or No Phone CRM Extension Set. Go to `My Preferences` and ' + 
                	'check for `Phone CRM Extension` ',            		
                width: '30%',
                type:'info',
            };
            
            var notify = Vtiger_Helper_Js.showPnotify(params);
    },
    
    /**
     * Function to disconnect ongoing outgoing call and close the connection
     */
    
    hangup : function() {
    	
    	var thisInstance = this;
		
    	if( thisInstance.twilio_token == '' ){ 
    		
    		thisInstance.showEmptyCallerIdPopup();
    		
    	} else {
    		
	    	Twilio.Device.disconnectAll();
	
	    	thisInstance.conn = false;
	    	
	    	thisInstance.getContainer().find('#dial_phone_number').val('');
	    	$("body").toggleClass("show_sidebar3");
	    	$("#log").text("");
    	}
    },
    
    /**
     * Function to transfer the Ongoing Outgoing Call to some other number
     */
    
    transfer : function() {    	
    	alert("coming soon...");    	
    	this.conn.mute(true);    	
    },
    
    /**
     * Function to open outgoing call popup and register popup events
     */
    
    registerTwilioCall : function(number,record) {
    	
    	var thisInstance = this;
    	
    	if( thisInstance.twilio_token == '' ){     	
    		thisInstance.showEmptyCallerIdPopup();
    		return false;
    	}
    		
		var postData = {"number": number};
	
		//number = number.replace('/[^0-9]/','');
		
		thisInstance.getContainer().find('#dial_phone_number').val(number);
		
		if( !$( "body" ).hasClass( "show_sidebar3" )){
			$("body").toggleClass("show_sidebar3");
		}
		
		thisInstance.registerDialPadClickEvents();
    	
		
    },
    
    
    
    /**
     * Function to registers events of buttons in outgoing popup
     */
     
    registerDialPadClickEvents : function(){
    	
    	var thisInstance = this;
    	
    	//thisInstance.getContainer().find('#dial_phone_number').val('');
    	
    	thisInstance.getContainer().find('#dial_phone_number').focusin(function() {
    		  $( this ).removeAttr( "placeholder" );
    	});
    	
    	thisInstance.getContainer().find('#dial_phone_number').focusout(function() {
    	   //console.log($(this).val());
    	   if($(this).val() == ''){
    		   $(this).attr('placeholder','Enter Phone Number');
    	   }
    	});
    	  
    	$.each(['0','1','2','3','4','5','6','7','8','9','star','pound'], function(index, value) {
			
    		thisInstance.getContainer().find('#button' + value).unbind( "click" );
				
    		thisInstance.getContainer().find('#button' + value).on('click',function(){ 

	            if(thisInstance.conn) {
	                if (value=='star') {
	                    thisInstance.conn.sendDigits('*');
	                } else if (value=='pound') {
	                	thisInstance.conn.sendDigits('#');
	            	} else {
	                	thisInstance.conn.sendDigits(value);
	                }
	            } else {
	            	var currentVal = thisInstance.getContainer().find('#dial_phone_number').val();
	            	if (value=='star') {
	            		currentVal = currentVal + '*';
	            	} else if (value=='pound') {
	            		currentVal = currentVal + '#';
	                } else {
	                	currentVal = currentVal + value;
	                }
	            	thisInstance.getContainer().find('#dial_phone_number').val(currentVal);
	            }
	            return false;
	        });
	    });
    	
    	
    	thisInstance.getContainer().find('.del-img').on('click', function(){
    		thisInstance.getContainer().find('#dial_phone_number').val('');
    		thisInstance.getContainer().find('#dial_phone_number').focus();
    	});
    	
    },
    

    listenConference : function(conf_sid, conf_name, e){
    	
    	var thisInstance = this;
    	
    	if( thisInstance.twilio_token == '' ){     	
    		thisInstance.showEmptyCallerIdPopup();
    		return false;
    	}
		
    	if( !(thisInstance.conn ) ){
    		
    		var container = thisInstance.getContainer();
    		
    		params = {
	    			//"CallerID"       	: container.find("#caller_id").val(),
	    			"listen_conference" : "true",
	    			"conf_sid" 			: conf_sid,
	    			"conf_name"			: conf_name
	    	};
	
    		//console.log(params);
    		
	    	var connection = Twilio.Device.connect(params);
	    	
	    	thisInstance.conn = connection;
	    	
	    	jQuery(e).toggle();
	    	jQuery(e).parent().find('.listen_end').toggle();
	    	
	    	
    	} else {
    		console.log("Conference Already Going");
    	}
    	
    },
    
    endlistenConference : function(conf_sid, conf_name, e){
    	
    	Twilio.Device.disconnectAll();

    	var thisInstance = this;
    	
    	thisInstance.conn = false;
    	
    	jQuery(e).toggle();
    	jQuery(e).parent().find('.listen_call').toggle();    	
    	
    }
  
};

//On Page Load
jQuery(document).ready(function() {	
	
	
	Vtiger_Twilio_Js.registerEvents();
	
	$('div.fixed_menu').find(".nav_trigger2").click(function() {		
		$("body").toggleClass("show_sidebar3");
		Vtiger_Twilio_Js.registerDialPadClickEvents();
	});
	
	$(".closesidebar3").click(function() {		
		$("body").toggleClass("show_sidebar3");
	});
	
});