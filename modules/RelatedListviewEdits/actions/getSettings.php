<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
class RelatedListviewEdits_getSettings_Action extends Vtiger_Action_Controller {

    function __construct() {
        parent::__construct();
     
    }



    public function checkPermission() {
        return true;
    }

	function process (Vtiger_Request $request) {
        //Get Modules setting
        $view_type = $request->get('viewType', '');
        $relatedListviewEditsInstance = new RelatedListviewEdits_Module_Model();
        $setting = $relatedListviewEditsInstance->getSetting($view_type);

        $response = new Vtiger_Response();
        $response->setResult($setting);
        $response->emit();
	}
}