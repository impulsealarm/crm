<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */

class RelatedListviewEdits_SaveAjax_Action extends Vtiger_Save_Action {

	public function process(Vtiger_Request $request) {
        $pmodule = $request->get('pmodule');
        $request->set('module', $pmodule);
        
        //Before Saving/updatig the values get the already exist data.
        if ($request->get('field') === 'cf_785' && in_array($request->getModule(), array('Leads'))) {
        	$scoreChanged = false;
        	$focusLeadOld = CRMEntity::getInstance($request->getModule());
        	$focusLeadOld->retrieve_entity_info($request->get('record'), $request->getModule());
        	$callDispoOldValue = $focusLeadOld->column_fields['cf_785'];
//         	$leadCreatedDate = $focusLeadOld->column_fields['modifiedtime'];
        	//Lead Age calculation.
//         	global $adb;
//         	$date_var = date ( "Y-m-d H:i:s" );
//         	$currentDateTime = $adb->formatDate ( $date_var, true );
//         	$a = date_diff(date_create($leadCreatedDate), date_create($currentDateTime));
//         	$leadAgesIndays = $a->format("%a");
//         	//Calculate the lead score values according to the age.
//         	$leadAgesScore = $leadAgesIndays*5;
        	
        	if($callDispoOldValue != $request->get('value')){
        		$scoreChanged = true;
        	}
        }
        

		$recordModel = $this->saveRecord($request);

		$fieldModelList = $recordModel->getModule()->getFields();
		$result = array();
		foreach ($fieldModelList as $fieldName => $fieldModel) {
            $recordFieldValue = $recordModel->get($fieldName);
            if(is_array($recordFieldValue) && $fieldModel->getFieldDataType() == 'multipicklist') {
                $recordFieldValue = implode(' |##| ', $recordFieldValue);
            }
			$fieldValue = $displayValue = Vtiger_Util_Helper::toSafeHTML($recordFieldValue);
			if ($fieldModel->getFieldDataType() !== 'currency' && $fieldModel->getFieldDataType() !== 'datetime' && $fieldModel->getFieldDataType() !== 'date') { 
				$displayValue = $fieldModel->getDisplayValue($fieldValue, $recordModel->getId());
			}

            if($fieldModel->isNameField() || $fieldModel->get('uitype') == 4){
                $displayValue = '<a href="'.$recordModel->getDetailViewUrl().'">'.$fieldValue.'</a>';
            }

			$result[$fieldName] = array('value' => $fieldValue, 'display_value' => $displayValue);
		}

		//Handling salutation type
		if ($request->get('field') === 'firstname' && in_array($request->getModule(), array('Contacts', 'Leads'))) {
			$salutationType = $recordModel->getDisplayValue('salutationtype');
			$firstNameDetails = $result['firstname'];
			$firstNameDetails['display_value'] = $salutationType. " " .$firstNameDetails['display_value'];
			if ($salutationType != '--None--') $result['firstname'] = $firstNameDetails;
		}
		
		/**
		 * Steps to : Call Disposition Triggers & Related Actions
		 * 1. Values - according to the Call Dispo.
		 * 2. Task - Need to be add a new Task.
		 * 3. Send Emails.
		 * 4. Fields updates in Lead Module:
		 * 		1. Lead Status.
		 * 		2. Score.
		 * 		3. Email Opt Out = TRUE
		 * 		4. Call Opt Out = TRUE
		 * 		5. Next Action
		 * 		6. Convert Lead to Contact/Account
		 * 
		 */
		if ($request->get('field') === 'cf_785' && in_array($request->getModule(), array('Leads'))) {
			$callDisPoValues = $result['cf_785'];
			$focusLead = CRMEntity::getInstance($request->getModule());
			$focusLead->retrieve_entity_info($request->get('record'), $request->getModule());
			//Lead Score.
			if($focusLead->column_fields['cf_791'] == "") {
				$focusLead->column_fields['cf_791'] = 0;
			}

			switch ($callDisPoValues['value']) {
				case "Left VM1":
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "CB on VM1";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
 					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
 					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
 					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
 					$focus->column_fields['sendnotification'] = "on";
 					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
 					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
 					//Set reminder.
 					$_REQUEST['set_reminder'] = "Yes";
 					$_REQUEST['remdays'] = 0;
 					$_REQUEST['remhrs'] = 0;
 					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					//Lead status update.
					$focusLead->column_fields['leadstatus'] = "Attempted to Contact";
					$focusLead->column_fields['cf_789'] = "Call Back";
				break;
				
				case "DNC" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "Add to DNC";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = 1;//Assigned to admins user(Matt).
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $currentDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = $currentDateTimeArray[1];
					$focus->mode = "Add";
					$focus->save("Events");
					//Lead status update.
					$focusLead->column_fields['leadstatus'] = "Lost - To Canvassing Team";
					$focusLead->column_fields['emailoptout'] = "1";
					$focusLead->column_fields['cf_787'] = "1";
					$focusLead->column_fields['cf_791'] = 0;
					break;
				
				case "Hangup" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "Call Back";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
 					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
 					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
 					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
 					$focus->column_fields['sendnotification'] = "on";
 					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
 					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
 					//Set reminder.
 					$_REQUEST['set_reminder'] = "Yes";
 					$_REQUEST['remdays'] = 0;
 					$_REQUEST['remhrs'] = 0;
 					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					//Lead status update.
					$focusLead->column_fields['leadstatus'] = "Attempted to Contact";
					$focusLead->column_fields['cf_789'] = "Call Back";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] = $focusLead->column_fields['cf_791'] - 10;
					break;
				
				case "No Answer" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "Call Back";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					//Lead status update.
					$focusLead->column_fields['leadstatus'] = "Attempted to Contact";
					$focusLead->column_fields['cf_789'] = "Call Back";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] -= 6;
					break;
				
				case "Left VM2" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "CB on VM2";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "Attempted to Contact";
					$focusLead->column_fields['cf_789'] = "Call Back";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] -= 10;
					break;
				
				case "Left VM3" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
						
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "CB on VM3";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "Last Attempt";
					$focusLead->column_fields['cf_789'] = "Save or Close Out";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] -= 20;
					break;
				
				case "CB" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "CB at Request";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "In Contact";
					$focusLead->column_fields['cf_789'] = "Call Back";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] -= 10;
					break;
				
				case "CB Appt" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "Call Appointment";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $currentDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "R - Set Call Appt";
					$focusLead->column_fields['cf_789'] = "Call Back at time";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] += 30;
					break;
				
				case "Got Partner" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 64800);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "FU w/ new Partner";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "Won - Partner";
					$focusLead->column_fields['cf_789'] = "AmEx Setup";
					$focusLead->column_fields['cf_791'] = 100;
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					//Convert the lead.
					$convertLeadArray = array (
							'transferRelatedRecordsTo' => 'Contacts',
							'assignedTo' => $focusLead->column_fields['assigned_user_id'],
							'leadId' => '10x'.$request->get('record'),
							'entities' => array (
									'Accounts' => array (
											'create' => 1,
											'name' => 'Accounts',
											'accountname' => $focusLead->column_fields['company'], 
									),
									'Contacts' => array (
											'create' => 1,
											'name' => 'Contacts',
											'lastname' => $focusLead->column_fields['lastname'],
											'firstname' => $focusLead->column_fields['firstname'],
											'email' => $focusLead->column_fields['email'] 
									) 
							) 
					);
					$this->convertLead($convertLeadArray, $currentUserModel);
					break;
				
				case "Got Homeowner" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
					
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "FU Re Lead";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = $dueDateTimeArray[1];
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "Won – Homeowner Lead";
					$focusLead->column_fields['cf_791'] = 100;
					break;
					
				case "Wants Program Info" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
						
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "FU on RRP Info";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = "10:00:00";//every 10.00AM due date.
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "Pitching Realtor";
					$focusLead->column_fields['cf_789'] = "Call Back Soon";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] += 30;
					break;
					
				case "Wants System Info" :
					global $adb;
					$date_var = date ( "Y-m-d H:i:s" );
					$currentDateTime = $adb->formatDate ( $date_var, true );
					$currentDateTimeArray = explode(" ", $currentDateTime);
					
					$date_close = date ( "Y-m-d H:i:s", time() + 86400);
					$dueDateTime = $adb->formatDate ( $date_close, true );
					$dueDateTimeArray = explode(" ", $dueDateTime);
				
// 					$currentUserModel = Users_Record_Model::getCurrentUserModel();
					$focus = CRMEntity::getInstance("Events");
					$focus->column_fields['subject'] = "FU on ADC Info";
					$focus->column_fields['eventstatus'] = "Planned";
					$focus->column_fields['activitytype'] = "Call";
					$focus->column_fields['parent_id'] = $request->get('record');
					$focus->column_fields['assigned_user_id'] = $focusLead->column_fields['assigned_user_id'];
					$focus->column_fields['date_start'] = $currentDateTimeArray[0];
					$focus->column_fields['due_date'] = $dueDateTimeArray[0];
					$focus->column_fields['sendnotification'] = "on";
					$focus->column_fields['time_start'] = $currentDateTimeArray[1];
					$focus->column_fields['time_end'] = "10:00:00";//every 10.00AM due date.
					//Set reminder.
					$_REQUEST['set_reminder'] = "Yes";
					$_REQUEST['remdays'] = 0;
					$_REQUEST['remhrs'] = 0;
					$_REQUEST['remmin'] = 15;
					$focus->mode = "Add";
					$focus->save("Events");
					$focusLead->column_fields['leadstatus'] = "Pitching Realtor";
					$focusLead->column_fields['cf_789'] = "Call Back Soon";
					if($scoreChanged)
					$focusLead->column_fields['cf_791'] += 30;
					break;
			}
			//update leads fields.
			$focusLead->id = $request->get('record');
			$focusLead->mode = "edit";
			$focusLead->save($request->getModule());
		}

		$result['_recordLabel'] = $recordModel->getName();
		$result['_recordId'] = $recordModel->getId();

		$response = new Vtiger_Response();
		$response->setEmitType(Vtiger_Response::$EMIT_JSON);
		$response->setResult($result);
		$response->emit();
	}

	/**
	 * Function to get the record model based on the request parameters
	 * @param Vtiger_Request $request
	 * @return Vtiger_Record_Model or Module specific Record Model instance
	 */
	public function getRecordModelFromRequest(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$recordId = $request->get('record');

		if(!empty($recordId)) {
			$recordModel = Vtiger_Record_Model::getInstanceById($recordId, $moduleName);
			$recordModel->set('id', $recordId);
			$recordModel->set('mode', 'edit');

			$fieldModelList = $recordModel->getModule()->getFields();
			foreach ($fieldModelList as $fieldName => $fieldModel) {
                //For not converting craetedtime and modified time to user format
                $uiType = $fieldModel->get('uitype');
                if ($uiType == 70) {
                    $fieldValue = $recordModel->get($fieldName);
                } else {
                    $fieldValue = $fieldModel->getUITypeModel()->getUserRequestValue($recordModel->get($fieldName));
                }
				

				if ($fieldName === $request->get('field')) {
					$fieldValue = $request->get('value');
				}
                $fieldDataType = $fieldModel->getFieldDataType();
                if ($fieldDataType == 'time') {
					$fieldValue = Vtiger_Time_UIType::getTimeValueWithSeconds($fieldValue);
				}
				if ($fieldValue !== null) {
					if (!is_array($fieldValue)) {
						$fieldValue = trim($fieldValue);
					}
					$recordModel->set($fieldName, $fieldValue);
				}
				$recordModel->set($fieldName, $fieldValue);
			}
		} else {
			$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
			$recordModel->set('mode', '');

			$fieldModelList = $moduleModel->getFields();
			foreach ($fieldModelList as $fieldName => $fieldModel) {
				if ($request->has($fieldName)) {
					$fieldValue = $request->get($fieldName, null);
				} else {
					$fieldValue = $fieldModel->getDefaultFieldValue();
				}
				$fieldDataType = $fieldModel->getFieldDataType();
				if ($fieldDataType == 'time') {
					$fieldValue = Vtiger_Time_UIType::getTimeValueWithSeconds($fieldValue);
				}
				if ($fieldValue !== null) {
					if (!is_array($fieldValue)) {
						$fieldValue = trim($fieldValue);
					}
					$recordModel->set($fieldName, $fieldValue);
				}
			} 
		}

		return $recordModel;
	}
	
	private function convertLead($entityValues,$currentUser) {
		try {
			vimport('~~/include/Webservices/ConvertLead.php');
			$result = vtws_convertlead($entityValues, $currentUser);
		} catch(Exception $e) {
			print_r($e);
		}
	}
}
