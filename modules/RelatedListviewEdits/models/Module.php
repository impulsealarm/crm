<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
class RelatedListviewEdits_Module_Model extends Vtiger_Module_Model {

    var $user;
    var $db;

    function __construct() {
        global $current_user;

        $this->user = $current_user;
        $this->db = PearDatabase::getInstance();
    }

    public function getFieldsFromSetting($module) {
        $fields = array();

        $query = "SELECT * FROM vte_rle_module_settings WHERE `active` = 1 AND `modulename` = ? LIMIT 1";
        $result = $this->db->pquery($query, array($module));

        if($this->db->num_rows($result)){
            $moduleModel = Vtiger_Module_Model::getInstance($module);
            $columnFieldsMapping = $moduleModel->getColumnFieldMapping();

            $module_setting = $this->db->fetchByAssoc($result);

            if(!empty($module_setting['fields'])){
                $fields_setting = json_decode(html_entity_decode($module_setting['fields']));
                foreach($columnFieldsMapping as $fieldColumn=>$fieldName){
                    if(in_array($fieldName, $fields_setting)){
                        $fields[$fieldColumn] = $fieldName;
                    }
                }
            }
            if($module_setting['allow_edit_all_fields']==1 || empty($fields)){
                $moduleModel = Vtiger_Module_Model::getInstance($module);
                $fields = $moduleModel->getColumnFieldMapping();
            }
        }

        return $fields;
    }

    public function getSetting($view_type = '') {
        $settings = array();

        if(!in_array($view_type, array('List', 'Detail'))){
            return $settings;
        }

        $query = "SELECT * FROM vte_rle_module_settings WHERE `active` = 1";
        if($view_type=='List'){
            $query .= ' AND ( active_type LIKE "List" OR active_type LIKE "Both") ';
        }else{
            $query .= ' AND ( active_type LIKE "RelatedList" OR active_type LIKE "Both") ';
        }
        $result = $this->db->pquery($query, array());

        if($this->db->num_rows($result)){
            while($row = $this->db->fetchByAssoc($result)){
                $moduleName = $row['modulename'];
                $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
                $settings[$moduleName] = $row;

                $columnFieldsMapping = $moduleModel->getColumnFieldMapping();
                $fields = array();
                if(!empty($row['fields'])){
                    $fields_setting = json_decode(html_entity_decode($row['fields']));
                    foreach($columnFieldsMapping as $fieldColumn=>$fieldName){
                        if(in_array($fieldName, $fields_setting)){
                            $fields[$fieldColumn] = $fieldName;
                        }
                    }
                }
                if($row['allow_edit_all_fields']==1 || empty($fields)){
                    $fields = $columnFieldsMapping;
                }

                $settings[$moduleName]['fields'] = $fields;
                $settings[$moduleName]['all_fields'] = $columnFieldsMapping;
            }
        }

        return $settings;
    }

    function getDataSource($module){
        $query = $_SESSION[$module.'_listquery'];
        $count_replace = 1;
        $query = str_replace('SELECT', 'SELECT vtiger_crmentity.crmid, ', $query, $count_replace);

        $result = $this->db->pquery($query, array());
        $listSource = array();
        if($this->db->num_rows($result)){
            while($row = $this->db->fetchByAssoc($result)){
                $listSource[$row['crmid']] = $row;
            }
        }

        return $listSource;
    }

    /**
     * Function to get Settings links for admin user
     * @return Array
     */
    public function getSettingLinks() {
        $settingsLinks = parent::getSettingLinks();
        $currentUserModel = Users_Record_Model::getCurrentUserModel();

        if ($currentUserModel->isAdminUser()) {
            $settingsLinks[] = array(
                'linktype' => 'LISTVIEWSETTING',
                'linklabel' => 'Settings',
                'linkurl' =>'index.php?module=RelatedListviewEdits&view=Settings&parent=Settings',
                'linkicon' => ''
            );
        }
        return $settingsLinks;
    }
}