<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
class RelatedListviewEdits_FieldEdit_View extends Vtiger_Index_View {

	function process (Vtiger_Request $request) {
        $pModuleName = $request->get('pmodule');
        $recordId = $request->get('record');
        $fieldName = $request->get('fieldname');

        if(!$recordId){
            exit;
        }

        $recordModel = Vtiger_Record_Model::getInstanceById($recordId, $pModuleName);
        $fieldModel = Vtiger_Field_Model::getInstance($fieldName, Vtiger_Module::getInstance($pModuleName));
        $fieldModel->set('fieldvalue', $recordModel->get($fieldName));

        $viewer = $this->getViewer ($request);
		$viewer->assign('FIELD_MODEL', $fieldModel);
		$viewer->assign('MODULE_NAME', $pModuleName);
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$viewer->view('FieldEdit.tpl', 'RelatedListviewEdits');

        exit;
	}
}