<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
require_once('data/CRMEntity.php');
require_once('data/Tracker.php');
require_once 'vtlib/Vtiger/Module.php';

class RelatedListviewEdits extends CRMEntity {
	/**
	 * Invoked when special actions are performed on the module.
	 * @param String Module name
	 * @param String Event Type (module.postinstall, module.disabled, module.enabled, module.preuninstall)
	 */
	function vtlib_handler($modulename, $event_type) {
		if($event_type == 'module.postinstall') {
            self::addWidgetTo();
			self::addDefaultSetting();
		} else if($event_type == 'module.disabled') {
			// TODO Handle actions when this module is disabled.
            self::removeWidgetTo();
		} else if($event_type == 'module.enabled') {
			// TODO Handle actions when this module is enabled.
            self::addWidgetTo();
		} else if($event_type == 'module.preuninstall') {
			// TODO Handle actions when this module is about to be deleted.
		} else if($event_type == 'module.preupdate') {
			// TODO Handle actions before this module is updated.
		} else if($event_type == 'module.postupdate') {
            self::removeWidgetTo();
			self::addWidgetTo();
            self::upgradeModule();
		}
	}
    

    /**
	 * Add header script to other module.
	 * @return unknown_type
	 */
	static function addWidgetTo() {
        global $adb;
		$widgetType = 'HEADERSCRIPT';
		$widgetName = 'RelatedListviewEditsJs';
		$link = 'layouts/vlayout/modules/RelatedListviewEdits/resources/RelatedListviewEdits.js';
		
		$module = Vtiger_Module::getInstance('Accounts');
		$module->addLink($widgetType, $widgetName, $link);
		//Fix issue: module does not work with Non Admin user_error
		$adb->pquery("UPDATE vtiger_links SET tabid=0 WHERE linklabel = ? ", array($widgetName));
        
        //add module to settings list sidebar
        $max_id=$adb->getUniqueID('vtiger_settings_field');
        $adb->pquery("INSERT INTO `vtiger_settings_field` (`fieldid`, `blockid`, `name`, `description`, `linkto`, `sequence`) VALUES(?, ?, ?, ?, ?, ?)",
            array($max_id, '4', 'VTE Related & Listview Edits', 'VTE Related & Listview Edits Setting', 'index.php?module=RelatedListviewEdits&view=Settings&parent=Settings', '999'));
	}   
	
	static function removeWidgetTo() {
        global $adb;
		$widgetType = 'HEADERSCRIPT';
		$widgetName = 'RelatedListviewEditsJs';

        $adb->pquery('DELETE FROM vtiger_links WHERE linktype=? AND linklabel=?', array($widgetType, $widgetName));

        $adb->pquery("DELETE FROM vtiger_settings_field WHERE `name` LIKE ?", array('VTE Related & Listview Edits'));
	}
	
	static function addDefaultSetting(){
        global $adb;

        $ignore_modules = array('RelatedListviewEdits');
        $where_extra = " ";
        if(!empty($ignore_modules)){
            $where_extra = " AND vtiger_tab.name NOT IN('".implode("','", $ignore_modules)."') ";
        }
        $result = $adb->pquery('SELECT *
                                    FROM vtiger_tab
                                    WHERE vtiger_tab.isentitytype = 1 AND vtiger_tab.presence = 0
                                        AND vtiger_tab.parent IS NOT NULL
	                                    AND vtiger_tab.parent != ""
                                    '.$where_extra.'
                                    ORDER BY vtiger_tab.name', array());
        if($adb->num_rows($result)){
            while($row = $adb->fetchByAssoc($result)){
                $adb->pquery("INSERT INTO vte_rle_module_settings(`modulename`,`active`,`active_type`,`fields`,`allow_edit_all_fields`) VALUES (?, '1', 'Both', '', '1')", array($row['name']));
            }
        }
    }

    static function upgradeModule(){
        global $adb;
        //Upgrade table structure
        $adb->pquery("  ALTER TABLE `vte_rle_module_settings`
                        ADD COLUMN `active_type`  varchar(50) NULL AFTER `active`");
    }
}
?>
