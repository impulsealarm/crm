<?php

class GoogleAddress_ActionAjax_Action extends Vtiger_Action_Controller {
	
	function checkPermission(Vtiger_Request $request) {
		return true;
    }
    
	function __construct() {
		parent::__construct();
		$this->exposeMethod("getConfigFields");
	}

	function process(Vtiger_Request $request) {
		$mode = $request->get('mode');
		if(!empty($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}
	
	function getConfigFields(Vtiger_Request $request){
		
		global $adb;
		
		$source_module = $request->get("source_module");
			
		$result['success'] = true;
		
		$result['result'] = $mapping = array();
			
		$result['result']['mapping'] = array();
		
		if($source_module == 'Leads'){

			$mapping = array(
				array(
					"street" => "lane",
					"postal_code" => "code",
					"city" => "city",
					"state" => "state",
					"country" => "country"
				)
			);
					     
		} else if($source_module == 'Contacts'){
			
			$mapping = array(
				array(
					"street" => "mailingstreet",
					"postal_code" => "mailingzip",
					"city" => "mailingcity",
					"state" => "mailingstate",
					"country" => "mailingcountry"
				),
				array(
					"street" => "otherstreet",
					"postal_code" => "otherzip",
					"city" => "othercity",
					"state" => "otherstate",
					"country" => "othercountry"
				)
			);	
			
		} else if($source_module == "Accounts"){

			$mapping = array(
				array(
					"street" => "bill_street",
					"postal_code" => "bill_code",
					"city" => "bill_city",
					"state" => "bill_state",
					"country" => "bill_country"
				),
				array(
					"street" => "ship_street",
					"postal_code" => "ship_code",
					"city" => "ship_city",
					"state" => "ship_state",
					"country" => "ship_country"
				)
			);	
		} else if($source_module == 'Realtor'){
			
			$mapping = array(
				array(
					"street" => "street",
					"postal_code" => "postal_code",
					"city" => "city_address",
					"state" => "state_address",
					"country" => "country_address"
				)
			);
		}
		
		if(!empty($mapping))
			$result['result']['mapping'] = $mapping;
		
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}
}