<?php
function SendReminder($entity){

	$currentUserModel = Users_Record_Model::getCurrentUserModel();
	
	$adb = PearDatabase::getInstance();
	
	$wsId = $entityData->getId();
	
	$parts = explode('x', $wsId);
	
	$entityId = $parts[1];
	
	$query = "select vtiger_realtor.* from vtiger_realtor 
		inner join vtiger_crmentity on vtiger_crmentity.crmid = vtiger_realtor.realtorid
		inner join vtiger_realtorcf on vtiger_realtor.realtorid = vtiger_realtorcf.realtorid
		where vtiger_crmentity.deleted = 0 and vtiger_crmentity.crmid = ?";
	
	$realtor_result = $adb->pquery($query,array($entityId));
	
	if($adb->num_rows($realtor_result)){

		require_once("modules/Emails/mail.php");
		
		while($realtor = $adb->fetchByAssoc($realtor_result)){
			
			$next_reminder_on = $realtor['next_reminder_on'];
			
			$realtorid = $realtor['realtorid'];
			
			if($next_reminder_on){
		
				$dateOne = new DateTime($next_reminder_on);
				
				$dateTwo = new DateTime();
				
				$interval = $dateOne->diff($dateTwo);
		
				$days = $interval->d;
				$hours = $interval->h;
				$mins = $interval->i;
				$secs = $interval->s;
				
				if($days >= 2 && ($hours > 0 || $mins > 0 || $secs > 0)){

					$realtorRecordModel = Vtiger_Record_Model::getInstanceById($realtorid, "Realtor");
					
					$template_id = "51";
					
					$emailTemplateModel = EmailTemplates_Record_Model::getInstanceById($template_id);
				            
					$subject = $emailTemplateModel->get('subject');
			
					$contents = $emailTemplateModel->get('body');
				
					$mergedDescription = getMergedDescription($contents, $currentUserModel->getId(), 'Users');
                
					$mergedDescription = getMergedDescription($mergedDescription, $realtorid, "Realtor");
					
					$to_email = $realtorRecordModel->get("primary_email");
					
					$form_email = $currentUserModel->get("email1");
					
					$from_name = Vtiger_Functions::getUserRecordLabel($currentUserModel->getId());
					
					$send = send_mail('Realtor', $to_email, $from_name, $form_email, $subject, $mergedDescription,'','','','','',true);
					
					$next_reminder_on = date("Y-m-d H:i:s", strtotime('+48 hours'));
					
					$adb->pquery("update vtiger_realtor set next_reminder_on = ? where realtorid = ?",array($next_reminder_on,$realtorid));
				}
			}
		}
	}
}
