<?php

class Realtor_ManageAffiliate_Action extends Vtiger_Action_Controller {
	
	public function checkPermission(Vtiger_Request $request) {
		return true;
	}
	
	function process(Vtiger_Request $request) {
		$mode = $request->get('mode');
		if($mode == 'Create') {
			$this->Create($request);
		} else {
			$this->ChangeAffiliateStatus($request);
		}
	}
	
	function Create(Vtiger_Request $request){
		
		$record = $request->get("record");
		
		$result = array();
		        
		$realtor_obj = CRMEntity::getInstance("Realtor");

		$realtor_obj->id = $record;
		
		$realtor_obj->retrieve_entity_info($record, "Realtor");
		 
        $email = $realtor_obj->column_fields["primary_email"];
        
        $firstname = $realtor_obj->column_fields["firstname"];

        $lastname = $realtor_obj->column_fields["lastname"];
        
        $manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
        
        $affiliate_info = array("email" => $email, "firstname" => $firstname, "lastname" => $lastname);
        
        $result = $manageAffiliateModel->CreateAffiliateUsingApi($affiliate_info);
        
        if(
			!empty($result) && 
			isset($result['affiliate_id']) && 
			$result['affiliate_id'] != ''
		){
        	
			$status = $result['info']['status'];
        	
        	if($status == 'active') 
        		$status = "activate";
        	if($status == 'inactive')
        		$status = "deactivate";
			
			$realtor_obj->column_fields['affiliate_status'] = $status;
			$realtor_obj->column_fields['affiliate_id'] = $result['affiliate_id'];
			$realtor_obj->mode = "edit";
			$realtor_obj->save("Realtor");
			
			$record = $realtor_obj->id;
			$recordModel = Vtiger_Record_Model::getInstanceById($record, "Realtor");
			$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
			$affiliate_id = $result['affiliate_id'];
			$result = array();
			$email = $realtor_obj->column_fields["primary_email"];
			$manageAffiliateModel->RegenerateAffiliateReferralLinks($record, $affiliate_id, $email);
		
        	$result = array();
        	$result['success'] = true;
        	$result['message'] = "Affiliate Create Successfully";	
        }
        
        $response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}
	
	function ChangeAffiliateStatus(Vtiger_Request $request){

		global $adb;
		
		$result = array();
		
		$id = $request->get("affiliate_id");
		
		$status = $request->get("status");
		
		$recordId = $request->get("record");

		$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
        
		$response = $manageAffiliateModel->ChangeAffiliateStatusUsingApi($id, $status);
		
		if(isset($response['error'])){
			$result['success'] = false;
			$result['result'] = $response;
		} else {
			
			$realtor_obj = CRMEntity::getInstance("Realtor");

			$realtor_obj->id = $recordId;
			
			$realtor_obj->retrieve_entity_info($recordId, "Realtor");
			
			$realtor_obj->column_fields['affiliate_status'] = $status;
			$realtor_obj->column_fields['affiliate_id'] = $id;
	
			$realtor_obj->mode = "edit";
			
			$realtor_obj->save("Realtor");
			
			$result['success'] = true;
			$result['message'] = "Affiliate account $status";
		}
		
        $response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}	
}