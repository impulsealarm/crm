<?php
class Realtor_RegenerateLinks_Action extends Vtiger_Action_Controller{
	
	public function checkPermission(Vtiger_Request $request) {
		return true;
	}
	
	function process(Vtiger_Request $request) {
		
		$record = $request->get("record");
		
		$recordModel = Vtiger_Record_Model::getInstanceById($record, "Realtor");
		
		$manageAffiliateModel = Payments_ManageAffiliate_Model::getInstance();
	       
		$affiliate_id = $recordModel->get("affiliate_id");
		
		$result = array();
		
		$email = $recordModel->get("primary_email");
	        
		if(!$affiliate_id){
			
	        $firstname = $recordModel->get("firstname");
	
	        $lastname = $recordModel->get("lastname");
	        
	        $affiliate_info = array("email" => $email, "firstname" => $firstname, "lastname" => $lastname);
	        
	        $response = $manageAffiliateModel->CreateAffiliateUsingApi($affiliate_info);
	        
	        if(
				!empty($response) && 
				isset($response['affiliate_id']) && 
				$response['affiliate_id'] != ''
			){

				$status = $response['info']['status'];
	        	
				$affiliate_id = $response['affiliate_id'];
				
	        	if($status == 'active') 
	        		$status = "activate";
	        	if($status == 'inactive')
	        		$status = "deactivate";
				
				$recordModel->set('affiliate_status', $status);
				$recordModel->set('affiliate_id', $response['affiliate_id']);
			} else {
	        	$result['success'] = false;
				$result['message'] = $response['error'];
	        }
		}
		
		if($affiliate_id > 0){

			$response = $manageAffiliateModel->RegenerateAffiliateReferralLinks($record, $affiliate_id, $email);

			if(!empty($response) && isset($response['status']) && $response['status'] == 'ok'){
				
				$shorten_links = $response['bonus_urls'];
				
				if($response['save_bonus_urls'] && !empty($shorten_links)){
					
					$counter = "1";
					
					foreach($shorten_links as $short_link){
						
						if($counter <= '3' ){
							
							$index = "bonus_url_".$counter;
							
							$recordModel->set($index, $short_link);
						}
						
						$counter++;
					}
										
					$recordModel->set('id', $record);
					$recordModel->set("mode", "edit");
					
					$recordModel->save();
						
					$result['success'] = true;
	        	}
			} else {
				$result['success'] = false;
				$result['message'] = $response['error'];
			}
		}

        $response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}
}