<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Realtor_UploadCSV_Action extends Vtiger_Action_Controller {
	
	public function checkPermission(Vtiger_Request $request) {
		return true;
	}

	public function process(Vtiger_Request $request) {
		
		$moduleName = $request->getModule();
        $recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
				
		$result = Vtiger_Util_Helper::transformUploadedFiles($_FILES, true);
		
		//$_FILES = $result['csvfile'];
		
		foreach($_FILES as $fileindex => $files)
		{
			if($files['name'] != '' && $files['size'] > 0)
			{
				$savedFilePath = $this->uploadDocument($files);
				if( $savedFilePath != '' ){
					$savedData = $this->parseAndSaveRecords($files, $savedFilePath);
					if($savedData){
						unlink($savedFilePath);
					}
				}
			}
		}
		
		if ($request->get('relationOperation')) {
			$parentRecordModel = Vtiger_Record_Model::getInstanceById($request->get('sourceRecord'), $request->get('sourceModule'));
			$loadUrl = $parentRecordModel->getDetailViewUrl();
		} else {
			$loadUrl = $recordModel->getListViewUrl();
		}

		header("Location: $loadUrl");
	}
	
	
	/*
	 * function to save uploaded csv file on server path
	 */

	public function uploadDocument($file_details) {
		
		global $adb,$current_user;
		global $upload_badext;
		
		if(empty($file_details)) return;

		$date_var = date('YmdHis');
		
		if (isset($file_details['original_name']) && $file_details['original_name'] != null) {
			$filename = $file_details['original_name'];
		} else {
			$filename = $file_details['name'];
		}
		
		$binFile = sanitizeUploadFileName($filename, $upload_badext);

		$filename = ltrim(basename(" " . $binFile)); //allowed filename like UTF-8 characters
		$filetype = $file_details['type'];
		$filesize = $file_details['size'];
		$filetmp_name = $file_details['tmp_name'];

		//get the file path inwhich folder we want to upload the file
		$upload_file_path = decideFilePath();

		//upload the file in server
		
		$savePath = $upload_file_path . $date_var . "_" . $binFile;
		$savePath = $upload_file_path . $binFile;
		
		$upload_status = move_uploaded_file($filetmp_name, $savePath);
		
		if($upload_status){
			return $savePath;
		}
		return "";	
	}
	
	
	/*
	 * function to parse uploaded CSV File
	 */
	
	public function parseAndSaveRecords( $file_details, $savedFilePath ){
		
		global $adb, $current_user;
		$moduleName = 'Realtor';
		
		$headers = array();
		$result_data = array();
		$final_data = array();
		
		$row = '-1';
		
		if (($handle = fopen($savedFilePath, "r")) !== FALSE) {
			
			while ( ($data = fgetcsv($handle) ) !== FALSE ) {
				
				if($row == '-1'){
					$headers = $data;
					foreach($headers as $k => $keyname){
						$headers[$k] = trim($keyname);
					}
					$row = 0;
				} else {
					
					if(!empty($data)){
						foreach($data as $key => $value){
							$result_data[$headers[$key]] = trim($value);							
						}
						$this->saveRow($result_data);
					}
					$row++;
				}
			}
			fclose($handle);
			return true;
		}
		return false;
	}
	
	public function saveRow($rowData = array()){
		
		global $adb;
		
		$adb->pquery("insert into realtor_data(mls_no,raw_name,first_name,last_name,primary_phone,company,primary_email,industry,
		assigned_to,county,realtor,street,city,state,zip,realtor_county,name,selling_date,selling_price) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
		array($rowData['MLS No'],$rowData['Raw Name'],$rowData['First Name'],$rowData['Last Name'],$rowData['Primary Phone'],$rowData['Company'],$rowData['Primary Email'],
		$rowData['Industry'],$rowData['Assigned To'],$rowData['County'],$rowData['Realtor'],$rowData['Street'],$rowData['City'],$rowData['State'],$rowData['Zip'],
		$rowData['County'],$rowData['Name'],$rowData['Selling Date'],$rowData['Selling Price']));
		
		return true;
		
		
		if( !isset($rowData['MLS No']) || $rowData['MLS No'] == '' ){
			return;
		}
		
		global $adb, $current_user;
		
		$moduleName = 'Realtor';
		
		$sellingPrice = $rowData['Selling Price'];
		$closingDate = $rowData['Selling Date'];
		
		$closing_fields = array(
			'mls_no' 				=>	(isset($rowData['MLS No'])) ? $rowData['MLS No'] : '' ,
			'closings_tks_name'		=>	(isset($rowData['Name'])) ? $rowData['Name'] : '' ,
			'closings_tks_street'	=>	(isset($rowData['Street'])) ? $rowData['Street'] : '',
			'closings_tks_city'		=>	(isset($rowData['City'])) ? $rowData['City'] : '',
			'closings_tks_state'	=>	(isset($rowData['State'])) ? $rowData['State'] : '',
			'closings_tks_zip'		=>	(isset($rowData['Zip'])) ? $rowData['Zip'] : '',
			'closings_tks_county'	=>	(isset($rowData['County'])) ? $rowData['County'] : '',
			'closings_tks_sellingprice'	=> (isset($rowData['Selling Price'])) ? $rowData['Selling Price'] : '',
			'closings_tks_sellingdate'	=> (isset($rowData['Selling Date'])) ? $rowData['Selling Date'] : '',
		);
		
		if( $sellingPrice != '' ){
			$sellingPrice = preg_replace('/\$/','',$sellingPrice);
			$sellingPrice = preg_replace('/,/','',$sellingPrice);
			$closing_fields['closings_tks_sellingprice'] = $sellingPrice;
		}
		
		if( $closingDate != '' ){
			echo $closingDate = date('Y-m-d', strtotime($closingDate));
			exit;
			$closing_fields['closings_tks_sellingdate'] = $closingDate;
		}
		
		if($rowData['Primary Email'] == '' || $rowData['Primary Email'] == 0){
			if($rowData['Primary Phone'] == '' || $rowData['Primary Phone'] == 0){
				return 0;
			}
		}
		
		$closing_assigned_user = $rowData['Assigned To'];
		
		if($closing_assigned_user){
			
			$closing_assigned_user_id = $this->getAssignedUserIdFromName($closing_assigned_user);
			
			if($closing_assigned_user_id > 0)
				$closing_fields['assigned_user_id'] = $closing_assigned_user_id;
		}
		
		//save closings
		$closing_crmId = $this->checkClosingsId($closing_fields);
		
		$realtor_fullName = $rowData['Realtor'];
		
		$realtorFields = array(
			'full_name'			=>	$realtor_fullName,
			'primary_email'		=>	(isset($rowData['Primary Email'])) ? $rowData['Primary Email'] : '',
			'primary_phone'		=>	(isset($rowData['Primary Phone'])) ? $rowData['Primary Phone'] : '',
			'company'			=>	(isset($rowData['Company'])) ? $rowData['Company'] : '',
			'office_phone'		=>	(isset($rowData['Selling Office 1 - Phone Number'])) ? $rowData['Selling Office 1 - Phone Number'] : '',
			'county'			=> 	(isset($rowData['County'])) ? $rowData['County'] : '',
			'closing_address' 	=> 	(isset($rowData['Name'])) ? $rowData['Name'] : '' ,
			'industry'			=>	(isset($rowData['Industry'])) ? $rowData['Industry'] : "",
		);
		
		$firstName = $rowData['First Name'];
		
		$lastName = $rowData['Last Name'];
		
		if($firstName == '' && $lastName == '' &&  $realtor_fullName != '' ){
			$nameParts = explode(' ', $realtor_fullName);
			
			if( count($nameParts) > 1 ){
				$lastName = trim($nameParts[count($nameParts)-1]);
				$firstName = trim(preg_replace('/'.$lastName.'/','',$realtor_fullName));
			} else {
				$lastName = '';
				$firstName = trim($nameParts[0]);
			}
		}
		
		$realtorFields['firstname'] = $firstName;
		
		$realtorFields['lastname'] = $lastName;
		
		$assigned_user = $rowData['Assigned To'];
		
		if($assigned_user){
			
			$assigned_user_id = $this->getAssignedUserIdFromName($assigned_user);
			
			if($assigned_user_id > 0)
				$realtorFields['assigned_user_id'] = $assigned_user_id;
		}
		
		$realtorId = $this->saveRealtor($realtorFields, $closing_crmId);
		
		if( $realtorId != '' && $closing_crmId != '' ){
			$adb->pquery(" UPDATE vtiger_closings SET closings_tks_realtor = ? 
			WHERE closingsid = ?", array( $realtorId, $closing_crmId ));
		}
	}
	
	public function checkClosingsId($closing_fields = array()){
		
		$crmId = '';
		
		global $adb;
		
		$skip_fields = array( 'mls_no' );
		
		$mls_no = $closing_fields['mls_no'];
		
		$query .= " select vtiger_closings.closingsid as id from vtiger_closings 
		inner join vtiger_crmentity on vtiger_crmentity.crmid = vtiger_closings.closingsid AND vtiger_crmentity.deleted = 0 and mls_no = ? ";
		
		$result = $adb->pquery( $query, array($mls_no) );
		
		$entityMode = '';
		
		if( $adb->num_rows($result) ){

			$crmId = $adb->query_result( $result, 0, 'id' );
		
			$recordModel = Vtiger_Record_Model::getInstanceById($crmId, 'Closings');
			$modelData = $recordModel->getData();
							
			$recordModel->set('id', $crmId);
			$entityMode = 'edit';
			
		
		} else {
			
			$entityMode = '';
			
			$recordModel = Vtiger_Record_Model::getCleanInstance('Closings');
			
			$modelData = $recordModel->getData();
		}
		
		foreach( $closing_fields as $fieldName => $findValue ){
			if($entityMode == 'edit' && $fieldName == 'assigned_user_id') continue;
		
			if( in_array($fieldName,$skip_fields) ) continue;			
			
			if($findValue != ''){
				$recordModel->set( $fieldName, $findValue);
			}
		}
		
		$recordModel->set('mode', $entityMode);
			
		if( isset($closing_fields['mls_no']) && $closing_fields['mls_no'] != '' ){
			$recordModel->set('mls_no', $closing_fields['mls_no']);
		}			
		if( isset($closing_fields['closings_tks_sellingprice']) && $closing_fields['closings_tks_sellingprice'] != '' ){
			$recordModel->set('closings_tks_sellingprice', $closing_fields['closings_tks_sellingprice']);
		}			
		if( isset($closing_fields['closings_tks_sellingdate']) && $closing_fields['closings_tks_sellingdate'] != '' ){
			$recordModel->set('closings_tks_sellingdate', $closing_fields['closings_tks_sellingdate']);
		}
						
		$recordModel->save();
		
		$crmId = $recordModel->getId();
	
		return $crmId;
	}
	
	
	
	public function saveRealtor($realtor_fields = array(), $closingId = ''){
		
		$crmId = '';
		
		global $adb;
		
		$skip_fields = array();// 'mls_no', 'closings_tks_sellingprice', 'closings_tks_sellingdate' );
		
		$query .= " SELECT vtiger_realtor.realtorid FROM vtiger_realtor INNER JOIN vtiger_crmentity on 
		vtiger_crmentity.crmid = vtiger_realtor.realtorid  AND vtiger_crmentity.deleted = 0 ";
		
		$realtor_found = false;
		
		if( isset($realtor_fields['primary_phone']) && $realtor_fields['primary_phone'] != '' && $realtor_fields['primary_phone'] != 0){
			
			$filteredPhone = preg_replace('/\D/', '', $realtor_fields['primary_phone']);
	    	
			$filteredPhone = substr($filteredPhone, -10);
			
			if(strlen($filteredPhone) > 6){
				$query1 = $query . " INNER JOIN vtiger_crm_phonenumbers on vtiger_crm_phonenumbers.crmid = vtiger_realtor.realtorid 
				AND vtiger_crm_phonenumbers.phone like '%{$filteredPhone}' ";	

				$phoneResult = $adb->pquery($query1, array());
				if($adb->num_rows($phoneResult)){
					$realtor_found = true;
					$crmId = $adb->query_result($phoneResult, 0, 'realtorid');
				}
			}
		}
		
		if( !($realtor_found) ){
		
			if( isset($realtor_fields['primary_email']) && $realtor_fields['primary_email'] != '' ){			
				$query1 = $query . " WHERE  primary_email = '{$realtor_fields['primary_email']}' ";	
				$emailResult = $adb->pquery($query1, array());
				if($adb->num_rows($emailResult)){
					$realtor_found = true;
					$crmId = $adb->query_result($emailResult, 0, 'realtorid');
				}
			}
		}
		
		$entityMode = '';
			
		if( $realtor_found && $crmId != '' ){			
			
			$entityMode = 'edit';
			$recordModel = Vtiger_Record_Model::getInstanceById($crmId, 'Realtor');
			$modelData = $recordModel->getData();
			$recordModel->set('id', $crmId);
								
		} else {
			
			$recordModel = Vtiger_Record_Model::getCleanInstance('Realtor');
			$modelData = $recordModel->getData();				
		}
		
		foreach( $realtor_fields as $fieldName => $findValue ){
			
			if($entityMode == 'edit' && $fieldName == 'assigned_user_id') continue;
			
			if( in_array($fieldName,$skip_fields) ) continue;			
			if($findValue != ''){
				$recordModel->set( $fieldName, $findValue);
			}
		}
		
		$recordModel->set('mode', $entityMode);
				
		if( $closingId != '' ){
			
			$closingRecordModel = Vtiger_Record_Model::getInstanceById( $closingId, 'Closings');
			$closing_fields = $closingRecordModel->getData();
			
			if( isset($closing_fields['closings_tks_sellingprice']) && $closing_fields['closings_tks_sellingprice'] != '' ){
				$recordModel->set('home_sellingprice', $closing_fields['closings_tks_sellingprice']);
			}			
			if( isset($closing_fields['closings_tks_sellingdate']) && $closing_fields['closings_tks_sellingdate'] != '' ){
				$recordModel->set('home_closingdate', $closing_fields['closings_tks_sellingdate']);
			}
		}
		
		$recordModel->save();
		$crmId = $recordModel->getId();
		
		return $crmId;
	}
	
	function getAssignedUserIdFromName($username){
		
		$adb = PearDataBase::getInstance();
		
		$result = $adb->pquery("select id from vtiger_users where user_name = ? and deleted = 0", array($username));
		
		if($adb->num_rows($result))
			return $adb->query_result($result,"0","id");
		else 
			return false;
	}
}