<?php
function SendSlackNotification($entityData){

	$adb = PearDatabase::getInstance();
	
	$moduleName = $entityData->getModuleName();
	
	$wsId = $entityData->getId();
	
	$parts = explode('x', $wsId);
	
	$entityId = $parts[1];
	
	$user_id = $entityData->get('assigned_user_id');
	
	$user_id = explode('x', $user_id);
	
	$user_id = $user_id[1];
	
	$username = Vtiger_Functions::getUserRecordLabel($user_id);
	
	$userModel = Users_Record_Model::getInstanceById($user_id,"Users");
			
	
	$message = '@'. $userModel->get("user_name") . ' ';
	
	$firstname = $entityData->get('firstname');
	
	if($firstname)
		$message .= $firstname." ";
	
	$lastname = $entityData->get('lastname');
	
	if($lastname)
		$message .= $lastname. " ";
	
	$message .= "has entered their referral gift payment information";
	
	$token = "xoxp-8998175120-8998299591-12708012006-112e0d39a8";
	
	$url = "https://slack.com/api/chat.postMessage";
	
	$post_params = array(
		"token" 	=> $token,
		"channel" 	=> "#sales",//C0L63T5DW
		"text"		=> $message,
		"as_user"	=> false,
		"username"  => 'Bridge'
	);
	
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_URL, $url);
	
	curl_setopt($ch, CURLOPT_POST, true);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
	  
	$response = curl_exec($ch);
		
	curl_close($ch);
	
}