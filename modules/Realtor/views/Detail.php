<?php
class Realtor_Detail_View extends Vtiger_Detail_View {
	
	function __construct() {
		parent::__construct();
	}
	
	public function process(Vtiger_Request $request){
		$this->ShowRealtorAffiliateURLs($request);
		parent::process($request);
	}
	
	function ShowRealtorAffiliateURLs(Vtiger_Request $request){

		$moduleName = $request->getModule();
		$recordId = $request->get('record');
		
		$viewer = $this->getViewer($request);
		$viewer->assign('MODULE_NAME',$moduleName);

		$realtor_urls = $this->getRealtorAffiliateURLs($recordId);
		
		if(!empty($realtor_urls)){
			$viewer->assign("REALTOR_REFERRAL_URLS", $realtor_urls);
		}
	}
	
	function getRealtorAffiliateURLs($record){
		
		$adb = PearDatabase::getInstance();
		
		$urls = array();
		
		$result = $adb->pquery("select * from vtiger_realtor_affiliate_urls where realtor_id = ?",array($record));
		
		if($adb->num_rows($result)){
			
			while($row = $adb->fetchByAssoc($result)){
				$urls[$row['referral_url_name']] = $row['referral_url'];	
			}
		}
		
		return $urls;
	}
}