<?php
class Realtor_Record_Model extends Vtiger_Record_Model{
	
	public function getListViewUrl() {
		$module = $this->getModule();
		return 'index.php?module='.$this->getModuleName().'&view=List';
	}
}