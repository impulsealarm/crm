<?php

/**
 * Realtor ListView Model Class
 */
class Realtor_ListView_Model extends Vtiger_ListView_Model {


	/**
	 * Function to get the list of listview links for the module
	 * @param <Array> $linkParams
	 * @return <Array> - Associate array of Link Type to List of Vtiger_Link_Model instances
	 */
	
	public function getListViewLinks($linkParams) {
		
		$links = parent::getListViewLinks($linkParams);
		
		$currentUserModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleModel = $this->getModule();

		if($currentUserModel->hasModuleActionPermission($moduleModel->getId(), 'EditView')) {
			$basicLink = array(
								'linktype' => 'LISTVIEW',
								'linklabel' => 'Upload Realtor and Closings',
								'linkurl' => 'index.php?module=Realtor&view=UploadCSV',
								'linkicon' => ''
			);
			
			$links['LISTVIEW'][] = Vtiger_Link_Model::getInstanceFromValues($basicLink);
		}
		return $links;
	}
	
	public function getListViewMassActions($linkParams) {
		$massActionLinks = parent::getListViewMassActions($linkParams);

		$currentUserModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		
		$SMSNotifierModuleModel = Vtiger_Module_Model::getInstance('SMSNotifier');
		if(!empty($SMSNotifierModuleModel) && $currentUserModel->hasModulePermission($SMSNotifierModuleModel->getId())) {
			$massActionLink = array(
				'linktype' => 'LISTVIEWMASSACTION',
				'linklabel' => 'LBL_SEND_SMS',
				'linkurl' => 'javascript:Vtiger_List_Js.triggerSendSms("index.php?module='.$this->getModule()->getName().'&view=MassActionAjax&mode=showSendSMSForm","SMSNotifier");',
				'linkicon' => ''
			);
			$massActionLinks['LISTVIEWMASSACTION'][] = Vtiger_Link_Model::getInstanceFromValues($massActionLink);
		}

		return $massActionLinks;
	}
	
}
