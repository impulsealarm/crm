<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Realtor_DetailView_Model extends Vtiger_DetailView_Model {

	public function getDetailViewLinks($linkParams) {
		
		$currentUserModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		
		$emailModuleModel = Vtiger_Module_Model::getInstance('Emails');
		
		$recordModel = $this->getRecord();

		$linkModelList = parent::getDetailViewLinks($linkParams);

		if($currentUserModel->hasModulePermission($emailModuleModel->getId())) {
			$basicActionLink = array(
				'linktype' => 'DETAILVIEWBASIC',
				'linklabel' => 'LBL_SEND_EMAIL',
				'linkurl' => 'javascript:Vtiger_Detail_Js.triggerSendEmail("index.php?module='.$this->getModule()->getName().
								'&view=MassActionAjax&mode=showComposeEmailForm&step=step1","Emails");',
				'linkicon' => ''
			);
			$linkModelList['DETAILVIEWBASIC'][] = Vtiger_Link_Model::getInstanceFromValues($basicActionLink);
		}
		
		$SMSNotifierModuleModel = Vtiger_Module_Model::getInstance('SMSNotifier');
		if(!empty($SMSNotifierModuleModel) && $currentUserModel->hasModulePermission($SMSNotifierModuleModel->getId())) {
			$basicActionLink = array(
				'linktype' => 'DETAILVIEWBASIC',
				'linklabel' => 'LBL_SEND_SMS',
				'linkurl' => 'javascript:Vtiger_Detail_Js.triggerSendSms("index.php?module='.$this->getModule()->getName().
								'&view=MassActionAjax&mode=showSendSMSForm","SMSNotifier");',
				'linkicon' => ''
			);
			$linkModelList['DETAILVIEW'][] = Vtiger_Link_Model::getInstanceFromValues($basicActionLink);
		}
		

		$affiliate = $this->CheckAffiliateForRecord($recordModel->getId());
		
		if(empty($affiliate)){
			
			$basicActionLink = array(
				'linktype' => 'DETAILVIEWBASIC',
				'linklabel' => 'Create Affiliate',
				'linkurl' => 'javascript:Realtor_Detail_Js.CreateAffiliate("index.php?module='.$this->getModule()->getName().
								'&action=ManageAffiliate&mode=Create",'.$recordModel->getId().');',
				'linkicon' => ''
			);
			
			$linkModelList['DETAILVIEWBASIC'][] = Vtiger_Link_Model::getInstanceFromValues($basicActionLink);
		
		} else {
			
			$affiliate_id = $affiliate['affiliate_id'];
			
			if(isset($affiliate['affiliate_status']) && $affiliate['affiliate_status'] != '' && $affiliate_id != '') {
				
				if($affiliate['affiliate_status'] == 'deactivate'){
					
					$basicActionLink = array(
						'linktype' => 'DETAILVIEWBASIC',
						'linklabel' => 'Activate Affiliate',
						'linkurl' => 'javascript:Realtor_Detail_Js.ChangeAffiliateStatus("index.php?module='.$this->getModule()->getName().
										'&action=ManageAffiliate&mode=ChangeAffiliateStatus","activate","'.$affiliate_id.'");',
						'linkicon' => ''
					);
					
					$linkModelList['DETAILVIEWBASIC'][] = Vtiger_Link_Model::getInstanceFromValues($basicActionLink);
				
				} else if($affiliate['affiliate_status'] == 'activate'){
					
					$basicActionLink = array(
						'linktype' => 'DETAILVIEWBASIC',
						'linklabel' => 'Deactivate Affiliate',
						'linkurl' => 'javascript:Realtor_Detail_Js.ChangeAffiliateStatus("index.php?module='.$this->getModule()->getName().
										'&action=ManageAffiliate&mode=ChangeAffiliateStatus","deactivate","'.$affiliate_id.'");',
						'linkicon' => ''
					);
					
					$linkModelList['DETAILVIEWBASIC'][] = Vtiger_Link_Model::getInstanceFromValues($basicActionLink);
				}
			}
		}
		
		$basicActionLink = array(
			'linktype' => 'DETAILVIEWBASIC',
			'linklabel' => 'Regenerate Link',
			'linkurl' => 'javascript:Realtor_Detail_Js.triggerRegenerateLinks("index.php?module='.$this->getModule()->getName().
							'&action=RegenerateLinks","' . $recordModel->getId() . '");',
			'linkicon' => ''
		);
		$linkModelList['DETAILVIEW'][] = Vtiger_Link_Model::getInstanceFromValues($basicActionLink);
		
		return $linkModelList;
	}
	
	function CheckAffiliateForRecord($recordId){
		
		global $adb;
		
		$affiliate_info = array();
		
		$result = $adb->pquery("select affiliate_id, affiliate_status from vtiger_realtor where realtorid = ?",array($recordId));
		
		if($adb->num_rows($result)){
			
			$affiliate_id = $adb->query_result($result, 0,"affiliate_id");
			
			if($affiliate_id){
				$affiliate_info['affiliate_id'] = $affiliate_id;
				$affiliate_info['affiliate_status'] = $adb->query_result($result,0,"affiliate_status");
			}
		}
			
		return $affiliate_info;
	}

}