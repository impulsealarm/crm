<?php

require_once('modules/com_vtiger_workflow/VTEntityCache.inc');
require_once('modules/com_vtiger_workflow/VTWorkflowUtils.php');

require_once('modules/com_vtiger_workflow/VTEmailRecipientsTemplate.inc');

require_once('include/Mandrill/MandrillAPI.php');
        	

class VTSendMandrillEmailTask extends VTTask{
	
	// Sending email takes more time, this should be handled via queue all the time.
	public $executeImmediately = false;

	public function getFieldNames(){
		return array("mtemplate", "recepient", 'emailcc', 'emailbcc', 'fromEmail');
	}
	
	public function doTask($entity){

		global $adb, $current_user;
		$util = new VTWorkflowUtils();
		$admin = $util->adminUser();
		$module = $entity->getModuleName();

		$taskContents = Zend_Json::decode($this->getContents($entity));
		$from_email	= $taskContents['fromEmail'];
		$from_name	= $taskContents['fromName'];
		$to_email	= $taskContents['toEmail'];
		$cc			= $taskContents['ccEmail'];
		$bcc		= $taskContents['bccEmail'];
		$mandrill_template_slug	= $taskContents['mtemplate'];

		$to_email = trim($to_email,',');
		
		if(!empty($to_email)) {
			
			$entityIdDetails = vtws_getIdComponents($entity->getId());
			$entityId = $entityIdDetails[1];
			
			
			
			
			//Storing the details of emails
			
			$userId = $current_user->id;
			$emailFocus = CRMEntity::getInstance('Emails');
			$emailFieldValues = array(
					'assigned_user_id' => $userId,
					'subject' => "test",
					'description' => "test",
					'from_email' => $from_email,
					'saved_toid' => $to_email,
					'ccmail' => $cc,
					'bccmail' => $bcc,
					'parent_id' => $entityId."@$userId|",
					'email_flag' => 'MANDRILL',
					'activitytype' => 'Emails',
					'date_start' => date('Y-m-d'),
					'time_start' => date('H:i:s'),
					'mode' => '',
					'id' => '',
                                        'email_status' => '',

			);
			$emailFocus->column_fields = $emailFieldValues;
			$emailFocus->save('Emails');
			
			$emailId = $emailFocus->id;
			
			if(!empty($emailId)) {
				$emailFocus->setEmailAccessCountValue($emailId);
			}
			
			if(!is_array($to_email)){
				$to_email = explode( ',', $to_email );
			}
			
			
			
			
			
			
			
			
			$to = array();
			$rcp_merge_vars = array();
			$recipient_metadata = array();
			$global_mergeVars = array();
			
			/* For Leads */
			
			require_once('data/CRMEntity.php');
			
			$parentModuleName = getSalesEntityType($entityId);
			
			$focus = CRMEntity::getInstance($parentModuleName);
			$focus->id = $entityId;
			$focus->retrieve_entity_info($entityId, $parentModuleName);
			
			/* To pass Record Labels as merge vars */
			
			$recordModel = Vtiger_Record_Model::getInstanceById($entityId, $parentModuleName);
			$moduleModel = $recordModel->getModule();
			$fieldList = $moduleModel->getFields();
			
			foreach( $fieldList as $fieldName => $fieldModel ){

				$fieldLabel = preg_replace( '/\s+/', '_', trim( vtranslate($fieldModel->get('label'), $parentModuleName) ) );
				$fieldValue = $recordModel->getDisplayValue( $fieldModel->get('name'), $entityId );
				
				if( $fieldModel->isOwnerField() ){
			    	$fieldValue = getOwnerName( $focus->column_fields[$fieldModel->get('name')] );
			    }
			    
			    $merge_variables[] = array(
					'name' => $fieldLabel,
				    'content' => $fieldValue
				);
			    
			}
		
			$assignedUser = $focus->column_fields['assigned_user_id'];
			$assigneeResult = $adb->pquery("SELECT user_name FROM vtiger_users WHERE id = ?", array($assignedUser));			
			if($adb->num_rows($assigneeResult)){
				$ownerModule = 'Users';			
				$from_email = getUserEmail($assignedUser);
				$from_name = getUserFullName($assignedUser);				
			}
			
			
			/* To Pass Owner Field Labels as Global Vars */
			
			if( $ownerModule == 'Users' ){
				
				$owner_recordModel = Users_Record_Model::getInstanceById($assignedUser, "Users");
				$owner_moduleModel = $owner_recordModel->getModule();
				$owner_fieldList = $owner_moduleModel->getFields();
				
				foreach( $owner_fieldList as $fieldName => $fieldModel ){
	
					$fieldLabel = preg_replace( '/\s+/', '_', trim( vtranslate($fieldModel->get('label'), "Users") ) );
					$fieldValue = $owner_recordModel->getDisplayValue( $fieldModel->get('name'), $assignedUser );				
				    			    
				    $global_mergeVars[] = array(
						'name' => 'User_'.$fieldLabel,
					    'content' => $fieldValue
					);			    
				}
			}
			
			
			
			/* Make array of toEmails (to) and  merge_vars for each recipient */
				
			foreach($to_email as $toEmail){		
							
				$toEmail = trim($toEmail);					
				if($toEmail == '') continue;
				
				$to[] = array(
					'email' => $toEmail,
					'type' => 'to'
				);
				
				$rcp_merge_vars[] = array(
		                'rcpt' => $toEmail,	//recipient email
		                'vars' => $merge_variables
		        );
		        
		        
				$recipient_metadata[] = array(
		        	'rcpt' => $toEmail,
		            'values' => array(
		            	'crmid' => $entity->getId(),
						'emailid' => $emailId
					)
		        );
			}
		
			
		
			
        	$template_name = $mandrill_template_slug; //unique slug of mandrill template
   
		    /*  Pass Data of any editable region of template via template_content : 
		     * 	name - region's mc-edit attribute i.e. <div mc:edit="footer"></div>
		     *  content - html content of editable region
		     */
		    
        	/*$template_content = array(
		        array(
		            'name' => 'footer',
		            'content' => '<i>This is test email via Mandrill API</i>'
		        )
		    );*/
		    
		    $template_content = array();
		    
		    $message = array(
		        
		        'from_email' => $from_email,
		        'from_name' => $from_name,
		        'to' => $to,
		    
		        'important' => true,
		    
		        'merge' => true,
		        'merge_language' => 'mailchimp',
		    
		    	/*  global vars across template whose value is to be same for all to emails */
		    
		        'global_merge_vars' => $global_mergeVars,
		        
		        
		        /*  receipient dependent vars */
		        
		        'merge_vars' => $rcp_merge_vars,
		        
		    
				'recipient_metadata' => $recipient_metadata,
		        
		        'track_opens' => true,
		        'track_clicks' => true,
		        'auto_text' => null,
		        'auto_html' => null,
		        'inline_css' => null,
		        'url_strip_qs' => null,
		        'preserve_recipients' => null,
		        'view_content_link' => null,
		        'tracking_domain' => null,
		        'signing_domain' => null,
		        'return_path_domain' => null,
		        	       
		    );
		    
    		$async = true;
    
    		try{
    			$mandrill = new MandrillAPI();
        		$result = $mandrill->sendTemplate($template_name, $template_content, $message, $async);
    		}catch(Exception $e){
    			$result = $e;
    		}
    		
		    

		}
		$util->revertUser();
	}
	
	/**
	 * Function to get contents of this task
	 * @param <Object> $entity
	 * @return <Array> contents
	 */
	public function getContents($entity, $entityCache=false) {
		if (!$this->contents) {
			global $adb, $current_user;
			$taskContents = array();
			$entityId = $entity->getId();

            $utils = new VTWorkflowUtils();
            $adminUser = $utils->adminUser();
			if (!$entityCache) {
				$entityCache = new VTEntityCache($adminUser);
			}

			$fromUserId = Users::getActiveAdminId();
			$entityOwnerId = $entity->get('assigned_user_id');
			if ($entityOwnerId) {
				list ($moduleId, $fromUserId) = explode('x', $entityOwnerId);
			}

			$ownerEntity = $entityCache->forId($entityOwnerId);
			if($ownerEntity->getModuleName() === 'Groups') {
				list($moduleId, $recordId) = vtws_getIdComponents($entityId);
				$fromUserId = Vtiger_Util_Helper::getCreator($recordId);
			}

			if ($this->fromEmail && !($ownerEntity->getModuleName() === 'Groups' && strpos($this->fromEmail, 'assigned_user_id : (Users) ') !== false)) {
				$et = new VTEmailRecipientsTemplate($this->fromEmail);
				$fromEmailDetails = $et->render($entityCache, $entityId);

				if(strpos($this->fromEmail, '&lt;') && strpos($this->fromEmail, '&gt;')) {
					list($fromName, $fromEmail) = explode('&lt;', $fromEmailDetails);
					list($fromEmail, $rest) = explode('&gt;', $fromEmail);
				} else {
					$fromEmail = $fromEmailDetails;
				}
			} else {
				$userObj = CRMEntity::getInstance('Users');
				$userObj->retrieveCurrentUserInfoFromFile($fromUserId);
				if ($userObj) {
					$fromEmail = $userObj->email1;
					$fromName =	$userObj->user_name;
				} else {
					$result = $adb->pquery('SELECT user_name, email1 FROM vtiger_users WHERE id = ?', array($fromUserId));
					$fromEmail = $adb->query_result($result, 0, 'email1');
					$fromName =	$adb->query_result($result, 0, 'user_name');
				}
			}

			if (!$fromEmail) {
                $utils->revertUser();
				return false;
			}

			$taskContents['fromEmail'] = $fromEmail;
			$taskContents['fromName'] =	$fromName;


			$et = new VTEmailRecipientsTemplate($this->recepient);
			$toEmail = $et->render($entityCache, $entityId);

			$ecct = new VTEmailRecipientsTemplate($this->emailcc);
			$ccEmail = $ecct->render($entityCache, $entityId);

			$ebcct = new VTEmailRecipientsTemplate($this->emailbcc);
			$bccEmail = $ebcct->render($entityCache, $entityId);

			if(strlen(trim($toEmail, " \t\n,")) == 0 && strlen(trim($ccEmail, " \t\n,")) == 0 && strlen(trim($bccEmail, " \t\n,")) == 0) {
                $utils->revertUser();
				return false;
			}

			$taskContents['toEmail'] = $toEmail;
			$taskContents['ccEmail'] = $ccEmail;
			$taskContents['bccEmail'] = $bccEmail;

			$st = new VTSimpleTemplate($this->mtemplate);
			$taskContents['mtemplate'] = $st->render($entityCache, $entityId);

			$this->contents = $taskContents;
            $utils->revertUser();
		}
		
		if(is_array($this->contents)) {
			$this->contents = Zend_Json::encode($this->contents);
		}
		return $this->contents;
	}
	
	


}
?>