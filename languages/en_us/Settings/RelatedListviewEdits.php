<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
$languageStrings = array(
	// Basic Strings
	'LBL_SETTING_HEADER'=>'Related & ListView Hover Edit',
    'LBL_FIELD_SETTING_TAB'=>'Fields',
    'LBL_RELATED_MODULE_SETTING_TAB'=>'Arrange Modules',
    'LBL_SELECT_MODULE'=>'Select module',
    'LBL_MODULE_ACTIVE'=>'Status',
    'MODULE_ACTIVE_OPTION'=>'Active',
    'MODULE_INACTIVE_OPTION'=>'Inactive',
    'LBL_ALLOW_EDIT_ALL_FIELDS'=>'Allow to edit all fields',
    'LBL_SELECT_FIELDS'=>'Select the fields to be edited. If no fields are selected - all fields will be editable.',
    'SELECT_FIELDS_OPTION'=>'Select fields',
    'LBL_MODULE_ACTIVE_TYPE'=>'Type',
    'MODULE_ACTIVE_TYPE_OPTION_LISTVIEW'=>'List View',
    'MODULE_ACTIVE_TYPE_OPTION_RELATEDLIST'=>'Related List',
    'MODULE_ACTIVE_TYPE_OPTION_BOTH'=>'Both',
);