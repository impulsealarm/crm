<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */
$languageStrings = array(
    'LBL_SELECT_ONE' => 'Select',
    'LBL_PBXMANAGER' =>'Twilio',
    'LBL_PBXMANAGER_CONFIG' => 'Twilio Server Details',
    'LBL_NOTE' => 'Note:',
    'LBL_INFO_WEBAPP_URL' => 'Configure your Twilio App URL in the  format', 
    'LBL_FORMAT_WEBAPP_URL' => '(protocol)://(asterisk_ip):(VtigerConnector_port)',
    'LBL_FORMAT_INFO_WEBAPP_URL' => 'ex:http://0.0.0.0:5000',
    'LBL_INFO_CONTEXT' => 'Vtiger Spezifischer Kontext in Ihrer Twilio-Server konfiguriert (extensions.conf)',
    'LBL_PBXMANAGER_INFO' => 'Configure Twilio Server Details after Installing Vtiger Twilio Connector in your Twilio Server',
    
    'webappurl'=>'Twilio Account SID',
    'vtigersecretkey'=>'Twilio Outgoing call App id',
    'outboundcontext' => 'Twilio Auth Token',
    'outboundtrunk' => 'Outbound Trunk',
    
);

$jsLanguageStrings = array(
    
);
?>  
