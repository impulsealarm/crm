<?php
/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
$languageStrings = array(
	// Basic Strings
	'LBL_SETTING_HEADER'=>'Global Search',
    'LBL_FIELD_SETTING_TAB'=>'Fields',
    'LBL_RELATED_MODULE_SETTING_TAB'=>'Arrange Modules',
    'LBL_SAVE_BTN'=>'Save',
    'LBL_CANCEL_BTN'=>'Cancel',
);

$jsLanguageStrings = array(
    'JS_EDIT_BTN' => 'Edit',
);