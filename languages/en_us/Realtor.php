<?php

$languageStrings = Array(
	'Realtor' => 'Realtor',
	'SINGLE_Realtor' => 'Realtor',
	'Realtor ID' => 'Realtor ID',
	'LBL_REALTOR_INFORMATION' => 'Realtor Details',
    'LBL_REALTOR_Custom'=>'Custom Information',
    'LBL_REALTOR_Checklist'=>'Checklist',
    'LBL_REALTOR_Address'=>'Address Deatail',
    'LBL_REALTOR_Description'=>'Description Detail'
);