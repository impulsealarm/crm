var connectionsArray = [];

var server = require('http').createServer(function(req, res) {
  
  res.writeHead(200);
  
  res.end('Hello');
  
});

io = require('socket.io').listen(server);

server.listen(8000);
   
io.sockets.on('connection', function(socket) {
  
	connectionsArray.push(socket);
   
	connectionsArray.forEach(function(tmpSocket) {
		tmpSocket.emit('notification', { message: 'Hello Sir' });
	});
  
});

