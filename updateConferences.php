<?php
	require_once('includes/main/WebUI.php');

    global $adb, $current_user;
	
	$adb = PearDatabase::getInstance();

	global $current_user;

	if(!$current_user) {
		$current_user = Users::getActiveAdminUser();
	}

	
	if( !function_exists('Services_Twilio_autoload') ) {    
		require 'modules/Twilio/twilio-php/Services/Twilio.php';
    }
    
	
	// Twilio REST API version
	$version = '2010-04-01';
	
	$serverModel = PBXManager_Server_Model::getInstance();
	
	$accountSid = $serverModel->get ( "webappurl" );	
	$token = $serverModel->get ( "outboundcontext" );
	
	$http = new Services_Twilio_TinyHttp(
		    'https://api.twilio.com',
		    array('curlopts' => array(
		        CURLOPT_SSL_VERIFYPEER => false,
		    ))
	);
	
	// Instantiate a new Twilio Rest Client
	
	$client = new Services_Twilio($accountSid, $token, $version, $http);

	 	
	$sqlQuery = " SELECT vtiger_pbxmanager.* FROM vtiger_pbxmanager 
	INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_pbxmanager.pbxmanagerid 
	WHERE 
	vtiger_crmentity.deleted = 0 AND 
	vtiger_pbxmanager.is_conference = '1' AND 
	vtiger_pbxmanager.sourceuuid is not null AND vtiger_pbxmanager.sourceuuid != '' 
	ORDER BY vtiger_pbxmanager.pbxmanagerid ASC ";
	
	$limitQuery = "";
	
	/*$limitQuery = " LIMIT 0, 1000";
	$limitQuery = " LIMIT 999, 1000";
	$limitQuery = " LIMIT 1999, 1000";
	$limitQuery = " LIMIT 2999, 1000";
	$limitQuery = " LIMIT 3999, 1000";
	$limitQuery = " LIMIT 4999, 1000";
	$limitQuery = " LIMIT 5999, 1000";*/
	
	
	
	
	$sqlQuery = $sqlQuery . $limitQuery;
	
	$conferencesResult = $adb->pquery($sqlQuery,array());
		
	$conferenceRows = $adb->num_rows($conferencesResult);
	
	//echo $conferenceRows;
	//exit;
	
	if( $conferenceRows ){
		
		for( $i=0; $i<$conferenceRows; $i++ ){
			
			$pbxmanagerid = $adb->query_result($conferencesResult, $i, 'pbxmanagerid');
			$sourceuuid = $adb->query_result($conferencesResult, $i, 'sourceuuid');
			$gateway = $adb->query_result($conferencesResult, $i, 'gateway');
			
			//echo $i. ". Updating CallSid : " . $sourceuuid;
			//echo "<br>";
			
			if( strtolower($gateway) != 'twilio' ){
				continue;
			}
			
			$to_result = $adb->pquery("SELECT * 
			FROM vtiger_conference_calls 
			WHERE BINARY call_sid = ?", array( $sourceuuid ));
						
			if( $adb->num_rows($to_result) ){
							
				$conference_pkey = $adb->query_result( $to_result, 0, 'conference_id' );
							
				$calledToResult = $adb->pquery("SELECT * 
				FROM vtiger_conference_calls 
				WHERE conference_id = ? AND 
				is_listener != '1' AND is_initiator != '1'", array($conference_pkey) );
				
				if( $adb->num_rows($calledToResult) ){
								
					$tocallsid = $adb->query_result( $calledToResult, 0, 'call_sid' );
								
					$tocallObj = $client->account->calls->get($tocallsid);
								
					$toNumber = $tocallObj->to;
								
					$to_no = preg_replace ( '/\D+/', '', $toNumber);
								
					if( $to_no != '' ){				    		
						    	
						$customer_result = getCustomerByPhone($to_no);
											    		
					    $updateParams['customernumber'] = $to_no;					
						$updateParams['customer'] = $customer_result['crmid'];
						$updateParams['customertype'] = $customer_result['setype'];								
						$updateParams['to_number'] = $to_no;
						$updateParams['sourceuuid'] = $sourceuuid;
									
						$adb->pquery(" UPDATE vtiger_pbxmanager 
									SET customernumber = ?, customer = ?, customertype = ?, to_number = ? 
									WHERE BINARY sourceuuid = ? ", $updateParams);
									
					}
				}
			}
			
		}
	}
	
	
	
	

	
	
	
	
	
	
	
    /* ===== START : COMMON FUNCTIONS USED IN ABOVE CODE ===== */
		




	/*
	* function to get user crm_extension based on username
	*/

	function getUserPhone($username = ''){	
		
		$adb = PearDatabase::getInstance();

		$result = '';

		if($username == '') return '';
		
		$query_result = $adb->pquery("select * from vtiger_users 
		where user_name = ?",array($username));

		if($adb->num_rows($query_result)){
			$result = $adb->query_result($query_result, 0, 'phone_crm_extension');
		}
		
		return $result;	
	}

	/*
	* function to get userid based on username
	*/

	function getUserId($username = ''){	
		$adb = PearDatabase::getInstance();
		
		$result = '';

		if($username == '') return '';

		$query_result = $adb->pquery("select * from vtiger_users 
		where user_name = ?",array($username));

		if($adb->num_rows($query_result)){
			$result = $adb->query_result($query_result, 0, 'id');
		}
		
		
		return $result;	
	}

	/*
	 * function to find and get user detail 
	 * based on crm_extension (without special characters)
	 */
	
	function getUserInfoWithNumber($number){
		
		$adb = PearDatabase::getInstance();
		
		$result = array('id' => '', 'setype' => '');
		
		$number = substr($number, -10);
	
	    $recordQuery = "SELECT * FROM vtiger_users WHERE 
	    (phone_crm_extension = '" . $number . "' OR phone_crm_extension like '%" . $number . "')";
		
		$recordResult = $adb->pquery($recordQuery,array());
		
		if($adb->num_rows($recordResult)){
			
			$crmid = $adb->query_result($recordResult, 0, 'id');
			
			if($crmid){
				$result['id'] = $crmid;
				$result['setype'] = "Users";
			}
			
		}			
		return $result;			
	}

	/*
	 * function to find Leads/Contacts/Accounts 
	 * based on phoneNumber(without special characters)
	 */

	function getCustomerByPhone($number){

		$number = substr($number, -10);
		
		global $current_user;
	
		if(!$current_user) {
			$current_user = Users::getActiveAdminUser();
		}
		
		$adb = PearDatabase::getInstance();
		$result = array('crmid' => '', 'setype' => '');
		
		
		$lookUpQuery = "SELECT * FROM vtiger_crm_phonenumbers inner join 
		vtiger_crmentity on vtiger_crmentity.crmid = vtiger_crm_phonenumbers.crmid  
		WHERE deleted = 0 and (phone = '" . $number . "' OR phone like '%" . $number . "')";
		
		$lookUpResult = $adb->pquery($lookUpQuery, array());
		
		$numRows = $adb->num_rows($lookUpResult);
		
		if($numRows){
			
			if($numRows == 1){
	
				$crmid = $adb->query_result($lookUpResult, 0, 'crmid');
				$result['crmid'] = $crmid;
				$result['setype'] = getSalesEntityType($crmid);
			
			} else {
				
				$t_crmid = ''; $t_setype = '';
				
				for($i=0; $i<$numRows; $i++){
					$t_crmid = $adb->query_result($lookUpResult, $i, 'crmid');
					if(getSalesEntityType($t_crmid) == 'Contacts'){
						break;
					}
				}
				
				$result['crmid'] = $t_crmid;
				$result['setype'] = getSalesEntityType($t_crmid);		
			}
		}

		return $result;
	}

	/*
	 * function to save callLog as PBXManager Record in database
	 */
		
	function saveRecord($params){
	
		$assigned_user = (isset($params['user'])) ? $params['user'] : '';
		
		$sid = $params['sourceuuid'];
	
		$details = array_change_key_case($params, CASE_LOWER);
		
		$moduleModel = Vtiger_Module_Model::getInstance('PBXManager');

		$recordModel = PBXManager_Record_Model::getCleanInstance();        
	
		$db = PearDatabase::getInstance();

		$exists_result = $db->pquery('SELECT * FROM vtiger_pbxmanager 
		INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_pbxmanager.pbxmanagerid 
		WHERE vtiger_pbxmanager.sourceuuid=? and 
		vtiger_crmentity.deleted = 0', array($sid));

		if($db->num_rows($exists_result)){
			$rowData = $db->query_result_rowdata($exists_result, 0);
			$recordModel->setData($rowData);        
		}        
   

  	  	$fieldModelList = $moduleModel->getFields();
    
    	foreach ($fieldModelList as $fieldName => $fieldModel) {	        	
    		if(isset($details[$fieldName])){
           		$fieldValue = $details[$fieldName];
            	$recordModel->set($fieldName, $fieldValue);
    		}
    	}
    
	    if($recordModel->get('pbxmanagerid') == ''){
			$recordModel->set('mode', '');
		} else {
			$recordModel->set('mode', 'edit');
			$recordModel->setId($recordModel->get('pbxmanagerid'));
		}
		
		$moduleModel->saveRecord($recordModel);

    	if( $assigned_user != '' && $assigned_user > 0 ){
    		$db->pquery("UPDATE vtiger_crmentity 
    		INNER JOIN vtiger_pbxmanager on vtiger_pbxmanager.pbxmanagerid = vtiger_crmentity.crmid 
    		SET smownerid = ? WHERE vtiger_crmentity.deleted = 0 and 
    		BINARY sourceuuid = ?", array($assigned_user, $sid));
    	}
    
	    $recordingResult = $db->pquery("select * from vtiger_pbxmanager_recordings 
	    where BINARY sourceuuid = ?",array($sid));
	    
	    if($db->num_rows($recordingResult)){
	    	$recordingurl = $db->query_result($recordingResult, 0, 'recordingurl');
	    	$db->pquery("UPDATE vtiger_pbxmanager set recordingurl = ? where BINARY 
	    	sourceuuid = ?",array($recordingurl, $sid));
	    }
	    
	}

	/* ===== END : COMMON FUNCTIONS USED IN ABOVE CODE ===== */

?>