<?php

class MandrillAPI {
    
    public $apikey = '2k8ICokplLZb2z8m4O2bBA';
    public $ch;
    public $root = 'https://mandrillapp.com/api/1.0';
    public $debug = false;
    
	public function __construct($apikey=null) {
		
		if( $apikey != '' && $apikey != null ){
			$this->apikey = $apikey;
		}
		
        $this->ch = curl_init();
        
        //curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mandrill-PHP/1.0.54');
        
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
	
        curl_setopt($this->ch, CURLOPT_POST, true);
        
        //curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 600);

        $this->root = rtrim($this->root, '/') . '/';
	}
	
	
	public function call($url, $params){
		
		$params['key'] = $this->apikey;
        $params = json_encode($params);
        $ch = $this->ch;

        curl_setopt($ch, CURLOPT_URL, $this->root . $url . '.json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_VERBOSE, $this->debug);


        $response_body = curl_exec($ch);

        $info = curl_getinfo($ch);
        if(curl_error($ch)) {
            $result = curl_error($ch);
        }
        
        $result = json_decode($response_body, true);

        return $result;
	}
	
	public function get_templateList($label = ''){
		$_params = array();
		if( $label != '' ){
			$_params["label"] = $label;
		}		
        return $this->call('templates/list', $_params);
	}
	
	
	public function sendTemplate($template_name, $template_content, $message, $async=false, $ip_pool=null, $send_at=null) {
        $_params = array("template_name" => $template_name, "template_content" => $template_content, "message" => $message, "async" => $async, "ip_pool" => $ip_pool, "send_at" => $send_at);
        return $this->call('messages/send-template', $_params);
    }
    
	
}
?>