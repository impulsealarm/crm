<?php

function vtws_getrealtor($element_data, $user){

	global $adb;
		
	if(isset($element_data['realtor_phone']) &&  $element_data['realtor_phone'])
		$realtor_phone_no = $element_data['realtor_phone'];
	
	if(isset($element_data['realtor_email']) &&  $element_data['realtor_email'])
		$realtor_email = $element_data['realtor_email'];
		
	$query = " SELECT vtiger_realtor.realtorid FROM vtiger_realtor INNER JOIN vtiger_crmentity on 
	vtiger_crmentity.crmid = vtiger_realtor.realtorid ";
	
	if( 
		isset($realtor_phone_no) && $realtor_phone_no != '' && $realtor_phone_no != 0
	){
		
		$filteredPhone = preg_replace('/\D/', '', $realtor_phone_no);
		
		$filteredPhone = substr($filteredPhone, -10);
		
		if(strlen($filteredPhone) > 6){
		
			$query1 = $query . " INNER JOIN vtiger_crm_phonenumbers on vtiger_crm_phonenumbers.crmid = vtiger_realtor.realtorid 
			where vtiger_crmentity.deleted = 0 AND vtiger_crm_phonenumbers.phone like '%{$filteredPhone}' ";	
		
			$realtorResult = $adb->pquery($query1, array());
			
			if($adb->num_rows($realtorResult)){
				
				$realtor_id = $adb->query_result($realtorResult, 0, 'realtorid');
				
				$realtor_id = vtws_getWebserviceEntityId('Realtor', $realtor_id);
				
				return array("realtor_id" => $realtor_id);
			}
		}
	}
	
	if( isset($realtor_email) && $realtor_email != '' ){			
		
		$query1 = $query . " WHERE  vtiger_realtor.primary_email = '{$realtor_email}' and vtiger_crmentity.deleted = 0 ";

		$realtorResult = $adb->pquery($query1, array());
			
		if($adb->num_rows($realtorResult)){
			
			$realtor_id = $adb->query_result($realtorResult, 0, 'realtorid');
			
			$realtor_id = vtws_getWebserviceEntityId('Realtor', $realtor_id);
			
			return array("realtor_id" => $realtor_id);
		}
	
	}
	
	
	return array("realtor_id" => 0);
	
}