<?php

	use Stripe\Stripe;
	use Stripe\Account;
	
	require('include/Stripe/init.php');
				
	function vtws_updatestripeaccount($element,$user){
		
		global $adb;
		
		if(!empty($element)){
			
			$manage_account = new ManageAccount();
			
			$update_data = $response = array();
		
			$update_data = $element;
			
			$update_data['first_name'] = $element['firstname'];
			
			$update_data['last_name'] = $element['lastname'];
			
			$update_data['REMOTE_ADDR'] = $element['REMOTE_ADDR'];
		
			if(isset($element['stripe_account_id']) && $element['stripe_account_id'] != '' && $element['stripe_account_id'] != '0'){
				
				$manage_account->managed_accountId = $element['stripe_account_id'];
			
			} else { // Create new stripe account
		
				$new_acc_info = $manage_account->CreateAccount($element['country']);
				
				$new_acc_id = $new_acc_info['id'];
		
				$manage_account->managed_accountId = $new_acc_id;
				
				$realtor_result = $adb->pquery("select * from vtiger_realtor inner join 
				vtiger_crmentity on crmid = realtorid where deleted  = 0 and primary_email = ?",array($element['email']));
				
				if($adb->num_rows($realtor_result)){
					$realtor_id = $adb->query_result($realtor_result, 0, "realtorid");
					
					
					$adb->pquery("update vtiger_realtor set stripe_account_id = ? where realtorid = ?",
					array($new_acc_id, $realtor_id));
					
					$realtor_obj = CRMEntity::getInstance("Realtor");
					$realtor_obj->id = $realtor_id;
					$realtor_obj->mode = "edit";
					$realtor_obj->retrieve_entity_info($realtor_id, "Realtor");
					$realtor_obj->column_fields['cf_919'] = 1;
					$realtor_obj->column_fields['cf_923'] = 0;
					$realtor_obj->save("Realtor");
					
					
					
				}
				
			
			}	

			$updated_response = $manage_account->UpdateAccountDetails($update_data);
			
			if(!empty($updated_response) && is_array($updated_response) && isset($updated_response['type']) && isset($updated_response['message'])){
		
				$updated_response['message'] = "Invalid Details.";
				
				$response['error'] = true;
			
				$response['result'] = $updated_response;
		
			} else {
			
				$stripe_token = $element['stripeToken'];
				
				$ext_acc_response = $manage_account->createExternalAccount($stripe_token);
				
				if(!empty($ext_acc_response) && isset($ext_acc_response['type']) && isset($ext_acc_response['message'])){
					
					//  To get the exact error Message comment below line.
					
					$ext_acc_response['message'] =  "Invalid Details";
					
					//$account = $manage_account->getAccountObject(); //To get the account Info
		
					//$accountInfo = $account->getLastResponse()->json;
					
					//$ext_acc_response['account_info'] = $accountInfo;
					
					$response['error'] = true;
					
					$response['result'] = $ext_acc_response;
				} else {
								
					$account = $manage_account->getAccountObject();
		
					$accountInfo = $account->getLastResponse()->json;
	
					$response['success'] = true;

					$response['result'] = $accountInfo; 
				}
			}
		}
			
		return $response;
	}

class ManageAccount {
		
	public $managed_accountId;
		
	function __construct() {
		
		global $Stripe_secretKey;
		
		Stripe::setApiKey($Stripe_secretKey);
	}
		
	function CreateAccount($country){
		
		$accountDetails = Account::create(array(
		  "managed" => true,
		  "country" => $country,
		));
		
		$accountInfo = $accountDetails->__toArray(true);
		
		$this->managed_accountId = $accountInfo['id'];
		
		return $accountInfo;
	}
	
	function getAccountObject(){
		
		$managed_accountId = $this->managed_accountId;
		
		//$account = Account::retrieve($managed_accountId);
		try {
			$account = Account::retrieve($managed_accountId);
		} catch(Exception $e){
			return $e->getMessage();
		}
		
		return $account;
	}
	
	function UpdateAccountDetails($data){
		
		$account = $this->getAccountObject();
		
		/*$dob = $data['dob'];	// Format mm/dd/yy
	
		$dob = explode("/",$dob);
		
		if(!empty($dob)){
			$day = $dob['0'];
			$month = $dob['1'];
			$year = $dob['2'];
			
			if($day < '10' && strlen($day) == '2')
				$day = substr($day,1);
		}
		*/
		
		$dob = $data['dob'];	// Format mm/dd/yy 
	
		$dob = explode("/",$dob);
		
		if(!empty($dob)){
			$month = $dob['0'];
			$day = $dob['1'];
			$year = $dob['2'];
		
		
			if($day < '10' && strlen($day) == '2')
				$day = substr($day,1);
		}
		
		$account->email = $data['email'];
		$account->legal_entity->first_name = $data['first_name'];
		$account->legal_entity->last_name = $data['last_name'];
		$account->legal_entity->type = $data['type'];
		$account->legal_entity->dob->day = $day;
		$account->legal_entity->dob->month = $month;
		$account->legal_entity->dob->year = $year;
		$account->tos_acceptance->date = time();
		$account->tos_acceptance->ip = $data['REMOTE_ADDR'];
		
		try {
			$res = $account->save();
			return $res;
		} catch(\Stripe\Error\Card $e) {
  			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\RateLimit $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\InvalidRequest $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\Authentication $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\ApiConnection $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\Base $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (Exception $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		}
	}
	
	function createExternalAccount($token){
		
		$account = $this->getAccountObject();
		try {
			$response = $account->external_accounts->create(
				array("external_account" => $token,
				"default_for_currency" => true)
			);
			return $response;
		} catch(\Stripe\Error\Card $e) {
  			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\RateLimit $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\InvalidRequest $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\Authentication $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\ApiConnection $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (\Stripe\Error\Base $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		} catch (Exception $e) {
			$body = $e->getJsonBody();
 			$err  = $body['error'];
 			return $err;
		}
	}

}
?>