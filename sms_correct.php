<?php
	require_once('includes/main/WebUI.php');

	require 'modules/Twilio/Services/Twilio.php';

	global $adb, $current_user;
	
	$adb = PearDatabase::getInstance();


        $smsnotifier_ids  = array();
 
        $smsnotifier_result = $adb->pquery("select * from vtiger_smsnotifier_status", array());
        
        for($i = 0; $i < $adb->num_rows($smsnotifier_result); $i++){
           $smsnotifier_ids[] = $adb->query_result($smsnotifier_result, $i, "smsnotifierid");
        }

       
        $smsnotifier_result = $adb->pquery("select * from vtiger_smsnotifier", array());
        
        for($i = 0; $i < $adb->num_rows($smsnotifier_result); $i++){
           if(!in_array($adb->query_result($smsnotifier_result, $i, "smsnotifierid"), $smsnotifier_ids)){
               $adb->pquery("update vtiger_crmentity set deleted = 1 where crmid = ?", 
               array($adb->query_result($smsnotifier_result, $i, "smsnotifierid")));

           }
        }

exit;






	global $current_user;

	$current_user = Users::getActiveAdminUser();
	
	$search_time = date('Y-m-d H:i:s',strtotime('-30 minutes'));

 	$datetime = new DateTime("".$search_time);
 	
	$datetime->setTimeZone(new DateTimeZone('UTC'));
 	
 	$search_time = $datetime->format('Y-m-d H:i:s');

	// Twilio REST API version
	$version = '2010-04-01';
	
	require_once 'modules/Twilio/Services/Twilio/Capability.php';		
	
	$serverModel = PBXManager_Server_Model::getInstance();
	
	$accountSid = $serverModel->get ( "webappurl" );
	
	$token = $serverModel->get ( "outboundcontext" );
	
	$client = new Services_Twilio($accountSid, $token, $version);
		
	foreach ($client->account->sms_messages->getIterator(0, 10, array("DateUpdated>" => $search_time)) as $sms){ 
		
                $from_no = $sms->from;

	        $to_no = $sms->to;
		
                $status = $sms->status;

		$direction = $sms->direction;
		
		$body = $sms->body;
		
		$smsid = $sms->sid;
		
		$to_no = preg_replace ( '/\D+/', '', $to_no);
	    
		$from_no = preg_replace ( '/\D+/', '', $from_no);

		$customer_result = getCustomerByPhone($to_no);
	
		$user_id = '';

		$se_type = '';

		$crmid = '';

		if(!empty($customer_result) && $customer_result['crmid'] != ''){
			
			$crmid = $customer_result['crmid'];
			
			$se_type = $customer_result['setype'];
			
		} else {
			
			$user_result = getUserInfoWithNumber($to_no);
			
			if(!empty($user_result)){
			    $user_id = $user_result['id'];
			}
			
		}
	
		$customer_result = getCustomerByPhone($from_no);
		
                if(!empty($customer_result) && $customer_result['crmid'] != ''){
			
			$crmid = $customer_result['crmid'];
			
			$se_type = $customer_result['setype'];
			
		} else {
		
			$user_result = getUserInfoWithNumber($from_no);

                        if(!empty($user_result)){

                            if($user_id == $user_result['id']) 
                               continue; 
                            else 
                              $user_id = $user_result['id'];


			}
			
		}

                if($user_id == '' || $crmid == '') continue;
	
		$alreadyExists = $adb->pquery("select * FROM vtiger_smsnotifier 
		INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_smsnotifier.smsnotifierid 
		INNER JOIN vtiger_smsnotifier_status on vtiger_smsnotifier_status.smsnotifierid = vtiger_smsnotifier.smsnotifierid 
		WHERE vtiger_crmentity.deleted = 0 and vtiger_smsnotifier_status.smsmessageid = ?
		",array($smsid));
		
		if($adb->num_rows($alreadyExists)){
			
			$adb->pquery("update vtiger_smsnotifier_status set status = ?, fromnumber = ?, tonumber = ? where vtiger_smsnotifier_status.smsmessageid = ?",
			array($status, $from_no, $to_no, $smsid));
			
			$smsnotifier_id = $adb->query_result($alreadyExists, 0, "crmid");
			
			$check_result = $adb->pquery("select * from vtiger_crmentityrel where crmid = ? and relcrmid = ?", array($smsnotifier_id, $crmid));
			
			if(! $adb->num_rows($check_result)){
				$adb->pquery("insert into vtiger_crmentityrel values(?,?,?,?)", array($smsnotifier_id, "SMSNotifier", $crmid, $se_type));
			}
			
		}
	}
























		/*
 			* function to get user crm_extension based on username
 		*/

		function getUserPhone($username = ''){	
			
			$adb = PearDatabase::getInstance();
	
			$result = '';
	
			if($username == '') return '';
			
			$query_result = $adb->pquery("select * from vtiger_users 
			where user_name = ?",array($username));
	
			if($adb->num_rows($query_result)){
				$result = $adb->query_result($query_result, 0, 'phone_crm_extension');
			}
			
			return $result;	
		}

		/*
 		* function to get userid based on username
 		*/

		function getUserId($username = ''){	
			$adb = PearDatabase::getInstance();
			
			$result = '';
	
			if($username == '') return '';
	
			$query_result = $adb->pquery("select * from vtiger_users 
			where user_name = ?",array($username));
	
			if($adb->num_rows($query_result)){
				$result = $adb->query_result($query_result, 0, 'id');
			}
			
			
			return $result;	
		}

		/*
		 * function to find and get user detail 
		 * based on crm_extension (without special characters)
		 */
		
		function getUserInfoWithNumber($number){
			
			$adb = PearDatabase::getInstance();
			
			$result = array('id' => '', 'setype' => '');
			
			$number = substr($number, -10);
		
		    $recordQuery = "SELECT * FROM vtiger_users WHERE 
		    (phone_crm_extension = '" . $number . "' OR phone_crm_extension like '%" . $number . "')";
			
			$recordResult = $adb->pquery($recordQuery,array());
			
			if($adb->num_rows($recordResult)){
				
				$crmid = $adb->query_result($recordResult, 0, 'id');
				
				if($crmid){
					$result['id'] = $crmid;
					$result['setype'] = "Users";
				}
				
			}
			
			return $result;
			
		}

		/*
		 * function to find Leads/Contacts/Accounts 
		 * based on phoneNumber(without special characters)
		 */

		function getCustomerByPhone($number){
	
			$number = substr($number, -10);
			
			global $current_user;
		
			if(!$current_user) {
				$current_user = Users::getActiveAdminUser();
			}
			
			$adb = PearDatabase::getInstance();
			$result = array('crmid' => '', 'setype' => '');
			
			
			$lookUpQuery = "SELECT * FROM vtiger_crm_phonenumbers inner join 
			vtiger_crmentity on vtiger_crmentity.crmid = vtiger_crm_phonenumbers.crmid  
			WHERE deleted = 0 and (phone = '" . $number . "' OR phone like '%" . $number . "')";
			
			$lookUpResult = $adb->pquery($lookUpQuery, array());
			
			$numRows = $adb->num_rows($lookUpResult);
			
			if($numRows){
				
				if($numRows == 1){
		
					$crmid = $adb->query_result($lookUpResult, 0, 'crmid');
					$result['crmid'] = $crmid;
					$result['setype'] = getSalesEntityType($crmid);
				
				} else {
					
					$t_crmid = ''; $t_setype = '';
					
					for($i=0; $i<$numRows; $i++){
						$t_crmid = $adb->query_result($lookUpResult, $i, 'crmid');
						if(getSalesEntityType($t_crmid) == 'Contacts'){
							break;
						}
					}
					
					$result['crmid'] = $t_crmid;
					$result['setype'] = getSalesEntityType($t_crmid);		
				}
			}
	
			return $result;
		}

		
		function saveRecord($params){
	
			$assigned_user = (isset($params['user'])) ? $params['user'] : '';
			
			$sid = $params['sourceuuid'];
	
			$details = array_change_key_case($params, CASE_LOWER);
			
			$moduleModel = Vtiger_Module_Model::getInstance('PBXManager');

			$recordModel = PBXManager_Record_Model::getCleanInstance();        
	
			$db = PearDatabase::getInstance();

			$exists_result = $db->pquery('SELECT * FROM vtiger_pbxmanager 
			INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_pbxmanager.pbxmanagerid 
			WHERE vtiger_pbxmanager.sourceuuid=? and 
			vtiger_crmentity.deleted = 0', array($sid));
	
			if($db->num_rows($exists_result)){
				$rowData = $db->query_result_rowdata($exists_result, 0);
				$recordModel->setData($rowData);        
			}        
   
	
  		  	$fieldModelList = $moduleModel->getFields();
    
    		foreach ($fieldModelList as $fieldName => $fieldModel) {	        	
    			if(isset($details[$fieldName])){
           			$fieldValue = $details[$fieldName];
            		$recordModel->set($fieldName, $fieldValue);
    			}
    		}
    
		    if($recordModel->get('pbxmanagerid') == ''){
				$recordModel->set('mode', '');
			} else {
				$recordModel->set('mode', 'edit');
				$recordModel->setId($recordModel->get('pbxmanagerid'));
			}
			
			$moduleModel->saveRecord($recordModel);
	
    		if( $assigned_user != '' && $assigned_user > 0 ){
    			$db->pquery("UPDATE vtiger_crmentity 
    			INNER JOIN vtiger_pbxmanager on vtiger_pbxmanager.pbxmanagerid = vtiger_crmentity.crmid 
    			SET smownerid = ? WHERE vtiger_crmentity.deleted = 0 and 
    			BINARY sourceuuid = ?", array($assigned_user, $sid));
    		}
    
		    $recordingResult = $db->pquery("select * from vtiger_pbxmanager_recordings 
		    where BINARY sourceuuid = ?",array($sid));
		    
		    if($db->num_rows($recordingResult)){
		    	$recordingurl = $db->query_result($recordingResult, 0, 'recordingurl');
		    	$db->pquery("UPDATE vtiger_pbxmanager set recordingurl = ? where BINARY 
		    	sourceuuid = ?",array($recordingurl, $sid));
		    }
		    
		}

		function filterCRMPhoneNumbers($already_synced = false, $sync_from_time, $current_user){
			
			$adb = PearDatabase::getInstance();
			
			$modules = array('Leads','Contacts', 'Accounts'); 
			
			$fieldNames = array();
			
			$phone_uiType = '11';
			
			foreach($modules as $moduleName){
				
				$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
			   
				$fieldsResult = $adb->pquery("select * from vtiger_field where uitype = ? and 
				tabid = ?", array($phone_uiType, $moduleModel->get('id')));
				
				$total_phone_fields = $adb->num_rows($fieldsResult);
				
				if( $total_phone_fields ){
					for( $i=0; $i<$total_phone_fields; $i++ ){
						$fieldNames[] = $adb->query_result($fieldsResult, $i, 'fieldname');
					}
				}
			   	
				$queryGenerator = new QueryGenerator($moduleName, $current_user);
		
				$queryGenerator->setFields(array_merge(array('id'),$fieldNames));
				
				$controller = new ListViewController($adb, $current_user, $queryGenerator);
			
				$recordQuery = $queryGenerator->getQuery();
		   
				$recordResult = $adb->pquery($recordQuery,array());
				
				$totalrecords = $adb->num_rows($recordResult);
					
				if($totalrecords){
					
					for($k=0; $k<$totalrecords; $k++){
						
						$crmid = $adb->query_result($recordResult, $k, $moduleModel->basetableid);
				
						$adb->pquery("delete from vtiger_crm_phonenumbers where crmid = ?",array($crmid));
						
						foreach($fieldNames as $fieldd){
							
							$phn = $adb->query_result($recordResult, $k, $fieldd);
							
							if($phn != ''){
								
								$phn = preg_replace ( '/\D+/', '', $phn);
			
								$adb->pquery("Insert into vtiger_crm_phonenumbers 
								set crmid = ?, phone = ?",array($crmid, $phn));
							
							}
						}
					}
				}
			}
		}
?>