<?php

ini_set ( "include_path", "../../" );
 
require "modules/Twilio/twilio-php/Services/Twilio.php"; 
require_once ('includes/main/WebUI.php');


// Your Account Sid and Auth Token from twilio.com/user/account

//$accountSid = 'AC3a9ccfb3c6f0f0b6aae6b389b847d399';	//live cred
//$authToken = '7d8f1ec51d2430a4b99dd9f62eb654c7';

/*
$_REQUEST['to'] = '+919501419020';
$_REQUEST['from'] = '';
$_REQUEST['message'] = 'Test SMS';
*/


$requestParams = $_REQUEST;


global $adb;
$adb = PearDatabase::getInstance();

$sms_credResult = $adb->pquery("select * from vtiger_smsnotifier_servers 
WHERE isactive = '1' and providertype = 'Twilio' ",array());

if($adb->num_rows($sms_credResult)){
	
	$accountSid = trim($adb->query_result($sms_credResult, 0, 'username'));
	$authToken = trim($adb->query_result($sms_credResult, 0, 'password'));
	
	$toNo = preg_replace('/[^0-9]/','',$_REQUEST['to']);
	$message = $_REQUEST['message'];
	$fromNo = '';
	
	if( isset($_REQUEST['from']) && $_REQUEST['from'] != '' ){
		$fromNo = preg_replace('/[^0-9]/','',$_REQUEST['from']);
	} else {
		$user = Users::getActiveAdminUser();
		$fromNo = $user->phone_crm_extension;
	}

	if( $fromNo != '' && $toNo != '' && $message != '' && $accountSid != '' && $authToken != '' ){
		
		if(strlen($fromNo) > 10){
			$fromNo = '+' . $fromNo;
		} else {
			$fromNo = '+1' . $fromNo;
		}
		
		if(strlen($toNo) > 10){
			$toNo = '+' . $toNo;
		} else {
			$toNo = '+1' . $toNo;
		}
		
		try{
			
			$http = new Services_Twilio_TinyHttp(
			    'https://api.twilio.com',
			    array('curlopts' => array(
			        CURLOPT_SSL_VERIFYPEER => false,
			        //CURLOPT_SSL_VERIFYHOST => 2,
			    ))
			);
			
			$client = new Services_Twilio($accountSid, $authToken, "2010-04-01", $http);
			
			$sms = $client->account->messages->create(array(
			  "From" => $fromNo,
			  "To" => $toNo,
			  "Body" => $message,
			  "StatusCallback" => "http://crm.impulsealarm.com/modules/Twilio/sms_status.php"
			));
			
			
			echo $sms->sid;
			

		}catch(Exception $e){			
			echo $e->getMessage();
		}
		
	} else {
		echo "Missing Mandatory Fields";
	}
	
}

exit;