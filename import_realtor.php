<?php
include("includes/main/WebUI.php");

$userid = 1;

$user_obj = CRMEntity::getInstance("Users");
$user_obj->id = $userid;
$user_obj->retrieve_entity_info($userid,"Users");
vglobal("current_user", $user_obj);

CreateRealtorFromLeads();

function CreateRealtorFromLeads(){
	
	global $adb;
	
	$lead_realtor_mapping = array(
		"firstname" => "firstname",
	    "phone" => "primary_phone",
	    "lastname" => "lastname",
	    "mobile" => "mobile_phone",
	    "company" => "company",
	    "fax" => "fax",
	    "designation" => "designation",
	    "email" => "primary_email",
	    "website" => "website",
	    "industry" => "industry",
	    "annualrevenue" => "annual_revenue",
	    "rating" => "rating",
	    "noofemployees" => "number_employees",
	    "secondaryemail" => "secondary_email",
	    "lane" => "street",
	    "code" => "postal_code",
	    "city" => "city_address",
	    "country" => "country_adress",
	    "state" => "state_address",
	    "pobox" => "po_box",
	    "description" => "description",
	    "emailoptout" => "email_opt_out",
	    "cf_785" => "call_dispo",
	);
	
	
	$result = $adb->pquery("select vtiger_leaddetails.leadid, vtiger_crmentity.smownerid from vtiger_leaddetails
			inner join vtiger_crmentity on vtiger_crmentity.crmid = vtiger_leaddetails.leadid
			where vtiger_crmentity.deleted = 0 and vtiger_leaddetails.industry = ? order by vtiger_leaddetails.leadid ASC",array('Real Estate'));
	
	if($adb->num_rows($result)){
		
		while($row = $adb->fetchByAssoc($result)){
			
			$leadid = $row['leadid'];
			
			$result = $adb->pquery("select * from vtiger_realtor where leadid = ?",array($leadid));
	
			if($adb->num_rows($result) > 0){
				continue;
			}
			
			$user_id = $row['smownerid'];
			
			$lead_obj = CRMEntity::getInstance("Leads");
	
			$lead_obj->id = $leadid;
			
			$lead_obj->retrieve_entity_info($leadid, "Leads");
			
			$realtor_detail = array();
			
			foreach($lead_realtor_mapping as $lead_field_name => $realtor_field_name){
				if($lead_obj->column_fields[$lead_field_name])
					$realtor_detail[$realtor_field_name] = $lead_obj->column_fields[$lead_field_name];
			}
			
			CreateRealtor($realtor_detail, $user_id, $leadid);
		}
	}
}


function CreateRealtor($data, $userid, $leadid){
	
	$realtor_id = 0;
	
	$adb = PearDatabase::getInstance();
	
	$realtor_obj = CRMEntity::getInstance("Realtor");
	
	$result = $adb->pquery("select * from vtiger_realtor where leadid = ?",array($leadid));
	
	if($adb->num_rows($result) > 0){
		$realtor_id = $adb->query_result($result, 0, "realtorid");
	}
	
	
	if(!$realtor_id){
	
		$query .= " SELECT vtiger_realtor.realtorid FROM vtiger_realtor INNER JOIN vtiger_crmentity on 
		vtiger_crmentity.crmid = vtiger_realtor.realtorid  AND vtiger_crmentity.deleted = 0 ";
		
		if( isset($data['primary_phone']) && $data['primary_phone'] != '' && $data['primary_phone'] != 0){
				
			$filteredPhone = preg_replace('/\D/', '', $data['primary_phone']);
			
			$filteredPhone = substr($filteredPhone, -10);
				
			if(strlen($filteredPhone) > 6){
				
				$query1 = $query . " INNER JOIN vtiger_crm_phonenumbers on vtiger_crm_phonenumbers.crmid = vtiger_realtor.realtorid 
				AND vtiger_crm_phonenumbers.phone like '%{$filteredPhone}' ";	

				$phoneResult = $adb->pquery($query1, array());
				
				if($adb->num_rows($phoneResult)){
					$realtor_id = $adb->query_result($phoneResult, 0, 'realtorid');
				}
				
			}
			
			if( !$realtor_id ){
			
				if( isset($data['primary_email']) && $data['primary_email'] != '' ){			
					$query1 = $query . " WHERE  primary_email = '{$data['primary_email']}' ";	
					$emailResult = $adb->pquery($query1, array());
					if($adb->num_rows($emailResult)){
						$realtor_id = $adb->query_result($emailResult, 0, 'realtorid');
					}
				}
			}
		}
	}

	if($realtor_id == 0){
		
		foreach($data as $name => $value){
			
			$realtor_obj->column_fields[$name] = $value;
		}
	
		$realtor_obj->column_fields['assigned_user_id'] = $userid;
		
		$realtor_obj->save("Realtor");
		
		$adb->pquery("update vtiger_realtor set leadid = ? where realtorid = ?", array($leadid, $realtor_obj->id));
		
	} else {
		$adb->pquery("update vtiger_realtor set leadid = ? where realtorid = ?", array($leadid, $realtor_id));
	}
	
}