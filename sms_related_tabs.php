<?php

$Vtiger_Utils_Log = true;
define('VTIGER6_REL_DIR', '');
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');
include_once('vtlib/Vtiger/Event.php');


$relatedModule = Vtiger_Module::getInstance("SMSNotifier");

$forModules = array('Leads','Accounts','Contacts');

foreach($forModules as $parentModule){
	$modInstance = Vtiger_Module::getInstance($parentModule);
	$modInstance->setRelatedList($relatedModule, $label='SMS', false, 'get_related_list');
}