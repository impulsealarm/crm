<?php /* Smarty version Smarty-3.1.7, created on 2015-08-27 11:56:59
         compiled from "/home/imagineavs/public_html/crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/TwilioOutgoing.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9824330855d1a5f82e99e6-43088220%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7efd0dc820123da1e31f00220e983d18dc770b04' => 
    array (
      0 => '/home/imagineavs/public_html/crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/TwilioOutgoing.tpl',
      1 => 1440676589,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9824330855d1a5f82e99e6-43088220',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_55d1a5f835bd6',
  'variables' => 
  array (
    'TWILIO_TOKEN' => 0,
    'MODULE' => 0,
    'TONUMBER' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55d1a5f835bd6')) {function content_55d1a5f835bd6($_smarty_tpl) {?><script type="text/javascript">
	Twilio.Device.setup("<?php echo $_smarty_tpl->tpl_vars['TWILIO_TOKEN']->value;?>
");
</script>


<div id="twilioCallContainer" class='modelContainer'>
	<div class="modal-header contentsBackground">
		<button data-dismiss="modal" class="close"
			title="<?php echo vtranslate('LBL_CLOSE');?>
">&times;</button>
		<span><strong><?php echo vtranslate('Calling to dialed number',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></span>
	</div>
	<div class="modal-body tabbable">
		&nbsp;&nbsp;
		<input type="text" id="number" name="number" value="<?php echo $_smarty_tpl->tpl_vars['TONUMBER']->value;?>
"  placeholder="Enter a phone number to call" />
		<hr>
		<button class="btn btn-success" onclick="Vtiger_Twilio_Js.call();">Call</button>
		<button class="btn btn-danger" onclick="Vtiger_Twilio_Js.hangup();">Hangup</button>
		<button class="btn btn-info" onclick="Vtiger_Twilio_Js.hold();" style="display:''" id='btnhold'>Hold</button>
		<button class="btn btn-info" onclick="Vtiger_Twilio_Js.unhold();" style="display:none" id='btnunhold'>UnHold</button>
		<button class="btn btn-warning" onclick="Vtiger_Twilio_Js.transfer();">Transfer</button>
		<hr>
		&nbsp;&nbsp;
		<div id="log">Loading pigeons...</div>
		<div id="dialpad">
				<table>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;width: 60px; font-weight: bold;" value="1" id="button1"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;width: 60px; font-weight: bold;" value="2" id="button2"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;width: 60px; font-weight: bold;" value="3" id="button3"></td>
				</tr>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="4" id="button4"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="5" id="button5"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="6" id="button6"></td>
				</tr>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="7" id="button7"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="8" id="button8"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="9" id="button9"></td>
				</tr>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="*" id="buttonstar"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="0" id="button0"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="#" id="buttonpound"></td>
				</tr>
				</table>
			</div>
	</div>
</div><?php }} ?>