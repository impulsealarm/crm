<?php /* Smarty version Smarty-3.1.7, created on 2015-08-16 04:55:21
         compiled from "/home/imagineavs/public_html/crm/includes/runtime/../../layouts/vlayout/modules/Settings/RelatedListviewEdits/Settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:169551855355d017b960f143-20128191%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fb8a15fd03764100e6230860b3244fec2e713a51' => 
    array (
      0 => '/home/imagineavs/public_html/crm/includes/runtime/../../layouts/vlayout/modules/Settings/RelatedListviewEdits/Settings.tpl',
      1 => 1439672287,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '169551855355d017b960f143-20128191',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'QUALIFIED_MODULE' => 0,
    'MODULE' => 0,
    'LIST_MODULES' => 0,
    'MODULE_ITEM' => 0,
    'CURRENT_MODULE' => 0,
    'FIELD' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_55d017b976c34',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55d017b976c34')) {function content_55d017b976c34($_smarty_tpl) {?>
 
<div class="container-fluid">
    <form id="rle_form" name="rleForm" action="" method="post">
        <div class="contentHeader row-fluid">
            <h3 class="span8 textOverflowEllipsis"><?php echo vtranslate('LBL_SETTING_HEADER',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</h3>
            <span class="pull-right"><button type="button" id="rel_setting_save_btn" class="btn btn-success"><strong><?php echo vtranslate('LBL_SAVE',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></button></span>
        </div>
        <hr>
        <div class="clearfix"></div>

        <div class="listViewContentDiv row-fluid" id="listViewContents">
            <div class="row marginBottom10px">
                <div class="span4">
                    <label><?php echo vtranslate('LBL_SELECT_MODULE',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</label>
                </div>
                <div class="span8">
                    <select name="current_module" class="chzn-select" onchange="this.form.submit();">
                    <?php  $_smarty_tpl->tpl_vars['MODULE_ITEM'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['MODULE_ITEM']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['LIST_MODULES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['MODULE_ITEM']->key => $_smarty_tpl->tpl_vars['MODULE_ITEM']->value){
$_smarty_tpl->tpl_vars['MODULE_ITEM']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['MODULE_ITEM']->value['name'];?>
" <?php if ($_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['name']==$_smarty_tpl->tpl_vars['MODULE_ITEM']->value['name']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['MODULE_ITEM']->value['tablabel'];?>
</option>
                    <?php } ?>
                    </select>
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span4">
                    <label><?php echo vtranslate('LBL_MODULE_ACTIVE',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</label>
                </div>
                <div class="span8">
                    <select name="module_active" class="chzn-select">
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['active']==1){?>selected<?php }?>><?php echo vtranslate('MODULE_ACTIVE_OPTION',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</option>
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['active']==0){?>selected<?php }?>><?php echo vtranslate('MODULE_INACTIVE_OPTION',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</option>
                    </select>
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span4">
                    <label><?php echo vtranslate('LBL_MODULE_ACTIVE_TYPE',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</label>
                </div>
                <div class="span8">
                    <select name="module_active_type" class="chzn-select">
                        <option value="List" <?php if ($_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['active_type']=='List'){?>selected<?php }?>><?php echo vtranslate('MODULE_ACTIVE_TYPE_OPTION_LISTVIEW',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</option>
                        <option value="RelatedList" <?php if ($_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['active_type']=='RelatedList'){?>selected<?php }?>><?php echo vtranslate('MODULE_ACTIVE_TYPE_OPTION_RELATEDLIST',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</option>
                        <option value="Both" <?php if ($_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['active_type']=='Both'){?>selected<?php }?>><?php echo vtranslate('MODULE_ACTIVE_TYPE_OPTION_BOTH',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</option>
                    </select>
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span4">
                    <label><?php echo vtranslate('LBL_ALLOW_EDIT_ALL_FIELDS',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</label>
                </div>
                <div class="span8">
                    <input value="1" name="allow_edit_all_fields" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['allow_edit_all_fields']==1){?>checked<?php }?> />
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span12">
                    <label><?php echo vtranslate('LBL_SELECT_FIELDS',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</label>
                </div>
                <div class="span12">
                    <select name="module_fields[]" class="chzn-select" multiple style="width: 100%;" data-placeholder="<?php echo vtranslate('SELECT_FIELDS_OPTION',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
">
                        <?php  $_smarty_tpl->tpl_vars['FIELD'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['FIELD']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['FIELD']->key => $_smarty_tpl->tpl_vars['FIELD']->value){
$_smarty_tpl->tpl_vars['FIELD']->_loop = true;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['FIELD']->value['fieldname'];?>
" <?php if ($_smarty_tpl->tpl_vars['FIELD']->value['selected']==1){?>selected<?php }?>><?php echo vtranslate($_smarty_tpl->tpl_vars['FIELD']->value['fieldlabel'],$_smarty_tpl->tpl_vars['CURRENT_MODULE']->value['module']);?>
</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>

<?php }} ?>