<?php /* Smarty version Smarty-3.1.7, created on 2016-01-13 06:09:14
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Realtor/UploadCSV.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17188746935695ea0a75ef06-37829194%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f7b2ec5e42a20b64741eea278f5372d5b10f368c' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Realtor/UploadCSV.tpl',
      1 => 1450761419,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17188746935695ea0a75ef06-37829194',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'CURRENT_USER_MODEL' => 0,
    'MODULE' => 0,
    'WIDTHTYPE' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_5695ea0a7953c',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5695ea0a7953c')) {function content_5695ea0a7953c($_smarty_tpl) {?>
<div>

	<div class='container-fluid editViewContainer'>
        
		<form class="form-horizontal recordEditView" id="UploadCSV" name="UploadCSV" method="post" action="index.php" enctype="multipart/form-data">
      
			<?php $_smarty_tpl->tpl_vars['WIDTHTYPE'] = new Smarty_variable($_smarty_tpl->tpl_vars['CURRENT_USER_MODEL']->value->get('rowheight'), null, 0);?>
			
			<input type="hidden" name="module" value="<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
" />
            <input type="hidden" name="action" value="UploadCSV" />
			
			<div class="contentHeader row-fluid">
                <h3 class="span8 textOverflowEllipsis" title="Upload File">Upload CSV File</h3>
                <span class="pull-right">
                    <button class="btn btn-success" type="submit"><strong><?php echo vtranslate('Upload',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></button>
                    
				</span>				
            </div>
            
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                <tbody>
                    <tr>
						<td class="fieldLabel <?php echo $_smarty_tpl->tpl_vars['WIDTHTYPE']->value;?>
">Select File
						</td>
						<td class="<?php echo $_smarty_tpl->tpl_vars['WIDTHTYPE']->value;?>
">
							<div class="fileUploadContainer">
								<input type="file" class="input-large" data-validation-engine="validate[required,funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" name="csvfile" 
								value="" />
								
								<div class="uploadedFileDetails">
									<div class="uploadedFileSize"></div>
									<div class="uploadedFileName"></div>
									
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
				
</div><?php }} ?>