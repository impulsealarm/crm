<?php /* Smarty version Smarty-3.1.7, created on 2015-10-20 08:12:28
         compiled from "/home/imagineavs/public_html/imp_crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/DetailPopup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3319923115625f76cd7e404-73630789%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f848486b57b9435f70c7452c984c77cb3cfb8e5' => 
    array (
      0 => '/home/imagineavs/public_html/imp_crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/DetailPopup.tpl',
      1 => 1445328547,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3319923115625f76cd7e404-73630789',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'MODULE_MODEL' => 0,
    'MODULE_NAME' => 0,
    'RECORD' => 0,
    'NAME_FIELD' => 0,
    'FIELD_MODEL' => 0,
    'PICKLIST_DEPENDENCY_DATASOURCE' => 0,
    'MODULE_SUMMARY' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_5625f76ce1d5a',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5625f76ce1d5a')) {function content_5625f76ce1d5a($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate (vtemplate_path('SidePopup.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="bodyContents">
	<div class="mainContainer row-fluid" style="margin-top: 1px;" >
		<div class="contentsDiv popupcontentsDiv marginLeftZero" id="rightPanel" style="min-height:550px;">
			
			<?php $_smarty_tpl->tpl_vars["MODULE_NAME"] = new Smarty_variable($_smarty_tpl->tpl_vars['MODULE_MODEL']->value->get('name'), null, 0);?>
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['MODULE_NAME']->value;?>
" id="module" name="module" />
			<input type="hidden" value="DetailPopup" id="view" name="view" />
			<input id="recordId" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['RECORD']->value->getId();?>
" />
			
			<div class="detailViewContainer">
			
				<div class="row-fluid detailViewTitle">
					<div class="span12">
						<div class="row-fluid">
						
						
							<div class="span3">
								<div class="row-fluid">
									<span class="span10 margin0px">
										<span class="row-fluid">
											<span class="recordLabel font-x-x-large textOverflowEllipsis span pushDown" title="<?php echo $_smarty_tpl->tpl_vars['RECORD']->value->getName();?>
">
												<?php  $_smarty_tpl->tpl_vars['NAME_FIELD'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['NAME_FIELD']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MODULE_MODEL']->value->getNameFields(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['NAME_FIELD']->key => $_smarty_tpl->tpl_vars['NAME_FIELD']->value){
$_smarty_tpl->tpl_vars['NAME_FIELD']->_loop = true;
?>
													<?php $_smarty_tpl->tpl_vars['FIELD_MODEL'] = new Smarty_variable($_smarty_tpl->tpl_vars['MODULE_MODEL']->value->getField($_smarty_tpl->tpl_vars['NAME_FIELD']->value), null, 0);?>
														<?php if ($_smarty_tpl->tpl_vars['FIELD_MODEL']->value->getPermissions()){?>
															<span class="<?php echo $_smarty_tpl->tpl_vars['NAME_FIELD']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['RECORD']->value->get($_smarty_tpl->tpl_vars['NAME_FIELD']->value);?>
</span>&nbsp;
														<?php }?>
												<?php } ?>
											</span>
										</span>
									</span>
								</div>
							</div>
							
							<div class="span7">
								<div class="pull-right detailViewButtoncontainer">
									<div class="btn-toolbar">
										
										
										<span class="btn-group">											
											<button class="btn" id="Leads_detailView_basicAction_LBL_SEND_EMAIL" 
											onclick='javascript:Vtiger_DetailPopup_Js.triggerSendEmail("index.php?module=<?php echo $_smarty_tpl->tpl_vars['MODULE_NAME']->value;?>
&view=MassActionAjax&mode=showComposeEmailForm&step=step1","Emails");' >
												<strong>Send Email</strong>
											</button>
										</span>
										
										<span class="btn-group">										
											<button class="btn" 
											onclick='javascript:Vtiger_DetailPopup_Js.triggerSendSms("index.php?module=<?php echo $_smarty_tpl->tpl_vars['MODULE_NAME']->value;?>
&view=MassActionAjax&mode=showSendSMSForm","SMSNotifier");' >
												<strong><?php echo vtranslate('Send SMS',$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
</strong>
											</button>
										</span>
							
									</div>
								</div>
							</div>
							
							
							
						</div>
					</div>
				</div>
				
				<div class="detailViewInfo row-fluid">
					<div class=" span12 ">
						<form id="detailViewPopupForm" data-name-fields='<?php echo ZEND_JSON::encode($_smarty_tpl->tpl_vars['MODULE_MODEL']->value->getNameFields());?>
' method="POST">
							<?php if (!empty($_smarty_tpl->tpl_vars['PICKLIST_DEPENDENCY_DATASOURCE']->value)){?> 
                                  <input type="hidden" name="picklistDependency" value="<?php echo Vtiger_Util_Helper::toSafeHTML($_smarty_tpl->tpl_vars['PICKLIST_DEPENDENCY_DATASOURCE']->value);?>
"> 
                            <?php }?> 
							<div class="contents">
							
								<div class="row-fluid">
									<div class="span10">
										<div class="summaryView row-fluid">
											<?php echo $_smarty_tpl->tpl_vars['MODULE_SUMMARY']->value;?>

										</div>
									</div>
								</div>
								
							</div>
						</form>
					</div>
				</div>
				
			</div>
		
		</div>
	</div>
</div>

<?php }} ?>