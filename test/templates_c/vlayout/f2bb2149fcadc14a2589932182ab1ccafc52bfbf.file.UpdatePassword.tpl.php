<?php /* Smarty version Smarty-3.1.7, created on 2015-12-05 21:49:12
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Users/UpdatePassword.tpl" */ ?>
<?php /*%%SmartyHeaderCode:156343198856618b49077022-42364592%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f2bb2149fcadc14a2589932182ab1ccafc52bfbf' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Users/UpdatePassword.tpl',
      1 => 1449308896,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '156343198856618b49077022-42364592',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_56618b490b963',
  'variables' => 
  array (
    'CURRENT_USER_MODEL' => 0,
    'MODULE' => 0,
    'USERID' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56618b490b963')) {function content_56618b490b963($_smarty_tpl) {?><style>#pswd_info ul, li {margin:0;padding:0;list-style-type:none;}#pswd_info {position:absolute;bottom:34%;right:40%;width:250px;padding:15px;background:#fefefe;border-radius:5px;box-shadow:0 1px 3px #ccc;border:1px solid #ddd;display:none;}#pswd_info::before {content: "\25B2";position:absolute;top:-12px;left:45%;font-size:14px;line-height:14px;color:#ddd;text-shadow:none;display:block;}#pswd_info h4 {margin:0 0 10px 0;padding:0;font-weight:normal;}.invalid {background:url(layouts/vlayout/skins/images/invalid.png) no-repeat 0 50%;padding-left:22px;line-height:24px;color:#ec3f41;}.valid {background:url(layouts/vlayout/skins/images/valid.png) no-repeat 0 50%;padding-left:22px;line-height:24px;color:#3a7d34;}</style><div class='container-fluid editViewContainer'><form class="form-horizontal recordEditView" id="EditView" name="EditView" method="post" action="index.php" enctype="multipart/form-data"><?php $_smarty_tpl->tpl_vars['WIDTHTYPE'] = new Smarty_variable($_smarty_tpl->tpl_vars['CURRENT_USER_MODEL']->value->get('rowheight'), null, 0);?><input type="hidden" name="module" value="<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
" /><input type="hidden" name="userid" value="<?php echo $_smarty_tpl->tpl_vars['USERID']->value;?>
" /><div class="contentHeader row-fluid"><span class="pull-right"><button class="btn btn-success" type="submit"><strong><?php echo vtranslate('LBL_SAVE',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></button><a class="btn btn-success" type="reset" onclick="javascript:window.location.href='index.php';">Remind Me Later</a></span></div><table class="table table-bordered blockContainer showInlineTable equalSplit"><thead><tr><th class="blockHeader" colspan="2"><?php echo vtranslate('Update Password',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</th></tr></thead><tbody><tr><?php if (!$_smarty_tpl->tpl_vars['CURRENT_USER_MODEL']->value->isAdminUser()){?><td><?php echo vtranslate('LBL_OLD_PASSWORD',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td><input type="password" name="old_password" data-validation-engine="validate[required]"/></td><?php }?></tr><tr><td><?php echo vtranslate('LBL_NEW_PASSWORD',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td><input type="password" name="new_password" data-validation-engine="validate[required]"/><br><span id="password_strength"></span></td></tr><tr><td><?php echo vtranslate('LBL_CONFIRM_PASSWORD',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td><input type="password" name="confirm_password" data-validation-engine="validate[required]"/></td></tr></tbody></table></form><!--	START:	5Dec,2015 Changes		--><div id="pswd_info"><h4>Password must meet the following requirements:</h4><ul><li id="letter" class="invalid">At least <strong>one letter</strong></li><li id="capital" class="invalid">At least <strong>one capital letter</strong></li><li id="number" class="invalid">At least <strong>one number</strong></li><li id="spl_char" class="invalid">At least <strong>one special character</strong></li><li id="length" class="invalid">Be at least <strong>6 characters</strong></li></ul></div><!--	END: 	5Dec,2015 Changes		--></div><?php }} ?>