<?php /* Smarty version Smarty-3.1.7, created on 2015-12-02 23:44:53
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/SMSNotifier/SMSBox.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2126998706565f8275644769-83241767%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c912bc60e4285c624de831c65a9ec7c3b1367e95' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/SMSNotifier/SMSBox.tpl',
      1 => 1446555173,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2126998706565f8275644769-83241767',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ROWDATA' => 0,
    'PARENT_CRMID' => 0,
    'PARENT_RECORD' => 0,
    'SENDTO_PHONE' => 0,
    'SMSES' => 0,
    'SMSRECORD' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_565f82756dd21',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565f82756dd21')) {function content_565f82756dd21($_smarty_tpl) {?>
<?php $_smarty_tpl->tpl_vars['PARENT_CRMID'] = new Smarty_variable($_smarty_tpl->tpl_vars['ROWDATA']->value['parent_crmid'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['PARENT_RECORD'] = new Smarty_variable(Vtiger_Record_Model::getInstanceById($_smarty_tpl->tpl_vars['PARENT_CRMID']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['FILTERED_PHONE'] = new Smarty_variable($_smarty_tpl->tpl_vars['ROWDATA']->value['phone'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['SENDTO_PHONE'] = new Smarty_variable($_smarty_tpl->tpl_vars['ROWDATA']->value['origphone'], null, 0);?>


<div class="sms_box" id="sms_<?php echo $_smarty_tpl->tpl_vars['PARENT_CRMID']->value;?>
">

	<div class="sms_header" >
		<a target="_blank" style="color:white;font-size:16px;" href="index.php?module=<?php echo $_smarty_tpl->tpl_vars['PARENT_RECORD']->value->getModuleName();?>
&view=Detail&record=<?php echo $_smarty_tpl->tpl_vars['PARENT_CRMID']->value;?>
">
		<b><?php echo Vtiger_Functions::getCRMRecordLabel($_smarty_tpl->tpl_vars['PARENT_CRMID']->value);?>
</b>
		</a> 
		<br>
		(<?php echo $_smarty_tpl->tpl_vars['SENDTO_PHONE']->value;?>
)
	</div>
	
	<div class="sms_contents sms_threads">
	
		<?php $_smarty_tpl->tpl_vars["SMSES"] = new Smarty_variable(SMSNotifier_AllSms_Model::getSmallSmsHistory($_smarty_tpl->tpl_vars['PARENT_CRMID']->value,$_smarty_tpl->tpl_vars['SENDTO_PHONE']->value), null, 0);?>
		
		<?php  $_smarty_tpl->tpl_vars['SMSRECORD'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['SMSRECORD']->_loop = false;
 $_smarty_tpl->tpl_vars['SMSINDEX'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['SMSES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['SMSRECORD']->key => $_smarty_tpl->tpl_vars['SMSRECORD']->value){
$_smarty_tpl->tpl_vars['SMSRECORD']->_loop = true;
 $_smarty_tpl->tpl_vars['SMSINDEX']->value = $_smarty_tpl->tpl_vars['SMSRECORD']->key;
?>
			
			<?php if ($_smarty_tpl->tpl_vars['SMSRECORD']->value->get('direction')=='incoming'){?>
			
				<span class="receive_msg" >
				<?php echo $_smarty_tpl->tpl_vars['SMSRECORD']->value->get('message');?>

				</span>
								
			<?php }elseif($_smarty_tpl->tpl_vars['SMSRECORD']->value->get('direction')=='outgoing'){?>
			
				<span class="sent_msg" >
				<?php echo $_smarty_tpl->tpl_vars['SMSRECORD']->value->get('message');?>

				</span>
							
			<?php }?>
			
		<?php } ?>
		
	</div>
	
	<div class="sms_footer" >
		
		<form method="post" style="padding:2%;">
	
			<input type="hidden" name="selected_ids" value=<?php echo ZEND_JSON::encode(array($_smarty_tpl->tpl_vars['PARENT_CRMID']->value));?>
>
	
			<input type="hidden" name="parent_crmid" value="<?php echo $_smarty_tpl->tpl_vars['PARENT_CRMID']->value;?>
" />
			<input type="hidden" name="sendto" value="<?php echo $_smarty_tpl->tpl_vars['SENDTO_PHONE']->value;?>
" />
			<input type="text" name="message" value="" style=" width: 80%;" />&nbsp;
			<input type="button" name="sendmessage" value="Send" />&nbsp;
			<img src="layouts/vlayout/skins/images/loading.gif" style="width:5%; " class="progressImg hide" />
			
	
		</form>
		
	</div>
	
</div>


<?php }} ?>