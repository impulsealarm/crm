<?php /* Smarty version Smarty-3.1.7, created on 2015-09-09 08:56:43
         compiled from "/home/imagineavs/public_html/imp_crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/SidePopup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:141217598655e996a1417aa9-54231822%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7e9cf9b8809de5b38aa86874e76deacbd5e2d9bd' => 
    array (
      0 => '/home/imagineavs/public_html/imp_crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/SidePopup.tpl',
      1 => 1441788656,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '141217598655e996a1417aa9-54231822',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_55e996a142085',
  'variables' => 
  array (
    'CURRENT_USER_MODEL' => 0,
    'USER_MODEL' => 0,
    'CALLERID' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55e996a142085')) {function content_55e996a142085($_smarty_tpl) {?><!-- ------------------ START - Twilio Outgoing Call Side Popup ------------- -->

<link rel='stylesheet' href='layouts/vlayout/skins/custom-style.css'>


<div id="push_sidebarphone">    
	
	<div id="twilioCallContainer" style="width:95%;padding:2%">        
		
		<?php $_smarty_tpl->tpl_vars['CALLERID'] = new Smarty_variable('', null, 0);?>
			
		<?php if (isset($_smarty_tpl->tpl_vars['CURRENT_USER_MODEL']->value)){?>
			<?php $_smarty_tpl->tpl_vars['CALLERID'] = new Smarty_variable($_smarty_tpl->tpl_vars['CURRENT_USER_MODEL']->value->get('phone_crm_extension'), null, 0);?>
		<?php }elseif(isset($_smarty_tpl->tpl_vars['USER_MODEL']->value)){?>
			<?php $_smarty_tpl->tpl_vars['CALLERID'] = new Smarty_variable($_smarty_tpl->tpl_vars['USER_MODEL']->value->get('phone_crm_extension'), null, 0);?>			
		<?php }?>
		
		<input type = "hidden" id = "caller_id" value = "<?php echo $_smarty_tpl->tpl_vars['CALLERID']->value;?>
"/>
			
		<div style="    padding-top: 9px;">
		    <span id="log" style="font-size:13px;width:60%;float:left;text-algign:left;">Loading...</span>
		
			<span class="close">
				<a href="javascript:void(0)"><img class="closesidebar3" src="layouts/vlayout/skins/images/closebtn.png" width="30" height="30"></a>
			</span>
		</div>
		
		<div style="clear:both;"></div>
		<br>
		
		<div style="text-align:left;" >
				<input type="text" id="dial_phone_number" name="number" value=""  placeholder="Enter Phone Number" style=" width: 75%;margin-bottom:0px;"/>
				<img src="layouts/vlayout/skins/images/delete-icon.png" class="del-img"/>
		</div>
			
		<div style="clear:both;"></div>
			
		<hr/>
			
		<div style="text-align:left;">
			<button class=" btn-success" onclick="Vtiger_Twilio_Js.call();">Call</button>
			<button class=" btn-danger" onclick="Vtiger_Twilio_Js.hangup();">Hangup</button>
			<button class="btn btn-info" onclick="Vtiger_Twilio_Js.hold();" style="display:none;" id='btnhold'>Hold</button>
			<button class="btn btn-info" onclick="Vtiger_Twilio_Js.unhold();" style="display:none;" id='btnunhold'>UnHold</button>
			<button class=" btn-warning" onclick="Vtiger_Twilio_Js.transfer();" >Transfer</button>
		</div>
			
		<div style="clear:both;"></div>
			
		<hr/>
			
		<div style="text-align:center;">
			<table style="width:90%">
				<tr>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="1" id="button1"></td>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="2" id="button2"></td>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="3" id="button3"></td>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="*" id="buttonstar"></td>
					
				</tr>
				<tr>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="4" id="button4"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="5" id="button5"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="6" id="button6"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="#" id="buttonpound"></td>
				</tr>
				<tr>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="7" id="button7"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="8" id="button8"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="9" id="button9"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="0" id="button0"></td>
				</tr>
			</table>
		</div>
		
	</div>
</div>


<div class="fixed_menu">
	<ul>
		<li>
			<a href="#" data-toggle="tooltip" data-placement="left" title="Phone">
				<span class="nav_trigger2 open_call_popup"><img src="layouts/vlayout/skins/images/fixed_icon.png"></span>
			</a>
		</li>		
	</ul>        
</div>


<!-- ------------------ END - Twilio Outgoing Call Side Popup ------------- -->
<?php }} ?>