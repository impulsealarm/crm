<?php /* Smarty version Smarty-3.1.7, created on 2016-01-11 11:27:00
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Payments/ShowSMSCodePopup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:114597931756939184c70820-19025873%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e172840b7e2fb6020ed4746c4774391ac7bca3bc' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Payments/ShowSMSCodePopup.tpl',
      1 => 1452511256,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '114597931756939184c70820-19025873',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'MODULE' => 0,
    'MASS_APPROVED_PAYMENTS' => 0,
    'VIEW' => 0,
    'MODE' => 0,
    'VIEWNAME' => 0,
    'SELECTED_IDS' => 0,
    'EXCLUDED_IDS' => 0,
    'RECORD' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_56939184ca729',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56939184ca729')) {function content_56939184ca729($_smarty_tpl) {?>

<div class="modelContainer" style='min-width:350px;'><div class="modal-header contentsBackground"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3>SMS Code Verification</h3></div><form class="form-horizontal" id = "validate_code" name="validate_code" method="post"><input type="hidden" name="module" value="<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
"><?php if ($_smarty_tpl->tpl_vars['MASS_APPROVED_PAYMENTS']->value==true){?><input type="hidden" name="view" value="<?php echo $_smarty_tpl->tpl_vars['VIEW']->value;?>
"><input type="hidden" name="mode" value="<?php echo $_smarty_tpl->tpl_vars['MODE']->value;?>
" /><input type="hidden" name="viewname" value="<?php echo $_smarty_tpl->tpl_vars['VIEWNAME']->value;?>
" /><input type="hidden" name="selected_ids" value=<?php echo ZEND_JSON::encode($_smarty_tpl->tpl_vars['SELECTED_IDS']->value);?>
><input type="hidden" name="excluded_ids" value=<?php echo ZEND_JSON::encode($_smarty_tpl->tpl_vars['EXCLUDED_IDS']->value);?>
><?php }else{ ?><input type="hidden" name="action" value="ApprovePayments"><input type="hidden" name="record" id="record" value="<?php echo $_smarty_tpl->tpl_vars['RECORD']->value;?>
" /><?php }?><div class="modal-body"><div class="row-fluid"><div class="control-group"><label class="control-label"><b> SMSCode: </b></label><div class="controls"><input id="sms_code" data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" name="sms_code" type="text" class="autoComplete"value="" placeholder="Enter SMS Code" /></div></div></div></div><div class="modal-footer"><button class="btn btn-success" type="submit" name="saveButton"><strong>Save</strong></button><a class="cancelLink cancelLinkContainer pull-right" type="reset" data-dismiss="modal">Cancel</a></div></form><?php }} ?>