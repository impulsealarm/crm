<?php /* Smarty version Smarty-3.1.7, created on 2016-01-07 06:40:28
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Settings/Workflows/Tasks/VTSendMandrillEmailTask.tpl" */ ?>
<?php /*%%SmartyHeaderCode:917109489568e085c5214f8-80005495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd2cbb0d2d3f6b5fdacaf6f719785778e4f37cb35' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Settings/Workflows/Tasks/VTSendMandrillEmailTask.tpl',
      1 => 1442314018,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '917109489568e085c5214f8-80005495',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'QUALIFIED_MODULE' => 0,
    'TASK_OBJECT' => 0,
    'FROM_EMAIL_FIELD_OPTION' => 0,
    'EMAIL_FIELD_OPTION' => 0,
    'MANDRILL_TEMPLATES' => 0,
    'M_TEMPLATE' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_568e085c5a5b8',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_568e085c5a5b8')) {function content_568e085c5a5b8($_smarty_tpl) {?>
<div id="VtMandrillEmailTaskContainer"><div class="row-fluid"><div class="row-fluid padding-bottom1per"><span class="span7 row-fluid"><span class="span2"><?php echo vtranslate('LBL_FROM',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</span><input data-validation-engine='validate[]' name="fromEmail" class="span9 fields" type="text" value="<?php echo $_smarty_tpl->tpl_vars['TASK_OBJECT']->value->fromEmail;?>
" /></span><span class="span5"><select id="fromEmailOption" style="min-width: 300px" class="chzn-select" data-placeholder=<?php echo vtranslate('LBL_SELECT_OPTIONS',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
><option></option><?php echo $_smarty_tpl->tpl_vars['FROM_EMAIL_FIELD_OPTION']->value;?>
</select></span></div><div class="row-fluid padding-bottom1per"><span class="span7 row-fluid"><span class="span2"><?php echo vtranslate('LBL_TO',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
<span class="redColor">*</span></span><input data-validation-engine='validate[required]' name="recepient" class="span9 fields" type="text" value="<?php echo $_smarty_tpl->tpl_vars['TASK_OBJECT']->value->recepient;?>
" /></span><span class="span5"><select style="min-width: 300px" class="task-fields chzn-select" data-placeholder=<?php echo vtranslate('LBL_SELECT_OPTIONS',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
><option></option><?php echo $_smarty_tpl->tpl_vars['EMAIL_FIELD_OPTION']->value;?>
</select></span></div><div class="row-fluid padding-bottom1per <?php if (empty($_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailcc)){?>hide <?php }?>" id="ccContainer"><span class="span7 row-fluid"><span class="span2"><?php echo vtranslate('LBL_CC',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</span><input class="span9 fields" type="text" name="emailcc" value="<?php echo $_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailcc;?>
" /></span><span class="span5"><select class="task-fields" data-placeholder='<?php echo vtranslate('LBL_SELECT_OPTIONS',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
' style="min-width: 300px"><option></option><?php echo $_smarty_tpl->tpl_vars['EMAIL_FIELD_OPTION']->value;?>
</select></span></div><div class="row-fluid padding-bottom1per <?php if (empty($_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailbcc)){?>hide <?php }?>" id="bccContainer"><span class="span7 row-fluid"><span class="span2"><?php echo vtranslate('LBL_BCC',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</span><input class="span9 fields" type="text" name="emailbcc" value="<?php echo $_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailbcc;?>
" /></span><span class="span5"><select class="task-fields" data-placeholder='<?php echo vtranslate('LBL_SELECT_OPTIONS',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
' style="min-width: 300px"><option></option><?php echo $_smarty_tpl->tpl_vars['EMAIL_FIELD_OPTION']->value;?>
</select></span></div><div class="row-fluid padding-bottom1per <?php if ((!empty($_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailcc))&&(!empty($_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailbcc))){?> hide <?php }?>"><span class="span8 row-fluid"><span class="span2">&nbsp;</span><span class="span9"><a class="cursorPointer <?php if ((!empty($_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailcc))){?>hide<?php }?>" id="ccLink"><?php echo vtranslate('LBL_ADD_CC',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</a>&nbsp;&nbsp;<a class="cursorPointer <?php if ((!empty($_smarty_tpl->tpl_vars['TASK_OBJECT']->value->emailbcc))){?>hide<?php }?>" id="bccLink"><?php echo vtranslate('LBL_ADD_BCC',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</a></span></span></div><div class="row-fluid padding-bottom1per"><span class="span7 row-fluid"><span style="margin-top: 7px" class="span4"><?php echo vtranslate('Select Mandrill Template',$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
<span class="redColor">*</span></span>&nbsp;&nbsp;<span class="span8"><select style="min-width: 250px" id="mtemplate" name="mtemplate" data-validation-engine='validate[required]' class="chzn-select" data-placeholder="Select an Option"><!-- <option value=""></option> --><?php if (isset($_smarty_tpl->tpl_vars['MANDRILL_TEMPLATES']->value)){?><?php  $_smarty_tpl->tpl_vars['M_TEMPLATE'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['M_TEMPLATE']->_loop = false;
 $_smarty_tpl->tpl_vars['M_INDEX'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['MANDRILL_TEMPLATES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['M_TEMPLATE']->key => $_smarty_tpl->tpl_vars['M_TEMPLATE']->value){
$_smarty_tpl->tpl_vars['M_TEMPLATE']->_loop = true;
 $_smarty_tpl->tpl_vars['M_INDEX']->value = $_smarty_tpl->tpl_vars['M_TEMPLATE']->key;
?><option value="<?php echo $_smarty_tpl->tpl_vars['M_TEMPLATE']->value['slug'];?>
" <?php if ($_smarty_tpl->tpl_vars['TASK_OBJECT']->value->mtemplate==$_smarty_tpl->tpl_vars['M_TEMPLATE']->value['slug']){?> selected <?php }?> ><?php echo vtranslate($_smarty_tpl->tpl_vars['M_TEMPLATE']->value['name'],$_smarty_tpl->tpl_vars['QUALIFIED_MODULE']->value);?>
</option><?php } ?><?php }?></select></span></span></div></div></div>	<?php }} ?>