<?php /* Smarty version Smarty-3.1.7, created on 2015-11-09 16:53:39
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/SMSNotifier/RelatedModuleBlocksDetail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16320572765640cf93e76f69-74433080%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '385cf654f3985db6f87f5d3621166ddde6d21636' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/SMSNotifier/RelatedModuleBlocksDetail.tpl',
      1 => 1443028158,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16320572765640cf93e76f69-74433080',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'RELATED_MODULES' => 0,
    'RELATED_ENTITY' => 0,
    'ModuleName' => 0,
    'ENTITYNAME' => 0,
    'Status' => 0,
    'FromNumber' => 0,
    'ToNumber' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_5640cf93eb0d9',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5640cf93eb0d9')) {function content_5640cf93eb0d9($_smarty_tpl) {?><br/>
	
<table class ="table table-bordered equalSplit detailview-table">
	<thead>
		<tr>
			<th class="blockHeader">
				<img class="cursorPointer alignMiddle blockToggle  hide  " src="layouts/vlayout/skins/nature/images/arrowRight.png" data-mode="hide" data-id="109">
				<img class="cursorPointer alignMiddle blockToggle " src="layouts/vlayout/skins/nature/images/arrowDown.png" data-mode="show" data-id="109">
				&nbsp;&nbsp;Related Type
			</th>
			<th>Related To</th>
			<th>Status</th>
			<th>From Number</th>
			<th>To Number</th>
		</tr>
	</thead>
	<tbody>
		<?php $_smarty_tpl->tpl_vars['FromNumber'] = new Smarty_variable($_smarty_tpl->tpl_vars['RELATED_MODULES']->value['from_number'], null, 0);?>
		<?php $_smarty_tpl->tpl_vars['ToNumber'] = new Smarty_variable($_smarty_tpl->tpl_vars['RELATED_MODULES']->value['to_number'], null, 0);?>
		<?php $_smarty_tpl->tpl_vars['Status'] = new Smarty_variable($_smarty_tpl->tpl_vars['RELATED_MODULES']->value['status'], null, 0);?>
		<?php  $_smarty_tpl->tpl_vars['RELATED_ENTITY'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['RELATED_ENTITY']->_loop = false;
 $_smarty_tpl->tpl_vars['ModuleName'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['RELATED_MODULES']->value['related_to']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['RELATED_ENTITY']->key => $_smarty_tpl->tpl_vars['RELATED_ENTITY']->value){
$_smarty_tpl->tpl_vars['RELATED_ENTITY']->_loop = true;
 $_smarty_tpl->tpl_vars['ModuleName']->value = $_smarty_tpl->tpl_vars['RELATED_ENTITY']->key;
?>
			<?php  $_smarty_tpl->tpl_vars['ENTITYNAME'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ENTITYNAME']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['RELATED_ENTITY']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ENTITYNAME']->key => $_smarty_tpl->tpl_vars['ENTITYNAME']->value){
$_smarty_tpl->tpl_vars['ENTITYNAME']->_loop = true;
?>
				<tr>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;"><?php echo $_smarty_tpl->tpl_vars['ModuleName']->value;?>
</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;"><?php echo $_smarty_tpl->tpl_vars['ENTITYNAME']->value;?>
</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;"><?php echo $_smarty_tpl->tpl_vars['Status']->value;?>
</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;"><?php echo $_smarty_tpl->tpl_vars['FromNumber']->value;?>
</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;"><?php echo $_smarty_tpl->tpl_vars['ToNumber']->value;?>
</span>
					</td>	
				</tr>
			<?php } ?>
		<?php } ?>
	</tbody>
</table>
<?php }} ?>