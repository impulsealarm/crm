<?php /* Smarty version Smarty-3.1.7, created on 2015-12-01 05:18:01
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Mobile/generic/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2103954486565d2d894918a7-71028661%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '60428a476c9bc32d00245518c6490910375597b7' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Mobile/generic/List.tpl',
      1 => 1440792531,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2103954486565d2d894918a7-71028661',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_MODE' => 0,
    '_PAGER' => 0,
    '_MODULE' => 0,
    '_SEARCH' => 0,
    '_RECORDS' => 0,
    '_RECORD' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_565d2d8954afd',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565d2d8954afd')) {function content_565d2d8954afd($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("modules/Mobile/generic/Header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<body>

<div id="__listview__" <?php if ($_smarty_tpl->tpl_vars['_MODE']->value=='search'){?>style='display:none;'<?php }?>>
	<table width=100% cellpadding=0 cellspacing=0 border=0>
	<tr class="toolbar">
		<td><a class="link" href="javascript:window.close();"><img src="resources/images/iconza/royalblue/undo_32x32.png" border="0"></a></td>
		<td width="100%">
			<h1 class='page_title'>
			
			<?php if ($_smarty_tpl->tpl_vars['_PAGER']->value&&$_smarty_tpl->tpl_vars['_PAGER']->value->hasPrevious()){?>
			<a class="link" href="?_operation=listModuleRecords&module=<?php echo $_smarty_tpl->tpl_vars['_MODULE']->value->name();?>
&page=<?php echo $_smarty_tpl->tpl_vars['_PAGER']->value->previous();?>
&search=<?php echo $_smarty_tpl->tpl_vars['_SEARCH']->value;?>
"><img src="resources/images/iconza/yellow/left_arrow_24x24.png" border="0"></a>
			<?php }else{ ?>
			<a class="link" href="javascript:void(0);"><img src="resources/images/iconza/white/left_arrow_24x24.png" border="0"></a>
			<?php }?>
			
			<?php echo $_smarty_tpl->tpl_vars['_MODULE']->value->label();?>

			
			<?php if ($_smarty_tpl->tpl_vars['_PAGER']->value&&$_smarty_tpl->tpl_vars['_PAGER']->value->hasNext(count($_smarty_tpl->tpl_vars['_RECORDS']->value))){?>
			<a class="link" href="?_operation=listModuleRecords&module=<?php echo $_smarty_tpl->tpl_vars['_MODULE']->value->name();?>
&page=<?php echo $_smarty_tpl->tpl_vars['_PAGER']->value->next();?>
&search=<?php echo $_smarty_tpl->tpl_vars['_SEARCH']->value;?>
"><img src="resources/images/iconza/yellow/right_arrow_24x24.png" border="0"></a>
			<?php }else{ ?>
			<a class="link" href="javascript:void(0);"><img src="resources/images/iconza/white/right_arrow_24x24.png" border="0"></a>
			<?php }?>
			
			</h1>
		</td>
		<td align="right" style="padding-right: 5px;"><a class="link" href="javascript:void(0);" onclick="$fnT('__listview__', '__searchbox__'); $fnFocus('__searchbox__q_');" target="_self"><img src="resources/images/iconza/yellow/lens_32x32.png" border="0"></a></td>
	</tr>
	
	<tr>
		<td colspan="3">	
		
			<table width=100% cellpadding=0 cellspacing=0 border=0 class="table_list">
				<?php  $_smarty_tpl->tpl_vars['_RECORD'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_RECORD']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_RECORDS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_RECORD']->key => $_smarty_tpl->tpl_vars['_RECORD']->value){
$_smarty_tpl->tpl_vars['_RECORD']->_loop = true;
?>
				<tr>
				<td width="100%">
					<a href="?_operation=fetchRecordWithGrouping&record=<?php echo $_smarty_tpl->tpl_vars['_RECORD']->value->id();?>
" target="_self"><?php echo $_smarty_tpl->tpl_vars['_RECORD']->value->label();?>
</a>
				</td>
				<td>
					<a href="?_operation=fetchRecordWithGrouping&record=<?php echo $_smarty_tpl->tpl_vars['_RECORD']->value->id();?>
" target="_self" class="link_rhook"><img src="resources/images/iconza/royalblue/right_arrow_16x16.png" border="0"></a>								
				</td>
				</tr>
				
				<?php }
if (!$_smarty_tpl->tpl_vars['_RECORD']->_loop) {
?>
				
				<tr class="info">
				<td width=25% align="right">
					<img src="resources/images/iconza/royalblue/info_24x24.png" border=0 />
				</td>
				<td width=100% align="left" valign="center">
					<?php if ($_smarty_tpl->tpl_vars['_PAGER']->value->hasPrevious()){?>
					<p>No more records found.</p>
					<?php }else{ ?>
					<p>No records available.</p>
					<?php }?>
				</td>
				</tr>
				
				<?php } ?>
			</table>
		
		</td>
	</tr>
	</table>
</div>

<div id="__searchbox__" <?php if ($_smarty_tpl->tpl_vars['_MODE']->value!='search'){?>style='display:none;'<?php }?>>
	<table width=100% cellpadding=0 cellspacing=0 border=0>
	<tr class="toolbar">
		<td><a class="link" href="?_operation=searchConfig&module=<?php echo $_smarty_tpl->tpl_vars['_MODULE']->value->name();?>
" target="_self"><img src="resources/images/iconza/yellow/wrench_32x32.png" border="0"></a></td>
		<td width="100%">
			<h1 class='page_title'>
			Search <?php echo $_smarty_tpl->tpl_vars['_MODULE']->value->label();?>

			</h1>
		</td>
		<td align="right" style="padding-right: 5px;"><a class="link" href="javascript:void(0);" onclick="$fnT('__searchbox__','__listview__');"><img src="resources/images/iconza/yellow/zoom_out_32x32.png" border="0"></a></td>
	</tr>
	
	<tr>
		<td colspan=3 align="center">
		
			<form action="index.php" method="GET" onsubmit="if(this.search.value == '') return false;">
				<input type="hidden" name="_operation" value="listModuleRecords" />
				<input type="hidden" name="module" value="<?php echo $_smarty_tpl->tpl_vars['_MODULE']->value->name();?>
" />
				<input id='__searchbox__q_' type="text" name="search" class="searchbox" value="<?php echo $_smarty_tpl->tpl_vars['_SEARCH']->value;?>
"/>
			</form>
		
		</td>
		
	</tr>
	</table>
</div>

</body>

<?php echo $_smarty_tpl->getSubTemplate ("modules/Mobile/generic/Footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>