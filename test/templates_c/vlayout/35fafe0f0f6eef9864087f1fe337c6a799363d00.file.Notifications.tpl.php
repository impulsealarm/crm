<?php /* Smarty version Smarty-3.1.7, created on 2015-11-09 16:52:16
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/Notifications.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10453485705640cf4017edd3-63179439%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35fafe0f0f6eef9864087f1fe337c6a799363d00' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/Vtiger/Notifications.tpl',
      1 => 1444898855,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10453485705640cf4017edd3-63179439',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_5640cf401a400',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5640cf401a400')) {function content_5640cf401a400($_smarty_tpl) {?>
<style>
#notification_li{
	position:relative;
	font-family:arial;
}
#notificationContainer {
	background-color: #fff;
	border: 1px solid rgba(100, 100, 100, .4);
	-webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
	overflow: visible;
	position: absolute;
	top: 30px;
	margin-left: -325px;
	width: 450px;
	z-index: 0;
	display: none;
}

#notificationContainer:before {
	content: '';
	display: block;
	position: absolute;
	width: 0;
	height: 0;
	color: transparent;
	border: 10px solid black;
	border-color: transparent transparent white;
	margin-top: -20px;
	margin-left: 322px;
}
#notificationTitle {
	z-index: 1000;
	font-weight: bold;
	padding: 8px;
	font-size: 13px;
	background-color: #ffffff;
	/*width: 384px;*/
	border-bottom: 1px solid #dddddd;
}
#notificationsBody {
	padding: 10px 0px 0px 12px !important;
	/*min-height:300px;*/
	/*height: 350px;*/
	overflow: auto;
}
#notificationFooter {
	background-color: #e9eaed;
	text-align: center;
	font-weight: bold;
	padding: 8px;
	font-size: 12px;
	border-top: 1px solid #dddddd;
}
#notification_count {
	padding: 0px 5px 0px 5px;
	background: #cc0000;
	color: #ffffff;
	font-weight: bold;
	margin-left: 15px;
	border-radius: 9px;
	position: absolute;
	margin-top: -10px;
	font-size: 11px;
	display:none;
}

#notificationContainer #notificationsBody {
	padding: 10px 10px 0px 12px !important;	
}
</style>

<script type="text/javascript" src="layouts/vlayout/modules/Vtiger/resources/Notifications.js"></script>


<span class="dropdown span settingIcons notificationSpan" style="margin-right: 10px;">

    <span id="notification_count" class="notification_count">0</span>
								
	<a id="notificationLink" href="#">
		<img src="<?php echo vimage_path('info.png');?>
" alt="Notifications" title="Notifications">
	</a>
	
	<div id="notificationContainer" class="pull-right">
		<div id="notificationTitle">Notifications</div>
		<div id="notificationsBody" class="notifications">
		</div>

		<div id="notificationFooter">
			<!-- <span style="cursor:pointer" onClick="window.location='index.php'">See All</span> -->
		</div>
	</div>
	
</span>
<?php }} ?>