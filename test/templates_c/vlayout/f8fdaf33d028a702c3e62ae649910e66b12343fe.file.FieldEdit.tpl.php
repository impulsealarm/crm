<?php /* Smarty version Smarty-3.1.7, created on 2015-08-31 10:27:13
         compiled from "/home/imagineavs/public_html/imp_crm/includes/runtime/../../layouts/vlayout/modules/RelatedListviewEdits/FieldEdit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:39458250655e42c013d3cd5-08953436%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f8fdaf33d028a702c3e62ae649910e66b12343fe' => 
    array (
      0 => '/home/imagineavs/public_html/imp_crm/includes/runtime/../../layouts/vlayout/modules/RelatedListviewEdits/FieldEdit.tpl',
      1 => 1440792531,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '39458250655e42c013d3cd5-08953436',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'FIELD_MODEL' => 0,
    'USER_MODEL' => 0,
    'MODULE_NAME' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_55e42c014a4cd',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55e42c014a4cd')) {function content_55e42c014a4cd($_smarty_tpl) {?>

    <style type="text/css">
        .vte-edit input, .vte-edit textarea, .vte-edit select, .vte-edit .uneditable-input{
            margin-bottom: 0 !important
        }
        .vte-edit-save-btn{
            background: url(layouts/vlayout/modules/RelatedListviewEdits/resources/save.png) no-repeat center right;
            width: 28px;
            height: 24px;
            display: inline-block;
            vertical-align: middle;
        }
        .vte-edit-cancel-btn{
            background: url(layouts/vlayout/modules/RelatedListviewEdits/resources/cancel.png) no-repeat center right;
            width: 26px;
            height: 24px;
            display: inline-block;
            vertical-align: middle;
        }
    </style>
<span class="vte-edit" style="position: relative; display: inline-block;" data-fieldname="<?php echo $_smarty_tpl->tpl_vars['FIELD_MODEL']->value->get('name');?>
"><?php echo $_smarty_tpl->getSubTemplate (vtemplate_path($_smarty_tpl->tpl_vars['FIELD_MODEL']->value->getUITypeModel()->getTemplateName(),'RelatedListviewEdits'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('FIELD_MODEL'=>$_smarty_tpl->tpl_vars['FIELD_MODEL']->value,'USER_MODEL'=>$_smarty_tpl->tpl_vars['USER_MODEL']->value,'MODULE'=>$_smarty_tpl->tpl_vars['MODULE_NAME']->value), 0);?>
<i title="<?php echo vtranslate('LBL_SAVE_BTN','RelatedListviewEdits');?>
" class="vte-edit-save-btn">&nbsp;</i><i title="<?php echo vtranslate('LBL_CANCEL_BTN','RelatedListviewEdits');?>
" class="vte-edit-cancel-btn">&nbsp;</i><?php if ($_smarty_tpl->tpl_vars['FIELD_MODEL']->value->getFieldDataType()=='multipicklist'){?><input type="hidden" class="fieldname" value='<?php echo $_smarty_tpl->tpl_vars['FIELD_MODEL']->value->get('name');?>
[]' data-prev-value='<?php echo $_smarty_tpl->tpl_vars['FIELD_MODEL']->value->getDisplayValue($_smarty_tpl->tpl_vars['FIELD_MODEL']->value->get('fieldvalue'));?>
' /><?php }else{ ?><input type="hidden" class="fieldname" value='<?php echo $_smarty_tpl->tpl_vars['FIELD_MODEL']->value->get('name');?>
' data-prev-value='<?php echo Vtiger_Util_Helper::toSafeHTML($_smarty_tpl->tpl_vars['FIELD_MODEL']->value->getDisplayValue($_smarty_tpl->tpl_vars['FIELD_MODEL']->value->get('fieldvalue')));?>
' /><?php }?></span><?php }} ?>