<?php /* Smarty version Smarty-3.1.7, created on 2015-12-07 10:22:02
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/PBXManager/ConferenceList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11915079285649d50072c2b4-89297373%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9c57864a4581daaa2cec06c0f87b60f5bd128e59' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/PBXManager/ConferenceList.tpl',
      1 => 1449483033,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11915079285649d50072c2b4-89297373',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_5649d500795d9',
  'variables' => 
  array (
    'CURRENT_USER_MODEL' => 0,
    'CONFERENCES' => 0,
    'CONFERENCE_DETAIL' => 0,
    'WIDTHTYPE' => 0,
    'PARTICIPANT' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5649d500795d9')) {function content_5649d500795d9($_smarty_tpl) {?>
<div>
	<div class="row-fluid detailViewTitle">
		<div class="span5">
			<div class="row-fluid">
				<span class="span10 margin0px">
					<span class="row-fluid">
						<span class="recordLabel font-x-x-large textOverflowEllipsis span pushDown" 
						title="Conferences">
							<span class="productname">Conferences</span>&nbsp;
						</span>
					</span>
				</span>
			</div>
		</div>
	</div>

	<hr/>

	<div class="listViewEntriesDiv" id="all_conferences">
		
		<?php $_smarty_tpl->tpl_vars['WIDTHTYPE'] = new Smarty_variable($_smarty_tpl->tpl_vars['CURRENT_USER_MODEL']->value->get('rowheight'), null, 0);?>
		<table class="table table-bordered listViewEntriesTable">
			<thead>
				<tr class="listViewHeaders">				
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">Conference Name</a>
					</th>
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">Status</a>
					</th>
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">Active Participants</a>
					</th>
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">&nbsp;</a>
					</th>				
				</tr>
			</thead>
			
			<?php if (!empty($_smarty_tpl->tpl_vars['CONFERENCES']->value)){?>
			
				<?php  $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->_loop = false;
 $_smarty_tpl->tpl_vars['ITER'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['CONFERENCES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->key => $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value){
$_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->_loop = true;
 $_smarty_tpl->tpl_vars['ITER']->value = $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->key;
?>
				
				<tr class="listViewEntries" id="<?php echo $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['friendly_name'];?>
" >
					<td class="listViewEntryValue <?php echo $_smarty_tpl->tpl_vars['WIDTHTYPE']->value;?>
"  nowrap><?php echo $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['friendly_name'];?>
</td>
					<td class="listViewEntryValue <?php echo $_smarty_tpl->tpl_vars['WIDTHTYPE']->value;?>
"  nowrap><?php echo $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['status'];?>
</td>
					<td class="listViewEntryValue <?php echo $_smarty_tpl->tpl_vars['WIDTHTYPE']->value;?>
 participants"  nowrap>
					<?php if (!empty($_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['participants'])){?>
						<?php  $_smarty_tpl->tpl_vars['PARTICIPANT'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['PARTICIPANT']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['participants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['PARTICIPANT']->key => $_smarty_tpl->tpl_vars['PARTICIPANT']->value){
$_smarty_tpl->tpl_vars['PARTICIPANT']->_loop = true;
?>
							<?php echo $_smarty_tpl->tpl_vars['PARTICIPANT']->value['display_name'];?>
<br/>
						<?php } ?>
					<?php }?>
					</td>
					<td class="listViewEntryValue <?php echo $_smarty_tpl->tpl_vars['WIDTHTYPE']->value;?>
"  nowrap>
						<?php if ($_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['status']!='completed'){?>
							<input type="button" value="Listen Call" class="listen_call" 
							onClick="javascript:Vtiger_Twilio_Js.listenConference('<?php echo $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['sid'];?>
','<?php echo $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['friendly_name'];?>
', this);" />
							&nbsp;
							
							<input type="button" value="End Call" class="listen_end hide"
							onClick="javascript:Vtiger_Twilio_Js.endlistenConference('<?php echo $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['sid'];?>
','<?php echo $_smarty_tpl->tpl_vars['CONFERENCE_DETAIL']->value['friendly_name'];?>
', this);" />
						<?php }?>
					</td>	
				</tr>
						
				<?php } ?>
				
			<?php }else{ ?>
				<tr class="listViewEntries" id="no_participant">
					<td colspan="4" class="listViewEntryValue"  nowrap style="text-align:center;">No Ongoing Conferences</td>
				</tr>
			<?php }?>
			
		</table>
	</div>
	
</div><?php }} ?>