<?php /* Smarty version Smarty-3.1.7, created on 2015-12-02 23:44:40
         compiled from "/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/SMSNotifier/AllSmsPreProcess.tpl" */ ?>
<?php /*%%SmartyHeaderCode:700797246565f82683c8751-92188042%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ced74673fc875b00b58b1e8cc4e119a0c4da89bd' => 
    array (
      0 => '/home/damirbadzic/crm/includes/runtime/../../layouts/vlayout/modules/SMSNotifier/AllSmsPreProcess.tpl',
      1 => 1446633768,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '700797246565f82683c8751-92188042',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'MODULE' => 0,
    'CURRENT_USER_MODEL' => 0,
    'QUICK_LINKS' => 0,
    'SIDEBARWIDGET' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_565f826845fc4',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565f826845fc4')) {function content_565f826845fc4($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate (vtemplate_path("Header.tpl",$_smarty_tpl->tpl_vars['MODULE']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate (vtemplate_path("BasicHeader.tpl",$_smarty_tpl->tpl_vars['MODULE']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="bodyContents">
	<div class="mainContainer row-fluid">
		<?php $_smarty_tpl->tpl_vars['LEFTPANELHIDE'] = new Smarty_variable($_smarty_tpl->tpl_vars['CURRENT_USER_MODEL']->value->get('leftpanelhide'), null, 0);?>
		<div class="span2 row-fluid " id="leftPanel" style="min-height:550px;">
			
			
			
			<div class="row-fluid">				
				<div class="sideBarContents" style = "margin:10px 0;">
					<div class="clearfix"></div>
					
					
					
					<div class="quickWidgetContainer accordion">
						
						<?php $_smarty_tpl->tpl_vars['val'] = new Smarty_variable(1, null, 0);?>
						
						<?php  $_smarty_tpl->tpl_vars['SIDEBARWIDGET'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['SIDEBARWIDGET']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['QUICK_LINKS']->value['SIDEBARWIDGET']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['SIDEBARWIDGET']->key => $_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value){
$_smarty_tpl->tpl_vars['SIDEBARWIDGET']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['SIDEBARWIDGET']->key;
?>
							<div class="quickWidget">
								<div class="accordion-heading accordion-toggle quickWidgetHeader" data-target="#<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
_sideBar_<?php echo Vtiger_Util_Helper::replaceSpaceWithUnderScores($_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value->getLabel());?>
"
										data-parent="#quickWidgets" data-label="<?php echo $_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value->getLabel();?>
"
										data-widget-url="<?php echo $_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value->getUrl();?>
" >
									
									<h5 class="title widgetTextOverflowEllipsis pull-right" title="<?php echo vtranslate($_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value->getLabel(),$_smarty_tpl->tpl_vars['MODULE']->value);?>
"><?php echo vtranslate($_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value->getLabel(),$_smarty_tpl->tpl_vars['MODULE']->value);?>
</h5>
									<div class="loadingImg hide pull-right">
										<div class="loadingWidgetMsg"><strong><?php echo vtranslate('LBL_LOADING_WIDGET',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="widgetContainer accordion-body" id="<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
_sideBar_<?php echo Vtiger_Util_Helper::replaceSpaceWithUnderScores($_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value->getLabel());?>
" data-url="<?php echo $_smarty_tpl->tpl_vars['SIDEBARWIDGET']->value->getUrl();?>
"></div>
							</div>
						<?php } ?>
						
					</div>
					
					
					
				</div>
			</div>
			
			
			
		</div>
		<div class="contentsDiv  span10 marginLeftZero" id="rightPanel" style="min-height:550px;">
			
			<div class="sms_chart" >
			        <div class="row-fluid" style="margin-bottom:20px;"></div>
				<hr/>
				<div class="row-fluid"></div>
	
				<span class="sent_msg hide">
					I sent
				</span>
				<span class="receive_msg hide">
					I received
				</span>
            <?php }} ?>