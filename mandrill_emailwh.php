<?php
$requestData = array();

if( isset($_REQUEST['mandrill_events']) ){  
  $requestData = $_REQUEST['mandrill_events'];
  $requestData  = json_decode(stripslashes($requestData), true);
}


if( is_array($requestData) && !empty($requestData) ){	

	require_once('includes/main/WebUI.php');
	require_once('include/Mandrill/MandrillAPI.php');
  
	global $adb, $current_user;
	
	$adb = PearDatabase::getInstance();

	if(!$current_user) {
		$current_user = Users::getActiveAdminUser();
	}
	
	foreach($requestData as $activityEvent){
		
		if(empty($activityEvent)) continue;
		
		$mandrill_messageId = $activityEvent['_id'];
		$activity_data = $activityEvent['msg'];
		
                $event_triggered = $activityEvent['event'];

		$message_state = '';	//message state (sent)
		$parent_crmid = '';		//recipient crmid
		$crm_emailid = '';	// crm activityid
		$save_mode = '';
		
		
		//Set message state
		
		if( isset($activity_data['state']) ){
			$message_state = $activity_data['state'];
		}
		
		//Set Recipient CrmId/EmailId from MetaData 
		
		if( isset($activity_data['metadata']) ){
			
			if( isset($activity_data['metadata']['crmid']) ){

				$crmEntityId = $activity_data['metadata']['crmid'];			
				$entityIdDetails = vtws_getIdComponents($crmEntityId);
				$parent_crmid = $entityIdDetails[1];
			
			}
		
			if( isset($activity_data['metadata']) && isset($activity_data['metadata']['emailid']) ){	
				$crm_emailid = $activity_data['metadata']['emailid'];			
			}
		}
		
		
		//Get Message Full Content From Mandrill Curl Request using Message ID
		
		$apiparams = array("id" => $mandrill_messageId);

		//$apiurl = 'messages/info';
		$apiurl = 'messages/content';
	        	
		$mandrillapi = new MandrillAPI();
		$apiResult = $mandrillapi->call($apiurl, $apiparams);

		/*echo "<pre>";
		print_r($apiResult);
		echo "</pre>";*/
		//exit;
		
		
		
		/* Check if message already associated
		 * If not then create message entry in vtiger_mandrill_activitycrmrel table, 
		 * we'll update it later
		 */
		
		$mandrillRel_exists_Result = $adb->pquery("SELECT * from vtiger_mandrill_activitycrmrel WHERE messageid = ?", array($mandrill_messageId));
		
		if( !$adb->num_rows($mandrillRel_exists_Result) ){

			$adb->pquery(" INSERT into vtiger_mandrill_activitycrmrel set messageid = ?, parent_crmid = ?, activityid = ?, state = ?",
			array($mandrill_messageId, $parent_crmid, $crm_emailid, $message_state));
		
		} else {
			
			$savedData = $adb->query_result_rowdata($mandrillRel_exists_Result, 0);	
			$parent_crmid = ($savedData['parent_crmid'] != '' && $parent_crmid == '') ? $savedData['parent_crmid'] : $parent_crmid;
			$crm_emailid = ($savedData['activityid'] != '' && $crm_emailid == '') ? $savedData['activityid'] : $crm_emailid;
			
		}

                if( isset($apiResult['status']) && $apiResult['status'] == 'error' ){
			continue;
		}
		
		
		/*
		 * If no emailid => create new Email Activity	
		 * else edit subject and content
		 */			
		
		if( $crm_emailid != '' && $crm_emailid != null ){				
			$save_mode = 'edit';
		} else {
			$save_mode = '';
		}
		
		$userId = $current_user->id;		

		$emailFocus = CRMEntity::getInstance('Emails');
	
		if($save_mode == ''){
			
			$emailFieldValues = array(
					'assigned_user_id' => $userId,
					'subject' => $apiResult['subject'],
					'description' => $apiResult['html'],
					'from_email' => $apiResult['from_email'],
					'saved_toid' => $apiResult['to']['email'],
					'ccmail' => '',
					'bccmail' => '',
					'parent_id' => $parent_crmid."@$userId|",
					'email_flag' => 'MANDRILL',
					'activitytype' => 'Emails',
					'date_start' => date('Y-m-d'),
					'time_start' => date('H:i:s'),
					'mode' => '',
					'id' => ''
			);
			$emailFocus->column_fields = $emailFieldValues;
			
		} else {
			
			$emailFocus->id = $crm_emailid;
			$emailFocus->retrieve_entity_info($crm_emailid, "Emails");
			
			
			$to = Zend_Json::decode(html_entity_decode($emailFocus->column_fields['saved_toid']));
			$cc = Zend_Json::decode(html_entity_decode($emailFocus->column_fields['ccmail']));
			$bcc = Zend_Json::decode(html_entity_decode($emailFocus->column_fields['bccmail']));
		
			$emailFocus->column_fields['saved_toid'] = (is_array($to)) ? implode(',',$to) : $to;
			$emailFocus->column_fields['ccmail'] = (is_array($cc)) ? implode(',',$cc) : $cc;
			$emailFocus->column_fields['bccmail'] = (is_array($bcc)) ? implode(',',$bcc) : $bcc;
			
			$emailFocus->column_fields['description'] = $apiResult['html'];
			$emailFocus->column_fields['subject'] = $apiResult['subject'];
		
			$emailFocus->column_fields['mode'] = 'edit';
			$emailFocus->column_fields['id'] = $emailFocus->id;
			
			$emailFocus->mode = 'edit';
		}
                $emailFocus->column_fields['email_status'] = ucwords($event_triggered);
		

		
		$emailFocus->save('Emails');
				
		$crm_emailid = $emailFocus->id;
		
		if(!empty($crm_emailid)) {
					
			if( $save_mode == '' ){
				
				if( $parent_crmid != '' && $parent_crmid > 0 ){
					$adb->pquery('insert into vtiger_seactivityrel values(?,?)', array($parent_crmid, $crm_emailid));
				}
				
				$emailFocus->setEmailAccessCountValue($crm_emailid);

				//$adb->pquery("INSERT into vtiger_mandrill_activitycrmrel SET messageid = ?, parent_crmid = ?, activityid = ?, state = ?", 
				//	array($mandrill_messageId, $parent_crmid, $crm_emailid, $message_state));
			}	
				
			$adb->pquery("UPDATE vtiger_mandrill_activitycrmrel SET parent_crmid = ?, activityid = ?, state = ? 
			WHERE messageid = ?", array($parent_crmid, $crm_emailid, $message_state, $mandrill_messageId));
						
		} 
		
		// end save_mode = ''
		
		
		echo "<br>EmailId : " . $crm_emailid . "<br>";
		
		//Set Access Count
		$message_accessCount = 0;
		$message_accessCount = (isset($activity_data['opens'])) ? ($message_accessCount + count($activity_data['opens'])) : 0;
		$message_accessCount = (isset($activity_data['clicks'])) ? ($message_accessCount + count($activity_data['clicks'])) : 0;
			
		
		if( $parent_crmid > 0 &&  $crm_emailid > 0 ){
			
			
			$countExistResult = $adb->pquery("SELECT access_count 
			FROM vtiger_email_track WHERE crmid = ? and mailid = ?", array($parent_crmid, $crm_emailid));
			
			if($adb->num_rows($countExistResult)){
				$adb->pquery("update vtiger_email_track set access_count = ? 
				WHERE crmid = ? and mailid = ?", array($message_accessCount, $parent_crmid, $crm_emailid));
			} else {
				$adb->pquery("INSERT INTO vtiger_email_track(crmid, mailid, access_count) 
				VALUES(?,?,?)", array($parent_crmid, $crm_emailid, $message_accessCount));
			}
		}
		
		
		
		
	}
}
?>