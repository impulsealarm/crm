<?php
require_once('includes/main/WebUI.php');

global $adb, $current_user;

$request = $_REQUEST;

if( 
	isset($request['To']) && $request['To'] != '' &&
	isset($request['SmsStatus']) && strtolower($request['SmsStatus']) == 'received'
){
	
	$adb = PearDatabase::getInstance();
	
	if(!$current_user) {
		$current_user = Users::getActiveAdminUser();
	}
	
	$to_number = $request['To'];
	
	$from_number = $request['From'];
	
	$smsid = $request['SmsSid'];
	
	$message = $request['Body'];
	
	$message_status = $request['SmsStatus'];
	
		
	/* Twilio will redirect here only if any registered crm user receives a sms
	 * so save sms if `To` user is found
	 */
	
	$phoneNumber = preg_replace ( '/\D+/', '', trim($request['To']));

	
	$to_userresult = $adb->pquery( 'select * from vtiger_users 
		where phone_crm_extension like ? or phone_crm_extension like ?', 
		array("%".trim($phoneNumber), "%".trim($to_number))
	);
	
	// if Number Found
	
	if ($adb->num_rows( $to_userresult ) > 0) {
		
		$assignedUserId = $adb->query_result($to_userresult, 0, 'id');
		
		//save as SMSNotifier record
		
		$save_mode = '';
		
		$alreadyExists = $adb->pquery("SELECT vtiger_smsnotifier.* FROM vtiger_smsnotifier 
		INNER JOIN vtiger_crmentity on vtiger_crmentity.crmid = vtiger_smsnotifier.smsnotifierid 
		INNER JOIN vtiger_smsnotifier_status on vtiger_smsnotifier_status.smsnotifierid = vtiger_smsnotifier.smsnotifierid 
		WHERE vtiger_crmentity.deleted = 0 and vtiger_smsnotifier_status.smsmessageid = ?
		",array($smsid));
		
		if($adb->num_rows($alreadyExists)){
			$save_mode = 'edit';
			
			//Ignore this Case,let Logs Handle this Case
			continue;
			
		}

		$moduleName = 'SMSNotifier';
		$focus = CRMEntity::getInstance($moduleName);
		
		if($save_mode == 'edit'){
			
			$focus->id = $adb->query_result($alreadyExists,0,'smsnotifierid');
			$focus->retrieve_entity_info($focus->id, $moduleName);
			$focus->mode = 'edit';
			$adb->pquery("delete from vtiger_smsnotifier_status where smsmessageid = ?",array($smsid));
		}

		$focus->column_fields['message'] = $message;
		$focus->column_fields['assigned_user_id'] = $current_user->id;
		$focus->column_fields['direction'] = 'incoming';
		$focus->column_fields['notified']= '0';
		$focus->save($moduleName);

		//reset assigned_user_id to user: who as to_number as phone_crm_extension
		$adb->pquery("update vtiger_crmentity set smcreatorid = ?, smownerid = ?, modifiedby = ? 
		where crmid = ?", array($assignedUserId, $assignedUserId, $assignedUserId, $focus->id));
		
		
		//To relate set from_no as related record for incoming sms
		$fromPhoneNumber = preg_replace( '/\D+/', '', trim($from_number) );
		
		$fromRecord = getUserWithNumber($fromPhoneNumber);
		
		if( $fromRecord['id'] == '' ){
			$fromRecord = getCustomerByPhone($fromPhoneNumber);
		}
		
		if( isset($fromRecord['id']) && $fromRecord['id'] != '' ){
			
			$linktoModule = getSalesEntityType($fromRecord['id']); 
			
			if( $linktoModule !== '' && $linktoModule !== false ) {
				
				//relateEntities($focus, $moduleName, $focus->id, $linktoModule, $fromRecord['id']);
				
				$check_result = $adb->pquery("select * from vtiger_crmentityrel where crmid = ? and relcrmid = ? and relmodule = ? ", array($focus->id, $fromRecord['id'], $linktoModule));
				
				if(!$adb->num_rows($check_result)){
					$adb->pquery("insert into vtiger_crmentityrel values(?,?,?,?)", array($focus->id, "SMSNotifier", $fromRecord['id'], $linktoModule));
				}
			}			
		}
		
		//save sms text and status number
		$needlookup = 1;
		$responseID = $smsid;
		$responseStatus = $message_status;
		$responseStatusMessage = '';
		
		$adb->pquery("INSERT INTO 
		vtiger_smsnotifier_status(smsnotifierid,tonumber,status,smsmessageid,needlookup, fromnumber) 
		VALUES(?,?,?,?,?,?)",
		array($focus->id, $request['To'], $message_status, $smsid, $needlookup, $from_number)
		);
		
		/* --- START: Notification Changes 10-2015 ---- */
		
		if($focus->id != ''){
			
			$focus->retrieve_entity_info($focus->id, 'SMSNotifier');
			
			$notificationContent = "";
		
			$smsnotifierid = $focus->id;          
			$sms_fromNumber = $from_number;
			
			$smsFrom = "";
            
        	$fromResult = $adb->pquery("select * from vtiger_crmentityrel where crmid = ?",array($smsnotifierid));
            
        	if(!$adb->num_rows($fromResult)){
        		$smsFrom .= $sms_fromNumber;        	
        	} else {        		
	            $relcrmid = $adb->query_result($fromResult, 0, 'relcrmid');
	            $relatedmodule = $adb->query_result($fromResult, 0, 'relmodule');
	            $entityName = getEntityName($relatedmodule,array($relcrmid));
	            $smsFrom .= '<a href="index.php?module='.$relatedmodule.'&view=Detail&record='.$relcrmid.'">'.$entityName[$relcrmid].'</a> ('.$sms_fromNumber.')';
        	}
        
        	$notificationContent .= " New SMS Received From: " . $smsFrom;
        	$notificationContent .= "<br>";
        	$notificationContent .= substr($message,0,15);
        	if(strlen($message) > 15 ){
        		$notificationContent .= "...";
        	}
            
			$adb->pquery("INSERT INTO vtiger_notifications(createdtime, parentid, parentmodule, linkurl, content, userid) 
			VALUES(?,?,?,?,?,?)",
			array($focus->column_fields['modifiedtime'], $smsnotifierid, $moduleName, 'index.php?module='.$moduleName.'&view=Detail&record='.$smsnotifierid, 
			$notificationContent, $assignedUserId));
		
		
		}
		
            
        
		/* --- END: Notification Changes 10-2015 ---- */
		
	}
}

//exit;




/*
 * function to find and get user id 
 * based on crm_extension (without special characters)
 */
function getUserWithNumber($number){

	$adb = PearDatabase::getInstance();
	
	$result = array('id' => '', 'setype' => 'Users');
	
	$number = substr($number, -10);
	$number = preg_replace ( '/\D+/', '', trim($number));
	

    $recordQuery = "SELECT * FROM vtiger_users WHERE 
    (phone_crm_extension = '" . $number . "' OR phone_crm_extension like '%" . $number . "')";
	
	$recordResult = $adb->pquery($recordQuery,array());
	
	if($adb->num_rows($recordResult)){
		$result['id'] = $adb->query_result($recordResult, 0, 'id');		
	}
	
	return $result;
	
}


/*
 * function to find Leads/Contacts/Accounts 
 * based on phoneNumber(without special characters)
 */

function getCustomerByPhone($number){
	
	$number = substr($number, -10);
	
	global $current_user;

	if(!$current_user) {
		$current_user = Users::getActiveAdminUser();
	}
	
	$adb = PearDatabase::getInstance();
	$result = array('id' => '', 'setype' => '');
	
	$lookUpQuery = "SELECT * FROM vtiger_crm_phonenumbers inner join 
	vtiger_crmentity on vtiger_crmentity.crmid = vtiger_crm_phonenumbers.crmid  
	WHERE 
	deleted = 0 and 
	(phone = '" . $number . "' OR phone like '%" . $number . "')";
			
	$lookUpResult = $adb->pquery($lookUpQuery, array());
	
	$numRows = $adb->num_rows($lookUpResult);
	
	if($numRows){
		
		if($numRows == 1){

			$crmid = $adb->query_result($lookUpResult, 0, 'crmid');
			$result['id'] = $crmid;
			$result['setype'] = getSalesEntityType($crmid);
		
		} else {
			
			$t_crmid = ''; $t_setype = '';
			
			/*for($i=0; $i<$numRows; $i++){
				$t_crmid = $adb->query_result($lookUpResult, $i, 'crmid');
				if(getSalesEntityType($t_crmid) == 'Contacts'){
					break;
				}
			}*/
			
			$contactid = '';
			$realtorid = '';
			$crm1id = '';
			
			for( $i=0; $i<$numRows; $i++ ){
				
				if( $adb->query_result($lookUpResult, $i, "crmid") == '' ) continue;
				
				$crmModuleName = getSalesEntityType( $adb->query_result($lookUpResult, $i, "crmid") );
				
				if( $crmModuleName == 'Realtor' ){
					$realtorid = $adb->query_result($lookUpResult, $i, "crmid");
				} else if( $crmModuleName == 'Contacts' ){
					$contactid = $adb->query_result($lookUpResult, $i, "crmid");
				} else {
					$crm1id = $adb->query_result($lookUpResult, $i, "crmid");
				}
			}
	
			if( $contactid != '' ){
				$t_crmid = $contactid;
			} else {
				if( $realtorid != '' ){
					$t_crmid = $realtorid;
				} else {
					$t_crmid = $crm1id;
				}
			}
				
			$result['id'] = $t_crmid;
			$result['setype'] = getSalesEntityType($t_crmid);		
		}
	}
	return $result;
}
?>
