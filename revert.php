<?php

/*
 * +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * **********************************************************************************
 */
require_once 'include/utils/utils.php';
require_once 'include/utils/VtlibUtils.php';
require_once 'modules/Emails/class.phpmailer.php';
require_once 'modules/Emails/mail.php';
require_once 'modules/Vtiger/helpers/ShortURL.php';
global $adb;
$adb = PearDatabase::getInstance ();

$perm_qry = "SELECT * FROM `vtiger_leadscf` WHERE `cf_763` LIKE '%http://maps.google.com/?q=%'";
$perm_result = $adb->pquery ( $perm_qry, array () );

$perm_rows = $adb->num_rows ( $perm_result );
for($i = 0; $i < $perm_rows; $i ++) {
	$map = $adb->query_result ( $perm_result, $i, "cf_763" );
	if($map!=""){
		$leadid = $adb->query_result ( $perm_result, $i, "leadid" );
		
		$a = str_replace("http://maps.google.com/?q=", "", $map);
		
		$sqlUp = "update vtiger_leadscf set cf_763=? where leadid=?";
		$params = array($a, $leadid);
		$adb->pquery($sqlUp, $params,true, "Error restoring records :");
	}
}
?>