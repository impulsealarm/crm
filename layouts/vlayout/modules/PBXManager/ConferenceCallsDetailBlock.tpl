<table class="table table-bordered blockContainer">	
	<tr>
		<th colspan="4">
			<span class="inventoryLineItemHeader">Related Calls</span>
		</th>
    </tr>
    <tr>
		<td><b>Type</b></td>
        <td><b>From</b></td>
        <td><b>To</b></td>
        <td><b>CallSid</b></td>
	</tr>
    
	{if empty($RELATED_CALLS)}
		
		<tr>
			<td colspan="4"> No Related Calls </td>
		</tr>
	
	{else}
	
		{assign var=CONF_CALLS value=$RELATED_CALLS.calls}
		
		{if !empty($CONF_CALLS)}		
			{foreach key=ITER item=CALL from=$CONF_CALLS}
				<tr>
					<td>{$CALL.type}</td>
					<td>{$CALL.from}</td>
					<td>{$CALL.to}</td>
					<td>{$CALL.callsid}</td>
				</tr>
			{/foreach}	
		{else}
			<tr>
				<td colspan="4"> No Related Calls </td>
			</tr>
		{/if}	
	
	{/if}
    
</table>