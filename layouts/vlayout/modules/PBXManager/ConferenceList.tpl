
<div>
	<div class="row-fluid detailViewTitle">
		<div class="span5">
			<div class="row-fluid">
				<span class="span10 margin0px">
					<span class="row-fluid">
						<span class="recordLabel font-x-x-large textOverflowEllipsis span pushDown" 
						title="Conferences">
							<span class="productname">Conferences</span>&nbsp;
						</span>
					</span>
				</span>
			</div>
		</div>
	</div>

	<hr/>

	<div class="listViewEntriesDiv" id="all_conferences">
		
		{assign var=WIDTHTYPE value=$CURRENT_USER_MODEL->get('rowheight')}
		<table class="table table-bordered listViewEntriesTable">
			<thead>
				<tr class="listViewHeaders">				
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">Conference Name</a>
					</th>
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">Status</a>
					</th>
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">Active Participants</a>
					</th>
					<th nowrap>
						<a href="javascript:void(0);" class="listViewHeaderValues">&nbsp;</a>
					</th>				
				</tr>
			</thead>
			
			{if !empty($CONFERENCES) }
			
				{foreach key=ITER item=CONFERENCE_DETAIL from=$CONFERENCES}
				
				<tr class="listViewEntries" id="{$CONFERENCE_DETAIL.friendly_name}" >
					<td class="listViewEntryValue {$WIDTHTYPE}"  nowrap>{$CONFERENCE_DETAIL.friendly_name}</td>
					<td class="listViewEntryValue {$WIDTHTYPE}"  nowrap>{$CONFERENCE_DETAIL.status}</td>
					<td class="listViewEntryValue {$WIDTHTYPE} participants"  nowrap>
					{if !empty($CONFERENCE_DETAIL.participants)}
						{foreach item=PARTICIPANT from=$CONFERENCE_DETAIL.participants}
							{$PARTICIPANT.display_name}<br/>
						{/foreach}
					{/if}
					</td>
					<td class="listViewEntryValue {$WIDTHTYPE}"  nowrap>
						{if $CONFERENCE_DETAIL.status != 'completed' }
							<input type="button" value="Listen Call" class="listen_call" 
							onClick="javascript:Vtiger_Twilio_Js.listenConference('{$CONFERENCE_DETAIL.sid}','{$CONFERENCE_DETAIL.friendly_name}', this);" />
							&nbsp;
							
							<input type="button" value="End Call" class="listen_end hide"
							onClick="javascript:Vtiger_Twilio_Js.endlistenConference('{$CONFERENCE_DETAIL.sid}','{$CONFERENCE_DETAIL.friendly_name}', this);" />
						{/if}
					</td>	
				</tr>
						
				{/foreach}
				
			{else}
				<tr class="listViewEntries" id="no_participant">
					<td colspan="4" class="listViewEntryValue"  nowrap style="text-align:center;">No Ongoing Conferences</td>
				</tr>
			{/if}
			
		</table>
	</div>
	
</div>