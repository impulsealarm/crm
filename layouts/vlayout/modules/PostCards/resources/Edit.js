Vtiger_Edit_Js("PostCards_Edit_Js",{},{
	
	registerLeavePageWithoutSubmit : function(form){
        return true;
    },
	
	/**
	 * Function which will register module change event
	 */
	registerChangeEventForModule : function(){
		var thisInstance = this;
		var advaceFilterInstance = Vtiger_AdvanceFilter_Js.getInstance();
		var filterContainer = advaceFilterInstance.getFilterContainer();
		filterContainer.on('change','select[name="modulename"]',function(e){
			thisInstance.loadFields();
		});
	},
	
	/**
	 * Function to load condition list for the selected field
	 * @params : fieldSelect - select element which will represents field list
	 * @return : select element which will represent the condition element
	 */
	loadFields : function() {
		var moduleName = jQuery('select[name="modulename"]').val();
		var allFields = jQuery('[name="moduleFields"]').data('value');
		var fieldSelectElement = jQuery('select[name="templateFields"]');
		var options = '';
		for(var key in allFields) {
			if(allFields.hasOwnProperty(key) && key == moduleName) {
				var moduleSpecificFields = allFields[key];
				var len = moduleSpecificFields.length;
				for (var i = 0; i < len; i++) {
					var fieldName = moduleSpecificFields[i][0].split(':');
					options += '<option value="'+moduleSpecificFields[i][1]+'"';
					if(fieldName[0] == moduleName) {
						options += '>'+fieldName[1]+'</option>';
					} else {
						options += '>'+moduleSpecificFields[i][0]+'</option>';
					}
				}
			}
		}
		
		if(options == '')
			options = '<option value="">None</option>';
		
		fieldSelectElement.empty().html(options).trigger("liszt:updated");
		return fieldSelectElement;
		
	},
	
	registerFillTemplateContentEvent : function() {
		jQuery('#templateFields').change(function(e){
			var textarea = jQuery("#back_msg_textarea");
			var value = jQuery(e.currentTarget).val();
			var textarea_val = jQuery("#back_msg_textarea").val();
			textarea_val += value;
			jQuery(textarea).val(textarea_val);
		});
	},
	
	/**
	 * Function to register event for image delete
	 */
	registerEventForImageDelete : function(){
		
		var formElement = this.getForm();
		
		var recordId = formElement.find('input[name="record"]').val();
		
		formElement.find('.imageDelete').on('click',function(e){
			
			var element = jQuery(e.currentTarget);
			
			var parentTd = element.closest('td');
			
			var imageUploadElement = parentTd.find("input:file");
			
			element.closest('div').remove();
		
		});
	},
	
	registerSelectBackOptionEvent : function(){
		
		var thisInstance = this;
	
		jQuery(".select_back_option").on("change", function(){
			
			var show = jQuery(this).val();
			
			var form = thisInstance.getForm();
				
			jQuery(".back_options").removeClass("hide");
			  
			if(show == 'file'){
				jQuery(".back_options .back_text").addClass("hide");
				jQuery(".back_options .back_input_file").removeClass("hide");
			} else {
				jQuery(".back_options .back_text").removeClass("hide");
				jQuery(".back_options .back_input_file").addClass("hide");
			}
		});
	},

	
	registerBasicEvents : function(container) {
		this._super(container);
		this.registerChangeEventForModule();
		this.registerFillTemplateContentEvent();
		this.registerSelectBackOptionEvent();
		
	}
});