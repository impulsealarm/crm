{strip}
<div class='editViewContainer'>

	<form class="form-horizontal recordEditView" id="EditView" name="EditView" method="post" action="index.php" enctype="multipart/form-data">
		
		{assign var=QUALIFIED_MODULE_NAME value={$MODULE}}
		{assign var=WIDTHTYPE value=$USER_MODEL->get('rowheight')}
		
		<input type="hidden" name="module" value="{$MODULE}" />
		<input type="hidden" name="action" value="Save" />
		<input type="hidden" name="record" value="{$RECORD_ID}" />

		<div class="contentHeader row-fluid">
			
			{assign var=SINGLE_MODULE_NAME value='SINGLE_'|cat:$MODULE}
			
			{if $RECORD_ID neq ''}
				<span class="span8 font-x-x-large textOverflowEllipsis" title="{vtranslate('LBL_EDITING', $MODULE)} {vtranslate($SINGLE_MODULE_NAME, $MODULE)} {decode_html($RECORD->get('description'))}">
					{vtranslate('Edit', $MODULE)} {vtranslate($SINGLE_MODULE_NAME, $MODULE)}
				</span>
			{else}
				<span class="span8 font-x-x-large textOverflowEllipsis">
					{vtranslate('Create', $MODULE)} {vtranslate($SINGLE_MODULE_NAME, $MODULE)}
				</span>
			{/if}
			
			<span class="pull-right">
				<button class="btn btn-success" type="submit"><strong>{vtranslate('LBL_SAVE', $MODULE)}</strong></button>
				<a class="cancelLink" type="reset" onclick="javascript:window.history.back();">{vtranslate('LBL_CANCEL', $MODULE)}</a>
			</span>
		</div>
		<table class="table table-bordered blockContainer showInlineTable">
			<tr>
				<th class="blockHeader" colspan="4">{vtranslate('SINGLE_PostCards', $MODULE)}</th>
			</tr>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}">{vtranslate('Post Card Name', $MODULE)}</td>
				<td class="fieldValue {$WIDTHTYPE}">
					<input type="text" name="name" value="{$RECORD->get('name')}"/>
				</td>
			</tr>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><span class="redColor">*</span>{vtranslate('Size', $MODULE)}</td>
				<td class="fieldValue {$WIDTHTYPE}">
					<input type="radio" name="size" value="4x6" class="image_size" data-validation-engine="validate[required,funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" {if $RECORD->get('size') eq '4x6'}checked="checked" {/if} /> 4x6 &nbsp;
					<input type="radio" name="size" value="6x11" class="image_size" data-validation-engine="validate[required,funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" {if $RECORD->get('size') eq '6x11'}checked="checked" {/if} /> 6x11
				</td>	
			</tr>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><span class="redColor">*</span>{vtranslate('Front', $MODULE)}</td>
				
				
				<td class="fieldValue {$WIDTHTYPE}">
				
					<span class="span10">
						
						<input type="file" accept="image/jpeg, image/png" id = "front" class="input-large" name = "front" data-validation-engine="validate[funcCall[ValidateFrontImage]]">
					
						{if $IMAGE_DETAILS|count gt 0 and 'front'|array_key_exists:$IMAGE_DETAILS}
						
							{assign var=IMAGE_INFO value=$IMAGE_DETAILS['front']}
							
							<div class="row-fluid">
							
								{if !empty($IMAGE_INFO.path) && !empty({$IMAGE_INFO.name})}
									
									<span class="span5">
										
										<img src="{$IMAGE_INFO.path}" style="height: 250px; width: 350px;" value="{$IMAGE_INFO.name}" />
										
										<input type="hidden" id ="frontImage" class="input-large" name="frontImage" value = "1">
									
									</span>
									
									<span class="span3 row-fluid">
										
										<span class="row-fluid existingImages">[{$IMAGE_INFO.name}]</span>
										<span class="row-fluid"><input type="button" id="file_front" value="Delete" class="imageDelete"></span>
									
									</span>
									
								{/if}
								
							</div>
						{/if}
						
						
					</span>
				</td>
			</tr>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><span class="redColor">*</span>{vtranslate('Back', $MODULE)}</td>
				<td class="fieldValue {$WIDTHTYPE}">
					
					<input type="radio" name="choose_back" value="file" class="select_back_option" data-validation-engine="validate[required,funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" data-name="choose_back" {if $BACK_OPTION eq 'file'} checked="checked" {/if}> Choose File&nbsp;
					
					<input type="radio" name="choose_back" value="message" class="select_back_option" data-validation-engine="validate[required,funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" data-name="choose_back" {if $BACK_OPTION eq 'message'} checked="checked" {/if}> Message
					
					<div class="back_options row-fluid {if $BACK_OPTION neq ''}show{else}hide{/if}" style="margin-top: 10px;">
						<span class="span10">
							
							<div class="back_text {if $BACK_OPTION eq 'message'} show {else} hide{/if}">
								<span class="filterContainer">
									<input type="hidden" name="moduleFields" data-value='{ZEND_JSON::encode($ALL_FIELDS)|escape}' />
									<span class="span4 conditionRow">
										<select class="chzn-select" name="modulename" >
											<option value="none">{vtranslate('LBL_SELECT_MODULE',$MODULE)}</option>
											{foreach key=MODULENAME item=FILEDS from=$ALL_FIELDS}
												{if $MODULENAME eq '0'}
													<option value="generalFields">{vtranslate('LBL_GENERAL_FIELDS', $MODULE)}</option>
												{else}
													<option value="{$MODULENAME}">{vtranslate($MODULENAME, $MODULENAME)}</option>
												{/if}
											{/foreach}
										</select>
									</span>&nbsp;&nbsp;
									<span class="span6">
										<select class="chzn-select span5" id="templateFields" name="templateFields">
											<option value="">{vtranslate('LBL_NONE',$MODULE)}</option>
										</select>
									</span>
								</span>
								<textarea name="back" rows="8" id="back_msg_textarea" maxlength="672" placeholder="Message Here (max 350 characters)" class="span11" style="margin-top: 10px;" data-validation-engine="validate[required,funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" >
									{if $BACK_OPTION eq 'message'}{decode_html($BACK)}{/if}
								</textarea>
							</div>
							
							<div class="back_input_file {if $BACK_OPTION eq 'file'} show {else} hide{/if}">
							
								<input type="file" accept="image/jpeg, image/png" id="back_file" class="input-large" name="back_file" data-validation-engine = "validate[funcCall[ValidateBackImage]]">
								
								{if $BACK_OPTION eq 'file'}
									{if $IMAGE_DETAILS|count gt 0 and 'back'|array_key_exists:$IMAGE_DETAILS}
										
						
										{assign var=IMAGE_INFO value=$IMAGE_DETAILS['back']}
										
										<div class="row-fluid">
											{if !empty($IMAGE_INFO.path) && !empty({$IMAGE_INFO.name})}
												<span class="span5">
													<input type="hidden" id ="backImage" class="input-large" name="backImage" value = "1">
													<img src="{$IMAGE_INFO.path}" style="height: 250px; width: 350px;" />
												</span>
												<span class="span3 row-fluid">
													<span class="row-fluid existingImages">[{$IMAGE_INFO.name}]</span>
													<span class="row-fluid"><input type="button" id="file_back" value="Delete" class="imageDelete"></span>
												</span>
											{/if}
										</div>
									{/if}
								{/if}
								
								
							</div>
						</span>
					</div>
				</td>
			</tr>
		</table>
		<br/>
		<table class="table table-bordered blockContainer showInlineTable">
			<thead>
				<tr>
					<th class="blockHeader" colspan="4">Description Details</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="fieldLabel medium">
						<label class="muted pull-right marginRight10px">{vtranslate('LBL_DESCRIPTION', $MODULE)}</label>
					</td>
					<td class="fieldValue medium" colspan="3">
						<div class="row-fluid">
							<span class="span10">
								<textarea id="PostCards_editView_fieldName_description" class="span11 " name="description">
									{decode_html($RECORD->get('description'))}
								</textarea>
							</span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		{include file="EditViewActions.tpl"|@vtemplate_path:$MODULE}
	</form>
{/strip}

<script>

	var front_image_width;
	var front_image_height;
	
	var back_image_width;
	var back_image_height;
	
	var _URL = window.URL;
	
	$('input[name="front"]').change(function(e){
		
		var file, img;
		
		if ((file = this.files[0])) {
			
			img = new Image();
			
			img.onload = function () {
				front_image_width = this.width;
				front_image_height = this.height;
			};
			
			img.src = _URL.createObjectURL(file);
		}
	});
	
	$('input[name="back_file"]').change(function(e){
		
		var file, img;
		
		if ( (file = this.files[0]) ) {
			
			img = new Image();
			
			img.onload = function () {
				back_image_width = this.width;
				back_image_height = this.height;
			};
			
			img.src = _URL.createObjectURL(file);
		}
		
	});
	
	
	function ValidateFrontImage(field, rules, i, options) {
		
		var _URL = window.URL || window.webkitURL;
	
		var size = jQuery.trim(jQuery('input[name="size"]:checked').val());
		
		if(jQuery('input[name="front"]').val() == '' && !jQuery("#frontImage").length ){
			
			rules.push('required'); 
		
		} else if(jQuery('input[name="front"]').val() != '') {
		
			front = jQuery('input[name="front"]')[0];
			
			var file, img;
				
			if ( (file = front.files[0]) ) {

				if(size == "4x6"){
					
					if(
						front_image_width != '1275' && 
						front_image_height != '1875'
					){
						return 'Please select image of 4.25 in x 6.25 in size.';
					
					} 
					
				} else if(size == "6x11"){
					
					if( 
						front_image_width != '1875' && 
						front_image_height != '3375' 
					){
						return 'Please select image of 6.25 in x 11.25 in size';
					}
				}
			}
		}
	}
	
	function ValidateBackImage(field, rules, i, options) {
	
		var size = jQuery.trim(jQuery('input[name="size"]:checked').val());
		
		front = jQuery('input[name="back_file"]')[0];
		
		var back_option = jQuery(".select_back_option:checked").val();
		
		if(back_option == 'file') {
			
			if(jQuery('input[name="back_file"]').val() == '' && !jQuery("#backImage").length ){
				
				rules.push('required');  
			
			} else if(jQuery('input[name="back_file"]').val() != '') {
				
				var file, img;
			
				if ( (file = front.files[0]) ) {

					if(size == "4x6"){
				
						if(
							back_image_width != '1275' && 
							back_image_height != '1875'
						){
							return 'Please select image of 4.25 in x 6.25 in size.';
					
						} 
					
					} else if(size == "6x11"){
				
						if( 
							back_image_width != '1875' &&
							back_image_height != '3375' 
						){
							return 'Please select image of 6.25 in x 11.25 in size';
						}
					}
				}
			}
		}
	}
	
</script>