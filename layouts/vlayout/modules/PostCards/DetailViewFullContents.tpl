{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	{assign var=WIDTHTYPE value=$USER_MODEL->get('rowheight')}
	<table class="table table-bordered detailview-table">
		<thead>
			<tr>
				<th class="blockHeader" colspan="4">{vtranslate('LBL_POSTCARDS_INFORMATION', $MODULE_NAME)}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><label class="muted marginRight10px">{vtranslate('Post Card Name', $MODULE_NAME)}</label></td>
				<td class="fieldValue {$WIDTHTYPE}">{$RECORD->get('name')}</td>
			</tr>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><label class="muted marginRight10px">{vtranslate('Size', $MODULE_NAME)}</label></td>
				<td class="fieldValue {$WIDTHTYPE}">{$RECORD->get('size')}</td>
			</tr> 
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><label class="muted marginRight10px">{vtranslate('Front',$MODULE_NAME)}</label></td>
				<td class="fieldValue {$WIDTHTYPE}">
					<div id="imageContainer" width="300" height="200">
						{assign var=IMAGE_INFO value=$IMAGE_DETAILS['front']}
						{if $IMAGE_INFO.path neq ''}
							<img src="{$IMAGE_INFO.path}" width="300px" style="height: 200px;">
						{/if}
					</div>
				</td>
			</tr>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><label class="muted marginRight10px">{vtranslate('Back',$MODULE_NAME)}</label></td>
				<td class="fieldValue {$WIDTHTYPE}">
					{if 'back'|array_key_exists:$IMAGE_DETAILS}
						<div id="imageContainer" width="300" height="200">
							{if $IMAGE_DETAILS['back'].path neq ''}
								<img src="{$IMAGE_DETAILS['back'].path}" width="300px" style="height: 200px;">
							{/if}
						</div>
					{else}
						{decode_html($RECORD->get('back'))}
					{/if}
				</td>
			</tr>
			<tr>
				<td class="fieldLabel {$WIDTHTYPE}"><label class="muted marginRight10px">{vtranslate('Description', $MODULE_NAME)}</label></td>
				<td class="fieldValue {$WIDTHTYPE}">{decode_html($RECORD->get('description'))}</td>
			</tr>
		</tbody>
	</table>
	<br/>
{/strip}