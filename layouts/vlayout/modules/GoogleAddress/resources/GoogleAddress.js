jQuery.Class("GoogleAddress_Js",{
	
	editInstance:false,
    
	getInstance: function(){
        if(GoogleAddress_Js.editInstance == false){
            var instance = new GoogleAddress_Js();
            GoogleAddress_Js.editInstance = instance;
            return instance;
        }
        return GoogleAddress_Js.editInstance;
    },
    
    autoPlaces:function(felement, ref, fullstreet) {
        
    	var dataMapping = mappingAddress[felement];
        
    	var field = dataMapping.street;
        
    	var results = document.getElementById(field+'_results');
        
    	results.innerHTML ="";
        
    	results.style.display ='none';

        var form = jQuery('#EditView');
        
        var service= new google.maps.places.PlacesService(results);
        
        service.getDetails({
                reference: ref
        }, function(place, status){
        	
        	if(status== google.maps.places.PlacesServiceStatus.OK){
        		
        		results.innerHTML=place.adr_address;
                    /*console.log(place.adr_address);
                     console.log(place);*/

                form.find('[name="'+dataMapping.street+'"]').val(jQuery('#'+field+'_results').find('.street-address').html());
        		//form.find('[name="'+dataMapping.street+'"]').val(fullstreet);
                form.find('[name="'+dataMapping.city+'"]').val(jQuery('#'+field+'_results').find('.locality').html());
                form.find('[name="'+dataMapping.state+'"]').val(jQuery('#'+field+'_results').find('.region').html());
                form.find('[name="'+dataMapping.postal_code+'"]').val(jQuery('#'+field+'_results').find('.postal-code').html());
                form.find('[name="'+dataMapping.country+'"]').val(jQuery('#'+field+'_results').find('.country-name').html());

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (addressType=='administrative_area_level_1') {
                        var val = place.address_components[i]['short_name'];
                        if(jQuery('#'+field+'_results').find('.region').html() == null) {
                            form.find('[name="'+dataMapping.state+'"]').val(val);
                        }else if(jQuery('#'+field+'_results').find('.locality').html() == null) {
                            form.find('[name="'+dataMapping.state+'"]').val(val);
                        }
                    }else if(addressType=='sublocality_level_1') {
                        var val = place.address_components[i]['short_name'];
                        if(jQuery('#'+field+'_results').find('.locality').html() == null){
                            form.find('[name="'+dataMapping.city+'"]').val(val);
                        }
                        form.find('[name="'+dataMapping.sublocality+'"]').val(val);
                    }
                }
            }
        });
    }
},{

});

function loadScript() {
   
	if((typeof google != 'undefined' && typeof google.maps != 'undefined') || jQuery('#view').val() == 'List') return;
    
    var script = document.createElement('script');
    
    script.type = 'text/javascript';
    
    script.src = 'https://maps.google.com/maps/api/js?&v=3.exp&sensor=true&libraries=places&callback=initialize';
    
    document.body.appendChild(script);
}

window.onload = loadScript;

function initialize() {

}

var mappingAddress = null;

jQuery(document).ready(function () {
        
	var form=jQuery('#EditView');
        
	var module=form.find('input[name="module"]').val();

    if(typeof(module) != 'undefined') {

        jQuery(document).on('click','*', function () {
            jQuery(document).find('div[id$="_results"]').hide();
        });

        var actionParams = {
            "type":"POST",
            "url":'index.php?module=GoogleAddress&action=ActionAjax&mode=getConfigFields',
            "dataType":"json",
            "data" : {
                'source_module':module
            }
        };

        AppConnector.request(actionParams).then(
        	function(data) {
        		
        		data = data.result;
        		var response = data.result['mapping'];
        		var mapping=data.result['mapping'];
                var countries=data.result.countries;

               // console.log(mapping);
        		mappingAddress = mapping;
                
        		for (felement in mapping) {
                    
        			var dataMapping = mapping[felement];
                    
        			var field = dataMapping.street;

                    form.find('[name="'+field+'"]').parent().closest('td').append('<div id="'+field+'_results" data-id="'+felement+'" style="border: 1px solid #ccc; position: absolute; z-index:1; background: #fff; padding: 0 7px; display: none"></div>')
                    
                    form.on('keypress','[name="'+field+'"]', function(e) {

                        var fieldName=jQuery(e.currentTarget).attr('name');
                        
                        var textVal=jQuery(e.currentTarget).val();
                        
                        var results= jQuery('#'+fieldName+'_results');
                        
                        var elementId=results.data('id');
                        
                        results.html('');
                        
                        results.hide();
                        
                        var resultsHtml='';
                        
                        if(textVal){
                            
                        	if(countries !='' && countries!= null) {
                                
                        		jQuery.each(countries,function(index,value) {
                                    
                        			var service= new google.maps.places.AutocompleteService();
                                    
                        			service.getPlacePredictions({
                                        input:textVal,
                                        componentRestrictions:{country: value}
                                    }, function (predictions, status){
                                        if(status != google.maps.places.PlacesServiceStatus.OK){
                                            //alert(status);
                                            return;
                                        }
                                        for(var i= 0, prediction; prediction= predictions[i]; i++){
                                            resultsHtml += '<div style="cursor:pointer;margin:5px 0;" id="default_address" onclick="GoogleAddress_Js.autoPlaces('+ elementId + ', \''+ prediction.reference+ '\',\''+ prediction.terms[0].value+ '\')" onmouseover="jQuery(this).css(\'background-color\',\'#ffff00\')" onmouseout="jQuery(this).css(\'background-color\',\'\')">'+ prediction.description+ '</div>';
                                        }
                                        results.html(resultsHtml);
                                        results.show();
                                    });
                                });
                            }else{
                                
                            	var service= new google.maps.places.AutocompleteService();
                                
                            	service.getPlacePredictions({
                                    input: textVal
                                }, function (predictions, status){
                                    if(status != google.maps.places.PlacesServiceStatus.OK){
                                        //alert(status);
                                        return;
                                    }
                                    for(var i= 0, prediction; prediction= predictions[i]; i++){
                                        resultsHtml += '<div style="cursor:pointer;margin:5px 0;" id="default_address" onclick="GoogleAddress_Js.autoPlaces('+ elementId + ', \''+ prediction.reference+ '\',\''+ prediction.terms[0].value+ '\')" onmouseover="jQuery(this).css(\'background-color\',\'#ffff00\')" onmouseout="jQuery(this).css(\'background-color\',\'\')">'+ prediction.description+ '</div>';
                                    }
                                    results.html(resultsHtml);
                                    results.show();
                                });
                            }
                        }
                    });
                }
            }
        );
    }
});