/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */

var Vtiger_RelatedListviewEdits_Js = {

    settings : [],
    moduleSetting : null,
    listViewContainer : null,

    init : function(){
        if(this.validListViewData()){
            var aDeferred = jQuery.Deferred();
            var thisInstance = this;
            var viewName = app.getViewName();
            var params = {
                'module' : 'RelatedListviewEdits',
                'action' : 'getSettings',
                'viewType' : viewName
            };

            AppConnector.request(params).then(
                function(data){
                    if(data.success){
                        if(data.result){
                            thisInstance.settings = data.result;
                            thisInstance.registerEvents();
                        }
                    }
                    aDeferred.resolve(data);
                },

                function(error){
                    aDeferred.reject(error);
                }
            );

            return aDeferred.promise();
        }
    },

    validListViewData : function(){
        var viewName = app.getViewName();
        if(viewName == 'List'){
            if(jQuery('#listViewContents .listViewEntriesTable tr.listViewEntries').length > 0){
                this.listViewContainer = jQuery('#listViewContents');
                return true;
            }
        }

        if(viewName == 'Detail'){
            if(jQuery('.relatedContainer .listViewEntriesTable tr.listViewEntries').length > 0){
                this.listViewContainer = jQuery('.relatedContainer');
                return true;
            }
        }

        return false;
    },

    updateCells : function(){
        var viewName = app.getViewName();
        var allFieldsSetting = this.moduleSetting['all_fields'];
        var columnFields = [];

        if(viewName=='List'){
            this.listViewContainer.find('.listViewEntriesTable tr.listViewHeaders th a.listViewHeaderValues').each(function(){
                columnFields.push(jQuery(this).data('columnname'));
            });
        }
        if(viewName=='Detail'){
            this.listViewContainer.find('.listViewEntriesTable tr.listViewHeaders th a.relatedListHeaderValues').each(function(){
                columnFields.push(jQuery(this).data('fieldname'));
            });
        }

        if(columnFields.length > 0){
            this.listViewContainer.find('.listViewEntriesTable tr.listViewEntries').each(function(index1){
                if(viewName=='List'){
                    var tdElements = jQuery(this).find('td.listViewEntryValue');
                }
                if(viewName=='Detail'){
                    var tdElements = jQuery(this).find('td');
                }

                tdElements.each(function(index2){
                    if(typeof columnFields[index2] != 'undefined'){
                        var columnname = columnFields[index2];
                        var fieldname = '';

                        if(typeof allFieldsSetting[columnname] != 'undefined'){
                            fieldname = allFieldsSetting[columnname];
                        }
                        jQuery(this).attr('data-fieldname', fieldname);
                        jQuery(this).attr('data-columnname', columnname);
                    }
                });
            });
        }
    },

    addHoverEditBtn : function(){
        var fields = this.moduleSetting['fields'];
        var editButton = '<span class="vte-edit-show-btn cursorPointer" style="position: absolute; right: 0; display: none;;">&nbsp;<i title="Edit" class="icon-pencil"></i></span>';
        for(var columnname in fields){
            this.listViewContainer.find('.listViewEntriesTable tr.listViewEntries').each(function(){
                var tdElement = jQuery(this).find('td[data-columnname='+columnname+']');
                var currentContent = tdElement.html();
                tdElement.html('<span class="vte-display-value">'+currentContent+'</span>');
                tdElement.append(editButton);
                tdElement.css({position: 'relative'});
                tdElement.addClass('vte-hover-edit-container');
            });
        }
    },

    registerShowHoverEditBtn : function(){
        var thisInstance = this;
        thisInstance.listViewContainer.find('td.vte-hover-edit-container').mouseover(function(){
            thisInstance.listViewContainer.find('td.vte-hover-edit-container .vte-edit-show-btn').css('display', 'none');
            if(jQuery(this).find('.vte-edit').length == 0){
                jQuery(this).find('.vte-edit-show-btn').css('display', 'inline');
            }
        }).mouseout(function(){
            thisInstance.listViewContainer.find('td.listViewEntryValue .vte-edit-show-btn').css('display', 'none');
        });
    },

    showHoverEditForm : function(record, tdElement){
        var aDeferred = jQuery.Deferred();
        var thisInstance = this;
        var params = {
            'module' : 'RelatedListviewEdits',
            'pmodule' : this.getModuleName(),
            'view' : 'FieldEdit',
            'fieldname' : tdElement.data('fieldname'),
            'record' : record
        };

        AppConnector.request(params).then(
            function(response){
                if(response){
                    var dataType = tdElement.data('field-type');
                    tdElement.find('.vte-display-value').css('display', 'none');
                    tdElement.append(response);
                    tdElement.attr('width', tdElement.find('.vte-edit').width()+48);
                    if(dataType=='date'){
                        tdElement.attr('width',200);
                    }
                    if(dataType=='reference'){
                        tdElement.attr('width',230);
                    }
                    if(dataType=='currency'){
                        tdElement.attr('width',200);
                    }
                    var vtigerEditInstance = new Vtiger_Edit_Js();
                    vtigerEditInstance.referenceModulePopupRegisterEvent(tdElement);
                    vtigerEditInstance.registerClearReferenceSelectionEvent(tdElement);
                    vtigerEditInstance.registerAutoCompleteFields(tdElement);
                    app.registerEventForDatePickerFields(tdElement);
                    tdElement.find('select,input').trigger("liszt:updated");
                }
                aDeferred.resolve(response);
            },

            function(error){
                aDeferred.reject(error);
            }
        );

        return aDeferred.promise();
    },

    validateField : function(field){
        var errorMessage = '';
        var rulesParsing = field.data('validation-engine');
        var getRules = /validate\[(.*)\]/.exec(rulesParsing);
        if (!getRules)
            return errorMessage;
        var str = getRules[1];
        var rules = str.split(/\[|,|\]/);

        for(var i=0; i<rules.length; i++){
            if(rules[i]=='required' && field.val() == ''){
                errorMessage = '* This field is required';
            }
            if(rules[i]=='Vtiger_Base_Validator_Js.invokeValidation'){
                var validated = Vtiger_Base_Validator_Js.invokeValidation(field);
                if(typeof validated != 'undefined'){
                    errorMessage = validated;
                }
            }
        }
        return errorMessage;
    },

    SaveField : function(tdElement){
        var aDeferred = jQuery.Deferred();

        var data = {};
        var fieldtype = tdElement.data('field-type');
        var fieldname = tdElement.data('fieldname');
        var value = tdElement.find('[name='+fieldname+']').val();

        //validate before save
        var errorMessage = this.validateField(tdElement.find('[name='+fieldname+']'));
        if(errorMessage != ''){
            tdElement.find('[name='+fieldname+']').validationEngine('showPrompt', errorMessage , 'error','topLeft',true);
            return;
        }

        data['field'] = fieldname;
        data['value'] = value;
        data['record'] = this.getRecord(tdElement);
        data['pmodule'] = this.getModuleName();
        data['module'] = 'RelatedListviewEdits';
        data['action'] = 'SaveAjax';
        if(fieldtype=='boolean'){
            data['value'] = tdElement.find('[name='+fieldname+']').is(':checked') ? 1 : 0;
        }

        var progressIndicatorElement = jQuery.progressIndicator({
            'message' : 'Saving...',
            'position' : 'html'
        });

        AppConnector.request(data).then(
            function(reponseData){
                if(reponseData.success){
                    progressIndicatorElement.progressIndicator({'mode':'hide'});
                    tdElement.find('.vte-edit').remove();
                    tdElement.find('.vte-display-value').html(reponseData.result[fieldname]['display_value']);
                    tdElement.find('.vte-display-value').css('display', '');
                    tdElement.removeAttr('width');
                }else{
                    progressIndicatorElement.progressIndicator({'mode':'hide'});
                }
            }
        );

        return aDeferred.promise();
    },

    registerCancelEvent : function(){
        //this.listViewContainer.find('.vte-edit-cancel-btn').unbind('click');
        this.listViewContainer.on('click','.vte-edit-cancel-btn',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            var button = e.target;
            var tdElement = jQuery(button).closest('td');
            tdElement.find('.vte-edit').remove();
            tdElement.removeAttr('width');
            tdElement.find('.vte-display-value').show();
            return;
        });
    },

    registerSaveEvent : function(){
        var thisInstance = this;
        //this.listViewContainer.find('.vte-edit-save-btn').unbind('click');
        this.listViewContainer.on('click','.vte-edit-save-btn',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            var tdElement = jQuery(e.target).closest('td');
            thisInstance.SaveField(tdElement);
            return;
        });
    },

    registerEditBtnEvent : function(){
        var thisInstance = this;
        var viewName = app.getViewName();
        this.listViewContainer.on('click','td.vte-hover-edit-container i.icon-pencil',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            var button = e.target;
            var tdElement = jQuery(button).closest('td');
            var record = thisInstance.getRecord(tdElement);

            //hide another
            tdElement.closest('tbody').find('td.vte-hover-edit-container').each(function(){
                jQuery(this).removeAttr('width');
                jQuery(this).find('.vte-edit').remove();
                jQuery(this).find('.vte-display-value').show();
            });
            //show current element
            thisInstance.showHoverEditForm(record, tdElement);
        });
    },

    /*
     * Function to register the list view row click event
     */
    registerCellClickEvent: function(){
        this.listViewContainer.on('click','td.vte-hover-edit-container',function(e){
            e.preventDefault();
            if(jQuery(e.target, jQuery(e.currentTarget)).is('td.vte-hover-edit-container')
                || jQuery(e.target, jQuery(e.currentTarget)).is('span.vte-display-value')) {
                jQuery(e.target, jQuery(e.currentTarget)).closest('tr').trigger('click');
            }else if(jQuery(e.target, jQuery(e.currentTarget)).is('a')) {
                //do something
                return;
            }
            e.stopPropagation();
            return;
        });
    },

    getRecord : function(tdElement){
        var viewName = app.getViewName();
        if(viewName == 'List'){
            return jQuery(tdElement).closest('tr').find('td:first-child .listViewEntriesCheckBox').val();
        }
        if(viewName == 'Detail'){
            return jQuery(tdElement).closest('tr').data('id');
        }

        return;
    },

    getModuleName : function(){
        var viewName = app.getViewName();
        if(viewName == 'List'){
            return app.getModuleName();
        }
        if(viewName == 'Detail'){
            return this.listViewContainer.find('input[name=relatedModuleName]').val();
        }

        return;
    },

    registerEvents : function(){
        var currentModule = this.getModuleName();
        var viewName = app.getViewName();

        if(typeof this.settings[currentModule] == 'undefined'){
            return;
        }

        this.moduleSetting = this.settings[currentModule];

        this.updateCells();
        this.addHoverEditBtn();
        this.registerShowHoverEditBtn();
        this.registerCellClickEvent();
        this.registerEditBtnEvent();
        this.registerCancelEvent();
        this.registerSaveEvent();
    }
}

jQuery(document).ready(function () {
    Vtiger_RelatedListviewEdits_Js.init();
    app.listenPostAjaxReady(function() {
        Vtiger_RelatedListviewEdits_Js.init();
    });
});