{* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** *}
{strip}
{literal}
    <style type="text/css">
        .vte-edit input, .vte-edit textarea, .vte-edit select, .vte-edit .uneditable-input{
            margin-bottom: 0 !important
        }
        .vte-edit-save-btn{
            background: url(layouts/vlayout/modules/RelatedListviewEdits/resources/save.png) no-repeat center right;
            width: 28px;
            height: 24px;
            display: inline-block;
            vertical-align: middle;
        }
        .vte-edit-cancel-btn{
            background: url(layouts/vlayout/modules/RelatedListviewEdits/resources/cancel.png) no-repeat center right;
            width: 26px;
            height: 24px;
            display: inline-block;
            vertical-align: middle;
        }
    </style>
{/literal}

<span class="vte-edit" style="position: relative; display: inline-block;" data-fieldname="{$FIELD_MODEL->get('name')}">
    {include file=vtemplate_path($FIELD_MODEL->getUITypeModel()->getTemplateName(),'RelatedListviewEdits') FIELD_MODEL=$FIELD_MODEL USER_MODEL=$USER_MODEL MODULE=$MODULE_NAME}
    <i title="{vtranslate('LBL_SAVE_BTN', 'RelatedListviewEdits')}" class="vte-edit-save-btn">&nbsp;</i>
    <i title="{vtranslate('LBL_CANCEL_BTN', 'RelatedListviewEdits')}" class="vte-edit-cancel-btn">&nbsp;</i>
    {if $FIELD_MODEL->getFieldDataType() eq 'multipicklist'}
    <input type="hidden" class="fieldname" value='{$FIELD_MODEL->get('name')}[]' data-prev-value='{$FIELD_MODEL->getDisplayValue($FIELD_MODEL->get('fieldvalue'))}' />
    {else}
    <input type="hidden" class="fieldname" value='{$FIELD_MODEL->get('name')}' data-prev-value='{Vtiger_Util_Helper::toSafeHTML($FIELD_MODEL->getDisplayValue($FIELD_MODEL->get('fieldvalue')))}' />
    {/if}
</span>
{/strip}