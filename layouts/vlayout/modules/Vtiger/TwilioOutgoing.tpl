<script type="text/javascript">
	Twilio.Device.setup("{$TWILIO_TOKEN}");
</script>


<div id="twilioCallContainer" class='modelContainer'>
	<div class="modal-header contentsBackground">
		<button data-dismiss="modal" class="close"
			title="{vtranslate('LBL_CLOSE')}">&times;</button>
		<span><strong>{vtranslate('Calling to dialed number', $MODULE)}</strong></span>
	</div>
	<div class="modal-body tabbable">
		&nbsp;&nbsp;
		<input type="text" id="number" name="number" value="{$TONUMBER}"  placeholder="Enter a phone number to call" />
		<hr>
		<button class="btn btn-success" onclick="Vtiger_Twilio_Js.call();">Call</button>
		<button class="btn btn-danger" onclick="Vtiger_Twilio_Js.hangup();">Hangup</button>
		<button class="btn btn-info" onclick="Vtiger_Twilio_Js.hold();" style="display:''" id='btnhold'>Hold</button>
		<button class="btn btn-info" onclick="Vtiger_Twilio_Js.unhold();" style="display:none" id='btnunhold'>UnHold</button>
		<button class="btn btn-warning" onclick="Vtiger_Twilio_Js.transfer();">Transfer</button>
		<hr>
		&nbsp;&nbsp;
		<div id="log">Loading pigeons...</div>
		<div id="dialpad">
				<table>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;width: 60px; font-weight: bold;" value="1" id="button1"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;width: 60px; font-weight: bold;" value="2" id="button2"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;width: 60px; font-weight: bold;" value="3" id="button3"></td>
				</tr>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="4" id="button4"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="5" id="button5"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="6" id="button6"></td>
				</tr>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="7" id="button7"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="8" id="button8"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="9" id="button9"></td>
				</tr>
				<tr>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="*" id="buttonstar"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="0" id="button0"></td>
				<td><input type="button" class="well squeezedWell span1" style="margin-right: 10px;margin-top: -10px;width: 60px; font-weight: bold;" value="#" id="buttonpound"></td>
				</tr>
				</table>
			</div>
	</div>
</div>