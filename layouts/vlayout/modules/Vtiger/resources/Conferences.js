var Vtiger_Conferences_Js = {

	conferencesContainer : false,
	conferenceInterval : false,
	
	/**
	 *  Main Function to be called on Page Load
	 */
	
	registerEvents : function(){
		
		var thisInstance = this;
		
		thisInstance.conferenceInterval = setInterval("Vtiger_Conferences_Js.getConferences()", 10000);

	},
	
	
	getConferences : function(){
		
		var conferencesDiv = jQuery("#all_conferences");		
		
		if( conferencesDiv.length ){
			
		} else {
			clearInterval(thisInstance.conferenceInterval);
			thisInstance.conferenceInterval = false;
			return false;
		}
		
		var thisInstance = this;
		var ajax_url = "index.php?module=Twilio&action=OngoingConferences";
		
		/*if( thisInstance.isNotificationPopupOpen() ){
			ajax_url += "&setSeen=true";
		}*/
		
		AppConnector.request(ajax_url).then(function(data){
			
			if( data.success && data.result ) {	
				
				if( data.result ){					
					
					var conferences = data.result.conferences;					
						
					if( conferences.length ){						
						if( conferencesDiv.find('table').find('tr#no_participant').length ){
							conferencesDiv.find('table').find('tr#no_participant').remove();
						}
					} else {
						conferencesDiv.find('table').find('tr.listViewEntries').remove();
						var newRow = '';
						newRow += '<tr class="listViewEntries" id="no_participant">' + 
						'<td colspan="4" class="listViewEntryValue"  nowrap style="text-align:center;">No Ongoing Conferences</td>' + 
						'</tr>';
						conferencesDiv.find('table').append(newRow);
					}
					
					for(var i=0; i<(conferences.length); i++){
						
						var conf = conferences[i];
						var friendlyName = conf['friendly_name'];
						
						if( conferencesDiv.find('tr#'+friendlyName).length ){
						
							//if conference row already present
							
							if( conf.status == 'completed' ){
							
								//remove completed conference row 
								
								conferencesDiv.find('tr#'+friendlyName).remove();
								
							} else {
								
								//update participants
								
								var participants = conf.participants;
								
								var htmldiv = '';
								
								for(var j=0; j<(participants.length); j++){								
									htmldiv += participants[j].display_name + "<br/>";
								}
								
								conferencesDiv.find('tr#'+friendlyName).find('td.participants').html(htmldiv);
							}
						
						} else {
							
							//append new tr to conferences div
							
							var lastRow = conferencesDiv.find('table tr:last');
							
							var confRow = thisInstance.getConferenceRow(conf);
							
							conferencesDiv.find('table').append(confRow);
						}
					}
						
				}
		 	}
		});
		
	},

	getConferenceRow : function(record){
		
		var newRow = '';
		
		newRow += '<tr class="listViewEntries" id="' + record.friendly_name + '" >';
		newRow += 
			'<td class="listViewEntryValue medium"  nowrap>' + record.friendly_name + '</td>' + 
			'<td class="listViewEntryValue medium"  nowrap>' + record.status + '</td>';
		
		newRow += '<td class="listViewEntryValue medium participants"  nowrap>';
		
		var participants = record.participants;
		
		var htmldiv = '';
		
		for(var j=0; j<(participants.length); j++){								
			htmldiv += participants[j].display_name + "<br/>";
		}
		
		newRow += htmldiv;
		
		newRow += '</td>'; 
		
		newRow += '<td class="listViewEntryValue medium"  nowrap>';

		if ( record.status != 'completed' ){
			
			newRow += '<input type="button" value="Listen Call" class="listen_call" onClick="javascript:Vtiger_Twilio_Js.listenConference(\'' + 
			record.sid + '\',\'' + record.friendly_name + '\', this);" />' + 
			'&nbsp;' + 			
			'<input type="button" value="End Call" class="listen_end hide" onClick="javascript:Vtiger_Twilio_Js.endlistenConference(\'' + 
			record.sid + '\',\'' + record.friendly_name + '\', this);" />';
		}
		
		newRow += '</td>';
		newRow += '</tr>';	
		
		return newRow;
		
	}	
    
};

//On Page Load
jQuery(document).ready(function() {	
	
	Vtiger_Conferences_Js.registerEvents();
	
});