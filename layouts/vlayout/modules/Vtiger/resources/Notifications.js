var Vtiger_Notifications_Js = {

	notificationsContainer : false,
	
	
	/**
	 *  Main Function to be called on Page Load
	 */
	
	registerEvents : function(){
		
		var thisInstance = this;
		
		//Popup Click

		$("#notificationContainer").click(function(){
			return false;
		});
		
		//Document Click
		
		$(document).click(function(){
			$("#notificationContainer").hide();
		});
		
		
		setInterval("Vtiger_Notifications_Js.getNotifications()", 10000);
		
		thisInstance.registerNotificationClickEvent();
		
		
		
	},
	
	
	getNotifications : function(){
		
		var thisInstance = this;
		var notification_url = "index.php?module=Vtiger&action=GetNotifications";
		
		if( thisInstance.isNotificationPopupOpen() ){
			notification_url += "&setSeen=true";
		}
		
		AppConnector.request(notification_url).then(function(data){
			
			if( data.success && data.result ) {	
				
				if( data.result ){					
					
					var notifications = data.result.notifications;					
					
					var totalCount = data.result.count;
					
					if( thisInstance.isNotificationPopupOpen() ){
						
						thisInstance.setNewCount('0');
						thisInstance.hideCount();
						
						if( notifications.length && notifications.length > 0){
							
							if( jQuery('#notificationsBody').find('.noti-row').length == 0 ){
								document.getElementById("notificationsBody").innerHTML = '';	
							}
							
							for(var i=0; i<(notifications.length); i++){
								
								var newDiv = thisInstance.getNewNotificationDiv(notifications[i]);
								
								if( jQuery('#notificationContainer').find('#notid_' + notifications[i].id).length == 0 ){								
									
									if( !(notifications[i].seen == '1') ){
										jQuery('#notificationsBody').prepend(newDiv);
									} else {
										jQuery('#notificationsBody').append(newDiv);
									}
									
								} else {
									jQuery('#notificationContainer').find('#notid_' + notifications[i].id).replaceWith( newDiv );
								}
								
								thisInstance.checkForSlimScroll();
							}
						} else {
							document.getElementById("notificationsBody").innerHTML = 'No Notification Yet';							
						}
							
					} else {

						thisInstance.setNewCount(totalCount);
						
						if( totalCount > 0 ){
							thisInstance.showCount();
						} else {
							thisInstance.hideCount();
						}
					}
				}
		 	}
		});
		
	},
	
	isNotificationPopupOpen : function(){
		return $('#notificationContainer').is(':visible');		
	},
	
	getNewNotificationDiv : function(record){
				
		var newdiv2 = '<div id = "notid_' + record.id + '" class="row-fluid noti-row" style="width:inherit; padding:3px;  border-bottom:1px solid #C7C6C6;';
		
		if(record.linkurl != ''){
			newdiv2 += 'cursor:pointer;';
		} 
		
		newdiv2 += '"';
		
		if(record.linkurl != ''){
			if(record.parentmodule == 'Emails'){
				newdiv2 += ' onClick=' + "'" + record.linkurl + "'";
			} else {
				newdiv2 +=  ' onClick="javascript:window.location.href=' + "'" + record.linkurl + "';" + '"';
			}
		}
		
		newdiv2 += ' >';
		
		var innerhtml = record.content;
		
		if( !(record.seen == '1') ){
			innerhtml = '<b>' + innerhtml + '</b>';
		}
		
		innerhtml += '<br><span style="color:grey;font-size:11px;"><i>' + record.addedon + '</i></span>';

		newdiv2 += innerhtml + '</div>';
		
		return newdiv2;
		
	},
	
	setNewCount : function(value){
		jQuery(".notification_count").html(value);
	},
	
	hideCount : function(){
		jQuery(".notification_count").hide();
	},
	
	showCount : function(){
		jQuery(".notification_count").show();
	},
	
	fadeOutCount : function(){
		jQuery(".notification_count").fadeOut("slow");
	},
	
	togglePopup : function(){
		$("#notificationContainer").fadeToggle(200);		
		jQuery('#notificationsBody').html('');
	},
	
	registerNotificationClickEvent : function(){
		
		var thisInstance = this;
		jQuery("#notificationLink").click(function(){
			thisInstance.togglePopup();
			thisInstance.getNotifications();			
			return false;			
		});
		
	},
	
	checkForSlimScroll : function(){
		
		var thisInstance = this;
		
		if( thisInstance.isNotificationPopupOpen() ){

			if( jQuery('#notificationsBody').height() > 390 && jQuery('#notificationContainer').find('.slimScrollDiv').length == '0' ){
				jQuery('#notificationsBody').slimScroll({
					height: '390px',
					railVisible: true,
					alwaysVisible: true,
					size: '6px'
				});
			}
		}
	}
	
	
    
};

//On Page Load
jQuery(document).ready(function() {	
	
	Vtiger_Notifications_Js.registerEvents();
	
});