
jQuery.Class("Vtiger_DetailPopup_Js",{

    detailPopupInstance : false,

	getInstance: function(){
        if( Vtiger_DetailPopup_Js.detailPopupInstance == false ){
            var module = app.getModuleName();
            var view = app.getViewName();
            var moduleClassName = module+"_"+view+"_Js";
            var fallbackClassName = Vtiger_DetailPopup_Js;
            if(typeof window[moduleClassName] != 'undefined'){
                var instance = new window[moduleClassName]();
            }else{
                var instance = new fallbackClassName();
            }
            Vtiger_DetailPopup_Js.detailPopupInstance = instance;
        }
        return Vtiger_DetailPopup_Js.detailPopupInstance;
	},
	
	triggerSendEmail : function(detailActionUrl, module){
    	
		Vtiger_Helper_Js.checkServerConfig(module).then(function(data){
			
			if(data == true){
	            var currentInstance = Vtiger_DetailPopup_Js.getInstance();
	            var parentRecord = new Array();
	            var params = {};
	            parentRecord.push(currentInstance.getRecordId());
	            params['module'] = app.getModuleName();
	            params['view'] = "MassActionAjax";
	            params['selected_ids'] = parentRecord;
	            params['mode'] = "showComposeEmailForm";
	            params['step'] = "step1";
	            params['relatedLoad'] = true;
	            
	            Vtiger_Index_Js.showComposeEmailPopup(params);
			} else {
				alert(app.vtranslate('JS_EMAIL_SERVER_CONFIGURATION'));
			}
		});
	},
	
	triggerSendSms : function(massActionUrl, module) {
		
		var thisInstance = Vtiger_DetailPopup_Js.getInstance();
				
        Vtiger_Helper_Js.checkServerConfig(module).then(function(data){
			
        	if(data == true){
                
				thisInstance.triggerMassAction(massActionUrl, function(container){					
					var templatelist = container.find('[id="smstemplate"]');
					var smsarea = container.find('[id="message"]');					
					templatelist.on('change',function(){
						smsarea.val('');
						smsarea.val(templatelist.val());
					});					
				});
			} else {
				alert(app.vtranslate('JS_SMS_SERVER_CONFIGURATION'));
			}
		});
    }
   

},{
     
	detailViewContentHolder : false,
	detailViewForm : false,
	
	//constructor
	init : function() {
	},

	getContentHolder : function() {
		if(this.detailViewContentHolder == false) {
			this.detailViewContentHolder = jQuery('div.popupcontentsDiv div.contents');
		}
		return this.detailViewContentHolder;
	},

	/**
	 * Function which will give the detail view form
	 * @return : jQuery element
	 */
	getForm : function() {
		if(this.detailViewForm == false) {
			
			//this.detailViewForm = jQuery('#detailView');
			this.detailViewForm = jQuery('#detailViewPopupForm');
			
		}
		return this.detailViewForm;
	},

	getRecordId : function(){
		return jQuery('#recordId').val();
	},

	triggerMassAction : function(massActionUrl,callBackFunction) {

		var thisInstance = this;//Vtiger_DetailPopup_Js.getInstance();
		
		var progressIndicatorElement = jQuery.progressIndicator();
		
		var selectedIds = new Array();
	    selectedIds.push(thisInstance.getRecordId());
	    
        var postData = {
           "selected_ids": JSON.stringify(selectedIds)
        };
        var actionParams = {
			"type":"POST",
			"url":massActionUrl,
			"dataType":"html",
			"data" : postData
		};
		    
		AppConnector.request(actionParams).then(
			function(data) {
				progressIndicatorElement.progressIndicator({'mode' : 'hide'});
				if(data) {
					app.showModalWindow(data,
						function(data){
							if(typeof callBackFunction == 'function'){
								callBackFunction(data);
							}
						},
						{'text-align' : 'left'}
					);

				}
			},
			function(error,err){
				progressIndicatorElement.progressIndicator({'mode' : 'hide'});
			}
		);
	},

    /*
	 * Function to register the submit event
	 */
	
	registerMassActionSubmitEvent : function(){
        var thisInstance = this;
		
        jQuery('body').on('submit','#massSave',function(e){
        	
        	var form = jQuery(e.currentTarget);
        	
			if( form.find('#message').length && form.find('#smstemplate').length ){
			
			    var smsTextLength = form.find('#message').val().length;        
	            if(smsTextLength > 160) {
	                var params = {
	                    title : app.vtranslate('JS_MESSAGE'),
	                    text: app.vtranslate('LBL_SMS_MAX_CHARACTERS_ALLOWED'),
	                    animation: 'show',
	                    type: 'error'
	                };
	                Vtiger_Helper_Js.showPnotify(params);
	                e.preventDefault();
	                return false;
	            }
	            
	            
			}
			
			var submitButton = form.find('[name=saveButton]');
            submitButton.attr('disabled','disabled');
			
            thisInstance.massActionSave(form).then(
    				function(data) {    					
    				},
    				function(error,err){
    				}
    		);            
			e.preventDefault();
		});
        
        
	},

    massActionSave : function(form){
		
		var aDeferred = jQuery.Deferred();
		var massActionUrl = form.serializeFormData();
		
        var progressIndicatorElement = jQuery.progressIndicator({
            'position' : 'html',
            'blockInfo' : {
                'enabled' : true
            }
        });
		AppConnector.request(massActionUrl).then(
			function(data) {
                progressIndicatorElement.progressIndicator({
                    'mode' : 'hide'
                });
				app.hideModalWindow();
				aDeferred.resolve(data);
			},
			function(error,err){
				app.hideModalWindow();
				aDeferred.reject(error,err);
			}
		);
		return aDeferred.promise();
	},

	

	/*
	 * Function to register the click event of email field
	 */
	registerEmailFieldClickEvent : function(){
		var detailContentsHolder = this.getContentHolder();
		detailContentsHolder.on('click','.emailField',function(e){
			e.stopPropagation();
		});
	},

	/*
	 * Function to register the click event of phone field
	 */
	registerPhoneFieldClickEvent : function(){
		var detailContentsHolder = this.getContentHolder();
                detailContentsHolder.on('click','.phoneField',function(e){
			e.stopPropagation();
		});
	},

	/*
	 * Function to register the click event of url field
	 */
	registerUrlFieldClickEvent : function(){
		var detailContentsHolder = this.getContentHolder();
		detailContentsHolder.on('click','.urlField',function(e){
			e.stopPropagation();
		});
	},

	

	triggerDisplayTypeEvent : function() {
		var widthType = app.cacheGet('widthType', 'narrowWidthType');
		if(widthType) {
			var elements = jQuery('#detailView').find('td');
			elements.addClass(widthType);
		}
	},

	


	
	registerEvents : function(){
		var thisInstance = this;
		
		
		thisInstance.registerMassActionSubmitEvent();

	}
});