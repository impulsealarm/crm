{include file='SidePopup.tpl'|@vtemplate_path}

<div class="bodyContents">
	<div class="mainContainer row-fluid" style="margin-top: 1px;" >
		<div class="contentsDiv popupcontentsDiv marginLeftZero" id="rightPanel" style="min-height:550px;">
			
			{assign var="MODULE_NAME" value=$MODULE_MODEL->get('name')}
			<input type="hidden" value="{$MODULE_NAME}" id="module" name="module" />
			<input type="hidden" value="DetailPopup" id="view" name="view" />
			<input id="recordId" type="hidden" value="{$RECORD->getId()}" />
			
			<div class="detailViewContainer">
			
				<div class="row-fluid detailViewTitle">
					<div class="span12">
						<div class="row-fluid">
						
						
							<div class="span3">
								<div class="row-fluid">
									<span class="span10 margin0px">
										<span class="row-fluid">
											<span class="recordLabel font-x-x-large textOverflowEllipsis span pushDown" title="{$RECORD->getName()}">
												{foreach item=NAME_FIELD from=$MODULE_MODEL->getNameFields()}
													{assign var=FIELD_MODEL value=$MODULE_MODEL->getField($NAME_FIELD)}
														{if $FIELD_MODEL->getPermissions()}
															<span class="{$NAME_FIELD}">{$RECORD->get($NAME_FIELD)}</span>&nbsp;
														{/if}
												{/foreach}
											</span>
										</span>
									</span>
								</div>
							</div>
							
							<div class="span7">
								<div class="pull-right detailViewButtoncontainer">
									<div class="btn-toolbar">
										
										
										<span class="btn-group">											
											<button class="btn" id="Leads_detailView_basicAction_LBL_SEND_EMAIL" 
											onclick='javascript:Vtiger_DetailPopup_Js.triggerSendEmail("index.php?module={$MODULE_NAME}&view=MassActionAjax&mode=showComposeEmailForm&step=step1","Emails");' >
												<strong>Send Email</strong>
											</button>
										</span>
										
										<span class="btn-group">										
											<button class="btn" 
											onclick='javascript:Vtiger_DetailPopup_Js.triggerSendSms("index.php?module={$MODULE_NAME}&view=MassActionAjax&mode=showSendSMSForm","SMSNotifier");' >
												<strong>{vtranslate('Send SMS', $MODULE_NAME)}</strong>
											</button>
										</span>
							
									</div>
								</div>
							</div>
							
							
							
						</div>
					</div>
				</div>
				
				<div class="detailViewInfo row-fluid">
					<div class=" span12 ">
						<form id="detailViewPopupForm" data-name-fields='{ZEND_JSON::encode($MODULE_MODEL->getNameFields())}' method="POST">
							{if !empty($PICKLIST_DEPENDENCY_DATASOURCE)} 
                                  <input type="hidden" name="picklistDependency" value="{Vtiger_Util_Helper::toSafeHTML($PICKLIST_DEPENDENCY_DATASOURCE)}"> 
                            {/if} 
							<div class="contents">
							
								<div class="row-fluid">
									<div class="span10">
										<div class="summaryView row-fluid">
											{$MODULE_SUMMARY}
										</div>
									</div>
								</div>
								
							</div>
						</form>
					</div>
				</div>
				
			</div>
		
		</div>
	</div>
</div>

