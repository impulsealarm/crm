<!-- ------------------ START - Twilio Outgoing Call Side Popup ------------- -->

<link rel='stylesheet' href='layouts/vlayout/skins/custom-style.css'>


<div id="push_sidebarphone">    
	
	<div id="twilioCallContainer" style="width:95%;padding:2%">        
		
		{assign var=CALLERID value=""}
		{assign var=TRAINING_MODE value=""}
			
		{if isset($CURRENT_USER_MODEL) }
			
			{if $CURRENT_USER_MODEL->get('phone_crm_extension') eq '14152148115'}
			{assign var=CALLERID value='19255495633'}
			{else}
			{assign var=CALLERID value=$CURRENT_USER_MODEL->get('phone_crm_extension')}
			{/if}
			
			{assign var=TRAINING_MODE value=$CURRENT_USER_MODEL->get('training_mode')}
		{else if isset($USER_MODEL) }
			{if $USER_MODEL->get('phone_crm_extension') eq '14152148115'}
			{assign var=CALLERID value='19255495633'}			
			{else}
			{assign var=CALLERID value=$USER_MODEL->get('phone_crm_extension')}			
			{/if}
			
			{assign var=TRAINING_MODE value=$USER_MODEL->get('training_mode')}
		{/if}
		
		<input type = "hidden" id = "caller_id" value = "{$CALLERID}"/>
		<input type = "hidden" id = "training_mode" value = "{$TRAINING_MODE}"/>
			
		<div style="    padding-top: 9px;">
		    <span id="log" style="font-size:13px;width:60%;float:left;text-align:left;">Loading...</span>
		
			<span class="close">
				<a href="javascript:void(0)"><img class="closesidebar3" src="layouts/vlayout/skins/images/closebtn.png" width="30" height="30"></a>
			</span>
		</div>
		
		<div style="clear:both;"></div>
		<br>
		
		<div style="text-align:left;" >
				<input type="text" id="dial_phone_number" name="number" value=""  placeholder="Enter Phone Number" style=" width: 75%;margin-bottom:0px;"/>
				<img src="layouts/vlayout/skins/images/delete-icon.png" class="del-img"/>
		</div>
			
		<div style="clear:both;"></div>
			
		<hr/>
			
		<div style="text-align:left;">
			<button class=" btn-success" onclick="Vtiger_Twilio_Js.call();">Call</button>
			<button class=" btn-danger" onclick="Vtiger_Twilio_Js.hangup();">Hangup</button>
		</div>
			
		<div style="clear:both;"></div>
			
		<hr/>
			
		<div style="text-align:center;">
			<table style="width:90%">
				<tr>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="1" id="button1"></td>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="2" id="button2"></td>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="3" id="button3"></td>
					<td><input type="button" class="" style="margin-right: 1%; font-weight: bold;width: 40px; " value="*" id="buttonstar"></td>
					
				</tr>
				<tr>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="4" id="button4"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="5" id="button5"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="6" id="button6"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="#" id="buttonpound"></td>
				</tr>
				<tr>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="7" id="button7"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="8" id="button8"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="9" id="button9"></td>
					<td><input type="button" class="" style="margin-right: 1%;margin-top: 1%; font-weight: bold;width: 40px; " value="0" id="button0"></td>
				</tr>
			</table>
		</div>
		
	</div>
</div>


<div class="fixed_menu">
	<ul>
		<li>
			<a href="#" data-toggle="tooltip" data-placement="left" title="Phone">
				<span class="nav_trigger2 open_call_popup"><img src="layouts/vlayout/skins/images/fixed_icon.png"></span>
			</a>
		</li>		
	</ul>        
</div>


<!-- ------------------ END - Twilio Outgoing Call Side Popup ------------- -->
