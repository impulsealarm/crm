<div class="modelContainer" style='min-width:350px;'>
		
	<div class="modal-header contentsBackground">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Stripe Account Doesn't Exists</h3>
	</div>
    
    <form class="form-horizontal" id = "model_window" name="check_option" method="post">
		
		<input type="hidden" name="module" value="{$MODULE}">
		<input type="hidden" name="action" value="StripeActionAjax">        
		
		{if $REALTOR gt '0'}
			<input type="hidden" name ="realtor" value="{$REALTOR}" />
		{/if}
		
		<div class="modal-body">
        	<div class="row-fluid">
            	<div class="control-group">
            		<div class="controls" style="float: left;margin-left: 0px;">
                 		<input type="checkbox" name="save_record" value="yes"> 
                 	</div><label style = "margin-left:20px;">
                    	<b>Selected realtor doesn't have stripe account, Do you still want to save and send e-mail reminder?</b>
                   	</label>
               </div>
			</div>
    	</div>  
        <div class="modal-footer">
			<button class="btn btn-success" type="submit" name="saveButton">
				<strong>Save</strong>
			</button>
			<a class="cancelLink cancelLinkContainer pull-right" type="reset" data-dismiss="modal">Cancel</a>
		</div>
	</form>
</div>