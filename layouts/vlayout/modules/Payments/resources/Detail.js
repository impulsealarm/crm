/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Vtiger_Detail_Js("Payments_Detail_Js",{
	
	
},{
	ApproveNowPaymentEvent : function(){
		
		var thisInstance = this;
	
		jQuery('#approveNow').click(function(e) {

			var record = thisInstance.getRecordId();
            
			var params = {
	 			'module' : app.getModuleName(),
	 			'action' : 'CheckSMSCodePopup',
	 		}
			
			var progressIndicatorElement = jQuery.progressIndicator({
				'message' : "Progressing",
				'position' : 'html',
				'blockInfo' : {
					'enabled' : true
				}
			});
			
			AppConnector.request(params).then(
					function(data) {
						
						var response = data['result'];
						
						var result = response['success'];
						
						var showSMSPopup = response['show_sms_popup'];
						
						if(result == true) {
			
							if(showSMSPopup){

								var popupParams = {
									"module"	: "Payments",
									"view"		: "ShowSMSCodePopup",
									"record"	: record
								};
								
								AppConnector.request(popupParams).then(
									function(data) {
										
										progressIndicatorElement.hide();
										
										var ValidateForm = function(data){
											
											jQuery('#validate_code').validationEngine({
												'onValidationComplete' : function(form,valid){
													return valid;
												}
											});
											
											thisInstance.SubmitSMSCode();
										}
										
										app.showModalWindow(data,function(data){
											
											if(typeof ValidateForm == 'function'){
												ValidateForm(data);
											}
										
										});
									}
								);
							} else {
								
								var params = {
									"module":"Payments",
									"action":"ApprovePayments",
									"record":record
								};
								
								AppConnector.request(params).then(
									function(paymentdata) {
										
										progressIndicatorElement.hide();
										
										var payment_response = paymentdata['result'];
										
										var message = payment_response.message;
										
										if(payment_response.success){
											var params = {
								                title: message,
								                text: "Transaction Successfull",
								                type: "success"
									        };
										} else {
											var params = {
								                title: "Error",
								                text: message,
								                type: "error"
									        };
										}
										Vtiger_Helper_Js.showPnotify(params);
										location.reload();
									} 
								);
							}
						} else {
							progressIndicatorElement.hide();
							
							if(response.message == 'require_sms_server_setting'){
								alert(app.vtranslate('JS_SMS_SERVER_CONFIGURATION'));
							} else {
								
								var notifyParams = {
									text: response.message,
						            type: "error"
						        };
								
								Vtiger_Helper_Js.showPnotify(notifyParams);
							}
						}
					}
			);
		});
	},
	
	SubmitSMSCode : function() {
		
		var aDeferred = jQuery.Deferred();
		
		jQuery('#validate_code').on('submit',function(e){
			
			var validationResult = jQuery(e.currentTarget).validationEngine('validate');
			
			if(validationResult == true){
				
				var progressInstance = jQuery.progressIndicator();
				
				var formData = jQuery(e.currentTarget).serializeFormData();
				
				AppConnector.request(formData).then(
					function(Data){
						
						progressInstance.hide();
						
						var res = Data['result']['success'];
						
						if(res == false){
						
							jQuery("#sms_code").val("");
							
							Vtiger_Helper_Js.showPnotify(Data['result']['message']);
						
						} else {
							var message = Data['result']['message'];
							
							app.hideModalWindow();
							
							if(message == 'Paid'){
							
								var params = {
						                title: message,
						                text: "Transaction Successfull",
						                type: "success"
						        };
							
							} else {
								
								var params = {
						                title: message['type'],
						                text: message['message'],
						                type: "error"
						        };
								
							}
							Vtiger_Helper_Js.showPnotify(params);
							location.reload();
						} 
					}
				);
			}
			e.preventDefault();
		});
		return aDeferred.promise();
	},
	
	registerEvents : function(){
	
		var thisInstance = this;
		
		this._super();
		
		thisInstance.ApproveNowPaymentEvent();
	},
	
});
	