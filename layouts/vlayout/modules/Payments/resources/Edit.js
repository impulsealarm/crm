Vtiger_Edit_Js("Payments_Edit_Js",{
   
},{
	
	checkStripeAccountAndContactReferral : false,
	
	/**
	 * This function will register before saving any record
	 * Check if Selected Realtor have Stripe Account or not.
	 * Check Contact Referral
	 * if Selected Realtor is affiliate and contact is referral then check referral belogs to affiliate
	 * if Selected Realtor is not affiliate and contact is referral then show model window
	*/
	
	registerRecordPreSaveEvent : function(form) {
		
		var thisInstance = this;
		
		if(typeof form == 'undefined') {
			form = this.getForm();
		}
		
		form.on(Vtiger_Edit_Js.recordPreSave, function(e, data) {

			var realtor = jQuery("input[name='realtor_id']").val();
						
			var contact = jQuery("input[name='contactid']").val();
						
			if(realtor && contact && !thisInstance.checkStripeAccountAndContactReferral){

				var progressIndicatorElement = jQuery.progressIndicator({
					'message' : "Progressing",
					'position' : 'html',
					'blockInfo' : {
		                'enabled' : true
		            }
				});
				
				var affiliate_params = {
					'module' : app.getModuleName(),
		 			'action' : 'ApprovePayments',
		 			'mode'	 : 'checkAffiliateAndReferral',
		 			'realtor': realtor,
		 			'contact': contact
		 		}
					
				AppConnector.request(affiliate_params).then(
					function(affiliate_data) {
						
						var response = affiliate_data.result
						
						if(response.success){
							
							var params = {
								'module' : app.getModuleName(),
					 			'action' : 'ApprovePayments',
					 			'mode'	 : 'checkStripeAccount',
					 			'realtor': realtor,
					 		}

							AppConnector.request(params).then(
								function(data) {
									
									var response = data['result'];

									var result = response['success'];

									if(result == false){

										var params = {
								 			'module': app.getModuleName(),
								 			'view'	: 'ShowStripeAlertWindow',
								 			'realtor'  : realtor
								 		}
										
										AppConnector.request(params).then(
											function(data) {

												progressIndicatorElement.progressIndicator({'mode':'hide'});
												
												if(data) {
													
													var callBackFunction = function(data){
														thisInstance.SaveStripeAlertOption();
													}
													
													app.showModalWindow(data,{'text-align' : 'left'});
													
													if(typeof callBackFunction == 'function'){
														callBackFunction(data);
													}
												}
											}
										);
									} else {
										thisInstance.checkStripeAccountAndContactReferral = true;
										form.submit();
									}
								}
							);
						} else {

							progressIndicatorElement.progressIndicator({'mode':'hide'});
							
							thisInstance.checkStripeAccountAndContactReferral = false;
							
							var error_params = {
				                title: "Error",
				                text: response.message,
				                type: "error"
					        };
							Vtiger_Helper_Js.showPnotify(error_params);
						}
					}
				);
				e.preventDefault();
			}
		});
	},
	
	SaveStripeAlertOption : function(){
		
		var thisInstance = this;

		var form = thisInstance.getForm();
		
		jQuery('#model_window').on('submit',function(e){
				
			app.hideModalWindow();
			

			var progressIndicatorElement = jQuery.progressIndicator({
				'message' : "Progressing",
				'position' : 'html',
				'blockInfo' : {
	                'enabled' : true
	            }
			});
			
			var formData = jQuery(e.currentTarget).serializeFormData();
			
			var selectedOption = formData.save_record;
			
			if(typeof selectedOption != 'undefined' && selectedOption == "yes"){
				
				AppConnector.request(formData).then(
				
					function(Data){
						
						var response = Data.result;
						
						if(response.success){
							thisInstance.checkStripeAccountAndContactReferral = true;
							form.submit();
						}
					}
				);
			} else {
				thisInstance.checkStripeAccountAndContactReferral = true;
				form.submit();
			}
			e.preventDefault();
		});
	},
	
	registerBasicEvents : function(container) {
		this._super(container);
		this.registerRecordPreSaveEvent(container);
	}
});