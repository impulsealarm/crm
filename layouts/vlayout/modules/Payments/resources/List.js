Vtiger_List_Js("Payments_List_Js",{
	
	triggerApprovePayments : function(massActionUrl,module){
		
		var thisInstance = this;
	
		var listInstance = thisInstance.getInstance();
		
		var validationResult = listInstance.checkListRecordSelected();
		
		if(validationResult != true){
		
			var selectedIds = listInstance.readSelectedIds(true);
			var excludedIds = listInstance.readExcludedIds(true);
			var cvId = listInstance.getCurrentCvId();
			var postData = listInstance.getDefaultParams();

			delete postData.module;
            delete postData.view;
            delete postData.parent;
            
            postData.selected_ids = selectedIds;
            postData.excluded_ids = excludedIds;
            postData.viewname = cvId;

			var params = {
	 			'module' : app.getModuleName(),
	 			'action' : 'CheckSMSCodePopup',
	 		}
			
			var progressIndicatorElement = jQuery.progressIndicator({
				'message' : "Progressing",
				'position' : 'html',
				'blockInfo' : {
					'enabled' : true
				}
			});
    		
    		AppConnector.request(params).then(
    			function(data){
    				
    				var response = data.result;
    				
					var showSMSPopup = response['show_sms_popup'];
					
    				if(response.success){
    		
    					if(showSMSPopup){

							var popupParams = {
								"module"	: "Payments",
								"view"		: "MassActionAjax",
								"mode"		: "showSMSCodePopup",
							};
							
							if(postData)
								popupParams = jQuery.extend(popupParams,postData);
							
							AppConnector.request(popupParams).then(
								function(data) {
									
									progressIndicatorElement.hide();
									
									var ValidateForm = function(data){
										
										jQuery('#validate_code').validationEngine({
											'onValidationComplete' : function(form,valid){
												return valid;
											}
										});
										
										thisInstance.SubmitSMSCode();
										//.then(function(Data){});
									}
									
									app.showModalWindow(data,function(data){
										
										if(typeof ValidateForm == 'function'){
											ValidateForm(data);
										}
									
									});
								}
							);
							
						} else {
	    					var actionParams = {
    							"type":"POST",
    							"url":massActionUrl,
    							"dataType":"html",
    							"data" : postData
    						};
        					
        					AppConnector.request(actionParams).then(
        			    		function(Data){

    								progressIndicatorElement.hide();
    								
        			    			Data = JSON.parse(Data);
    	    				
        			    			var res = Data['result']['success'];
    								
    								if(res == false){
    									Vtiger_Helper_Js.showPnotify(Data['result']['message']);
    								} else {
    									
    									var message = Data['result']['message'];
    									
    									if(message == 'Paid'){
    										var params = {
								                title: message,
								                text: "Payments Approved Successfully",
								                type: "success"
    								        };
    									}
    									Vtiger_Helper_Js.showPnotify(params);
    									location.reload();
    								}
        			    		}
        			    	);
						}
    				} else {
						progressIndicatorElement.hide();
						
						if(response.message == 'require_sms_server_setting'){
							alert(app.vtranslate('JS_SMS_SERVER_CONFIGURATION'));
						} else {
							
							var notifyParams = {
								text: response.message,
					            type: "error"
					        };
							
							Vtiger_Helper_Js.showPnotify(notifyParams);
						}
					}
    			}
    		);
			
		} else {
			listInstance.noRecordSelectedAlert();
		}
	},
	
	SubmitSMSCode : function() {
		
		var thisInstance = this;
		
		var aDeferred = jQuery.Deferred();
		
		jQuery('#validate_code').on('submit',function(e){
			
			var validationResult = jQuery(e.currentTarget).validationEngine('validate');
			
			if(validationResult == true){
				
				var progressInstance = jQuery.progressIndicator();
				
				var formData = jQuery(e.currentTarget).serializeFormData();
				
				var params = {
					"module": "Payments",
					"view": formData.view,
					"mode": formData.mode,
					"sms_code": formData.sms_code
				};
				AppConnector.request(params).then(
					function(data){
						
						var response = JSON.parse(data);
						
						response = response.result;
						
						if(response.success == true){
							
							delete formData.sms_code;
							
							formData.mode = "MassApprovePayments";
							
							AppConnector.request(formData).then(
								function(Data){
									
									progressInstance.hide();

									Data = JSON.parse(Data);
		    	    				
        			    			var res = Data['result']['success'];
    								
									if(res == false){
										Vtiger_Helper_Js.showPnotify(Data['result']['message']);
									} else {
										var message = Data['result']['message'];
										
										app.hideModalWindow();
										
										if(message == 'Paid'){
										
											var params = {
								                title: message,
								                text: "Payments Approved Successfully",
								                type: "success"
									        };
										
										} 
										Vtiger_Helper_Js.showPnotify(params);
										location.reload();
									} 
								}
							);
						}else {
							progressInstance.hide();
							
							jQuery("#sms_code").val("");
							Vtiger_Helper_Js.showPnotify(response.message);
						}
					}
				);
			}
			e.preventDefault();
		});
		return aDeferred.promise();
	}

},
{
});