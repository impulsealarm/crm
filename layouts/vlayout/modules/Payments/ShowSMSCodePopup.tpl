
{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	<div class="modelContainer" style='min-width:350px;'>
		
		<div class="modal-header contentsBackground">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h3>SMS Code Verification</h3>
		</div>
    
	    <form class="form-horizontal" id = "validate_code" name="validate_code" method="post">
			<input type="hidden" name="module" value="{$MODULE}">
			
			{* 8jan2016 Change If mass approved payment set required hidden fields *}
			
			{if $MASS_APPROVED_PAYMENTS eq true}
				<input type="hidden" name="view" value="{$VIEW}">        
			   	<input type="hidden" name="mode" value="{$MODE}" />
				<input type="hidden" name="viewname" value="{$VIEWNAME}" />
				<input type="hidden" name="selected_ids" value={ZEND_JSON::encode($SELECTED_IDS)}>
				<input type="hidden" name="excluded_ids" value={ZEND_JSON::encode($EXCLUDED_IDS)}>
       		{else}
		    	<input type="hidden" name="action" value="ApprovePayments">        
		   		<input type="hidden" name="record" id="record" value="{$RECORD}" />
		   	{/if}
		   	
		   	{* Changes END *}
		   	
		   	<div class="modal-body">
	        	<div class="row-fluid">
	            	<div class="control-group">
	            		<label class="control-label">
	                    	<b> SMSCode: </b>
	                   	</label>
	                 	<div class="controls">
	                 		<input id="sms_code" data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" name="sms_code" type="text" class="autoComplete" 
							value="" placeholder="Enter SMS Code" />
						</div>
	               </div>
	            </div>
	    	</div>  
	        <div class="modal-footer">
				<button class="btn btn-success" type="submit" name="saveButton">
					<strong>Save</strong>
				</button>
				<a class="cancelLink cancelLinkContainer pull-right" type="reset" data-dismiss="modal">Cancel</a>
			</div>
	    </form>
{/strip}