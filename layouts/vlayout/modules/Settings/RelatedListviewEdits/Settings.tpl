{* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** *}
 
<div class="container-fluid">
    <form id="rle_form" name="rleForm" action="" method="post">
        <div class="contentHeader row-fluid">
            <h3 class="span8 textOverflowEllipsis">{vtranslate('LBL_SETTING_HEADER', $QUALIFIED_MODULE)}</h3>
            <span class="pull-right"><button type="button" id="rel_setting_save_btn" class="btn btn-success"><strong>{vtranslate('LBL_SAVE', $MODULE)}</strong></button></span>
        </div>
        <hr>
        <div class="clearfix"></div>

        <div class="listViewContentDiv row-fluid" id="listViewContents">
            <div class="row marginBottom10px">
                <div class="span4">
                    <label>{vtranslate('LBL_SELECT_MODULE', $QUALIFIED_MODULE)}</label>
                </div>
                <div class="span8">
                    <select name="current_module" class="chzn-select" onchange="this.form.submit();">
                    {foreach item=MODULE_ITEM from=$LIST_MODULES}
                        <option value="{$MODULE_ITEM.name}" {if $CURRENT_MODULE.name eq $MODULE_ITEM.name}selected{/if}>{$MODULE_ITEM.tablabel}</option>
                    {/foreach}
                    </select>
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span4">
                    <label>{vtranslate('LBL_MODULE_ACTIVE', $QUALIFIED_MODULE)}</label>
                </div>
                <div class="span8">
                    <select name="module_active" class="chzn-select">
                        <option value="1" {if $CURRENT_MODULE.active eq 1}selected{/if}>{vtranslate('MODULE_ACTIVE_OPTION', $QUALIFIED_MODULE)}</option>
                        <option value="0" {if $CURRENT_MODULE.active eq 0}selected{/if}>{vtranslate('MODULE_INACTIVE_OPTION', $QUALIFIED_MODULE)}</option>
                    </select>
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span4">
                    <label>{vtranslate('LBL_MODULE_ACTIVE_TYPE', $QUALIFIED_MODULE)}</label>
                </div>
                <div class="span8">
                    <select name="module_active_type" class="chzn-select">
                        <option value="List" {if $CURRENT_MODULE.active_type eq 'List'}selected{/if}>{vtranslate('MODULE_ACTIVE_TYPE_OPTION_LISTVIEW', $QUALIFIED_MODULE)}</option>
                        <option value="RelatedList" {if $CURRENT_MODULE.active_type eq 'RelatedList'}selected{/if}>{vtranslate('MODULE_ACTIVE_TYPE_OPTION_RELATEDLIST', $QUALIFIED_MODULE)}</option>
                        <option value="Both" {if $CURRENT_MODULE.active_type eq 'Both'}selected{/if}>{vtranslate('MODULE_ACTIVE_TYPE_OPTION_BOTH', $QUALIFIED_MODULE)}</option>
                    </select>
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span4">
                    <label>{vtranslate('LBL_ALLOW_EDIT_ALL_FIELDS', $QUALIFIED_MODULE)}</label>
                </div>
                <div class="span8">
                    <input value="1" name="allow_edit_all_fields" type="checkbox" {if $CURRENT_MODULE.allow_edit_all_fields eq 1}checked{/if} />
                </div>
            </div>
            <div class="row marginBottom10px">
                <div class="span12">
                    <label>{vtranslate('LBL_SELECT_FIELDS', $QUALIFIED_MODULE)}</label>
                </div>
                <div class="span12">
                    <select name="module_fields[]" class="chzn-select" multiple style="width: 100%;" data-placeholder="{vtranslate('SELECT_FIELDS_OPTION', $QUALIFIED_MODULE)}">
                        {foreach item=FIELD from=$CURRENT_MODULE.fields}
                            <option value="{$FIELD.fieldname}" {if $FIELD.selected eq 1}selected{/if}>{vtranslate($FIELD.fieldlabel, $CURRENT_MODULE.module)}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>

