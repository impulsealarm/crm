/* ********************************************************************************
 * The content of this file is subject to the Related & List View Edits ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */
 
var Settings_RelatedListviewEdits_Js = {

    saveSetting : function() {
        var thisInstance = this;
        jQuery('#rel_setting_save_btn').on('click', function(){
            var form = jQuery(this.form);
            var data = form.serializeFormData();
            var progressIndicatorElement = jQuery.progressIndicator({
                'position' : 'html',
                'blockInfo' : {
                    'enabled' : true
                }
            });

            if(typeof data == 'undefined' ) {
                data = {};
            }
            data.module = app.getModuleName();
            data.parent = app.getParentModuleName();
            data.action = 'SaveSettings';

            AppConnector.request(data).then(
                function(data) {
                    if(data['success']) {
                        progressIndicatorElement.progressIndicator({'mode':'hide'});
                    } else {
                        progressIndicatorElement.progressIndicator({'mode':'hide'});
                    }
                    //window.location.href="index.php?module=RelatedListviewEdits&view=Settings&parent=Settings";
                },
                function(error, errorThrown){
                }
            );
        });
    },



    registerEvents : function() {
        Settings_RelatedListviewEdits_Js.saveSetting();
    }

}
jQuery(document).ready(function(){
    Settings_RelatedListviewEdits_Js.registerEvents();
});