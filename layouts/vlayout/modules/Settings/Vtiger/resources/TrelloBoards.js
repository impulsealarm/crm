jQuery.Class("Settings_Vtiger_TrelloBoards_Js",{},{
	
	getContainer : function() {
		return jQuery('#UsersTrelloBoardsContainer');
	},
	
	registerAddUserBoardEvent : function(){
		
		var thisInstance = this;
		
		jQuery("#add_user_board").on("click", function(){
			
			var container = thisInstance.getContainer();
			
			var SequenceNumber = jQuery(".wpRow",container).length;
			
			var newSequenceNumber = SequenceNumber+1;
			
			var newRow = jQuery('.wpRowCloneCopy',container).clone(true,true);
			
			newRow = newRow.removeClass('hide wpRowCloneCopy').addClass('wpRow').attr("id", "row_"+newSequenceNumber);
			
			newRow = newRow.appendTo(container);

			thisInstance.updateCurrentElementWithSequenceNumber(newRow, newSequenceNumber);
			
			thisInstance.checkItemRow();

			thisInstance.saveCount();
		});
	},
	
	updateCurrentElementWithSequenceNumber : function(wpRow, currentSequenceNumber, previousSequenceNumber){
		
		if(typeof previousSequenceNumber == 'undefined')
			previousSequenceNumber = "0";
		
		var classFields = new Array('trello_board_selection_','user_selection_');

		var selectionName = {
			'trello_board_selection_'	: 'board', 
			'user_selection_'			: 'user', 
		};
		
		for(var classIndex in classFields ) {
			
			var elementClass = classFields[classIndex];
			
			var expectedElementId = elementClass + currentSequenceNumber;
			
			var elementName = "trello_mapping["+previousSequenceNumber+"]["+selectionName[elementClass]+"]";
			
			var expectedElementName = "trello_mapping["+currentSequenceNumber+"]["+selectionName[elementClass]+"]";
			
			wpRow.find('.'+elementClass + previousSequenceNumber).removeClass(elementClass + previousSequenceNumber).addClass(expectedElementId)
				.filter('[name="'+elementName+'"]').attr('name', expectedElementName);
		}
		
		wpRow.find("select").each(function(){
			if(jQuery(this).hasClass("chzn-select")){
				jQuery(this).trigger("liszt:updated");
			} else {
				jQuery(this).addClass("chzn-select").chosen();
			}
		});
	},
	
	checkItemRow : function(){
		
		var thisInstance = this;
		
		var templateContainer = thisInstance.getContainer();
		
		var noRow = templateContainer.find('tr.wpRow').length;		

		if(noRow > 1){			
			templateContainer.find('.deleteRow').show();
		} else{
			templateContainer.find('.deleteRow').hide();
		}
	},

	
	registerRemoveUserBoardEvent : function(){
		
		var thisInstance = this;
		
		jQuery(".deleteRow").on("click", function(e){
			
			var element = jQuery(e.currentTarget);
			
			element.closest('tr.wpRow').remove();
			
			thisInstance.updateElementByOrder();
		});
		
		thisInstance.saveCount();
	},
	
	updateElementByOrder : function(){
		
		var thisInstance = this;
		
		var ItemContentsContainer  = thisInstance.getContainer();
		
		jQuery('tr.wpRow',ItemContentsContainer).each(function(index,domElement){
			
			var lineItemRow = jQuery(domElement);
			
			var currentIndex = lineItemRow.attr("id");

			currentIndex = currentIndex.split("_");

			currentIndex = currentIndex['1'];
			
			var newIndex = index+1;
			
			if(currentIndex != newIndex){
				lineItemRow.attr("id", "row_"+newIndex);
				thisInstance.updateCurrentElementWithSequenceNumber(lineItemRow, newIndex, currentIndex);	
			}
		});
		
		thisInstance.checkItemRow();			
		thisInstance.saveCount();
	},
	
	saveCount : function(){
		var thisInstance = this;
		var totalCount = thisInstance.getContainer().find('tr.wpRow').length;
		jQuery('#totalCount').val(totalCount);
	},
	
	registerSaveUserBoardMapping : function(){
		
		jQuery(".saveTrelloBoards").on("click", function(e){
			
			e.preventDefault();
			
			var progressIndicatorElement = jQuery.progressIndicator({
				'position' : 'html',
				'blockInfo' : {
					'enabled' : true
				}
			});
			
			var params = jQuery( "#usersTrelloBoardsForm" ).serialize();
			
			AppConnector.request(params).then(
				function(data) {
					
					progressIndicatorElement.progressIndicator({'mode' : 'hide'});
					
					if(data.success){
						
						var result = data.result;
						
						if(result.success){
							var params = {
								text: app.vtranslate('JS_TERMS_AND_CONDITIONS_SAVED')
							};
						} else {
							var params = {
								text: app.vtranslate('Something went wrong while saving.')
							};
						}
					} else {
						var params = {
							text: app.vtranslate('Something went wrong while saving.')
						};
					}
					Settings_Vtiger_Index_Js.showMessage(params);
				}
			);
		});

	},
	
	registerEvents : function(){
		var thisInstance = this;
		thisInstance.registerAddUserBoardEvent();
		thisInstance.registerRemoveUserBoardEvent();
		thisInstance.saveCount();
		thisInstance.registerSaveUserBoardMapping();
	}
});

jQuery(document).ready(function(e){
	var TrelloBoardsInstance = new Settings_Vtiger_TrelloBoards_Js();
});
	