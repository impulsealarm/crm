jQuery.Class("Settings_Vtiger_VtigerAffiliate_Js",{},{
	
	getContainer : function() {
		return jQuery('#VtigerAffiliateContainer');
	},
	
	registerAddMoreLinkEvent : function(){
		
		var thisInstance = this;
		
		jQuery("#add_more_link").on("click", function(){
			
			var SequenceNumber = jQuery(".affiliate_link").length;
			
			var newIndex = SequenceNumber + 1;
			
			thisInstance.addLink(newIndex);
			
		});
	},
	
	addLink : function(index){
		
		var thisInstance = this;
		
		var Container = thisInstance.getContainer();
		
		var newLinkString = "";

		newLinkString += '<tr class="affiliate_link">';
		newLinkString += '<td>&nbsp; <i class="icon-trash deleteLink cursorPointer" title="DELETE"></i> &nbsp;';
		newLinkString += '<input type="text" name="affiliate_links['+index+'][name]" value=""></td>';
		newLinkString += '<td><input type="text" name="affiliate_links['+index+'][url]" value="" class="fieldValue span5"/></td>';
		newLinkString += '</tr>';
		
		Container.append(newLinkString);
		
		thisInstance.checkLinkRow();
	},
	
	checkLinkRow : function(){
		
		var thisInstance = this;
		
		var templateContainer = thisInstance.getContainer();
		
		var noRow = templateContainer.find('tr.affiliate_link').length;		

		if(noRow > 1){			
			templateContainer.find('.deleteLink').show();
		} else{
			templateContainer.find('.deleteLink').hide();
		}
	},

	registerRemoveLinkEvent : function(){
		
		var thisInstance = this;
		
		jQuery(document).on("click",".deleteLink", function(e){
			var element = jQuery(e.currentTarget);
			element.closest('tr.affiliate_link').remove();
			thisInstance.checkLinkRow();
		});
	},
	
	registerSaveAffiliateURLs : function(){
		
		jQuery(".saveAffiliateLinks").on("click", function(e){

			e.preventDefault();
			
			var progressIndicatorElement = jQuery.progressIndicator({
				'position' : 'html',
				'blockInfo' : {
					'enabled' : true
				}
			});
			
			var params = jQuery("#vtigerAffiliateURLsForm").serialize();
			
			AppConnector.request(params).then(
				function(data) {
					
					progressIndicatorElement.progressIndicator({'mode' : 'hide'});

					if(data.success){
						
						var result = data.result;
						
						var params = {
							text: app.vtranslate(result.message)
						};
					} else {
						var params = {
							text: app.vtranslate('Something went wrong while saving.')
						};
					}
					Settings_Vtiger_Index_Js.showMessage(params);
					location.reload();
				}
			);
			
		});
	},
	
	registerEvents : function(){
		var thisInstance = this;
		thisInstance.checkLinkRow();
		thisInstance.registerAddMoreLinkEvent();
		thisInstance.registerRemoveLinkEvent();
		thisInstance.registerSaveAffiliateURLs();
	}
});

jQuery(document).ready(function(e){
	var VtigerAffiliateInstance = new Settings_Vtiger_VtigerAffiliate_Js();
	VtigerAffiliateInstance.registerEvents();
});