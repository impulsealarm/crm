{strip}
<div class="container-fluid">
	
	<div class="widget_header row-fluid">
		<div class="row-fluid"><h3>{vtranslate('Trello Boards', $QUALIFIED_MODULE)}</h3></div>
	</div>
	
	<hr>
	
	<div class="row-fluid btn-toolbar">
		<form class="form-horizontal" id="usersTrelloBoardsForm" method="post" action="index.php" enctype="multipart/form-data">
			<input type="hidden" name="module" value="Vtiger">
			<input type="hidden" name="parent" value="Settings">
			<input type="hidden" name="action" value="TrelloBoardSave">
			<table class="table table-bordered" style="width: 50%;"id="UsersTrelloBoardsContainer">
				<thead>
					<tr class="listViewHeaders">
						<th>{vtranslate('Users', $QUALIFIED_MODULE)}</th>
						<th>{vtranslate("Trello Boards", $QUALIFIED_MODULE)}</th>
					</tr>
				</thead>
				
				<tbody>
					
					<tr class="wpRowCloneCopy hide">
						<td class="fieldValue medium">
							&nbsp; <i class="icon-trash deleteRow cursorPointer" title="DELETE" style = "display:none;"></i> &nbsp;
							<select name="trello_mapping[0][user]" class="user_selection_0">
					    		<option value="">Select an option</option>
					    		{assign var=ALL_USER_LIST value=$USER_MODEL->getAccessibleUsers()}
								{foreach key=USER_ID item=USER_NAME from=$ALL_USER_LIST}
									<option value="{$USER_ID}">{$USER_NAME}</option>
								{/foreach}
					    	</select>
						</td>
						<td class="fieldValue medium">
							<select name="trello_mapping[0][board]" class="trello_board_selection_0">
								<option value="">Select an option</option>
								{foreach key=TRELLO_BOARD_ID item=TRELLO_BOARD_NAME from=$TRELLO_BOARDS}
									<option value="{$TRELLO_BOARD_ID}">{$TRELLO_BOARD_NAME}</option>
								{/foreach}
							</select>
						</td>
					</tr>
					{if $USER_TRELLO_MAPPING|@count gt 0}
					
						{if $TRELLO_BOARDS|@count gt 0} 
							
							{assign var=TotalRow value=$USER_TRELLO_MAPPING|@count}	
							{foreach key=ROW_INDEX item=USER_TRELLO_BOARD from=$USER_TRELLO_MAPPING}
								
								{assign var=row_no value=$ROW_INDEX+1}
								{assign var=user_id value=$USER_TRELLO_BOARD['user']}
								{assign var=board_id value=$USER_TRELLO_BOARD['board']}
								
								<tr class="wpRow" id="row_{$row_no}">
									
									<td class="fieldValue medium">
										&nbsp; <i class="icon-trash deleteRow cursorPointer" title="DELETE" 
											{if $TotalRow eq '1'} style = "display:none;"{/if}></i> &nbsp;
										<select name="trello_mapping[{$row_no}][user]" class="chzn-select user_selection_{$row_no}">
								    		<option value="">Select an option</option>
								    		{assign var=ALL_USER_LIST value=$USER_MODEL->getAccessibleUsers()}
											{foreach key=USER_ID item=USER_NAME from=$ALL_USER_LIST}
												<option value="{$USER_ID}"
													{if $user_id eq $USER_ID} selected {/if}
												>{$USER_NAME}</option>
											{/foreach}
								    	</select>
									</td>
									<td class="fieldValue medium">
										<select name="trello_mapping[{$row_no}][board]" class="chzn-select trello_board_selection_{$row_no}">
											<option value="">Select an option</option>
											{foreach key=TRELLO_BOARD_ID item=TRELLO_BOARD_NAME from=$TRELLO_BOARDS}
												<option value="{$TRELLO_BOARD_ID}" 
													{if $board_id eq $TRELLO_BOARD_ID} selected {/if}
												> {$TRELLO_BOARD_NAME} </option>
											{/foreach}
										</select>
									</td>
								</tr>
							{/foreach}
						{/if}
					{else}
					
						{if $TRELLO_BOARDS|@count gt 0} 
							<tr class="wpRow" id="row_1">
								
								<td class="fieldValue medium">
									&nbsp; <i class="icon-trash deleteRow cursorPointer" title="DELETE" style = "display:none;"></i> &nbsp;
									<select name="trello_mapping[1][user]" class="chzn-select user_selection_1">
							    		<option value="">Select an option</option>
							    		{assign var=ALL_USER_LIST value=$USER_MODEL->getAccessibleUsers()}
										{foreach key=USER_ID item=USER_NAME from=$ALL_USER_LIST}
											<option value="{$USER_ID}">{$USER_NAME}</option>
										{/foreach}
							    	</select>
								</td>
								<td class="fieldValue medium">
									<select name="trello_mapping[1][board]" class="chzn-select trello_board_selection_1">
										<option value="">Select an option</option>
										{foreach key=TRELLO_BOARD_ID item=TRELLO_BOARD_NAME from=$TRELLO_BOARDS}
											<option value="{$TRELLO_BOARD_ID}" > {$TRELLO_BOARD_NAME} </option>
										{/foreach}
									</select>
								</td>
							</tr>
						{else}
							<tr>
								<td colspan="2">Please Create Boards.</td>
							</tr>	
						{/if}
					{/if}
				</tbody>
			</table>
			
			<table style = "margin-top:10px;">
				<tr>
					<td colspan="3">
						<div>
							<div class="btn-group">
								<button type="button" class="btn addButton" id="add_user_board">
									<i class="icon-plus icon-white"></i><strong>Add User Board</strong>
								</button>
								<button class="btn btn-success saveTrelloBoards" style="margin-left:10px;">
									<strong>{vtranslate('LBL_SAVE', $QUALIFIED_MODULE)}</strong>
								</button>
							</div>
						</div>
					</td>
				 </tr>
			</table>
			
			<input type = "hidden" id = "totalCount" name = "totalCount"  value = "{if $TotalRow gt '0'}$TotalRow{else}1{/if}" />
		</form>	
	</div>
{/strip}