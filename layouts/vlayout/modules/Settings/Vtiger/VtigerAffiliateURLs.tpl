{strip}
<div class="container-fluid">
	<div class="widget_header row-fluid">
		<div class="row-fluid"><h3>{vtranslate('Vtiger Affiliate URLs', $QUALIFIED_MODULE)}</h3></div>
	</div>
	<hr>

    <div class="contents row-fluid btn-toolbar">
		<form class="form-horizontal" id="vtigerAffiliateURLsForm" method="post" action="index.php" enctype="multipart/form-data">
			<input type="hidden" name="module" value="Vtiger">
			<input type="hidden" name="parent" value="Settings">
			<input type="hidden" name="action" value="VtigerAffiliateURLs">
			<input type="hidden" name="mode" value="saveAffiliateURLs" />
			<table class="table table-bordered" id="VtigerAffiliateContainer" style="width: 70%;">
				<thead class="listViewHeaders">
					<th>Name</th>
					<th>Link</th>
				</thead>
				<tbody>
				{if $AFFILIATE_LINKS|@count gt 0}
				
					{foreach key=INDEX item=AffiliateURL from=$AFFILIATE_LINKS}
						
						{assign var=row value=$INDEX + 1}
						
						<tr class="affiliate_link">
							<td>
								&nbsp; <i class="icon-trash deleteLink cursorPointer" title="DELETE"></i> &nbsp;
								<input type="text" name="affiliate_links[{$row}][name]" value="{$AffiliateURL['name']}" />
							</td>
							<td><input type="text" name="affiliate_links[{$row}][url]" value="{$AffiliateURL['url']}" class="fieldValue span5"/></td>
						</tr>
					{/foreach}
				{else}
					<tr class="affiliate_link">
						<td>
							&nbsp; <i class="icon-trash deleteLink cursorPointer" title="DELETE"></i> &nbsp;
							<input type="text" name="affiliate_links[1][name]" value="" />
						</td>
						<td><input type="text" name="affiliate_links[1][url]" value="" class="fieldValue span5"/></td>
					</tr>
				{/if}
				</tbody>
			</table>
	        <table style = "margin-top:10px;">
				<tr>
					<td>
						<div>
							<div class="btn-group">
								<button type="button" class="btn addButton" id="add_more_link">
									<i class="icon-plus icon-white"></i><strong>Add More Link</strong>
								</button>
								<button class="btn btn-success saveAffiliateLinks" style="margin-left:10px;">
									<strong>{vtranslate('LBL_SAVE', $QUALIFIED_MODULE)}</strong>
								</button>
							</div>
						</div>
					</td>
				 </tr>
			</table>
		</form>
	</div>
</div>
{/strip}