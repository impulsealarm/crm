{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	{if $ALL_POST_CARDS|@count gt '0'}
		<div class="row-fluid">
			<div class="span2">{vtranslate('Post Cards',$QUALIFIED_MODULE)}<span class="redColor">*</span></div>
			<div class="span8 row-fluid">
				<span class="span6">
				 	<select name="postcard" class="chzn-select" data-validation-engine='validate[required]'>
	                    <option value="">{vtranslate('LBL_SELECT_OPTION','Vtiger')}</option>
	                    {foreach from=$ALL_POST_CARDS item=POSTCARD_MODEL key=POSTCARDID}
							<option value="{$POSTCARDID}" {if $TASK_OBJECT->postcard eq $POSTCARDID} selected {/if}>{$POSTCARD_MODEL->get("name")}</option>
						{/foreach}
					</select>	
				</span>
			</div>
		</div>
		{if $POSTCARD_ADDRESS_FIELDS|@count gt '0'}
			{assign var=COUNTER value='0'}
			{foreach key=FieldName item=FieldLabel from=$POSTCARD_ADDRESS_FIELDS}
				{if $COUNTER eq '0' or $COUNTER%2 eq '0' }
					<div class="row-fluid">
				{/if}
					<div class="span2">{vtranslate($FieldLabel,$QUALIFIED_MODULE)}<span class="redColor">*</span></div>
					<div class="span4 row-fluid">
						<span class="span6">
							<input type="hidden" name="input_{$FieldName}" class="inputFieldValue" value="{$TASK_OBJECT->$FieldName}"/>
							<select name="{$FieldName}" class="chzn-select" data-validation-engine='validate[required]'>
								<option value="">{vtranslate('LBL_SELECT_OPTION','Vtiger')}</option>
								{$ALL_FIELD_OPTIONS}
							</select>
						</span>	
					</div>
				{if $COUNTER > 0 and $COUNTER%2 eq '1' }
					</div>
				{/if}
				{assign var=COUNTER value=$COUNTER+1}
			{/foreach}
		{/if}
		
	{else}
		<div class="row-fluid">
			<div class="span8 row-fluid">No PostCards found. Please create atleast one Post Card</div>
		</div>	
	{/if}
{/strip}	
