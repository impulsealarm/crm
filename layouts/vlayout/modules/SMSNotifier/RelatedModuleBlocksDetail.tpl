<br/>
	
<table class ="table table-bordered equalSplit detailview-table">
	<thead>
		<tr>
			<th class="blockHeader">
				<img class="cursorPointer alignMiddle blockToggle  hide  " src="layouts/vlayout/skins/nature/images/arrowRight.png" data-mode="hide" data-id="109">
				<img class="cursorPointer alignMiddle blockToggle " src="layouts/vlayout/skins/nature/images/arrowDown.png" data-mode="show" data-id="109">
				&nbsp;&nbsp;Related Type
			</th>
			<th>Related To</th>
			<th>Status</th>
			<th>From Number</th>
			<th>To Number</th>
		</tr>
	</thead>
	<tbody>
		{assign var=FromNumber value=$RELATED_MODULES['from_number']}
		{assign var=ToNumber value=$RELATED_MODULES['to_number']}
		{assign var=Status value=$RELATED_MODULES['status']}
		{foreach item=RELATED_ENTITY key=ModuleName from=$RELATED_MODULES['related_to']}
			{foreach item=ENTITYNAME from=$RELATED_ENTITY}
				<tr>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;">{$ModuleName}</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;">{$ENTITYNAME}</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;">{$Status}</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;">{$FromNumber}</span>
					</td>
					<td class="fieldValue medium">
						<span class="value" data-field-type="text" style="white-space:normal;">{$ToNumber}</span>
					</td>	
				</tr>
			{/foreach}
		{/foreach}
	</tbody>
</table>
