{strip}
<div class="recordNamesList">
	<ul class="nav nav-list contents">
		{foreach item=recordsModel from=$RECORDS}
		<li>
			<span style="width:100%">
				<input type="checkbox" class="showrelatedsms pull-left" data-crmid="{$recordsModel->getId()}" data-label="{decode_html($recordsModel->getName())}" /> &nbsp; 
				
				<a style="width:70%;display: inline-block;" target="_blank" data-id={$recordsModel->getId()} 
				href="{$recordsModel->getDetailViewUrl()}" title="{decode_html($recordsModel->getName())}">{decode_html($recordsModel->getName())}</a>
			</span>
		</li>
		{foreachelse}
		<li style="text-align:center">{vtranslate('LBL_NO_RECORDS', $MODULE)}
		</li>
		{/foreach}

	</ul>
</div>
{/strip}