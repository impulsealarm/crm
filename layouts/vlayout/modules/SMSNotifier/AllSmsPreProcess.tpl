{include file="Header.tpl"|vtemplate_path:$MODULE}
{include file="BasicHeader.tpl"|vtemplate_path:$MODULE}
<div class="bodyContents">
	<div class="mainContainer row-fluid">
		{assign var=LEFTPANELHIDE value=$CURRENT_USER_MODEL->get('leftpanelhide')}
		<div class="span2 row-fluid " id="leftPanel" style="min-height:550px;">
			
			{* <!-- START : AllSms Views Side bar --> *}
			
			<div class="row-fluid">				
				<div class="sideBarContents" style = "margin:10px 0;">
					<div class="clearfix"></div>
					
					{* <!-- START : Side Bar Widgets --> *}
					
					<div class="quickWidgetContainer accordion">
						
						{assign var=val value=1}
						
						{foreach item=SIDEBARWIDGET key=index from=$QUICK_LINKS['SIDEBARWIDGET']}
							<div class="quickWidget">
								<div class="accordion-heading accordion-toggle quickWidgetHeader" data-target="#{$MODULE}_sideBar_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($SIDEBARWIDGET->getLabel())}"
										data-parent="#quickWidgets" data-label="{$SIDEBARWIDGET->getLabel()}"
										data-widget-url="{$SIDEBARWIDGET->getUrl()}" >
									
									<h5 class="title widgetTextOverflowEllipsis pull-right" title="{vtranslate($SIDEBARWIDGET->getLabel(), $MODULE)}">{vtranslate($SIDEBARWIDGET->getLabel(), $MODULE)}</h5>
									<div class="loadingImg hide pull-right">
										<div class="loadingWidgetMsg"><strong>{vtranslate('LBL_LOADING_WIDGET', $MODULE)}</strong></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="widgetContainer accordion-body" id="{$MODULE}_sideBar_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($SIDEBARWIDGET->getLabel())}" data-url="{$SIDEBARWIDGET->getUrl()}"></div>
							</div>
						{/foreach}
						
					</div>
					
					{* <!-- END : Side Bar Widgets --> *}
					
				</div>
			</div>
			
			{* <!-- END : AllSms Views Side bar --> *}
			
		</div>
		<div class="contentsDiv  span10 marginLeftZero" id="rightPanel" style="min-height:550px;">
			
			<div class="sms_chart" >
			        <div class="row-fluid" style="margin-bottom:20px;"></div>
				<hr/>
				<div class="row-fluid"></div>
	
				<span class="sent_msg hide">
					I sent
				</span>
				<span class="receive_msg hide">
					I received
				</span>
            