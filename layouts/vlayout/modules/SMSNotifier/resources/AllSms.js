jQuery.Class("SMSNotifier_AllSms_Js", {

	currentInstance : false
	
},{
	
	smsContainer : false,
	
	init : function() {
		SMSNotifier_AllSms_Js.currentInstance = this;
	},
	
	getSMSChartContainer : function(){
		if(this.smsContainer == false){
			this.setSMSChartContainer(jQuery('.sms_chart'));
		}
		return this.smsContainer;
	},
	
	setSMSChartContainer : function(element){
		this.smsContainer = element;
		return this;
	},
	
	
	
	checkForSlimScroll : function(){
		
		var thisInstance = this;
		
		var smsWidgets = jQuery('.sms_threads');
		smsWidgets.each(function(index, smsContentElement){						
			app.showScrollBar(jQuery(smsContentElement), {'height':jQuery(smsContentElement).height() + 'px'});
		});
	},
	
	registerSendSMSClick : function(){
		
		var sendElement = jQuery('input[name="sendmessage"]');
		
		sendElement.click(function(e){
			
			var currentTarget = jQuery(e.currentTarget);
			var form = jQuery(this).parent();
			
			var parentId = form.find('[name="parent_crmid"]').val();
			var messageElement = form.find('input[name="message"]');
			
			var smsBox = jQuery("#sms_"+parentId);
			
			var progressElement = smsBox.find('.progressImg');
			
			progressElement.toggle();
			sendElement.toggle();
						
			var SendSmsUrl = form.serializeFormData();
			SendSmsUrl.action = 'ReplyToNumber';
			SendSmsUrl.module = 'SMSNotifier';
			
			var sentMSg = SendSmsUrl.message;

			AppConnector.request(SendSmsUrl).then( function(data) {
				
				var obj = jQuery(".sent_msg:hidden").clone().removeClass('hide');				
				$(obj).html(sentMSg);				
				smsBox.find('.sms_threads').append(obj);
				
				messageElement.val('');
				progressElement.toggle();
				sendElement.toggle();
				
			}, function(error,err){});
			
		});
		
	},
	
	registerRefreshClickEvent : function(){
		var thisInstance = this;
		
		thisInstance.getSMSChartContainer().find('.refresh_sms').click(function(){
			window.location.reload();
		});
	},
	
	registerPageNavigateEvent : function(){
		var thisInstance = this;
		thisInstance.getSMSChartContainer().find('#gotopage').change(function(e){
			var currentTarget = jQuery(e.currentTarget);
			var newPage = currentTarget.val();
			window.location.href = "index.php?module=SMSNotifier&view=AllSms&pageNumber=" + newPage;
		});
	},
	
	registerSideBarWidgetSlimScroll : function(){
		
		var widgets = jQuery('div.widgetContainer');
		widgets.each(function(index,element){
			var widgetContainer = jQuery(element);
			//widgetContainer.css('height', '200');
			//app.showScrollBar(jQuery(element), {'height':jQuery(element).height() + 'px'});
			
		});
	},


	/**
	 * Function is used to load the sidebar widgets
	 * @param widgetContainer - widget container
	 * @param open - widget should be open or closed
	 */
	loadWidgets : function(widgetContainer, open) {

		var thisInstance = this;
		
		var message = jQuery('.loadingWidgetMsg').html();

		widgetContainer.html('');
		
		widgetContainer.progressIndicator({'message' : message});
		var url = widgetContainer.data('url');

		var listViewWidgetParams = {
			"type":"GET", "url":"index.php",
			"dataType":"html", "data":url
		};
		
		AppConnector.request(listViewWidgetParams).then(
			function(data){
				
				if(typeof open == 'undefined') open = true;
            	
				if(open){
					widgetContainer.progressIndicator({'mode':'hide'});					
					widgetContainer.css('height', 'auto');
				}
				
				widgetContainer.html(data);
            	
				app.showScrollBar(widgetContainer, {'height': '300px'});
				
				var label = widgetContainer.closest('.quickWidget').find('.quickWidgetHeader').data('label');
				//jQuery('.bodyContents').trigger('Vtiger.Widget.Load.'+label,jQuery(widgetContainer));
				
				if(jQuery(data).find(".showrelatedsms").length){					
					
					thisInstance.registerCheckBoxClickEvent();					
					
					jQuery('.sms_box').each(function(index,elem){
					
						var check_crmid = jQuery(elem).attr('id');
						check_crmid = check_crmid.replace(/sms_/i,"");
						
						if( check_crmid != '' && $('.showrelatedsms[data-crmid="'+check_crmid+'"]').length ){
							$('.showrelatedsms[data-crmid="'+check_crmid+'"]').attr('checked', true);
						}
												
					});
				}
				
			}
		);
	},

	loadWidgetsOnLoad : function(){
		
		var thisInstance = this;
		 
		var widgets = jQuery('div.widgetContainer');
		widgets.each(function(index,element){
			var widgetContainer = jQuery(element);
			widgetContainer.show();
			var key = widgetContainer.attr('id');
			thisInstance.loadWidgets(widgetContainer);
			//widgetContainer.addClass('in');
		});
	},
	
	registerCheckBoxClickEvent : function(){
		
		var thisInstance = this;
		
		$('.showrelatedsms').change(function(e){
			
			var currentTarget = jQuery(e.currentTarget);
			
			var parent_crmid = currentTarget.data('crmid');
			var parent_crmname = currentTarget.data('label');
			
			jQuery("#sms_"+parent_crmid).remove();
			
			if( currentTarget.prop('checked') ){
				
				thisInstance.getSMSBoxes({ 
					'page' : '1', 
					'parent_crmid' :parent_crmid, 
					'parent_crmname' : parent_crmname 
				}).then(
						function(data){},
						function(textStatus, errorThrown){}
				);
			}
			
		});
		
	},
	
	getDefaultParams : function() {
		var pageNumber = jQuery('#pageNumber').val();
		var module = app.getModuleName();
		var params = {
			'module': module,
			'page' : pageNumber,
			'view' : "AllSms"
		};

        //params.search_params = JSON.stringify(this.getListSearchParams());
        return params;
	},
	
	getSMSBoxes : function(urlParams) {
		
		var aDeferred = jQuery.Deferred();
		if(typeof urlParams == 'undefined') {
			urlParams = {};
		}

		var thisInstance = this;
		var loadingMessage = 'Loading..';
		var progressIndicatorElement = jQuery.progressIndicator({
			'message' : loadingMessage,
			'position' : 'html',
			'blockInfo' : {
				'enabled' : true
			}
		});

		var defaultParams = this.getDefaultParams();
		var urlParams = jQuery.extend(defaultParams, urlParams);

		var parent_crmid = urlParams.parent_crmid;
		var parent_name = urlParams.parent_crmname;
		
		AppConnector.request(urlParams).then(function(data){
			
			progressIndicatorElement.progressIndicator({
				'mode' : 'hide'
			});
            
			var smsContentsContainer = jQuery('.sms_chart');
			smsContentsContainer.append(data);
            
			thisInstance.checkForSlimScroll();
			
			thisInstance.registerSendSMSClick();
			
			aDeferred.resolve(data);
		
		},

		function(textStatus, errorThrown){
			aDeferred.reject(textStatus, errorThrown);
		});
		
		return aDeferred.promise();
	},
	
	registerEvents : function() {
		var thisInstance = this;
		
		var smsChartContainer = thisInstance.getSMSChartContainer();
		
		thisInstance.checkForSlimScroll();
		
		thisInstance.registerSendSMSClick();
		
		thisInstance.registerRefreshClickEvent();
		thisInstance.registerPageNavigateEvent();
		
		thisInstance.loadWidgetsOnLoad();
		
	}
}
);
