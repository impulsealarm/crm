
{assign var=PARENT_CRMID value=$ROWDATA.parent_crmid}
{assign var=PARENT_RECORD value=Vtiger_Record_Model::getInstanceById($PARENT_CRMID)}
{assign var=FILTERED_PHONE value=$ROWDATA.phone}
{assign var=SENDTO_PHONE value=$ROWDATA.origphone}


<div class="sms_box" id="sms_{$PARENT_CRMID}">

	<div class="sms_header" >
		<a target="_blank" style="color:white;font-size:16px;" href="index.php?module={$PARENT_RECORD->getModuleName()}&view=Detail&record={$PARENT_CRMID}">
		<b>{Vtiger_Functions::getCRMRecordLabel($PARENT_CRMID)}</b>
		</a> 
		<br>
		({$SENDTO_PHONE})
	</div>
	
	<div class="sms_contents sms_threads">
	
		{assign var="SMSES" value=SMSNotifier_AllSms_Model::getSmallSmsHistory($PARENT_CRMID, $SENDTO_PHONE)}
		
		{foreach key=SMSINDEX item=SMSRECORD from=$SMSES}
			
			{if $SMSRECORD->get('direction') eq 'incoming' }
			
				<span class="receive_msg" >
				{$SMSRECORD->get('message')}
				</span>
								
			{else if $SMSRECORD->get('direction') eq 'outgoing' }
			
				<span class="sent_msg" >
				{$SMSRECORD->get('message')}
				</span>
							
			{/if}
			
		{/foreach}
		
	</div>
	
	<div class="sms_footer" >
		
		<form method="post" style="padding:2%;">
	
			<input type="hidden" name="selected_ids" value={ZEND_JSON::encode(array($PARENT_CRMID))}>
	
			<input type="hidden" name="parent_crmid" value="{$PARENT_CRMID}" />
			<input type="hidden" name="sendto" value="{$SENDTO_PHONE}" />
			<input type="text" name="message" value="" style=" width: 80%;" />&nbsp;
			<input type="button" name="sendmessage" value="Send" />&nbsp;
			<img src="layouts/vlayout/skins/images/loading.gif" style="width:5%; " class="progressImg hide" />
			
	
		</form>
		
	</div>
	
</div>


