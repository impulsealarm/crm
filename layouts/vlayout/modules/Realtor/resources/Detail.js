/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Vtiger_Detail_Js("Realtor_Detail_Js",{
	
	CreateAffiliate : function(url, record){

		var params = url+'&record='+record;
		
		var progressInstance = jQuery.progressIndicator({
            'position' : 'html',
            'blockInfo' : {
                'enabled' : true
            }
        });
		
		AppConnector.request(params).then(
			
			function(data) {
				
				var response = data;
				
				progressInstance.progressIndicator({
					'mode' : 'hide'
				});
				 
				if(response.success){
					
					var result = response.result;

					if(typeof result.status != 'undefined' && result.status == 'error'){

						var params = {
							text: app.vtranslate(result.error),
							width: '35%'
						};
						
						Vtiger_Helper_Js.showPnotify(params);
					
					} else {
						
						var params = {
							text: app.vtranslate(result.message),
							width: '35%'
						};
						Vtiger_Helper_Js.showPnotify(params);
						
						location.reload();
					}
				}
			}
		);
	}, 
	
	ChangeAffiliateStatus : function(url, status, affiliateid){
		
		var currentInstance = Vtiger_Detail_Js.getInstance();
        
		var record = currentInstance.getRecordId();
		
		if(typeof record == 'undefined' || record == '') return false;
		
		url += "&record="+record+"&affiliate_id="+affiliateid+"&status="+status;
		
		var progressInstance = jQuery.progressIndicator({
            'position' : 'html',
            'blockInfo' : {
                'enabled' : true
            }
        });
		
		AppConnector.request(url).then(
			function(data) {
				
				var response = data;
				
				response = response.result;

				progressInstance.progressIndicator({
					'mode' : 'hide'
				});
				
				if(response.success){
					
					var text = "";
					
					var ActualElementId = "";
					
					var NewElementId = "";
					
					var onClick = 'javascript:Realtor_Detail_Js.ChangeAffiliateStatus("index.php?module=Realtor&action=ManageAffiliate&mode=ChangeAffiliateStatus",';
					
					if(status == 'deactivate'){
						
						onClick += '"activate",';
						
						text = "Activate Affiliate";
					
						ActualElementId = "Realtor_detailView_basicAction_Deactivate_Affiliate";
					
						NewElementId = "Realtor_detailView_basicAction_Activate_Affiliate";
					
					}else if(status == 'activate'){
						
						onClick += '"deactivate",';
						
						text = "Deactivate Affiliate";
					
						ActualElementId = "Realtor_detailView_basicAction_Activate_Affiliate";
						
						NewElementId = "Realtor_detailView_basicAction_Deactivate_Affiliate";
					}
					
					onClick += '"'+affiliateid+'"';
					
					onClick += ");";
					
					var params = {
						text: app.vtranslate(response.message),
						width: '35%'
					};
					
					Vtiger_Helper_Js.showPnotify(params);
					
					jQuery("#"+ActualElementId).attr("onclick", onClick).text(text);
					
					jQuery('#'+ActualElementId).attr('id',NewElementId);
					
				} else {

					var result = response.result;
					
					if(result.status == 'error'){
						
						var params = {
							text: app.vtranslate(result.error),
							width: '35%'
						};
						
						Vtiger_Helper_Js.showPnotify(params);
					}
				}
			}
		);			
	},
	
	
	triggerRegenerateLinks : function(url, record){
		
		url += "&record="+record;
		
		var progressInstance = jQuery.progressIndicator({
            'position' : 'html',
            'blockInfo' : {
                'enabled' : true
            }
        });
		
		AppConnector.request(url).then(
			function(data) {

				progressInstance.progressIndicator({
					'mode' : 'hide'
				});
				
				var response = data.result;
				
				if(response.success){
					var params = {
						title : app.vtranslate('JS_MESSAGE'),
	                    text: app.vtranslate('Links regenerated Successfully.'),
	                    animation: 'show',
	                    type: 'info'
					};
				} else {
					 var params = {
	                    title : app.vtranslate('JS_MESSAGE'),
	                    text: app.vtranslate(response.message),
	                    animation: 'show',
	                    type: 'error'
	                };
				}
					
				Vtiger_Helper_Js.showPnotify(params);
				location.reload();
			}
		);
	}
	
	/* END: --------------------------- Changes 4jan,2015 ----------------------------------------*/
	
},{
	

        


});