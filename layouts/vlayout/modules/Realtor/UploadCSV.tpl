
<div>

	<div class='container-fluid editViewContainer'>
        
		<form class="form-horizontal recordEditView" id="UploadCSV" name="UploadCSV" method="post" action="index.php" enctype="multipart/form-data">
      
			{assign var=WIDTHTYPE value=$CURRENT_USER_MODEL->get('rowheight')}
			
			<input type="hidden" name="module" value="{$MODULE}" />
            <input type="hidden" name="action" value="UploadCSV" />
			
			<div class="contentHeader row-fluid">
                <h3 class="span8 textOverflowEllipsis" title="Upload File">Upload CSV File</h3>
                <span class="pull-right">
                    <button class="btn btn-success" type="submit"><strong>{vtranslate('Upload', $MODULE)}</strong></button>
                    {*<!--
					<a class="cancelLink" type="reset" onclick="javascript:window.history.back();">{vtranslate('LBL_CANCEL', $MODULE)}</a>
					-->*}
				</span>				
            </div>
            
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                <tbody>
                    <tr>
						<td class="fieldLabel {$WIDTHTYPE}">Select File
						</td>
						<td class="{$WIDTHTYPE}">
							<div class="fileUploadContainer">
								<input type="file" class="input-large" data-validation-engine="validate[required,funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" name="csvfile" 
								value="" />
								
								<div class="uploadedFileDetails">
									<div class="uploadedFileSize"></div>
									<div class="uploadedFileName"></div>
									{*<div class="uploadFileSizeLimit redColor">
										{vtranslate('LBL_MAX_UPLOAD_SIZE',$MODULE)}&nbsp;<span class="maxUploadSize" data-value="{$MAX_UPLOAD_LIMIT}">{$MAX_UPLOAD_LIMIT_MB}{vtranslate('MB',$MODULE)}</span>
									</div>*}
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
				
</div>