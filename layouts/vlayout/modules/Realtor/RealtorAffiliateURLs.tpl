{strip}
	<table class="table table-bordered equalSplit detailview-table">
		<thead>
			<tr>
				<th class="blockHeader" colspan="3">
					<img class="cursorPointer alignMiddle blockToggle  hide  " src="{vimage_path('arrowRight.png')}"  data-mode="hide" data-id="130">
					<img class="cursorPointer alignMiddle blockToggle " src="{vimage_path('arrowDown.png')}" data-mode="show" data-id="130">
					&nbsp;&nbsp;Realtor Affiliate URLs
				</th>
			</tr>
		</thead>
	
		<tbody>
			{assign var=Counter value=0}
			
			{if $Counter eq '0'}
				<tr>
			{/if}
			
				{foreach key=URL_NAME item=URL from=$REALTOR_REFERRAL_URLS name=realtor_urls}
					
					<td class="fieldValue medium">{$URL_NAME} : <a href="{$URL}" target = "_blank">{$URL}</a></td>
					
					{assign var=Counter value=$Counter+1}
						
					{if $Counter gt 2}
						{assign var=Counter value=0}
						</tr>
					{/if}
				
					{if $smarty.foreach.realtor_urls.last and $Counter gt '0'}
						{if $Counter eq '1'}
							{assign var=TD_COLSPAN value=2}
						{else}
							{assign var=TD_COLSPAN value=1}
						{/if}
				        <td colspan="{$TD_COLSPAN}">&nbsp;</td>
				    {/if}
				{/foreach}
		</tbody>
	</table>
	<br/>
{/strip}