{strip}
<style>
	#pswd_info ul, li {
		margin:0; 
		padding:0; 
		list-style-type:none;
	}
	#pswd_info {
		position:absolute;
		bottom:34%;
		right:40%;
		width:250px;
		padding:15px;
		background:#fefefe; 
		border-radius:5px;
		box-shadow:0 1px 3px #ccc;
		border:1px solid #ddd;
		display:none;
	}
	#pswd_info::before {
		content: "\25B2";
		position:absolute;
		top:-12px;
		left:45%;
		font-size:14px;
		line-height:14px;
		color:#ddd;
		text-shadow:none;
		display:block;
	}
	#pswd_info h4 {
		margin:0 0 10px 0; 
		padding:0;
		font-weight:normal;
	}
	
	.invalid {
		background:url(layouts/vlayout/skins/images/invalid.png) no-repeat 0 50%;
		padding-left:22px;
		line-height:24px;
		color:#ec3f41;
	}
	.valid {
		background:url(layouts/vlayout/skins/images/valid.png) no-repeat 0 50%;
		padding-left:22px;
		line-height:24px;
		color:#3a7d34;
	}
</style>

<div class='container-fluid editViewContainer'>
        <form class="form-horizontal recordEditView" id="EditView" name="EditView" method="post" action="index.php" enctype="multipart/form-data">
            {assign var=WIDTHTYPE value=$CURRENT_USER_MODEL->get('rowheight')}
            
			<input type="hidden" name="module" value="{$MODULE}" />
            <input type="hidden" name="userid" value="{$USERID}" />
            
			<div class="contentHeader row-fluid">
                <span class="pull-right">
                    <button class="btn btn-success" type="submit"><strong>{vtranslate('LBL_SAVE', $MODULE)}</strong></button>
                    <a class="btn btn-success" type="reset" onclick="javascript:window.location.href='index.php';">Remind Me Later</a>
                </span>
            </div>
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                <thead>
                    <tr>
                        <th class="blockHeader" colspan="2">{vtranslate('Update Password', $MODULE)}</th>
                    </tr>
                </thead>
				
                <tbody>
                    
					<tr>						
						{if !$CURRENT_USER_MODEL->isAdminUser()}
							<td>{vtranslate('LBL_OLD_PASSWORD', $MODULE)}</td>
							<td>
								<input type="password" name="old_password" data-validation-engine="validate[required]"/>
                            </td>
                        {/if}
					</tr>
					<tr>
						<td>{vtranslate('LBL_NEW_PASSWORD', $MODULE)}</td>
						<td>
							<input type="password" name="new_password" data-validation-engine="validate[required]"/>
							<br>
							<span id="password_strength"></span>
						</td>
					</tr>
					<tr>
						<td>{vtranslate('LBL_CONFIRM_PASSWORD', $MODULE)}</td>
						<td>
							<input type="password" name="confirm_password" data-validation-engine="validate[required]"/>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	
		<!--	START:	5Dec,2015 Changes		-->
		<div id="pswd_info">
			<h4>Password must meet the following requirements:</h4>
			<ul>
				<li id="letter" class="invalid">At least <strong>one letter</strong></li>
				<li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
				<li id="number" class="invalid">At least <strong>one number</strong></li>
				<li id="spl_char" class="invalid">At least <strong>one special character</strong></li>
				<li id="length" class="invalid">Be at least <strong>6 characters</strong></li>
			</ul>
		</div>
		
		<!--	END: 	5Dec,2015 Changes		-->
</div>

{/strip}