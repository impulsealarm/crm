/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Users_Edit_Js("Users_UpdatePassword_Js",{
	
},{
	
	registerRecordPreSaveEvent : function(form){
		var thisInstance = this;
		form.on(Vtiger_Edit_Js.recordPreSave, function(e, data) {
			
			var new_password  = form.find('[name="new_password"]');
			var confirm_password = form.find('[name="confirm_password"]');
			var old_password  = form.find('[name="old_password"]');
			var userid = form.find('[name="userid"]').val();
			
			if( userid == '' ){
				window.location.href = "index.php";
				return false;
			}
			
			if(new_password.val() == confirm_password.val()){
				
				if(thisInstance.valid_pass(new_password.val())){
					var params = {
						'module': app.getModuleName(),
						'action' : "SaveAjax",
						'mode' : 'savePassword',
						'old_password' : old_password.val(),
						'new_password' : new_password.val(),
						'userid' : userid
					}
					AppConnector.request(params).then(
						function(data) {
							if(data.success){
								window.location.href = "index.php";
								return false;
							}else{
								old_password.validationEngine('showPrompt', app.vtranslate(data.error.message) , 'error','topLeft',true);
							}
						}
					);
				} else {
					new_password.validationEngine('showPrompt', app.vtranslate('JS_MEET_PASSWORD_REQUIREMENTS') , 'error','topLeft',true);
				}
			} else {
				confirm_password.validationEngine('showPrompt', app.vtranslate('JS_MISMATCH_PASSWORD') , 'error','topLeft',true);
			}
			
			e.preventDefault();
		});
	},
	
 
	registerLeavePageWithoutSubmit : function(form){
        
		/*
		InitialFormData = form.serialize();
        window.onbeforeunload = function(e){
        	if (InitialFormData != form.serialize() && form.data('submit') != "true") {
                return true;//app.vtranslate("JS_CHANGES_WILL_BE_LOST");
            }
        };*/
        
    },
    
    checkPasswordStrength : function(form){
    	
    	jQuery('input[name=new_password]').keyup(function() { 
    		
    		var pswd = jQuery(this).val();
    		
    		//validate the length
    		if ( pswd.length < 6 ) {
    			jQuery('#length').removeClass('valid').addClass('invalid');
    		} else {
    			jQuery('#length').removeClass('invalid').addClass('valid');
    		}
    		
    		//validate letter
    		if ( pswd.match(/[A-z]/) ) {
    			jQuery('#letter').removeClass('invalid').addClass('valid');
    		} else {
    			jQuery('#letter').removeClass('valid').addClass('invalid');
    		}
    		
    		//validate uppercase letter
    		if ( pswd.match(/[A-Z]/) ) {
    			jQuery('#capital').removeClass('invalid').addClass('valid');
    		} else {
    			jQuery('#capital').removeClass('valid').addClass('invalid');
    		}
    		
    		//validate number
    		if ( pswd.match(/\d/) ) {
    			jQuery('#number').removeClass('invalid').addClass('valid');
    		} else {
    			jQuery('#number').removeClass('valid').addClass('invalid');
    		}
    		
    		//Special Character
    		if(pswd.match(/[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/)){
    			jQuery('#spl_char').removeClass('invalid').addClass('valid');
        	} else {
        		jQuery('#spl_char').removeClass('valid').addClass('invalid');
        	}
    		
    	}).focus(function() {
    		jQuery('#pswd_info').show();
    	}).blur(function() {
    		jQuery('#pswd_info').hide();
    	});
    },

    valid_pass: function(password){
    	
    	var pwd_len = false;
    	var pwd_lwr_ltr = false;
    	var pwd_upr_ltr = false;
    	var pwd_num = false;
    	var pwd_spcl_char = false;
    	
    	if(password.length >= 6)
    		pwd_len= true;
		
		if(password.match(/[A-z]/))
			pwd_lwr_ltr = true;
		
		if(password.match(/[A-Z]/))
			pwd_upr_ltr = true;
		
		if(password.match(/\d/))
			pwd_num = true;
		
		if (password.match(/[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/))
			pwd_spcl_char = true;
    	
		
		if(pwd_len && pwd_lwr_ltr && pwd_upr_ltr && pwd_num && pwd_spcl_char)
			return true;
		
		return false;
    },
	/**
	 * register Events for my preference
	 */
	registerEvents : function(){
    	var thisIstance = this;
		this._super();
		var form = this.getForm();
		this.registerLeavePageWithoutSubmit(form);
		thisIstance.checkPasswordStrength(form);
	}
});